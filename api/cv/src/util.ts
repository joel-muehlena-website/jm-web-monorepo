export const getMonthNameById = (id: number) => {
  const getMonthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  if (id > getMonthNames.length || id < 0) {
    throw Error("No valid month provided");
  }

  return getMonthNames[id];
};

export const sleep = (ms: number) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
}