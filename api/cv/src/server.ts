import express from "express";
import cors from "cors"
import { registerHandlebarsHelpers } from "./hbs/helpers.js"
import { cvRoutes } from "./routes/cv/index.js";
import { initTemplateFileLoader } from "./templateLoader.js";
import { initLogger } from "./logger.js";
import { loadConfig } from "./config.js";
import { Unleash, startUnleash, InMemStorageProvider } from "unleash-client"

import { logFeatureState } from "./features.js"

export const CONFIG = await loadConfig();
export const LOGGER = initLogger({ level: CONFIG.log.level });

LOGGER.info("Loaded config", { config: CONFIG });

export let unleash: Unleash;
try {
  unleash = await startUnleash({
    appName: CONFIG.features.flags.unleash.appName,
    url: CONFIG.features.flags.unleash.url,
    instanceId: CONFIG.features.flags.unleash.instanceId,
    refreshInterval: CONFIG.features.flags.unleash.refreshInterval,
    storageProvider: new InMemStorageProvider(),
  });
  logFeatureState()
} catch (err) {
  LOGGER.error("Failed to start unleash client", { error: err })
  process.exit(1)
}

try {
  await initTemplateFileLoader(
    CONFIG.files.css,
    CONFIG.files.html,
  );
  LOGGER.info("Successfully loaded initial cv templates & registered watchers");
} catch (err) {
  LOGGER.error("Failed to load templates", err);
  process.exit(1);
}

const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use((req, res, next) => {
  const startTime = Date.now();
  next();
  const endTime = Date.now();
  LOGGER.info(`${req.method} ${req.baseUrl} ${res.statusCode} in ${endTime - startTime}ms`);
})

app.get("/healthz", (_, res) => {
  res.sendStatus(200);
});

registerHandlebarsHelpers();
app.use("/v2/cv-generator", cvRoutes);

app.listen(`${CONFIG.server.port}`, () => {
  LOGGER.info(`Server läuft auf port: ${CONFIG.server.port}`);
});
