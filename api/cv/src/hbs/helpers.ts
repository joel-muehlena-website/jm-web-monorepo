import hbs from "handlebars";
import { ExperienceType, GraduateState } from "../types.js";
import { getMonthNameById } from "../util.js";

export const registerHandlebarsHelpers = () => {
  hbs.registerHelper("experienceType", (type: ExperienceType) => {
    switch (type) {
      case ExperienceType.INTERNSHIP:
        return "Internship";
      case ExperienceType.JOB:
        return "Job";
    }
  });

  hbs.registerHelper("graduateState", (state: GraduateState) => {
    switch (state) {
      case GraduateState.NOT_GRADUATED:
        return "not graduated";
      case GraduateState.NOT_YET_GRADUATED:
        return "not yet graduated (still on it)";
      case GraduateState.GRADUATED:
        return "graduated";
    }
  });

  hbs.registerHelper("displayDateFromUTC", (date: Date | string) => {
    const newDate = new Date(date);
    const year = newDate.getFullYear();
    const month = newDate.getMonth();
    let monthName = getMonthNameById(month);

    return monthName + " " + year;
  });

  hbs.registerHelper("displayDateLangFormatted", (date: Date | string, lang: string) => {
    const newDate = new Date(date);
    const year = newDate.getFullYear();
    const month = newDate.getMonth();
    const day = newDate.getDate();
    let monthName = getMonthNameById(month);

    return `${day}.${month + 1 < 10 ? "0" + (month + 1) : month + 1}.${year}`;
  });
}