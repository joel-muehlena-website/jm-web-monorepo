import winston from "winston"

export type LoggerConfig = {
  level?: string;
}

export const initLogger = ({ level = "info" }: LoggerConfig): winston.Logger => {
  const logger = winston.createLogger({
    level,
    format: winston.format.json(),
    defaultMeta: { service: 'cv-service' },
    transports: [],
  });

  logger.add(new winston.transports.Console({
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.simple()
    )
  }));

  return logger;
}