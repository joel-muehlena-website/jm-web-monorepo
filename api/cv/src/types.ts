export enum ExperienceType {
  INTERNSHIP = 0,
  JOB = 1,
}

export enum GraduateState {
  NOT_GRADUATED = 0,
  NOT_YET_GRADUATED = 1,
  GRADUATED = 2,
}

export type Education = {
  current: boolean;
  _id: string;
  schoolName: string;
  schoolLocation: string;
  description: string;
  from: number;
  to: number;
  graduated: number;
  updatedAt: Date;
  createdAt: Date;
}

export type Skill = {
  _id: string;
  skillGroupName: string;
  skills: SkillElement[];
  createdAt: Date;
  updatedAt: Date;
}

export type SkillElement = {
  _id: string;
  skillName: string;
  level: number;
  description?: string;
}

export type Experience = {
  _id: string;
  companyName: string;
  companyLocation: string;
  type: number;
  title: string;
  from: Date;
  to: Date;
  description: string;
  updatedAt: Date;
  current: boolean;
  createdAt: Date;
}

export type CVSpecialData = {
  name: string;
  address: string[];
  phone: string;
  email: string;
  homepage: string;
  birthDate: Date;
  birthPlace: string;
  imageLink: string;
  other: string[];
  hobby: string[];
}

export type CVConfig = {
  lang: string;
  title: string;
}

export type CVData = {
  education: Array<Education>;
  skills: Array<Skill>;
  experience: Array<Experience>;
} & CVSpecialData & Partial<CVConfig>;