import express from "express";
import { chromium } from "playwright";

import { K8sApiDataFetcher } from "../../data-fetcher/K8sApiDataFetcher.js";
import { ICVDataFetcher } from "../../data-fetcher/ICVDataFetcher.js";
import { MockDataFetcher } from "../../data-fetcher/MockDataFetcher.js";
import { TEMPLATE_FILES } from "../../templateLoader.js";
import { createHash } from "crypto"
import { LOGGER } from "../../server.js";
import { InMemoryCache } from "../../cache/InMemoryCache.js";
import { CONFIG } from "../../server.js";

const router = express.Router();

const cache = new InMemoryCache<Buffer>();

// TODO think about caching for lang instead of caching by html hash -> saving downstream requests
router.get("/", async (req, res) => {
  const lang: string = (req.query.lang as string | undefined) ?? "en";
  const dataFetcher: ICVDataFetcher = process.env.NODE_ENV === "production" ? new K8sApiDataFetcher(CONFIG.remote) : new MockDataFetcher();

  try {
    const data = await dataFetcher.getAllData()

    data.lang = lang;
    data.title = data.title ?? "Curriculum Vitae"

    let langComp = TEMPLATE_FILES.html.get(lang);
    if (langComp === undefined) {
      langComp = TEMPLATE_FILES.html.get("en") as HandlebarsTemplateDelegate<any>;
    }

    const rendered = TEMPLATE_FILES.html.get(lang)!(data);

    const hashSum = createHash('sha256');
    hashSum.update(rendered);
    const hex = hashSum.digest('hex');
    LOGGER.debug(`Generated HTML with hash ${hex}`)

    const cacheResult = cache.get(hex)
    if (cacheResult !== undefined) {
      LOGGER.info("The requested CV was found in the cache. Sending cached version.")
      res.type("application/pdf");
      res.send(cacheResult);
      return;
    }

    // TODO create a pool of browsers and use them instead of creating a new one for every request
    const browser = await chromium.launch({
      headless: true,
      executablePath: process.env.CHROME_BIN ?? undefined,
    });
    const page = await browser.newPage();

    await page.setContent(rendered, { waitUntil: "domcontentloaded" });
    await page.emulateMedia({ media: "print" });
    await page.addStyleTag({ content: TEMPLATE_FILES.css });

    // Wait until all network activities are done
    await page.waitForLoadState("networkidle");

    let buffer = await page.pdf({
      format: "A4",
      displayHeaderFooter: false,
      printBackground: true,
      margin: {
        top: 30,
        bottom: 30,
      },
    });

    await browser.close();

    cache.set(hex, buffer)

    res.type("application/pdf");
    res.send(buffer);
  } catch (err) {
    console.error("Failed to generate pdf", err);
    res.status(500).json({
      code: 500,
      msg: "There was an error generating the pdf",
      error: err,
    });
  }
});

export const cvRoutes = router;