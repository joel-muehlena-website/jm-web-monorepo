import { LOGGER, unleash } from "./server.js"

export const logFeatureState = () => {
  LOGGER.info("Feature flags", {
    flags: {
      api_cv_enable_dedicated_cv_endpoint: unleash.isEnabled("api_cv_enable_dedicated_cv_endpoint")
    }
  })
}