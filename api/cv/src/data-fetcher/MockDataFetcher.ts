import { CVData, CVSpecialData, Education, Experience, Skill } from "../types.js"
import { ICVDataFetcher } from "./ICVDataFetcher.js"

export class MockDataFetcher implements ICVDataFetcher {
  async getAllData(): Promise<CVData> {
    try {
      const data = await Promise.all([this.getEducation(), this.getSkills(), this.getExperience(), this.getCVSpecialData()])

      return {
        education: data[0],
        skills: data[1],
        experience: data[2],
        ...data[3]
      }
    } catch (err) {
      console.error("Failed to fetch all data", err)
      throw err
    }
  }

  getEducation(): Promise<Education[]> {
    return new Promise(resolve => {
      resolve([
        {
          current: false,
          _id: "5be9e5653fb7a83910b7b282",
          schoolName: "Gymnasium Taunusstein",
          schoolLocation: "Taunusstein Bleidenstadt",
          description: "Gymnasium (5-9 Klasse)",
          from: 2012,
          to: 2017,
          graduated: 2,
          updatedAt: new Date("2021-08-10T09:35:12.281Z"),
          createdAt: new Date("2018-11-12T20:41:09.036Z")
        },
        {
          current: false,
          _id: "5cc97fb8191e380fe9d31e45",
          schoolName: "Silberbachschule",
          schoolLocation: "Taunusstein Wehen",
          description: "Grundschule (1-4 Klasse)",
          from: 2008,
          to: 2012,
          graduated: 2,
          updatedAt: new Date("2019-05-01T11:16:11.333Z"),
          createdAt: new Date("2019-05-01T11:15:04.855Z")
        },
        {
          current: false,
          _id: "5ca8b5c1fb6fc01d5661fa70",
          schoolName: "Friedrich-List-Schule",
          schoolLocation: "Wiesbaden",
          description: "Berufliches Gymnasium | Datenverarbeitung | 2,3",
          from: 2017,
          to: 2020,
          graduated: 2,
          updatedAt: new Date("2020-11-10T07:59:23.469Z"),
          createdAt: new Date("2023-09-07T08:15:59.257Z")
        },
        {
          current: true,
          _id: "5faa47c85caa63e3573ee68f",
          schoolName: "Darmstadt University of Applied Sciences",
          schoolLocation: "Darmstadt",
          description: "Informatik KoSi",
          from: 2020,
          to: 0,
          graduated: 1,
          updatedAt: new Date("2021-08-10T10:09:46.115Z"),
          createdAt: new Date("2020-11-10T07:56:56.777Z")
        }
      ])
    })
  }

  getSkills(): Promise<Skill[]> {
    return new Promise(resolve => {
      resolve([
        {
          _id: "61016e5170ce763d8bccf880",
          skillGroupName: "Programming Languages",
          skills: [
            {
              _id: "61016e5170ce763d8bccf883",
              skillName: "Java",
              level: 3
            },
            {
              _id: "61016e5170ce763d8bccf884",
              skillName: "C++",
              level: 3
            },
            {
              _id: "61016e5170ce763d8bccf885",
              skillName: "TypeScript / JavaScript / NodeJS",
              level: 3
            },
            {
              _id: "61016e5170ce763d8bccf886",
              skillName: "GoLang",
              level: 3
            },
            {
              skillName: "Rust",
              level: 1,
              _id: "64a57f3d5dfe83b5edb3af2b"
            }
          ],
          createdAt: new Date("2021-07-28T14:48:49.278Z"),
          updatedAt: new Date("2021-07-28T14:48:49.346Z")
        },
        {
          _id: "61016ebc70ce763d8bccf88a",
          skillGroupName: "Databases",
          skills: [
            {
              _id: "61016ebc70ce763d8bccf88d",
              skillName: "MySQL",
              level: 2
            },
            {
              _id: "61016ebc70ce763d8bccf88e",
              skillName: "MongoDB",
              level: 2
            },
            {
              skillName: "PostgreSQL",
              level: 2,
              _id: "64a57f755dfe83b5edb3af2c"
            }
          ],
          createdAt: new Date("2021-07-28T14:50:36.386Z"),
          updatedAt: new Date("2021-07-28T14:50:36.449Z")
        },
        {
          _id: "61019e64e5d5965700070a3e",
          skillGroupName: "Frameworks & Libs",
          skills: [
            {
              _id: "61019e65e5d5965700070a41",
              skillName: "ReactJS",
              level: 3
            },
            {
              _id: "61019e65e5d5965700070a42",
              skillName: "Angular",
              level: 2
            },
            {
              skillName: "Express.js",
              level: 3,
              _id: "61437f90d8ae42b1cd09e028"
            }
          ],
          createdAt: new Date("2021-07-28T18:13:56.957Z"),
          updatedAt: new Date("2021-07-28T18:13:57.026Z")
        },
        {
          _id: "61019e66e5d5965700070a45",
          skillGroupName: "Administration",
          skills: [
            {
              _id: "61019e67e5d5965700070a48",
              skillName: "Windows Server Environment & Active Directory",
              level: 2
            }
          ],
          createdAt: new Date("2021-07-28T18:13:58.964Z"),
          updatedAt: new Date("2021-09-16T19:30:06.000Z")
        },
        {
          _id: "61019e6ae5d5965700070a4c",
          skillGroupName: "DevOps",
          skills: [
            {
              _id: "61019e6ae5d5965700070a4f",
              skillName: "Gitlab CI",
              level: 2
            },
            {
              _id: "61019e6ae5d5965700070a50",
              skillName: "Ansible",
              level: 1
            },
            {
              skillName: "Kubernetes \\w Kubeadm",
              level: 2,
              _id: "61437fb1d8ae42b1cd09e029"
            },
            {
              skillName: "Docker",
              level: 2,
              _id: "61437fbbd8ae42b1cd09e02a"
            }
          ],
          createdAt: new Date("2021-07-28T18:14:02.750Z"),
          updatedAt: new Date("2021-07-28T18:14:02.810Z")
        },
        {
          _id: "61437dee3551954b18bd1f77",
          skillGroupName: "Other",
          skills: [
            {
              skillName: "HTML",
              level: 3,
              _id: "61437fcfd8ae42b1cd09e02b"
            },
            {
              skillName: "CSS / SASS",
              level: 3,
              description: "SASS is a CSS preprocessor",
              _id: "61437fd9d8ae42b1cd09e02c"
            }
          ],
          createdAt: new Date("2021-09-16T19:29:52.000Z"),
          updatedAt: new Date("2021-09-16T19:30:04.000Z")
        },
        {
          _id: "61437dee3551954b18bd1f77",
          skillGroupName: "Some other Skills without level",
          skills: [
            {
              skillName: "Test 1",
              level: 0,
              _id: "61437fcfd8ae42b1cd09e02b"
            },
            {
              skillName: "Test 2",
              level: 0,
              description: "Test Description",
              _id: "61437fd9d8ae42b1cd09e02c"
            }
          ],
          createdAt: new Date("2021-09-16T19:29:52.000Z"),
          updatedAt: new Date("2021-09-16T19:30:04.000Z")
        }
      ])
    })
  }

  getExperience(): Promise<Experience[]> {
    return new Promise(resolve => {
      resolve([
        {
          _id: "5cc987d0e7179a596b1922ac",
          companyName: "ZDF",
          companyLocation: "Mainz",
          type: 0,
          title: "Building services IT",
          from: new Date("2017-02-01T00:00:00.000Z"),
          to: new Date("2017-02-01T00:00:00.000Z"),
          description: "Two week internship in the IT Helpdesk department of the ZDF",
          updatedAt: new Date("2020-10-14T16:30:20.128Z"),
          current: false,
          createdAt: new Date("2023-09-07T08:17:09.547Z")
        },
        {
          _id: "5cc4dfabfb6fc00ed59ca3bc",
          companyName: "Jack Wolfskin",
          companyLocation: "Idstein",
          type: 1,
          title: "Temporary help IT",
          from: new Date("2019-02-01T00:00:00.000Z"),
          to: new Date("2020-04-01T00:00:00.000Z"),
          description: "Temporary help at Desktop Support at Jack Wolfskin",
          updatedAt: new Date("2020-10-13T20:55:30.462Z"),
          current: false,
          createdAt: new Date("2023-09-07T08:17:09.548Z")
        },
        {
          _id: "5eb013dcad1f8e8233827219",
          companyName: "tegut...",
          companyLocation: "Hünstetten",
          type: 1,
          title: "Cashier",
          current: false,
          from: new Date("2020-05-01T00:00:00.000Z"),
          to: new Date("2020-06-01T00:00:00.000Z"),
          description: "Cashier at tegut... in 65510 Hünstetten",
          updatedAt: new Date("2020-05-04T13:08:44.727Z"),
          createdAt: new Date("2020-05-04T13:08:44.736Z")
        },
        {
          _id: "5f84601b067b0e7c4a4ba4d9",
          companyName: "DFS - Deutsche Flugsicherung GmbH",
          companyLocation: "Langen",
          type: 1,
          title: "Dualer Student",
          from: new Date("2020-10-01T00:00:00.000Z"),
          to: new Date("2020-10-01T00:00:00.000Z"),
          current: true,
          description: "I work as a dual student in the field of computer science at DFS",
          updatedAt: new Date("2020-10-12T13:55:46.205Z"),
          createdAt: new Date("2020-10-12T13:54:35.354Z")
        }
      ])
    })
  }

  getCVSpecialData(): Promise<CVSpecialData> {
    return new Promise(resolve => {
      resolve({
        name: "Joel Mühlena",
        address: ["Weserstr. 5", "63225 Langen"],
        phone: "+49 152 / 21780140",
        birthDate: new Date("2001-08-11T00:00:00.000Z"),
        birthPlace: "Wiesbaden",
        email: "joel.rene (at) muehlena.de",
        homepage: "joel.muehlena.de",
        imageLink: "https://i.ibb.co/xJSWJJj/joel-2018-bewerbung-1mb-comp.jpg",
        other: [
          "Teamer at the AWO and the deanery",
          "Sound and PA for school events",
        ],
        hobby: ["Programming", "Videos", "Computer", " Judo (4. Kyu)"],
      })
    })
  }
}