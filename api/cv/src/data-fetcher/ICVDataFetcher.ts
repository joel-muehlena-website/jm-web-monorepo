import { CVData, CVSpecialData, Education, Experience, Skill } from "../types.js"


export interface ICVDataFetcher {
  getEducation(): Promise<Array<Education>>;
  getSkills(): Promise<Array<Skill>>;
  getExperience(): Promise<Array<Experience>>;
  getCVSpecialData(): Promise<CVSpecialData>;
  getAllData(): Promise<CVData>;
}