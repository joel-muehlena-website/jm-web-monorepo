import { LOGGER, unleash } from "../server.js";
import { CVData, CVSpecialData, Education, Experience, Skill } from "../types.js";
import { ICVDataFetcher } from "./ICVDataFetcher.js";
import { StaticDataFetcher } from "./StaticDataFetcher.js";

export type K8sApiDataFetcherConfig = {
  api: {
    baseURL: string,
    endpoints: {
      experience: string,
      education: string,
      skills: string,
      cvSpecialData: string,
    }
  }
}

export class K8sApiDataFetcher implements ICVDataFetcher {
  private readonly staticDataFetcher: ICVDataFetcher = new StaticDataFetcher();

  constructor(private readonly cfg: K8sApiDataFetcherConfig) { }

  async get<T = {}>(url: string, options: RequestInit = {}): Promise<T> {
    try {
      const result = await fetch(url, options);
      let data: unknown;

      if (result.headers.get("content-type")?.includes("application/json")) {
        data = await result.json()
        LOGGER.debug("Response is JSON... Parsing", { type: "json", json: data })
      } else {
        try {
          data = await result.text()
          LOGGER.debug("Response is text... Parsing", { type: "text", text: data })
        } catch (err) {
          data = "No parsable response body"
        }
      }
      if (!result.ok) {
        throw new Error(`Response not ok: ${result.status} ${result.statusText}; Data: ${data}`)
      }

      return data as T;
    } catch (err) {
      console.error(`Failed to fetch ${url} with options: ${options}`, err)
      throw err
    }
  }

  async getEducation(): Promise<Education[]> {
    try {
      const { data } = await this.get<{ data: Array<Education> }>(`${this.cfg.api.baseURL}${this.cfg.api.endpoints.education}`)
      return data
    } catch (err) {
      console.error("Failed to fetch education", err)
      throw err
    }
  }

  async getSkills(): Promise<Skill[]> {
    try {
      const { data } = await this.get<{ data: Array<Skill> }>(`${this.cfg.api.baseURL}${this.cfg.api.endpoints.skills}`)
      return data
    } catch (err) {
      LOGGER.error("Failed to fetch skills", err)
      throw err
    }
  }

  async getExperience(): Promise<Experience[]> {
    try {
      const { data } = await this.get<{ data: Array<Experience> }>(`${this.cfg.api.baseURL}${this.cfg.api.endpoints.experience}`)
      return data
    } catch (err) {
      LOGGER.error("Failed to fetch experience", err)
      throw err
    }
  }

  async getCVSpecialData(): Promise<CVSpecialData> {
    if (unleash.isEnabled("api_cv_enable_dedicated_cv_endpoint")) {
      LOGGER.debug("Using dedicated CV endpoint")
      try {
        const { data } = await this.get<{ data: CVSpecialData }>(`${this.cfg.api.baseURL}${this.cfg.api.endpoints.cvSpecialData}`)
        return data
      } catch (err) {
        LOGGER.error("Failed to fetch CVSpecialData", err)
        throw err
      }
    } else {
      LOGGER.debug("Using static CV data")
      return this.staticDataFetcher.getCVSpecialData()
    }
  }

  async getAllData(): Promise<CVData> {
    try {
      const data = await Promise.all([this.getEducation(), this.getSkills(), this.getExperience(), this.getCVSpecialData()])

      return {
        education: data[0],
        skills: data[1],
        experience: data[2],
        ...data[3]
      }
    } catch (err) {
      LOGGER.error("Failed to fetch all data", err)
      throw err
    }
  }
}