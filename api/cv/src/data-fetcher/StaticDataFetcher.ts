import { CVData, CVSpecialData, Education, Experience, Skill } from "../types.js"
import { ICVDataFetcher } from "./ICVDataFetcher.js"

export class StaticDataFetcher implements ICVDataFetcher {
  async getAllData(): Promise<CVData> {
    try {
      const data = await Promise.all([this.getEducation(), this.getSkills(), this.getExperience(), this.getCVSpecialData()])

      return {
        education: data[0],
        skills: data[1],
        experience: data[2],
        ...data[3]
      }
    } catch (err) {
      console.error("Failed to fetch all data", err)
      throw err
    }
  }

  getEducation(): Promise<Education[]> {
    throw new Error("Method not implemented.")
  }

  getSkills(): Promise<Skill[]> {
    throw new Error("Method not implemented.")
  }

  getExperience(): Promise<Experience[]> {
    throw new Error("Method not implemented.")
  }

  getCVSpecialData(): Promise<CVSpecialData> {
    return new Promise(resolve => {
      resolve({
        name: "Joel Mühlena",
        address: ["Weserstr. 5", "63225 Langen"],
        phone: "+49 152 / 21780140",
        birthDate: new Date("2001-08-11T00:00:00.000Z"),
        birthPlace: "Wiesbaden",
        email: "joel.rene (at) muehlena.de",
        homepage: "joel.muehlena.de",
        imageLink: "https://i.ibb.co/xJSWJJj/joel-2018-bewerbung-1mb-comp.jpg",
        other: [
          "Teamer at the AWO and the deanery",
          "Sound and PA for school events",
        ],
        hobby: ["Programming", "Videos", "Computer", " Judo (4. Kyu)"],
      })
    })
  }
}