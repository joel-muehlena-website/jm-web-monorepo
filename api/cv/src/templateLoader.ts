import fs from "fs-extra"
import hbs from "handlebars";
import { LOGGER } from "./server.js";

export type FileResult = {
  css: string;
  html: Map<string, HandlebarsTemplateDelegate>;
}

export const TEMPLATE_FILES: FileResult = {
  css: "",
  html: new Map(),
};

export const MAX_LANGUAGES = 20;

export const initTemplateFileLoader = async (cssPath: string, htmlPaths: Map<string, string>): Promise<boolean> => {
  LOGGER.debug("Loading templates from " + cssPath + " and " + htmlPaths.size + " html files");
  if (htmlPaths.size >= MAX_LANGUAGES) throw new Error("Too many languages. Max is " + MAX_LANGUAGES + ".")

  TEMPLATE_FILES.css = await fs.readFile(cssPath, "utf-8");

  fs.watchFile(cssPath, () => {
    LOGGER.info("Reloading css file");
    TEMPLATE_FILES.css = fs.readFileSync(cssPath, "utf-8");
  })

  for (let [lang, htmlPath] of htmlPaths) {
    TEMPLATE_FILES.html.set(lang, hbs.compile(await fs.readFile(htmlPath, "utf-8")))
    fs.watchFile(htmlPath, () => {
      LOGGER.info(`Reloading html file for language ${lang} (${htmlPath})`);
      TEMPLATE_FILES.html.set(lang, hbs.compile(fs.readFileSync(htmlPath, "utf-8")))
    })
  }

  return true;
}