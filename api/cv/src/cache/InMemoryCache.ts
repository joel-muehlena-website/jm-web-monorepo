import { LOGGER } from "../server.js";

export class InMemoryCache<T = string> {
  static DEFAULT_RETENTION_TIME = 60 * 60 * 1000; // 1 hour

  private data: Map<string, T>;
  private retentionTime: number;

  constructor(retentionTime?: number) {
    this.data = new Map<string, T>();
    this.retentionTime = retentionTime ?? InMemoryCache.DEFAULT_RETENTION_TIME;
  }

  set(key: string, value: T) {
    this.data.set(key, value);
    setTimeout(() => {
      LOGGER.debug(`Removing ${key} from cache, due to retention time.`);
      this.data.delete(key);
    }, this.retentionTime);
  }

  get(key: string) {
    return this.data.get(key);
  }

  has(key: string) {
    return this.data.has(key);
  }
}