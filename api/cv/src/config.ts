import { readdirSync } from "fs"
import path from "path";

const loadHTMLFileDests = async (): Promise<Map<string, string>> => {
  const html_prefix = process.env.HTML_FILE_PREFIX ?? "templates/"
  const entries = readdirSync(html_prefix, { withFileTypes: true });

  const files: Map<string, string> = new Map();

  for (let file of entries) {
    if (file.isDirectory() || !file.name.endsWith(".hbs")) continue;

    const lang = file.name.split(".")[0];
    files.set(lang, path.join(html_prefix, file.name));
  }

  return files;
}

const envNumber = (name: string): number | undefined => {
  const val = process.env[name];
  if (val === undefined) return undefined;
  try {
    return parseInt(val);
  } catch (_) {
    return undefined;
  }
}

export const loadConfig = async () => {
  const htmlFileDests = await loadHTMLFileDests();

  return {
    mode: process.env.NODE_ENV ?? "development",
    server: {
      port: process.env.PORT ?? 3003,
    },
    files: {
      "css": process.env.CSS_FILE ?? "templates/css/cv-pdf.css",
      "html": htmlFileDests,
    },
    remote: {
      api: {
        baseURL: process.env.API_BASE ?? "http://api-gateway.jm-api.svc.cluster.local:80",
        endpoints: {
          experience: process.env.API_ENDPOINT_EXPERIENCE ?? "/v1/experience",
          education: process.env.API_ENDPOINT_EDUCATION ?? "/v1/education",
          skills: process.env.API_ENDPOINT_SKILLS ?? "/v2/skill",
          cvSpecialData: process.env.API_ENDPOINT_CV_SPECIAL_DATA ?? "/v1/cv-special-data",
        }
      }
    },
    log: {
      level: process.env.LOG_LEVEL ?? "info",
    },
    features: {
      flags: {
        unleash: {
          url: process.env.UNLEASH_API_URL ?? "",
          appName: process.env.UNLEASH_APP_NAME ?? (process.env.NODE_ENV ?? "production"),
          instanceId: process.env.UNLEASH_INSTANCE_ID ?? "cv-1",
          refreshInterval: envNumber("UNLEASH_REFRESH_INTERVAL") ?? 15 * 1000,
        }
      }
    }
  }
}