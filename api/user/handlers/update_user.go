package handlers

import (
	"context"

	"github.com/google/uuid"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type updateUserRequest struct {
	ID string `uri:"id"`
	types.FullUser
}

type updateUserResponse struct {
	Msg string `json:"msg"`
}

func (handler *UserHandler) updateUser(baseCtx context.Context, req *updateUserRequest) (*updateUserResponse, int, error) {
	id, err := uuid.Parse(req.ID)
	if err != nil {
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid id passed", Errors: []string{err.Error()}}
	}

	uc := usecase.UserOperations{
		UserRepo: handler.userRepo,
	}

	err = uc.UpdateUser(baseCtx, id, req.FullUser)

	if err == repository.ErrNotFound {
		return nil, 404, &gomstoolkit.HTTPError{Code: 404, Message: "no user found", Errors: []string{err.Error()}}
	}

	if err != nil {
		handler.userService.Service.Logger.Error("Failed to query user (update)", zap.Error(err))
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to update user: internal error", Errors: []string{err.Error()}}
	}

	return &updateUserResponse{Msg: "Updated user"}, 200, nil
}
