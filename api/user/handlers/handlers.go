package handlers

import (
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	service "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
)

type UserHandler struct {
	userService *types.UserService
	userRepo    repository.UserRepository
}

func GetHandlers(userSvc *types.UserService, repo repository.UserRepository) []service.Hitable {
	services := make([]service.Hitable, 0)

	handler := UserHandler{
		userService: userSvc,
		userRepo:    repo,
	}

	getUserEndpoint := service.Endpoint[getUserRequest, getUserResponse]{
		Path:               "/user",
		Method:             service.GET,
		Handler:            handler.getUser,
		AuthorizationChain: []service.AuthorizationHandler{middleware.IsAuthenticated, middleware.IsPermittedForRole("jm_core::UserService::View::Allow")},
	}

	getUserByIdEndpoint := service.Endpoint[getUserByIdRequest, getUserByIdResponse]{
		Path:               "/user/:id",
		Method:             service.GET,
		Handler:            handler.getUserById,
		AuthorizationChain: []service.AuthorizationHandler{middleware.IsAuthenticated, middleware.Or(true, middleware.IsPermittedForRole("jm_core::UserService::View::Allow"), middleware.ShouldBeSelf("userID"))},
	}

	createUserEndpoint := service.Endpoint[createUserRequest, createUserResponse]{
		Path:                "/user",
		Method:              service.POST,
		HandlerController:   handler.createUser,
		RestrictContentType: []string{"application/json"},
		AuthorizationChain:  []service.AuthorizationHandler{middleware.IsAuthenticated, middleware.IsPermittedForRole("jm_core::UserService::Create::Allow")},
	}

	updateUserEndpoint := service.Endpoint[updateUserRequest, updateUserResponse]{
		Path:                "/user/:id",
		Method:              service.PATCH,
		Handler:             handler.updateUser,
		RestrictContentType: []string{"application/json"},
		AuthorizationChain:  []service.AuthorizationHandler{middleware.IsAuthenticated, middleware.Or(true, middleware.IsPermittedForRole("jm_core::UserService::Create::Allow"), middleware.ShouldBeSelf("userID"))},
	}

	deleteUserEndpoint := service.Endpoint[deleteUserRequest, deleteUserResponse]{
		Path:               "/user/:id",
		Method:             service.DELETE,
		Handler:            handler.deleteUser,
		AuthorizationChain: []service.AuthorizationHandler{middleware.IsAuthenticated, middleware.Or(true, middleware.IsPermittedForRole("jm_core::UserService::Delete::Allow"), middleware.ShouldBeSelf("userID"))},
	}

	// Auth, role jm-auth:user:manage
	services = append(services, &getUserEndpoint)
	// Auth, role jm-auth:user:manage || self -> match userid
	services = append(services, &getUserByIdEndpoint)
	// jm-auth:user:create
	services = append(services, &createUserEndpoint)
	// Auth, self -> match userId || jm-auth:user:manage
	services = append(services, &updateUserEndpoint)
	// Auth, self -> match userId || jm-auth:user:manage
	services = append(services, &deleteUserEndpoint)

	return services
}
