package handlers

import (
	"context"

	"github.com/google/uuid"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/repository"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type deleteUserRequest struct {
	ID string `uri:"id"`
}

type deleteUserResponse struct {
	Msg string `json:"msg"`
	ID  string `json:"id"`
}

func (handler *UserHandler) deleteUser(baseCtx context.Context, req *deleteUserRequest) (*deleteUserResponse, int, error) {
	id, err := uuid.Parse(req.ID)
	if err != nil {
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid id passed", Errors: []string{err.Error()}}
	}

	uc := usecase.UserOperations{
		UserRepo: handler.userRepo,
	}

	err = uc.DeleteUser(baseCtx, id)

	if err == repository.ErrNotFound {
		return nil, 404, &gomstoolkit.HTTPError{Code: 404, Message: "no user found", Errors: []string{err.Error()}}
	}

	if err != nil {
		handler.userService.Service.Logger.Error("Failed to query user (delete)", zap.Error(err))
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to delete user: internal error", Errors: []string{err.Error()}}
	}

	return &deleteUserResponse{Msg: "Successfully deleted", ID: req.ID}, 200, nil
}
