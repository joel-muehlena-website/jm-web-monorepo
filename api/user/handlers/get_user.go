package handlers

import (
	"context"

	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type getUserRequest struct {
	Limit  int `form:"limit"`
	Offset int `form:"offset"`
}

type getUserResponse struct {
	Users []types.PublicUser `json:"users"`
}

func (handler *UserHandler) getUser(baseCtx context.Context, req *getUserRequest) (*getUserResponse, int, error) {
	uc := usecase.UserOperations{
		UserRepo: handler.userRepo,
	}

	if req.Limit == 0 {
		req.Limit = 20
	}

	users, err := uc.GetUsers(baseCtx, req.Limit, req.Offset)

	if err == repository.ErrNotFound {
		return nil, 404, &gomstoolkit.HTTPError{Code: 404, Message: "no users found", Errors: []string{err.Error()}}
	}

	if err != nil {
		handler.userService.Service.Logger.Error("Failed to query users (get)", zap.Error(err))
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to get users: internal error", Errors: []string{err.Error()}}
	}

	return &getUserResponse{Users: users}, 200, nil
}
