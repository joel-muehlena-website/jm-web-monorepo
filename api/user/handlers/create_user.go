package handlers

import (
	"context"

	"github.com/google/uuid"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type createUserRequest struct {
	types.FullUser
}

type createUserResponse struct {
	ID uuid.UUID `json:"id"`
}

func (handler *UserHandler) createUser(baseCtx context.Context, controller gomstoolkit.RequestController, req *createUserRequest) (*createUserResponse, int, error) {
	uc := usecase.UserOperations{
		UserRepo: handler.userRepo,
	}

	errs, _ := gomstoolkit.Validate(&req.FullUser)

	if len(errs) > 0 {
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "failed to validate input: invalid", Errors: errs}
	}

	userId, err := uc.CreateUser(baseCtx, req.FullUser)
	if err != nil {
		handler.userService.Service.Logger.Error("Failed to query users (create)", zap.Error(err))
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to create user: internal error", Errors: []string{err.Error()}}
	}

	controller.Writer.Header().Set("Location", "/user/"+userId.String())
	return &createUserResponse{ID: userId}, 201, nil
}
