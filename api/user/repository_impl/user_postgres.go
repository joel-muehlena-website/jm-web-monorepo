package repository_impl

import (
	"context"
	"fmt"
	"strings"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
	gomstoolkit_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/log"
)

var _ repository.UserRepository = &UserRepositoryPostgres{}

type UserRepositoryPostgres struct {
	DBConn     *gomstoolkit_pg.PostgreSQLPoolConnection
	linkLogger *log.ZapTraceLinkLogger
}

func NewUserRepositoryPostgres(conn *gomstoolkit_pg.PostgreSQLPoolConnection) *UserRepositoryPostgres {
	repo := new(UserRepositoryPostgres)
	repo.DBConn = conn
	repo.linkLogger = log.NewZapTraceLinkLogger(zap.L().Named("user-repo-pg"))
	return repo
}

func (repo *UserRepositoryPostgres) GetUsers(baseCtx context.Context, limit int, offset int, options repository.UserOptions) ([]types.FullUser, error) {
	whereStr := ""
	cnt := 3
	params := make([]interface{}, 2)
	params[0] = limit
	params[1] = offset

	logger := repo.linkLogger.Ctx(baseCtx)

	if options.Email != "" {
		whereStr += fmt.Sprintf(" u.email LIKE '%%' || $%d || '%%'", cnt)
		params = append(params, options.Email)
		cnt++
	}

	if whereStr != "" {
		whereStr = "WHERE" + whereStr
	}

	sql := fmt.Sprintf(`SELECT 
		u.id,
		u.email,
		u.firstName,
		u.lastName,
		u.password,
		u.username,
		u.avatar,
		u.sessionrev,
		u.createdat,
		u.updatedat,
		COALESCE(json_agg(r.rolescope || ':' || r.roleName ) FILTER (WHERE r.userid IS NOT NULL), '[]') AS roles
	FROM "user" u
	LEFT JOIN user_role r ON r.userid = u.id %s
	GROUP BY u.id, u.email, u.firstName, u.lastName, u.password, u.username, u.avatar, u.sessionrev, u.createdat, u.updatedat
	LIMIT $1 OFFSET $2`, whereStr)

	ctx, cancel := context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
	defer cancel()
	rows, err := repo.DBConn.DBPool.Query(ctx, sql, params...)
	if err != nil {

		if err == pgx.ErrNoRows {
			return nil, repository.ErrNotFound
		}

		return nil, err
	}

	result := make([]types.FullUser, 0)

	for rows.Next() {
		var user types.FullUser
		err := rows.Scan(&user.Id, &user.Email, &user.FirstName, &user.LastName, &user.Password, &user.Username, &user.Avatar, &user.SessionRef, &user.CreatedAt, &user.UpdatedAt, &user.Roles)
		if err != nil {
			logger.Error("Failed to scan db row", zap.Error(err))
		}

		result = append(result, user)
	}

	return result, nil
}

func (repo *UserRepositoryPostgres) GetUser(baseCtx context.Context, id uuid.UUID) (types.FullUser, error) {
	logger := repo.linkLogger.Ctx(baseCtx)

	if baseCtx.Value(middleware.UserShouldBeSelfKeyEndpoint) == nil && baseCtx.Value(middleware.UserPermissionsKeyEndpoint) == nil {
		return types.FullUser{}, fmt.Errorf("no permissions provided to get user data")
	}

	checkSelf := false
	uid := uuid.UUID{}
	if baseCtx.Value(middleware.UserShouldBeSelfKeyEndpoint) != nil && baseCtx.Value(middleware.UserPermissionsKeyEndpoint) == nil {
		checkSelf = true
		uid = middleware.GetUserIdFromCtx(baseCtx)
	}
	logger.Debug("Should check self", zap.Bool("checkSelf", checkSelf), zap.String("uid", uid.String()))

	if checkSelf {
		if id != uid {
			return types.FullUser{}, fmt.Errorf("without elevated permissions you can only delete yourself")
		}
	}

	// whereStr := ""
	// cnt := 2
	// params := make([]interface{}, 1)
	// params[0] = id

	// if checkSelf {
	// 	whereStr += fmt.Sprintf("u.id = $%d", cnt)
	// 	params = append(params, uid)
	// 	cnt++
	// }

	sql := `SELECT 
				u.id, 
				u.email, 
				u.firstName, 
				u.lastName, 
				u.password, 
				u.username, 
				u.avatar,
				u.sessionrev,
				u.createdat,
				u.updatedat,
				COALESCE(json_agg(r.rolescope || ':' || r.roleName ) FILTER (WHERE r.userid IS NOT NULL), '[]') AS roles
			FROM "user" u
			LEFT JOIN user_role r ON r.userid = u.id
			WHERE id = $1
			GROUP BY u.id, u.email, u.firstName, u.lastName, u.password, u.username, u.avatar, u.sessionrev, u.createdat, u.updatedat`

	ctx, cancel := context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
	defer cancel()
	row := repo.DBConn.DBPool.QueryRow(ctx, sql, id)

	var user types.FullUser
	err := row.Scan(&user.Id, &user.Email, &user.FirstName, &user.LastName, &user.Password, &user.Username, &user.Avatar, &user.SessionRef, &user.CreatedAt, &user.UpdatedAt, &user.Roles)

	if err == pgx.ErrNoRows {
		err = repository.ErrNotFound
	}

	if err != nil {
		logger.Error("Failed to scan db row for id", zap.String("id", id.String()), zap.Error(err))
		return types.FullUser{}, err
	}

	return user, nil
}

func (repo *UserRepositoryPostgres) CreateUser(baseCtx context.Context, user types.FullUser) (uuid.UUID, error) {
	sqlCreateUser := `INSERT INTO "user"
		(id, email, firstname, lastname, password, username, avatar)
		VALUES
		($1, $2, $3, $4, $5, $6, $7)
		RETURNING id`

	params := make([]interface{}, 7)
	params[0] = user.Id
	params[1] = user.Email
	params[2] = user.FirstName
	params[3] = user.LastName
	params[4] = user.Password
	params[5] = user.Username
	params[6] = user.Avatar

	ctx, cancel := context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
	defer cancel()
	tx, err := repo.DBConn.DBPool.Begin(ctx)
	if err != nil {
		return uuid.Nil, err
	}

	defer gomstoolkit_pg.TxRollback(baseCtx, tx)

	ctx, cancel = context.WithTimeout(context.Background(), db.DB_CTX_DEFAULT_TIMEOUT)
	row := tx.QueryRow(ctx, sqlCreateUser, params...)
	cancel()

	var userId uuid.UUID
	err = row.Scan(&userId)
	if err != nil {
		return uuid.Nil, err
	}

	if len(*user.Roles) > 0 {
		sqlCreateRoles, params := createUserRolesSQL(userId, user.Roles)

		err = alterSQLUserRoles(baseCtx, tx, sqlCreateRoles, params)
		if err != nil {
			zap.S().Error("failed to create user roles", err)
			return uuid.Nil, err
		}
	}

	ctx, cancel = context.WithTimeout(context.Background(), db.DB_CTX_DEFAULT_TIMEOUT)
	defer cancel()
	err = tx.Commit(ctx)
	if err != nil {
		zap.S().Error("Failed to commit tx", err)
		return uuid.Nil, err
	}

	return userId, nil
}

func createUserRolesSQL(userid uuid.UUID, roles *[]string) (sql string, params []interface{}) {
	iRows := strings.TrimSuffix(strings.Repeat("($1, $2, $3), ", len(*roles)), ", ")
	params = make([]interface{}, len(*roles)*3)

	for i, role := range *roles {
		roleData := strings.Split(role, ":")
		cnt := i * 3
		params[cnt] = userid
		params[cnt+1] = roleData[0]
		params[cnt+2] = roleData[1]
	}

	sql = fmt.Sprintf(`INSERT INTO user_role
	(userid, rolescope, rolename)
	VALUES
	%s`, iRows)

	return
}

func alterSQLUserRoles(baseCtx context.Context, tx pgx.Tx, sql string, params []interface{}) error {
	ctx, cancel := context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
	rows, err := tx.Query(ctx, sql, params...)
	cancel()

	if err != nil {
		return err
	}

	rows.Close()

	return nil
}

func (repo *UserRepositoryPostgres) DeleteUser(baseCtx context.Context, id uuid.UUID) error {
	logger := repo.linkLogger.Ctx(baseCtx)

	if baseCtx.Value(middleware.UserShouldBeSelfKeyEndpoint) == nil && baseCtx.Value(middleware.UserPermissionsKeyEndpoint) == nil {
		return fmt.Errorf("no permissions provided to get user data")
	}

	checkSelf := false
	uid := uuid.UUID{}
	if baseCtx.Value(middleware.UserShouldBeSelfKeyEndpoint) != nil && baseCtx.Value(middleware.UserPermissionsKeyEndpoint) == nil {
		checkSelf = true
		uid = middleware.GetUserIdFromCtx(baseCtx)
	}
	logger.Debug("Should check self", zap.Bool("checkSelf", checkSelf), zap.String("uid", uid.String()))

	if checkSelf {
		if id != uid {
			return fmt.Errorf("without elevated permissions you can only delete yourself")
		}
	}

	sql := `DELETE FROM "user" WHERE id = $1`

	ctx, cancel := context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
	defer cancel()
	cmdTag, err := repo.DBConn.DBPool.Exec(ctx, sql, id)
	if err != nil {
		return err
	}

	if cmdTag.RowsAffected() == 0 {
		return repository.ErrNotFound
	}

	return nil
}

func (repo *UserRepositoryPostgres) UpdateUser(baseCtx context.Context, user types.FullUser) error {
	logger := repo.linkLogger.Ctx(baseCtx)

	if baseCtx.Value(middleware.UserShouldBeSelfKeyEndpoint) == nil && baseCtx.Value(middleware.UserPermissionsKeyEndpoint) == nil {
		return fmt.Errorf("no permissions provided to get user data")
	}

	checkSelf := false
	uid := uuid.UUID{}
	if baseCtx.Value(middleware.UserShouldBeSelfKeyEndpoint) != nil && baseCtx.Value(middleware.UserPermissionsKeyEndpoint) == nil {
		checkSelf = true
		uid = middleware.GetUserIdFromCtx(baseCtx)
	}
	logger.Debug("Should check self", zap.Bool("checkSelf", checkSelf), zap.String("uid", uid.String()))

	if checkSelf {
		if uid != user.Id {
			return fmt.Errorf("without elevated permissions you can only update yourself")
		}
	}

	userSet := ""
	userParams := []interface{}{user.Id}

	cnt := 2

	if user.Avatar != nil && *user.Avatar == "" {
		user.Avatar = nil
		userSet += fmt.Sprintf(", avatar = $%d", cnt)
		userParams = append(userParams, user.Avatar)
		cnt++
	}

	if user.Email != "" {
		userSet += fmt.Sprintf(", email = $%d", cnt)
		userParams = append(userParams, user.Email)
		cnt++
	}

	if user.FirstName != "" {
		userSet += fmt.Sprintf(", firstname = $%d", cnt)
		userParams = append(userParams, user.FirstName)
		cnt++
	}

	if user.LastName != "" {
		userSet += fmt.Sprintf(", lastname = $%d", cnt)
		userParams = append(userParams, user.LastName)
		cnt++
	}

	if user.Password != "" {
		userSet += fmt.Sprintf(", password = $%d", cnt)
		userParams = append(userParams, user.Password)
		cnt++
	}

	if user.Username != "" {
		userSet += fmt.Sprintf(", username = $%d", cnt)
		userParams = append(userParams, user.Username)
		cnt++
	}

	// Early return if nothing has changed
	if userSet == "" && user.Roles == nil {
		return fmt.Errorf("please add data to an update request")
	}

	ctx, cancel := context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
	tx, err := repo.DBConn.DBPool.Begin(ctx)

	cancel()
	if err != nil {
		return err
	}

	defer gomstoolkit_pg.TxRollback(baseCtx, tx)

	if userSet != "" {
		userSet = strings.TrimLeft(userSet, ", ")

		sql := fmt.Sprintf(`UPDATE "user" SET %s WHERE id = $1 RETURNING id`, userSet)
		row := tx.QueryRow(baseCtx, sql, userParams...)

		var id uuid.UUID

		err = row.Scan(&id)

		if err == pgx.ErrNoRows {
			err = repository.ErrNotFound
		} else if err != nil {
			zap.S().Error("Failed to scan db row for id", id, err)
			return err
		}
	}

	if user.Roles != nil {
		// Remove all roles because it is a lot easier and don't requires deep logic to determine the operation
		deleteSql := `DELETE FROM user_role WHERE userid = $1`
		ctx, cancel = context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
		rows, err := tx.Query(ctx, deleteSql, user.Id)
		cancel()

		if err != nil {
			zap.S().Error("failed to delete user roles for recreation (update)", err)
			return err
		}

		rows.Close()

		// Readd roles
		sqlCreateRoles, params := createUserRolesSQL(user.Id, user.Roles)

		err = alterSQLUserRoles(baseCtx, tx, sqlCreateRoles, params)
		if err != nil {
			zap.S().Error("failed to update user roles", err)
			return err
		}
	}

	ctx, cancel = context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
	err = tx.Commit(ctx)
	cancel()

	if err != nil {
		zap.L().Error("failed to commit db transaction", zap.Error(err))
		return err
	}

	return nil
}
