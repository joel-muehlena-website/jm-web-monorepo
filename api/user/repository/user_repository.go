package repository

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
)

var (
	ErrNotFound  = fmt.Errorf("Not found")
	ErrUserFetch = fmt.Errorf("failed to fetch user")
)

type UserOptions struct {
	Email string
}

type UserRepository interface {
	GetUsers(baseCtx context.Context, limit int, offset int, options UserOptions) ([]types.FullUser, error)
	GetUser(baseCtx context.Context, id uuid.UUID) (types.FullUser, error)
	CreateUser(baseCtx context.Context, user types.FullUser) (uuid.UUID, error)
	DeleteUser(baseCtx context.Context, id uuid.UUID) error
	UpdateUser(baseCtx context.Context, user types.FullUser) error
}
