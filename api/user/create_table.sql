DROP TABLE IF EXISTS user_role;
DROP TABLE IF EXISTS "user";

CREATE TABLE "user" (
    id uuid PRIMARY KEY,
    email text UNIQUE,
    firstName text NOT NULL,
    lastName text NOT NULL,
    password text NOT NULL,
    username varchar(255) NOT NULL UNIQUE,
    avatar text,
    sessionRev int4 DEFAULT 0,
    createdAt timestamp,
    updatedAt timestamp
);

CREATE TABLE user_role (
    userid uuid,
    roleScope varchar(64),
    roleName varchar(128),
    PRIMARY KEY(userid, roleScope, roleName),
    CONSTRAINT fk_user FOREIGN KEY (userId) REFERENCES "user"(id) ON DELETE CASCADE ON UPDATE RESTRICT
);

/*
Trigger Setup for createdat and updatedat
*/

DROP TRIGGER IF EXISTS preventUpdateCreatedAtForUserTrigger ON "user";
DROP TRIGGER IF EXISTS setUpdatedAtOnUpdateForUserTrigger ON "user";
DROP TRIGGER IF EXISTS setCreatedAtUpdatedAtForUserTrigger ON "user";


CREATE TRIGGER preventUpdateCreatedAtForUserTrigger BEFORE UPDATE ON "user"
  FOR EACH ROW EXECUTE PROCEDURE preventUpdateCreatedAt();

CREATE TRIGGER setUpdatedAtOnUpdateForUserTrigger BEFORE UPDATE ON "user"
  FOR EACH ROW EXECUTE PROCEDURE setUpdatedAtOnUpdate();

CREATE TRIGGER setCreatedAtUpdatedAtForUserTrigger BEFORE INSERT ON "user"
  FOR EACH ROW EXECUTE PROCEDURE setCreatedAtAndUpdatedAt();