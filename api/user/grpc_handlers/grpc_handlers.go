package grpc_handlers

import (
	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/proto"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/repository"
	gomstoolkit_cache "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/cache"
)

type UserGrpcServer struct {
	userRepo repository.UserRepository
	cache    gomstoolkit_cache.Cache
	pb.UnimplementedUserServer
}

func NewGrpcHandler(cache gomstoolkit_cache.Cache, repo repository.UserRepository) *UserGrpcServer {
	uSvr := new(UserGrpcServer)
	uSvr.userRepo = repo
	uSvr.cache = cache
	return uSvr
}
