package grpc_handlers

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/proto"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/use_case"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
)

func (s *UserGrpcServer) GetUserById(ctx context.Context, req *pb.GetUserByIdRequest) (*pb.PublicUser, error) {
	// TODO: GRPC_AUTH This is stupid and should be handled by some kind of correct middleware
	ctx = middleware.SetProto(ctx, "grpc")
	ctx = middleware.SkipProto(ctx, "grpc")

	uc := usecase.UserOperations{
		UserRepo: s.userRepo,
	}

	uid, err := uuid.Parse(req.GetUUID())
	if err != nil {
		return nil, fmt.Errorf("failed to parse UUID: %w", err)
	}

	user, err := uc.GetUserById(ctx, uid)
	if err != nil {
		return nil, fmt.Errorf("failed to get user by id: %w", err)
	}

	return types.NativeToProtoPublicUser(&user), nil
}

func (s *UserGrpcServer) GetFullUserById(ctx context.Context, req *pb.GetUserByIdRequest) (*pb.FullUser, error) {
	// TODO: GRPC_AUTH This is stupid and should be handled by some kind of correct middleware
	ctx = middleware.SetProto(ctx, "grpc")
	ctx = middleware.SkipProto(ctx, "grpc")

	uc := usecase.UserOperations{
		UserRepo: s.userRepo,
	}

	uid, err := uuid.Parse(req.GetUUID())
	if err != nil {
		return nil, fmt.Errorf("failed to parse UUID: %w", err)
	}

	user, err := uc.GetFullUserById(ctx, uid)
	if err != nil {
		return nil, fmt.Errorf("failed to get user by id: %w", err)
	}

	return types.NativeToProtoFullUser(&user), nil
}
