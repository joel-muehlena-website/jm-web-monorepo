package grpc_handlers

import (
	"context"
	"fmt"

	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/proto"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/use_case"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
)

func (s *UserGrpcServer) CreateUser(ctx context.Context, user *pb.FullUser) (*pb.CreateUserResponse, error) {
	// TODO: GRPC_AUTH This is stupid and should be handled by some kind of correct middleware
	ctx = middleware.SetProto(ctx, "grpc")
	ctx = middleware.SkipProto(ctx, "grpc")

	uc := usecase.UserOperations{
		UserRepo: s.userRepo,
	}

	newUserId, err := uc.CreateUser(ctx, *types.ProtoToNativeFullUser(user))
	if err != nil {
		return nil, fmt.Errorf("failed to create user: %w", err)
	}

	return &pb.CreateUserResponse{UUID: newUserId.String()}, nil
}
