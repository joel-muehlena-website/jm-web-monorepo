package grpc_handlers

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/proto"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/use_case"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
)

func (s *UserGrpcServer) DeleteUser(ctx context.Context, req *pb.DeleteUserRequest) (*pb.DeleteUserResponse, error) {
	// TODO: GRPC_AUTH This is stupid and should be handled by some kind of correct middleware
	ctx = middleware.SetProto(ctx, "grpc")
	ctx = middleware.SkipProto(ctx, "grpc")

	uc := usecase.UserOperations{
		UserRepo: s.userRepo,
	}

	uid, err := uuid.Parse(req.GetUUID())
	if err != nil {
		return nil, fmt.Errorf("failed to parse UUID: %w", err)
	}

	err = uc.DeleteUser(ctx, uid)
	if err != nil {
		return nil, fmt.Errorf("failed to delete user: %w", err)
	}

	return &pb.DeleteUserResponse{}, nil
}
