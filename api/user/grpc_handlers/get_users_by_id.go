package grpc_handlers

import (
	"bytes"
	"context"
	"encoding/gob"
	"io"
	"sync"

	"github.com/google/uuid"
	"go.uber.org/zap"
	"google.golang.org/grpc/metadata"

	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/proto"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	gomstoolkit_cache "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/cache"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/log"
)

// TODO try to extract stream stuff to own finction for DRY and better testability

func (s *UserGrpcServer) GetUsersById(stream pb.User_GetUsersByIdServer) error {
	logger := log.NewZapTraceLinkLogger(zap.L()).Ctx(stream.Context())
	meta, ok := metadata.FromIncomingContext(stream.Context())

	if !ok {
		logger.Debug("New get users by id stream request but failed to extract metadata", zap.String("type", "grpc"))
	} else {
		logger.Debug("New get users by id stream request", zap.String("type", "grpc"), zap.Object("metadata", gomstoolkit.ZapMap2FieldsGrpcMetadata(meta)))
	}

	uc := usecase.UserOperations{
		UserRepo: s.userRepo,
	}

	// TODO: GRPC_AUTH This is stupid and should be handled by some kind of correct middleware
	ctx := middleware.SetProto(stream.Context(), "grpc")
	ctx = middleware.SkipProto(ctx, "grpc")
	ctx, cancel := context.WithCancel(ctx)

	cnt := 10

	idChan := make(chan string, cnt)
	resChan := make(chan *pb.UserResult, cnt)

	wgSend := &sync.WaitGroup{}

	for i := 0; i < cnt; i++ {
		// Fetch user data of database
		go getUser(ctx, uc, idChan, resChan, s.cache)
	}

	// Return data to client if has any
	// Not allowed to run send concurrently
	// Otherwise there is a sent headers after sent a like problem
	go func() {
		for {
			select {
			case data, ok := <-resChan:
				if !ok {
					continue
				}

				if data.Errors != nil && len(data.Errors) > 0 {
					logger.Error("Error getting user by id", zap.Strings("errors", data.Errors))
				}

				err := stream.Send(data)
				if err != nil {
					logger.Error("Error sending user", zap.Error(err))
					wgSend.Done()
					continue
				}
				wgSend.Done()
				logger.Debug("Sent user", zap.String("id", data.User.Id))
			case <-ctx.Done():
				return
			}
		}
	}()

	// Clean up go routines after exiting handler
	defer func() {
		cancel()
		close(resChan)
	}()

	for {
		var in pb.GetUserByIdRequest
		err := stream.RecvMsg(&in)

		if err == io.EOF {
			close(idChan)
			break
		}

		if err != nil {
			close(idChan)
			return err
		}

		wgSend.Add(1)
		// TODO improve to use in and wait for x ids before requesting
		idChan <- in.GetUUID()
	}

	wgSend.Wait()

	return nil
}

func getUser(ctx context.Context, uc usecase.UserOperations, ids <-chan string, responses chan<- *pb.UserResult, cache gomstoolkit_cache.Cache) {
	logger := log.NewZapTraceLinkLogger(zap.L()).Ctx(ctx)

	for {
		select {
		case <-ctx.Done():
			return
		case id, ok := <-ids:

			if !ok {
				continue
			}

			logger.Debug("Received id fetching user", zap.String("id", id))

			uId, err := uuid.Parse(id)
			if err != nil {
				logger.Debug("Skipping invalid id", zap.Error(err))
				continue
			}

			if data, err := cache.Get(id); err != nil {
				logger.Error("Failed to get from cache", zap.String("id", id), zap.Error(err))
				cache.Delete(id)
			} else {
				dataBytes := bytes.NewBuffer(data)
				dec := gob.NewDecoder(dataBytes)

				user := new(pb.PublicUser)
				err := dec.Decode(user)
				if err != nil {
					logger.Error("Failed to decode from cache", zap.String("id", user.Id), zap.Error(err))
				} else {
					logger.Debug("Got user from cache", zap.String("id", id))
					responses <- &pb.UserResult{User: user}
					continue
				}
			}

			user, err := uc.GetUserById(ctx, uId)
			logger.Debug("Got user", zap.String("id", id), zap.Error(err))
			if err != nil {
				responses <- &pb.UserResult{Errors: []string{err.Error()}, User: &pb.PublicUser{Id: id}}
				continue
			}

			transformedUser := types.NativeToProtoPublicUser(&user)

			// Save transformed user into the cache
			go func(user *pb.PublicUser, cache gomstoolkit_cache.Cache) {
				var dataBytes bytes.Buffer
				enc := gob.NewEncoder(&dataBytes)

				err := enc.Encode(user)
				if err != nil {
					logger.Error("Failed to save to cache (encode)", zap.String("id", user.Id), zap.Error(err), zap.Any("user", *user))
				}

				data := make([]byte, dataBytes.Len())
				copy(data, dataBytes.Bytes())

				err = cache.Set(user.Id, data)
				if err != nil {
					logger.Error("Failed to save to cache", zap.String("id", user.Id), zap.Error(err), zap.Any("user", *user))
					return
				}
			}(transformedUser, cache)

			responses <- &pb.UserResult{User: transformedUser}
		}
	}
}
