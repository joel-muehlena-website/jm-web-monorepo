package grpc_handlers

import (
	"context"
	"fmt"

	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/proto"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/use_case"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
)

func (s *UserGrpcServer) GetFullUsers(ctx context.Context, req *pb.GetUsersRequest) (*pb.GetUsersResponse, error) {
	// TODO: GRPC_AUTH This is stupid and should be handled by some kind of correct middleware
	ctx = middleware.SetProto(ctx, "grpc")
	ctx = middleware.SkipProto(ctx, "grpc")

	uc := usecase.UserOperations{
		UserRepo: s.userRepo,
	}

	limit := 20
	offset := 0

	if req.Limit != nil {
		limit = int(req.GetLimit())
	}

	if req.Offset != nil {
		offset = int(req.GetOffset())
	}

	users, err := uc.GetFullUsersOptions(ctx, limit, offset, repository.UserOptions{Email: req.GetOptions().GetEmail()})
	if err != nil {
		return nil, fmt.Errorf("failed to get users: %w", err)
	}

	return &pb.GetUsersResponse{Users: types.NativeToProtoFullUserArray(users)}, nil
}
