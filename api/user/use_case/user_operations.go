package usecase

import (
	"context"
	"crypto/md5"
	"fmt"

	"github.com/google/uuid"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
)

type UserOperations struct {
	UserRepo repository.UserRepository
}

func (uc *UserOperations) GetUsers(baseCtx context.Context, limit int, offset int) ([]types.PublicUser, error) {
	return uc.GetUsersOptions(baseCtx, limit, offset, repository.UserOptions{})
}

func (uc *UserOperations) GetUsersOptions(baseCtx context.Context, limit int, offset int, options repository.UserOptions) ([]types.PublicUser, error) {
	fullUsers, err := uc.UserRepo.GetUsers(baseCtx, limit, offset, options)
	if err != nil {
		return nil, err
	}

	users := make([]types.PublicUser, len(fullUsers))

	for i, user := range fullUsers {

		if user.Avatar == nil || *user.Avatar == "" {
			user.Avatar = useGravatarIfNoAvatar(user.Avatar, user.Email)
		}

		users[i] = user.PublicUser
	}

	return users, nil
}

func (uc *UserOperations) GetFullUsersOptions(baseCtx context.Context, limit int, offset int, options repository.UserOptions) ([]types.FullUser, error) {
	fullUsers, err := uc.UserRepo.GetUsers(baseCtx, limit, offset, options)
	if err != nil {
		return nil, err
	}

	for _, user := range fullUsers {
		if user.Avatar == nil || *user.Avatar == "" {
			user.Avatar = useGravatarIfNoAvatar(user.Avatar, user.Email)
		}
	}

	return fullUsers, nil
}

func (uc *UserOperations) GetUserById(baseCtx context.Context, id uuid.UUID) (types.PublicUser, error) {
	user, err := uc.UserRepo.GetUser(baseCtx, id)

	if user.Avatar == nil || *user.Avatar == "" {
		user.Avatar = useGravatarIfNoAvatar(user.Avatar, user.Email)
	}

	return user.PublicUser, err
}

func (uc *UserOperations) GetFullUserById(baseCtx context.Context, id uuid.UUID) (types.FullUser, error) {
	user, err := uc.UserRepo.GetUser(baseCtx, id)

	if user.Avatar == nil || *user.Avatar == "" {
		user.Avatar = useGravatarIfNoAvatar(user.Avatar, user.Email)
	}

	return user, err
}

func (uc *UserOperations) DeleteUser(baseCtx context.Context, id uuid.UUID) error {
	return uc.UserRepo.DeleteUser(baseCtx, id)
}

func (uc *UserOperations) UpdateUser(baseCtx context.Context, id uuid.UUID, user types.FullUser) error {
	user.Id = id

	err := uc.UserRepo.UpdateUser(baseCtx, user)

	return err
}

func (uc *UserOperations) CreateUser(baseCtx context.Context, user types.FullUser) (uuid.UUID, error) {
	user.Id = uuid.New()

	if user.Roles == nil {
		user.Roles = &[]string{}
	}

	id, err := uc.UserRepo.CreateUser(baseCtx, user)

	if err == nil {
		zap.L().Debug("Created new user", zap.String("id", id.String()))
	}

	return id, err
}

func useGravatarIfNoAvatar(avatar *string, email string) *string {
	const gravatarBaseURL = "https://www.gravatar.com/avatar"
	hash := md5.Sum([]byte(email))

	if avatar == nil || *avatar == "" {
		avatar = new(string)
		*avatar = fmt.Sprintf("%s/%x?d=mp", gravatarBaseURL, hash)
	}

	return avatar
}
