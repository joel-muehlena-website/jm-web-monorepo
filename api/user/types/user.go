package types

import (
	"time"

	"github.com/google/uuid"
)

type FullUser struct {
	PublicUser
	Password   string `json:"password" form:"password" validate:"required,min=8"`
	SessionRef int    `json:"sessionRef" form:"sessionRef"`
}

type PublicUser struct {
	Id        uuid.UUID `json:"id" form:"id"`
	FirstName string    `json:"firstName" form:"firstName" validate:"required"`
	LastName  string    `json:"lastName" form:"lastName" validate:"required"`
	Username  string    `json:"username" form:"username" validate:"required,min=3"`
	Avatar    *string   `json:"avatar" form:"avatar" validate:"omitempty,url"`
	Email     string    `json:"email" form:"email" validate:"required,email"`
	Roles     *[]string `json:"roles" form:"roles[]" validate:"omitempty,dive,contains=:"`
	CreatedAt time.Time `json:"createdAt" form:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt" form:"updatedAt"`
}
