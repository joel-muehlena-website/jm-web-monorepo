package types

import (
	"github.com/google/uuid"
	"google.golang.org/protobuf/types/known/timestamppb"

	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/proto"
)

func NativeToProtoPublicUser(pUser *PublicUser) *pb.PublicUser {
	avatar := ""

	if pUser.Avatar != nil {
		avatar = *pUser.Avatar
	}

	return &pb.PublicUser{
		Id:        pUser.Id.String(),
		FirstName: pUser.FirstName,
		LastName:  pUser.LastName,
		Username:  pUser.Username,
		Avatar:    avatar,
		Email:     pUser.Email,
		Roles:     *pUser.Roles,
		CreatedAt: timestamppb.New(pUser.CreatedAt),
		UpdatedAt: timestamppb.New(pUser.UpdatedAt),
	}
}

func ProtoToNativePublicUser(pUser *pb.PublicUser) *PublicUser {
	avatar := new(string)
	*avatar = pUser.Avatar

	return &PublicUser{
		Id:        uuid.MustParse(pUser.Id),
		FirstName: pUser.FirstName,
		LastName:  pUser.LastName,
		Username:  pUser.Username,
		Avatar:    avatar,
		Email:     pUser.Email,
		Roles:     &pUser.Roles,
		CreatedAt: pUser.CreatedAt.AsTime(),
		UpdatedAt: pUser.UpdatedAt.AsTime(),
	}
}

func NativeToProtoFullUserArray(pUser []FullUser) []*pb.FullUser {
	newUsers := make([]*pb.FullUser, len(pUser))

	for i, user := range pUser {
		newUsers[i] = NativeToProtoFullUser(&user)
	}

	return newUsers
}

func NativeToProtoFullUser(pUser *FullUser) *pb.FullUser {
	return &pb.FullUser{
		PublicUser:  NativeToProtoPublicUser(&pUser.PublicUser),
		Password:    pUser.Password,
		SessionsRef: int64(pUser.SessionRef),
	}
}

func ProtoToNativeFullUser(pUser *pb.FullUser) *FullUser {
	return &FullUser{
		PublicUser: *ProtoToNativePublicUser(pUser.PublicUser),
		Password:   pUser.Password,
		SessionRef: int(pUser.SessionsRef),
	}
}
