package types

import (
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	service_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
)

type UserService struct {
	Service *gomstoolkit.Service
}

type UserServiceConfig struct {
	Service  gomstoolkit.Config              `yaml:"service"`
	DBConfig service_pg.PostgreSQLPoolConfig `yaml:"dbConfig"`
}
