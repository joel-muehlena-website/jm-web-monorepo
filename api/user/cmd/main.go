package main

import (
	"flag"
	"os"
	"time"

	"go.uber.org/zap"

	gomstoolkit_cache "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/cache"
	gomstolkit_util "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/util"

	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/proto"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/grpc_handlers"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/handlers"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/repository_impl"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	service "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	service_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/utility"
)

func main() {
	configServerUrl := flag.String("config-server-url", types.CONFIG_SERVER_URL_DEV, "URL of the config server")
	configServerFilePath := flag.String("config-server-file-path", "user.dev.yaml", "Path to the config file on the config server")
	useSvcEnv := flag.Bool("use-svc-env", true, "Use the JM_SVC_ENV environment variable")
	flag.Parse()

	userSvc := types.UserService{}

	if *useSvcEnv && os.Getenv("JM_SVC_ENV") == "production" {
		*configServerUrl = types.CONFIG_SERVER_URL_PROD
		*configServerFilePath = "user.prod.yaml"
	}

	result := gomstolkit_util.FetchConfigServerConfig(*configServerUrl, *configServerFilePath)
	userServiceConfig := utility.ParseConfig[types.UserServiceConfig](result)

	svc := service.New(service.WithServiceConfig(&userServiceConfig.Service), service.WithLogger(&service.DefaultLoggerConfig))
	userSvc.Service = svc

	pool := service_pg.NewPool(svc.Logger, userServiceConfig.DBConfig)

	ur := repository_impl.NewUserRepositoryPostgres(pool)

	if httpRef, ok := svc.SubServiceCommander.Load("http"); ok {
		httpRef.SubService.(*service.Http).RegisterEndpoint(handlers.GetHandlers(&userSvc, ur)...)
	}

	cache := gomstoolkit_cache.NewBigCache(1 * time.Minute)
	err := cache.Init()
	if err != nil {
		svc.Logger.Fatal("Failed to init cache", zap.Error(err))
	}

	if grpcRef, ok := svc.SubServiceCommander.Load("grpc"); ok {
		grpcHandler := grpc_handlers.NewGrpcHandler(cache, ur)
		service.RegisterGrpc[pb.UserServer](grpcRef.SubService.(*service.Grpc), pb.RegisterUserServer, grpcHandler)
	} else {
		svc.Logger.Fatal("Failed to load grpc sub service")
	}

	err = pool.Connect()
	if err != nil {
		svc.Logger.Fatal("Failed to connect to db", zap.Error(err))
	}

	defer pool.DBPool.Close()
	err = svc.Run()
	if err != nil {
		svc.Logger.Fatal("Failed to run author service", zap.Error(err))
	}

	svc.Logger.Info("Exiting author service application")
}
