package handlers

import (
	"context"

	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type getTagsResponse struct {
	Tags []string `json:"tags"`
}

func (handler *PostHandler) getTags(baseCtx context.Context, req *gomstoolkit.EmptyRequest) (*getTagsResponse, int, error) {
	uc := usecase.TagOperations{
		TagRepository: handler.tagRepository,
	}

	tags, err := uc.Get(baseCtx)
	if err != nil {
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to fetch tags", Errors: []string{err.Error()}}
	}

	return &getTagsResponse{Tags: tags}, 200, nil
}
