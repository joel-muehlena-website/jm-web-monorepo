package handlers

import (
	"context"
	"errors"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgconn"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type createPostRequest struct {
	types.Post
}

type createPostResponse struct {
	ID uuid.UUID `json:"id"`
}

func (handler *PostHandler) createPost(baseCtx context.Context, controller gomstoolkit.RequestController, req *createPostRequest) (*createPostResponse, int, error) {
	errStr, _ := gomstoolkit.Validate(req)

	if len(errStr) >= 1 {
		return nil, 400, &gomstoolkit.HTTPError{Message: "failed to validate request", Errors: errStr}
	}

	uc := usecase.PostOperations{
		PostRepository: handler.postRepository,
		Cache:          handler.authorCache,
	}

	postId, err := uc.CreatePost(baseCtx, req.Post)
	if err != nil {
		var pgErr *pgconn.PgError

		if errors.As(err, &pgErr) {
			if pgErr.ConstraintName == "unique_title" {
				return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "failed to create post because there is already a post with this name", Errors: []string{err.Error(), pgErr.Detail}}
			}
		}

		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to create post: internal error", Errors: []string{err.Error()}}
	}

	controller.Writer.Header().Set("Location", "/post/"+postId.String())
	return &createPostResponse{ID: postId}, 201, nil
}
