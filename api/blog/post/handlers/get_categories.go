package handlers

import (
	"context"

	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type getCategoriesResponse struct {
	Categories []string `json:"categories"`
}

func (handler *PostHandler) getCategories(baseCtx context.Context, req *gomstoolkit.EmptyRequest) (*getCategoriesResponse, int, error) {
	uc := usecase.CategoryOperations{
		CategoryRepository: handler.categoryRepository,
	}

	categories, err := uc.Get(baseCtx)
	if err != nil {
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to fetch categories", Errors: []string{err.Error()}}
	}

	return &getCategoriesResponse{Categories: categories}, 200, nil
}
