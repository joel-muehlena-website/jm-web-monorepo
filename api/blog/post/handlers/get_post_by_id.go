package handlers

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type getPostByIdRequest struct {
	Id string `uri:"id"`
}

type getPostByIdResponse struct {
	Post types.Post `json:"post"`
}

func (handler *PostHandler) getPostById(baseCtx context.Context, req *getPostByIdRequest) (*getPostByIdResponse, int, error) {
	id, err := uuid.Parse(req.Id)
	if err != nil {
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid id passed", Errors: []string{err.Error()}}
	}

	uc := usecase.PostOperations{
		PostRepository: handler.postRepository,
		Cache:          handler.authorCache,
		RemoteServices: &handler.postService.Service.RemoteServices,
	}

	post, err := uc.GetPostById(baseCtx, id)

	if err == repository.ErrNotFound {
		return nil, 404, &gomstoolkit.HTTPError{Code: 404, Message: "no post with this id found", Errors: []string{err.Error()}}
	}

	if err != nil {
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to get post: internal error", Errors: []string{err.Error()}}
	}

	return &getPostByIdResponse{Post: post}, 200, nil
}
