package handlers

import (
	"context"
	"errors"

	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgconn"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type updatePostRequest struct {
	Id   string     `uri:"id"`
	Post types.Post `json:"post"`
}

type updatePostResponse struct {
	Msg string    `json:"msg"`
	Id  uuid.UUID `json:"id"`
}

func (handler *PostHandler) updatePost(baseCtx context.Context, req *updatePostRequest) (*updatePostResponse, int, error) {
	id, err := uuid.Parse(req.Id)
	if err != nil {
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid id passed", Errors: []string{err.Error()}}
	}

	uc := usecase.PostOperations{
		PostRepository: handler.postRepository,
		Cache:          handler.authorCache,
		RemoteServices: &handler.postService.Service.RemoteServices,
	}

	userId := middleware.GetUserIdFromCtx(baseCtx)
	zap.L().Debug("Extracted user id in http request (update post)", zap.String("userId", userId.String()))

	err = uc.UpdatePost(baseCtx, id, userId, req.Post)
	if err != nil {
		var pgErr *pgconn.PgError

		if errors.As(err, &pgErr) {
			if pgErr.ConstraintName == "unique_title" {
				return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "failed to update post because this title is already taken", Errors: []string{err.Error(), pgErr.Detail}}
			}
		}

		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to update post: internal error", Errors: []string{err.Error()}}
	}

	return &updatePostResponse{Msg: "Updated post", Id: id}, 200, nil
}
