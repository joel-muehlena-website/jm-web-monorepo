package handlers

import (
	"time"

	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/types"
	service "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	gomstoolkit_cache "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/cache"
)

type PostHandler struct {
	postService        *types.PostService
	authorCache        gomstoolkit_cache.Cache
	postRepository     repository.PostRepository
	tagRepository      repository.TagRepository
	categoryRepository repository.CategoryRepository
}

func GetHandlers(postSvc *types.PostService, postRepository repository.PostRepository, tagRepository repository.TagRepository, categoryRepository repository.CategoryRepository) []service.Hitable {
	services := make([]service.Hitable, 0)

	handler := PostHandler{
		postService:        postSvc,
		authorCache:        gomstoolkit_cache.NewBigCache(3 * time.Minute),
		postRepository:     postRepository,
		tagRepository:      tagRepository,
		categoryRepository: categoryRepository,
	}

	handler.authorCache.Init()

	getPostEndpoint := service.Endpoint[getPostRequest, getPostResponse]{
		Path:    "/post",
		Method:  service.GET,
		Handler: handler.getPost,
	}

	getTagsEndpoint := service.Endpoint[service.EmptyRequest, getTagsResponse]{
		Path:    "/tag",
		Method:  service.GET,
		Handler: handler.getTags,
	}

	getCategoriesEndpoint := service.Endpoint[service.EmptyRequest, getCategoriesResponse]{
		Path:    "/category",
		Method:  service.GET,
		Handler: handler.getCategories,
	}

	getPostByIdEndpoint := service.Endpoint[getPostByIdRequest, getPostByIdResponse]{
		Path:    "/post/:id",
		Method:  service.GET,
		Handler: handler.getPostById,
	}

	createPostEndpoint := service.Endpoint[createPostRequest, createPostResponse]{
		Path:                "/post",
		Method:              service.POST,
		HandlerController:   handler.createPost,
		RestrictContentType: []string{"application/json"},
		AuthorizationChain:  []service.AuthorizationHandler{middleware.IsAuthenticated, middleware.IsPermittedForRole("jm_blog::BlogPost::Create::Allow")},
	}

	updatePostEndpoint := service.Endpoint[updatePostRequest, updatePostResponse]{
		Path:                "/post/:id",
		Method:              service.PATCH,
		Handler:             handler.updatePost,
		RestrictContentType: []string{"application/json"},
		AuthorizationChain:  []service.AuthorizationHandler{middleware.IsAuthenticated, middleware.IsPermittedForRole("jm_blog::BlogPost::Create::Allow")},
	}

	deletePostEndpoint := service.Endpoint[deletePostRequest, deletePostResponse]{
		Path:               "/post/:id",
		Method:             service.DELETE,
		Handler:            handler.deletePost,
		AuthorizationChain: []service.AuthorizationHandler{middleware.IsAuthenticated, middleware.IsPermittedForRole("jm_blog::BlogPost::Delete::Allow")},
	}

	// TODO: Restrict edit and delete to author only (by authorId) or if blog admin	(by role)

	// Public
	services = append(services, &getPostByIdEndpoint)
	// Public
	services = append(services, &getPostEndpoint)
	// Public
	services = append(services, &getTagsEndpoint)
	// Public
	services = append(services, &getCategoriesEndpoint)
	// Auth, role jm-blog:post:create
	services = append(services, &createPostEndpoint)
	// Auth, self authorId -> match (fetch author)
	services = append(services, &updatePostEndpoint)
	// Auth, self authorId -> match (fetch author)
	services = append(services, &deletePostEndpoint)

	return services
}
