package handlers

import (
	"context"
	"time"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type getPostRequest struct {
	// Should start with 0 for page 1 (like array indexing)
	Page     uint32   `form:"page"`
	Limit    uint32   `form:"limit"`
	AuthorID string   `form:"authorId"`
	Tags     []string `form:"tags"`
	Category string   `form:"category"`
	Before   string   `form:"before"`
	After    string   `form:"after"`
}

type getPostResponse struct {
	Posts   []types.Post `json:"posts"`
	PostCnt int          `json:"post_cnt"`
}

func (handler *PostHandler) getPost(baseCtx context.Context, req *getPostRequest) (*getPostResponse, int, error) {
	uc := usecase.PostOperations{
		PostRepository: handler.postRepository,
		Cache:          handler.authorCache,
		RemoteServices: &handler.postService.Service.RemoteServices,
	}

	var authorIDQuery uuid.UUID
	var beforeQuery, afterQuery time.Time

	if req.AuthorID != "" {
		authorID, err := uuid.Parse(req.AuthorID)
		if err != nil {
			return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid author id passed in query", Errors: []string{err.Error()}}
		}
		authorIDQuery = authorID
	}

	if req.Before != "" {
		before, err := time.Parse(time.RFC3339, req.Before)
		if err != nil {
			return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid before date passed in query", Errors: []string{err.Error()}}
		}
		beforeQuery = before
	}

	if req.After != "" {
		after, err := time.Parse(time.RFC3339, req.After)
		if err != nil {
			return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid after date passed in query", Errors: []string{err.Error()}}
		}
		afterQuery = after
	}

	filter := types.PostFilter{
		Offset:   req.Page,
		Limit:    req.Limit,
		Category: req.Category,
		AuthorId: authorIDQuery,
		Tags:     req.Tags,
		Before:   beforeQuery,
		After:    afterQuery,
	}

	posts, err := uc.GetPosts(baseCtx, filter)

	if err == repository.ErrNotFound {
		return nil, 404, &gomstoolkit.HTTPError{Code: 404, Message: "no posts found", Errors: []string{err.Error()}}
	}

	if err != nil {
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to get posts: internal error", Errors: []string{err.Error()}}
	}

	return &getPostResponse{Posts: posts, PostCnt: len(posts)}, 200, nil
}
