package handlers

import (
	"context"

	"github.com/google/uuid"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
)

type deletePostRequest struct {
	Id string `uri:"id"`
}

type deletePostResponse struct {
	Id  uuid.UUID `json:"id"`
	Msg string    `json:"msg"`
}

func (handler *PostHandler) deletePost(baseCtx context.Context, req *deletePostRequest) (*deletePostResponse, int, error) {
	id, err := uuid.Parse(req.Id)
	if err != nil {
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid id passed", Errors: []string{err.Error()}}
	}

	uc := usecase.PostOperations{
		PostRepository: handler.postRepository,
		Cache:          handler.authorCache,
	}

	userId := middleware.GetUserIdFromCtx(baseCtx)
	zap.L().Debug("Extracted user id in http request (delete post)", zap.String("userId", userId.String()))

	err = uc.DeletePost(baseCtx, id, userId)

	if err == repository.ErrNotFound {
		return nil, 404, &gomstoolkit.HTTPError{Code: 404, Message: "no post with this id found", Errors: []string{err.Error()}}
	}

	if err != nil {
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to delete post: internal error", Errors: []string{err.Error()}}
	}

	return &deletePostResponse{Id: id, Msg: "Successfully deleted post"}, 200, nil
}
