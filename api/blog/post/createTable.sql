DROP TABLE IF EXISTS post_tags;
DROP TABLE IF EXISTS post;

CREATE TABLE post (
    id uuid PRIMARY KEY NOT NULL,
    authorid uuid,
    title varchar(255) NOT NULL,
    description varchar(300) NOT NULL,
    content text NOT NULL,
    category varchar(255),
    createdat timestamp,
    updatedat timestamp DEFAULT now(),
    CONSTRAINT unique_title UNIQUE(title)
);

CREATE TABLE post_tag (
    postid uuid,
    tag varchar(255),
    PRIMARY KEY(postid, tag),
    CONSTRAINT post_fk FOREIGN KEY (postid) REFERENCES post(id) ON DELETE CASCADE ON UPDATE CASCADE
);


DROP TRIGGER IF EXISTS preventUpdateCreatedAtForPostTrigger ON post;
DROP TRIGGER IF EXISTS setUpdatedAtOnUpdateForPostTrigger ON post;
DROP TRIGGER IF EXISTS setCreatedAtUpdatedAtForPostTrigger ON post;

CREATE TRIGGER preventUpdateCreatedAtForPostTrigger BEFORE UPDATE ON post
  FOR EACH ROW EXECUTE PROCEDURE preventUpdateCreatedAt();

CREATE TRIGGER setUpdatedAtOnUpdateForPostTrigger BEFORE UPDATE ON post
  FOR EACH ROW EXECUTE PROCEDURE setUpdatedAtOnUpdate();

CREATE TRIGGER setCreatedAtUpdatedAtForPostTrigger BEFORE INSERT ON post
  FOR EACH ROW EXECUTE PROCEDURE setCreatedAtAndUpdatedAt();