package main

import (
	"flag"
	"os"

	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/handlers"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository_impl"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/types"
	service "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	service_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
	gomstolkit_util "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/util"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/utility"
)

func main() {
	configServerUrl := flag.String("config-server-url", types.CONFIG_SERVER_URL_DEV, "URL of the config server")
	configServerFilePath := flag.String("config-server-file-path", "blog/post.dev.yaml", "Path to the config file on the config server")
	useSvcEnv := flag.Bool("use-svc-env", true, "Use the JM_SVC_ENV environment variable")
	flag.Parse()

	postSvc := types.PostService{}

	if *useSvcEnv && os.Getenv("JM_SVC_ENV") == "production" {
		*configServerUrl = types.CONFIG_SERVER_URL_PROD
		*configServerFilePath = "blog/post.prod.yaml"
	}

	result := gomstolkit_util.FetchConfigServerConfig(*configServerUrl, *configServerFilePath)
	postServiceConfig := utility.ParseConfig[types.PostServiceConfig](result)

	svc := service.New(service.WithServiceConfig(&postServiceConfig.Service), service.WithLogger(&service.DefaultLoggerConfig))
	postSvc.Service = svc

	pool := service_pg.NewPool(svc.Logger, postServiceConfig.DBConfig)

	postRepository := repository_impl.NewPostRepositoryPostgres(pool)
	tagRepository := repository_impl.NewTagRepositoryPostgres(nil, pool)
	categoryRepository := repository_impl.NewCategoryRepositoryPostgres(pool)

	if httpRef, ok := svc.SubServiceCommander.Load("http"); ok {
		httpRef.SubService.(*service.Http).RegisterEndpoint(handlers.GetHandlers(&postSvc, postRepository, tagRepository, categoryRepository)...)
	}

	err := pool.Connect()
	if err != nil {
		svc.Logger.Fatal("Failed to connect to db", zap.Error(err))
	}
	defer pool.DBPool.Close()

	err = svc.Run()
	if err != nil {
		svc.Logger.Fatal("Failed to run author service", zap.Error(err))
	}
}
