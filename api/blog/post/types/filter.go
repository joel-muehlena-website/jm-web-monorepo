package types

import (
	"time"

	"github.com/google/uuid"
)

type PostFilter struct {
	Offset   uint32
	Limit    uint32
	AuthorId uuid.UUID
	Tags     []string
	Category string
	Before   time.Time
	After    time.Time
}
