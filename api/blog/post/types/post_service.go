package types

import (
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	gomstoolkit_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
)

type PostService struct {
	Service *gomstoolkit.Service
}

type PostServiceConfig struct {
	Service  gomstoolkit.Config                  `yaml:"service"`
	DBConfig gomstoolkit_pg.PostgreSQLPoolConfig `yaml:"dbConfig"`
}
