package types

import (
	"time"

	"github.com/google/uuid"
)

type Post struct {
	Id             uuid.UUID `json:"id" form:"id"`
	AuthorId       uuid.UUID `json:"authorId" form:"authorId" validate:"required"`
	AuthorFistName string    `json:"authorFirstName" form:"-"`
	AuthorLastName string    `json:"authorLastName" form:"-"`
	AuthorAvatar   string    `json:"authorAvatar" form:"-"`
	Title          string    `json:"title" form:"title" validate:"required,min=5"`
	Description    string    `json:"description" form:"description" validate:"required,min=10"`
	Content        string    `json:"content" form:"content" validate:"required,min=50"`
	Category       string    `json:"category" form:"category" validate:"required,min=4"`
	CreatedAt      time.Time `json:"createdAt"`
	UpdatedAt      time.Time `json:"updatedAt"`
	Tags           []string  `json:"tags" form:"tag[]" validate:"required,min=1"`
}

// Represents just the data scheme in the database
type PostTag struct {
	PostId uuid.UUID `db:"postid"`
	Tag    string    `db:"tag"`
}
