package types

const (
	CONFIG_SERVER_URL_DEV  = "http://localhost:8081/config"
	CONFIG_SERVER_URL_PROD = "http://config-server.jm-core.svc.cluster.local/config"
)

const (
	AUTHOR_SVC_ID = "AUTHOR_SVC_ID"

	PROD_AUTHOR_SVC_URL = "author-service.jm-blog.svc.cluster.local:5555"
	DEV_AUTHOR_SVC_URL  = "localhost:5556"
)
