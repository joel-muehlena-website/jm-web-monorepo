package repository_impl

import (
	"context"
	"fmt"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository"
	gomstoolkit_db "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
	gomstoolkit_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
)

var _ repository.CategoryRepository = &categoryRepositoryPostgres{}

type categoryRepositoryPostgres struct {
	dbConn *gomstoolkit_pg.PostgreSQLPoolConnection
}

func NewCategoryRepositoryPostgres(conn *gomstoolkit_pg.PostgreSQLPoolConnection) *categoryRepositoryPostgres {
	repo := new(categoryRepositoryPostgres)
	repo.dbConn = conn
	return repo
}

func (repo *categoryRepositoryPostgres) Get(baseCtx context.Context) ([]string, error) {
	sql := `SELECT DISTINCT category FROM post`

	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	rows, err := repo.dbConn.DBPool.Query(ctx, sql)
	defer cancel()

	if err != nil {
		return nil, fmt.Errorf("failed to query database: %w", err)
	}

	categories := make([]string, 0)
	for rows.Next() {
		var cat string
		err = rows.Scan(&cat)
		if err != nil {
			return nil, fmt.Errorf("failed to parse query result: %w", err)
		}

		categories = append(categories, cat)
	}

	if len(categories) == 0 {
		return nil, repository.ErrNotFound
	}

	return categories, nil
}
