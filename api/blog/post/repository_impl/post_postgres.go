package repository_impl

import (
	"context"
	"fmt"
	"strings"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/use_case"

	gomstoolkit_db "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
	gomstoolkit_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
)

var _ repository.PostRepository = &postRepositoryPostgres{}

type postRepositoryPostgres struct {
	DBConn *gomstoolkit_pg.PostgreSQLPoolConnection
}

func NewPostRepositoryPostgres(conn *gomstoolkit_pg.PostgreSQLPoolConnection) *postRepositoryPostgres {
	repo := new(postRepositoryPostgres)
	repo.DBConn = conn
	return repo
}

func (repo *postRepositoryPostgres) Get(baseCtx context.Context, filter types.PostFilter) ([]types.Post, error) {
	// sql := `SELECT
	// 					p.id,
	// 					p.authorid,
	// 					p.title,
	// 					p.description,
	// 					p.content,
	// 					p.category,
	// 					p.createdat,
	// 					p.updatedat,
	// 					COALESCE(json_agg(t.tag) FILTER (WHERE t.tag IS NOT NULL), '[]') AS tags
	// 				FROM
	// 					post p
	// 					LEFT JOIN post_tag t ON p.id = t.postid
	// 				%s
	// 				GROUP BY
	// 					p.id,
	// 					p.authorid,
	// 					p.title,
	// 					p.description,
	// 					p.content,
	// 					p.category,
	// 					p.createdat,
	// 					p.updatedat
	// 				ORDER BY p.createdat DESC LIMIT $1 OFFSET $2`

	sql := `SELECT
							p.id,
							p.authorid,
							p.title,
							p.description,
							p.content,
							p.category,
							p.createdat,
							p.updatedat,
							COALESCE(array_agg(t.tag) FILTER (WHERE t.tag IS NOT NULL), ARRAY[]::varchar[]) AS tags
					FROM (
							SELECT DISTINCT
									p.id,
									p.authorid,
									p.title,
									p.description,
									p.content,
									p.category,
									p.createdat,
									p.updatedat
							FROM
									post p
									INNER JOIN post_tag t ON p.id = t.postid
							%s
					) p
					LEFT JOIN post_tag t ON p.id = t.postid
					GROUP BY
							p.id,
							p.authorid,
							p.title,
							p.description,
							p.content,
							p.category,
							p.createdat,
							p.updatedat
					%s
					ORDER BY p.createdat DESC LIMIT $1 OFFSET $2;`

	params := []interface{}{filter.Limit, filter.Offset}
	where := []string{}
	havingStr := ""

	cnt := 3
	if filter.AuthorId != uuid.Nil {
		where = append(where, fmt.Sprintf("p.authorid = $%d", cnt))
		params = append(params, filter.AuthorId)
		cnt++
	}

	if filter.Category != "" {
		where = append(where, fmt.Sprintf("p.category = $%d", cnt))
		params = append(params, filter.Category)
		cnt++
	}

	if len(filter.Tags) > 0 {
		indices := fmt.Sprintf("$%d", cnt)
		params = append(params, filter.Tags[0])

		for i, t := range filter.Tags[1:] {
			indices += fmt.Sprintf(",$%d", cnt+i+1)
			params = append(params, t)
		}

		havingStr = fmt.Sprintf("HAVING COALESCE(array_agg(t.tag) FILTER (WHERE t.tag IS NOT NULL), ARRAY[]::varchar[]) @> ARRAY[%s]::varchar[]", indices)

		where = append(where, fmt.Sprintf("t.tag IN (%s)", indices))

		cnt += len(filter.Tags)
	}

	if !filter.Before.IsZero() {
		where = append(where, fmt.Sprintf("p.createdat < $%d", cnt))
		params = append(params, filter.Before)
		cnt++
	}

	if !filter.After.IsZero() {
		where = append(where, fmt.Sprintf("p.createdat > $%d", cnt))
		params = append(params, filter.After)
		cnt++
	}

	whereStr := strings.Join(where, " AND ")
	if whereStr != "" {
		whereStr = "WHERE " + whereStr
	}

	return repo.get(baseCtx, fmt.Sprintf(sql, whereStr, havingStr), params)
}

func (repo *postRepositoryPostgres) GetById(baseCtx context.Context, id uuid.UUID) (types.Post, error) {
	sql := `SELECT 
						p.id,
						p.authorid,
						p.title,
						p.description,
						p.content,
						p.category,
						p.createdat,
						p.updatedat,
						COALESCE(json_agg(t.tag) FILTER (WHERE t.tag IS NOT NULL), '[]') AS tags
					FROM
						post p
						LEFT JOIN post_tag t ON p.id = t.postid
					WHERE p.id = $1
					GROUP BY 
						p.id,
						p.authorid,
						p.title,
						p.description,
						p.content,
						p.category,
						p.createdat,
						p.updatedat`

	posts, err := repo.get(baseCtx, sql, []interface{}{id})
	if err != nil {
		return types.Post{}, err
	}

	return posts[0], err
}

func (repo *postRepositoryPostgres) get(baseCtx context.Context, sql string, params []interface{}) ([]types.Post, error) {
	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	rows, err := repo.DBConn.DBPool.Query(ctx, sql, params...)
	defer cancel()

	if err != nil {
		return nil, fmt.Errorf("failed to query database: %w", err)
	}

	posts := make([]types.Post, 0)
	for rows.Next() {
		var post types.Post
		post.Tags = make([]string, 0)
		err = rows.Scan(&post.Id, &post.AuthorId, &post.Title, &post.Description, &post.Content, &post.Category, &post.CreatedAt, &post.UpdatedAt, &post.Tags)
		if err != nil {
			return nil, fmt.Errorf("failed to parse query result: %w", err)
		}

		posts = append(posts, post)
	}

	if len(posts) == 0 {
		return nil, repository.ErrNotFound
	}

	return posts, nil
}

func (repo *postRepositoryPostgres) Create(baseCtx context.Context, post types.Post) (uuid.UUID, error) {
	sqlCreate := `INSERT INTO post (id, authorid, title, description, content, category) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`

	// Get transaction with beginning it
	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	tx, err := repo.DBConn.DBPool.Begin(ctx)
	cancel()
	if err != nil {
		return uuid.Nil, err
	}

	// Rollback in every case. Except commit has been called before explicitly
	defer gomstoolkit_pg.TxRollback(baseCtx, tx)

	// Create author
	ctx, cancel = context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	row := tx.QueryRow(ctx, sqlCreate, post.Id, post.AuthorId, post.Title, post.Description, post.Content, post.Category)
	cancel()

	var retId uuid.UUID
	err = row.Scan(&retId)

	if err != nil {
		return uuid.Nil, err
	}

	// Create tags
	tuc := usecase.TagOperations{
		TagRepository: NewTagRepositoryPostgres(tx, nil),
	}

	err = tuc.Set(baseCtx, usecase.CREATE_TAG, retId, post.Tags)
	if err != nil {
		return uuid.Nil, err
	}

	// Commit transaction to persist it
	ctx, cancel = context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	err = tx.Commit(ctx)
	cancel()
	if err != nil {
		return uuid.Nil, err
	}

	return retId, nil
}

func (repo *postRepositoryPostgres) Delete(baseCtx context.Context, id uuid.UUID, userId uuid.UUID) error {
	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)

	// Just delete the post here. SQL DELETE CASCADE must do the rest
	cmdTag, err := repo.DBConn.DBPool.Exec(ctx, "DELETE FROM post WHERE id = $1 AND authorid=(SELECT id FROM author a WHERE a.userid = $2)", id, userId)
	defer cancel()

	if err != nil {
		return err
	}

	if cmdTag.RowsAffected() == 0 {
		return repository.ErrNotFound
	}

	return nil
}

func (repo *postRepositoryPostgres) Update(baseCtx context.Context, post types.Post, userId uuid.UUID) error {
	params := make([]interface{}, 2)
	paramsString := ""
	params[0] = post.Id
	params[1] = userId
	cnt := 3
	if post.Title != "" {
		params = append(params, post.Title)
		paramsString += fmt.Sprintf("title = $%d,", cnt)
		cnt++
	}

	if post.Description != "" {
		params = append(params, post.Description)
		paramsString += fmt.Sprintf("description = $%d,", cnt)
		cnt++
	}

	if post.Content != "" {
		params = append(params, post.Content)
		paramsString += fmt.Sprintf("content = $%d,", cnt)
		cnt++
	}

	if post.Category != "" {
		params = append(params, post.Category)
		paramsString += fmt.Sprintf("category = $%d,", cnt)
		cnt++
	}

	paramsString = strings.TrimRight(paramsString, ",")

	// Get transaction with beginning it
	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	tx, err := repo.DBConn.DBPool.Begin(ctx)
	cancel()
	if err != nil {
		return err
	}

	// Rollback in every case. Except commit has been called before explicitly
	defer gomstoolkit_pg.TxRollback(baseCtx, tx)

	if paramsString == "" && len(post.Tags) <= 0 {
		return fmt.Errorf("no data to change provided")
	}

	if paramsString != "" {
		sql := fmt.Sprintf("UPDATE post SET %s WHERE id = $1 AND authorid=(SELECT id FROM author a WHERE a.userid = $2)", paramsString)
		ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
		cmdTag, err := tx.Exec(ctx, sql, params...)
		cancel()

		if err != nil {
			return fmt.Errorf("failed to update post: %w", err)
		}

		if cmdTag.RowsAffected() == 0 {
			return repository.ErrNotFound
		}
	}

	// Update Tags
	if len(post.Tags) > 0 {
		tagRepo := NewTagRepositoryPostgres(tx, nil)
		err = tagRepo.Update(baseCtx, post.Id, post.Tags)

		if err != nil {
			return fmt.Errorf("failed to update tags for post: %w", err)
		}
	}

	// Commit transaction to persist it
	ctx, cancel = context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	err = tx.Commit(ctx)
	cancel()
	if err != nil {
		return err
	}

	return nil
}
