package repository_impl

import (
	"context"
	"fmt"
	"strings"

	"github.com/google/uuid"

	"github.com/jackc/pgx/v5"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository"
	gomstoolkit_db "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
	gomstoolkit_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
)

var _ repository.TagRepository = &tagRepositoryPostgres{}

type tagRepositoryPostgres struct {
	tx     pgx.Tx
	dbConn *gomstoolkit_pg.PostgreSQLPoolConnection
}

func NewTagRepositoryPostgres(tx pgx.Tx, conn *gomstoolkit_pg.PostgreSQLPoolConnection) *tagRepositoryPostgres {
	repo := new(tagRepositoryPostgres)
	repo.tx = tx
	repo.dbConn = conn
	return repo
}

func (repo *tagRepositoryPostgres) Create(baseCtx context.Context, postId uuid.UUID, tags []string) error {
	if repo.tx == nil {
		return fmt.Errorf("tx is not set")
	}

	if len(tags) == 0 {
		return fmt.Errorf("no tags provided")
	}

	cnt := 2
	tagValues := make([]string, len(tags))
	params := []interface{}{postId}
	for i, tag := range tags {
		tagValues[i] = fmt.Sprintf("($1, $%d)", cnt)
		params = append(params, tag)
		cnt++
	}

	sql := fmt.Sprintf(`INSERT INTO post_tag (postid, tag) VALUES %s`, strings.Join(tagValues, ","))

	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	_, err := repo.tx.Exec(ctx, sql, params...)
	defer cancel()

	if err != nil {
		return fmt.Errorf("failed to query database for tag creation: %w", err)
	}

	return nil
}

func (repo *tagRepositoryPostgres) Update(baseCtx context.Context, postId uuid.UUID, tags []string) error {
	if repo.tx == nil {
		return fmt.Errorf("tx is not set")
	}

	if len(tags) == 0 {
		return fmt.Errorf("no tags provided")
	}

	// Delete all tags for the post and then recreate is much less effort
	sql := `DELETE FROM post_tag WHERE postid = $1`
	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	_, err := repo.tx.Exec(ctx, sql, postId)
	defer cancel()

	if err != nil {
		return fmt.Errorf("failed to query database for tag creation: %w", err)
	}

	return repo.Create(baseCtx, postId, tags)
}

func (repo *tagRepositoryPostgres) Get(baseCtx context.Context) ([]string, error) {
	sql := `SELECT DISTINCT tag FROM post_tag`
	return repo.get(baseCtx, sql, []interface{}{})
}

func (repo *tagRepositoryPostgres) DynamicGet(baseCtx context.Context, value string) ([]string, error) {
	sql := `SELECT DISTINCT tag FROM post_tag WHERE tag LIKE $1%`
	return repo.get(baseCtx, sql, []interface{}{value})
}

func (repo *tagRepositoryPostgres) get(baseCtx context.Context, sql string, params []interface{}) ([]string, error) {
	if repo.dbConn == nil {
		return nil, fmt.Errorf("dbConn is not set")
	}

	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	rows, err := repo.dbConn.DBPool.Query(ctx, sql, params...)
	defer cancel()

	if err != nil {
		return nil, fmt.Errorf("failed to query database: %w", err)
	}

	tags := make([]string, 0)
	for rows.Next() {
		var tag string
		err = rows.Scan(&tag)
		if err != nil {
			return nil, fmt.Errorf("failed to parse query result: %w", err)
		}

		tags = append(tags, tag)
	}

	if len(tags) == 0 {
		return nil, repository.ErrNotFound
	}

	return tags, nil
}
