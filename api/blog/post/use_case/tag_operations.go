package usecase

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository"
)

type TagSetType uint8

const (
	CREATE_TAG TagSetType = iota
	UPDATE_TAG TagSetType = iota
)

type TagOperations struct {
	TagRepository repository.TagRepository
}

func (uc *TagOperations) Set(baseCtx context.Context, setType TagSetType, postId uuid.UUID, tags []string) error {
	var err error = nil

	switch setType {
	case CREATE_TAG:
		err = uc.TagRepository.Create(baseCtx, postId, tags)
	case UPDATE_TAG:
		err = uc.TagRepository.Update(baseCtx, postId, tags)
	default:
		err = fmt.Errorf("invalid type for tag operation")
	}

	return err
}

func (uc *TagOperations) Get(baseCtx context.Context) ([]string, error) {
	return uc.TagRepository.Get(baseCtx)
}
