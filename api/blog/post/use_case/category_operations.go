package usecase

import (
	"context"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository"
)

type CategoryOperations struct {
	CategoryRepository repository.CategoryRepository
}

func (uc *CategoryOperations) Get(baseCtx context.Context) ([]string, error) {
	return uc.CategoryRepository.Get(baseCtx)
}
