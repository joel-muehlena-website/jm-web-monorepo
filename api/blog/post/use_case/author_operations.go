package usecase

import (
	"context"
	"encoding/json"
	"fmt"

	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-middleware/providers/prometheus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	author_pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/proto"
	author_types "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/types"
	gomstoolkit_cache "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/cache"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/telemetry"
)

func FetchAuthors(baseCtx context.Context, url string, posts []types.Post, cache gomstoolkit_cache.Cache) ([]author_types.Author, error) {
	authors := make([]author_types.Author, 0)
	toFetch := &author_pb.GetAuthorsByIdRequest{
		UUIDs: make([]string, 0),
	}

	for _, post := range posts {
		cachedData, err := cache.Get(post.AuthorId.String())

		if err == nil {
			var author author_types.Author
			err = json.Unmarshal(cachedData, &author)
			if err != nil {
				cache.Delete(post.AuthorId.String())
			} else {
				authors = append(authors, author)
				continue
			}

		}

		toFetch.UUIDs = append(toFetch.UUIDs, post.AuthorId.String())
	}

	if len(toFetch.GetUUIDs()) == 0 {
		return authors, nil
	}

	metrics := grpc_prometheus.NewClientMetrics(grpc_prometheus.WithClientHandlingTimeHistogram())
	var opts []grpc.DialOption

	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	opts = append(opts, telemetry.GetGRPCClientHandler(), grpc.WithUnaryInterceptor(metrics.UnaryClientInterceptor()), grpc.WithStreamInterceptor(metrics.StreamClientInterceptor()))
	conn, err := grpc.Dial(url, opts...)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	client := author_pb.NewAuthorClient(conn)

	ctx, cancel := context.WithCancel(baseCtx)
	defer cancel()
	result, err := client.GetAuthorsById(ctx, toFetch)
	if err != nil {
		return nil, err
	}

	authors = append(authors, author_types.ProtoAuthorsToNatives(result.GetAuthors())...)

	return authors, nil
}

func FetchAndMapAuthors(baseCtx context.Context, url string, posts []types.Post, cache gomstoolkit_cache.Cache) error {
	authors, err := FetchAuthors(baseCtx, url, posts, cache)
	if err != nil {
		return fmt.Errorf("failed to map posts and authors: %w", err)
	}

	for i, post := range posts {
		for _, author := range authors {
			if post.AuthorId == author.Id {
				posts[i].AuthorFistName = author.FirstName
				posts[i].AuthorLastName = author.LastName
				posts[i].AuthorAvatar = author.Avatar
			}
		}
	}

	return nil
}
