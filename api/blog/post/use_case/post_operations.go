package usecase

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/types"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	gomstoolkit_cache "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/cache"
)

type PostOperations struct {
	PostRepository     repository.PostRepository
	CategoryRepository repository.CategoryRepository
	Cache              gomstoolkit_cache.Cache
	RemoteServices     *gomstoolkit.RemoteServices
}

func CheckAndSetLimit(limit *uint32) {
	if *limit <= 0 {
		*limit = 10
	}

	if *limit > 100 {
		*limit = 100
	}
}

func (uc *PostOperations) GetPosts(baseCtx context.Context, filter types.PostFilter) ([]types.Post, error) {
	CheckAndSetLimit(&filter.Limit)

	posts, err := uc.PostRepository.Get(baseCtx, filter)
	if err != nil {
		return nil, err
	}

	remoteService := uc.RemoteServices.Get(types.AUTHOR_SVC_ID)

	err = FetchAndMapAuthors(baseCtx, remoteService.GetUrlAuto(), posts, uc.Cache)
	if err != nil {
		return nil, err
	}

	return posts, nil
}

func (uc *PostOperations) GetPostById(baseCtx context.Context, id uuid.UUID) (types.Post, error) {
	post, err := uc.PostRepository.GetById(baseCtx, id)
	if err != nil {
		return types.Post{}, err
	}

	remoteService := uc.RemoteServices.Get(types.AUTHOR_SVC_ID)

	posts := []types.Post{post}
	err = FetchAndMapAuthors(baseCtx, remoteService.GetUrlAuto(), posts, uc.Cache)
	if err != nil {
		return types.Post{}, err
	}

	return posts[0], nil
}

func (uc *PostOperations) DeletePost(baseCtx context.Context, id uuid.UUID, userId uuid.UUID) error {
	return uc.PostRepository.Delete(baseCtx, id, userId)
}

func (uc *PostOperations) CreatePost(baseCtx context.Context, post types.Post) (uuid.UUID, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return uuid.Nil, fmt.Errorf("failed to create a uuid for the new post: %w", err)
	}

	post.Id = id

	pId, err := uc.PostRepository.Create(baseCtx, post)
	if err != nil {
		return uuid.Nil, err
	}

	return pId, nil
}

func (uc *PostOperations) UpdatePost(baseCtx context.Context, id uuid.UUID, userId uuid.UUID, post types.Post) error {
	post.Id = id
	return uc.PostRepository.Update(baseCtx, post, userId)
}

func (uc *PostOperations) GetCategories(baseCtx context.Context) ([]string, error) {
	return uc.CategoryRepository.Get(baseCtx)
}
