package repository

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/post/types"
)

var ErrNotFound = fmt.Errorf("Not found")

type PostRepository interface {
	Get(baseCtx context.Context, filter types.PostFilter) ([]types.Post, error)
	GetById(baseCtx context.Context, id uuid.UUID) (types.Post, error)
	Create(baseCtx context.Context, post types.Post) (uuid.UUID, error)
	Delete(baseCtx context.Context, id uuid.UUID, userId uuid.UUID) error
	Update(baseCtx context.Context, post types.Post, userId uuid.UUID) error
}
