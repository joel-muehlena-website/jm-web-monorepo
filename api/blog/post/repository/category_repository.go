package repository

import "context"

type CategoryRepository interface {
	Get(baseCtx context.Context) ([]string, error)
}
