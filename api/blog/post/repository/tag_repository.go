package repository

import (
	"context"

	"github.com/google/uuid"
)

type TagRepository interface {
	Create(baseCtx context.Context, postId uuid.UUID, tags []string) error
	Update(baseCtx context.Context, postId uuid.UUID, tags []string) error
	Get(baseCtx context.Context) ([]string, error)
	DynamicGet(baseCtx context.Context, value string) ([]string, error)
}
