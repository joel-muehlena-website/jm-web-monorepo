-- name: GetAuthorById :one
SELECT
  a.id,
  a.alias,
  a.biography,
  a.createdat,
  a.updatedat,
  a.userid,
  a.website,
  COALESCE(json_agg(json_build_object('name', t.name, 'value', t.value)) FILTER (WHERE t.name IS NOT NULL), '[]')::json AS socials
FROM
  author a
  LEFT JOIN author_social t ON a.id = t.authorid
WHERE id = $1
GROUP BY a.id, a.alias, a.biography, a.userid, a.website, a.createdat, a.updatedat
ORDER BY  a.id
LIMIT 1;

-- name: GetAuthorByUserId :one
SELECT
  a.id,
  a.alias,
  a.biography,
  a.createdat,
  a.updatedat,
  a.userid,
  a.website,
  COALESCE(json_agg(json_build_object('name', t.name, 'value', t.value)) FILTER (WHERE t.name IS NOT NULL), '[]')::json AS socials
FROM
  author a
  LEFT JOIN author_social t ON a.id = t.authorid
WHERE a.userID = $1
GROUP BY a.id, a.alias, a.biography, a.userid, a.website, a.createdat, a.updatedat
ORDER BY  a.id
LIMIT 1;


-- name: ListAuthors :many
SELECT
  a.id,
  a.alias,
  a.biography,
  a.createdat,
  a.updatedat,
  a.userid,
  a.website,
  COALESCE(json_agg(json_build_object('name', t.name, 'value', t.value)) FILTER (WHERE t.name IS NOT NULL), '[]')::json AS socials
FROM
  author a
  LEFT JOIN author_social t ON a.id = t.authorid
GROUP BY a.id, a.alias, a.biography, a.userid, a.website, a.createdat, a.updatedat
ORDER BY  a.id
LIMIT $1 OFFSET $2;

-- name: ListAuthorsLikeAlias :many
SELECT
  a.id,
  a.alias,
  a.biography,
  a.createdat,
  a.updatedat,
  a.userid,
  a.website,
  COALESCE(json_agg(json_build_object('name', t.name, 'value', t.value)) FILTER (WHERE t.name IS NOT NULL), '[]')::json AS socials
FROM
  author a
  LEFT JOIN author_social t ON a.id = t.authorid
WHERE a.alias LIKE '%' || sqlc.arg('alias')::text || '%'
GROUP BY a.id, a.alias, a.biography, a.userid, a.website, a.createdat, a.updatedat
ORDER BY  a.id
LIMIT $1 OFFSET $2;

-- name: ListAuthorsByMultipleIDs :many
SELECT
  a.id,
  a.alias,
  a.biography,
  a.createdat,
  a.updatedat,
  a.userid,
  a.website,
  COALESCE(json_agg(json_build_object('name', t.name, 'value', t.value)) FILTER (WHERE t.name IS NOT NULL), '[]')::json AS socials
FROM
  author a
  LEFT JOIN author_social t ON a.id = t.authorid
WHERE a.id = ANY (sqlc.arg('ids')::uuid[])
GROUP BY a.id, a.alias, a.biography, a.userid, a.website, a.createdat, a.updatedat
ORDER BY  a.id
LIMIT $1 OFFSET $2;

-- name: DeleteAuthorByID :exec
DELETE FROM author WHERE id = $1;

-- name: CreateNewAuthor :exec
INSERT INTO author (id, alias, biography, website, userid) VALUES ($1, $2, $3, $4, $5);

-- name: UpdateAuthor :exec
UPDATE author SET
  alias = (CASE WHEN @alias IS NOT NULL AND @alias != '' THEN @alias ELSE alias END),
  biography = COALESCE(sqlc.narg('biography'), biography),
  website = COALESCE(sqlc.narg('website'), website)
WHERE id = $1;