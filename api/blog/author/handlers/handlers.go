package handlers

import (
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
)

type AuthorHandler struct {
	AuthorService    *types.AuthorService
	AuthorRepository repository.AuthorRepository
}

func GetHandlers(authorSvc *types.AuthorService, repo repository.AuthorRepository) []gomstoolkit.Hitable {
	services := make([]gomstoolkit.Hitable, 0)

	handler := AuthorHandler{
		AuthorService:    authorSvc,
		AuthorRepository: repo,
	}

	getAuthorEndpoint := gomstoolkit.Endpoint[getAuthorRequest, getAuthorResponse]{
		Path:    "/author",
		Method:  gomstoolkit.GET,
		Handler: handler.getAuthor,
	}

	getAuthorByIdEndpoint := gomstoolkit.Endpoint[getAuthorByIdRequest, getAuthorByIdResponse]{
		Path:              "/author/:id",
		Method:            gomstoolkit.GET,
		HandlerController: handler.getAuthorDataById,
	}

	createAuthorEndpoint := gomstoolkit.Endpoint[createAuthorRequest, createAuthorResponse]{
		Path:                "/author",
		Method:              gomstoolkit.POST,
		HandlerController:   handler.createAuthor,
		RestrictContentType: []string{"application/json"},
		AuthorizationChain:  []gomstoolkit.AuthorizationHandler{middleware.IsAuthenticated, middleware.IsPermittedForRole("jm_blog::BlogAuthor::Create::Allow")},
	}

	updateAuthorEndpoint := gomstoolkit.Endpoint[updateAuthorRequest, updateAuthorResponse]{
		Path:                "/author/:id",
		Method:              gomstoolkit.PATCH,
		Handler:             handler.updateAuthor,
		RestrictContentType: []string{"application/json"},
		AuthorizationChain:  []gomstoolkit.AuthorizationHandler{middleware.IsAuthenticated, middleware.Or(true, middleware.IsPermittedForRole("jm_blog::BlogAuthor::Create::Allow"), middleware.ShouldBeSelf("authorID"))},
	}

	deleteAuthorEndpoint := gomstoolkit.Endpoint[deleteAuthorRequest, deleteAuthorResponse]{
		Path:               "/author/:id",
		Method:             gomstoolkit.DELETE,
		Handler:            handler.deleteAuthor,
		AuthorizationChain: []gomstoolkit.AuthorizationHandler{middleware.IsAuthenticated, middleware.Or(true, middleware.IsPermittedForRole("jm_blog::BlogAuthor::Delete::Allow"), middleware.ShouldBeSelf("authorID"))},
	}

	// Public
	services = append(services, &getAuthorEndpoint)
	// Public
	services = append(services, &getAuthorByIdEndpoint)
	// Auth, role: jm-blog:author:create
	services = append(services, &createAuthorEndpoint)
	// Auth, self -> userID match
	services = append(services, &updateAuthorEndpoint)
	// Auth, self -> userID match || role jm-blog:author:delete
	services = append(services, &deleteAuthorEndpoint)

	return services
}
