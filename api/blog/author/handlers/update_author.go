package handlers

import (
	"context"
	"errors"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgconn"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type updateAuthorRequest struct {
	Id     string       `uri:"string"`
	Author types.Author `json:"author"`
}

type updateAuthorResponse struct {
	Msg string    `json:"msg"`
	Id  uuid.UUID `json:"id"`
}

// Note you must use this handler with json to edit or delete any socials otherwise they will be
// in creation mode and no operation is performed
func (authorHandler *AuthorHandler) updateAuthor(baseCtx context.Context, req *updateAuthorRequest) (*updateAuthorResponse, int, error) {
	id, err := uuid.Parse(req.Id)
	if err != nil {
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid id passed", Errors: []string{err.Error()}}
	}

	// Parse RAW passed socials via form to Socials type
	if req.Author.SocialsRaw != nil && len(req.Author.SocialsRaw) > 0 {
		parsedSocials, err := usecase.ParseSocials(req.Author.SocialsRaw)
		if err != nil {
			httpErr, ok := err.(*gomstoolkit.HTTPError)

			if !ok {
				return &updateAuthorResponse{}, 500, &gomstoolkit.HTTPError{Code: 500, Message: "Failed to parse socials", Errors: []string{err.Error()}}
			}

			return &updateAuthorResponse{}, httpErr.Code, httpErr
		}

		req.Author.Socials = parsedSocials
	}

	uc := usecase.AuthorsOperations{
		AuthorRepo:     authorHandler.AuthorRepository,
		RemoteServices: &authorHandler.AuthorService.Service.RemoteServices,
	}

	err = uc.UpdateAuthor(baseCtx, id, req.Author)
	if err != nil {
		var pgErr *pgconn.PgError

		if errors.As(err, &pgErr) {
			if pgErr.ConstraintName == "unique_alias" {
				return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "failed to update author because this alias is already taken", Errors: []string{err.Error(), pgErr.Detail}}
			}
		}

		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "internal error", Errors: []string{err.Error()}}
	}

	return &updateAuthorResponse{Msg: "Updated author", Id: id}, 200, nil
}
