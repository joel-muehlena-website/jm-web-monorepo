package handlers

import (
	"context"
	"errors"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgconn"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/use_case"

	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type createAuthorRequest struct {
	CommitOnSocialsFail bool `json:"commitOnSocialsFail" form:"commitOnSocialsFail"`
	types.Author
}

type createAuthorResponse struct {
	ID uuid.UUID `json:"id"`
}

func (authorHandler *AuthorHandler) createAuthor(baseCtx context.Context, controller gomstoolkit.RequestController, req *createAuthorRequest) (*createAuthorResponse, int, error) {
	errStr, err := gomstoolkit.Validate(req)
	if err != nil {
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "internal error", Errors: []string{err.Error()}}
	}

	if len(errStr) >= 1 {
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "failed to validate request", Errors: errStr}
	}

	// Parse RAW passed socials via form to Socials type
	if req.Author.SocialsRaw != nil && len(req.Author.SocialsRaw) > 0 {
		parsedSocials, err := usecase.ParseSocials(req.Author.SocialsRaw)
		if err != nil {
			httpErr, ok := err.(*gomstoolkit.HTTPError)

			if !ok {
				return &createAuthorResponse{}, 500, &gomstoolkit.HTTPError{Code: 500, Message: "Failed to parse socials", Errors: []string{err.Error()}}
			}

			return &createAuthorResponse{}, httpErr.Code, httpErr
		}

		req.Author.Socials = parsedSocials
	}

	uc := usecase.AuthorsOperations{
		AuthorRepo:     authorHandler.AuthorRepository,
		RemoteServices: &authorHandler.AuthorService.Service.RemoteServices,
	}

	authorId, err := uc.CreateAuthor(baseCtx, req.Author)
	if err != nil {

		var pgErr *pgconn.PgError

		if errors.As(err, &pgErr) {
			if pgErr.ConstraintName == "unique_userid" {
				return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "failed to create author because there is already an author linked to this userid", Errors: []string{err.Error(), pgErr.Detail}}
			}

			if pgErr.ConstraintName == "unique_alias" {
				return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "failed to create author because this alias is already taken", Errors: []string{err.Error(), pgErr.Detail}}
			}
		}

		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to create author: internal error", Errors: []string{err.Error()}}
	}

	controller.Writer.Header().Set("Location", "/author/"+authorId.String())
	return &createAuthorResponse{ID: authorId}, 201, nil
}
