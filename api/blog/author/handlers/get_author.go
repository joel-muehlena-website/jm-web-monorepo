package handlers

import (
	"context"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type getAuthorRequest struct {
	Alias string `json:"alias" form:"alias"`
}

type getAuthorResponse struct {
	Authors []types.Author `json:"authors"`
}

func (authorHandler *AuthorHandler) getAuthor(baseCtx context.Context, req *getAuthorRequest) (*getAuthorResponse, int, error) {
	uc := usecase.AuthorsOperations{
		AuthorRepo:     authorHandler.AuthorRepository,
		RemoteServices: &authorHandler.AuthorService.Service.RemoteServices,
	}

	authors, err := uc.GetAuthors(baseCtx, req.Alias)

	if err == repository.ErrNotFound {
		return nil, 404, &gomstoolkit.HTTPError{Code: 404, Message: "no authors found", Errors: []string{err.Error()}}
	}

	if err != nil {
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to get authors: internal error", Errors: []string{err.Error()}}
	}

	return &getAuthorResponse{Authors: authors}, 200, nil
}
