package handlers

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/repository"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type deleteAuthorRequest struct {
	Id string `uri:"id"`
}

type deleteAuthorResponse struct {
	Msg string `json:"msg"`
	Id  string `json:"id"`
}

func (authorHandler *AuthorHandler) deleteAuthor(baseCtx context.Context, req *deleteAuthorRequest) (*deleteAuthorResponse, int, error) {
	id, err := uuid.Parse(req.Id)
	if err != nil {
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid id passed", Errors: []string{err.Error()}}
	}

	uc := usecase.AuthorsOperations{
		AuthorRepo:     authorHandler.AuthorRepository,
		RemoteServices: &authorHandler.AuthorService.Service.RemoteServices,
	}

	err = uc.DeleteAuthor(baseCtx, id)

	if err == repository.ErrNotFound {
		return nil, 404, &gomstoolkit.HTTPError{Code: 404, Message: "no author with this id found", Errors: []string{err.Error()}}
	}

	if err != nil {
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to delete author: internal error", Errors: []string{err.Error()}}
	}

	return &deleteAuthorResponse{Id: req.Id, Msg: "Successfully deleted author"}, 200, nil
}
