package handlers

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/use_case"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

type getAuthorByIdRequest struct {
	Id string `uri:"id"`

	// Valid types: authorById, authorByUserId, authorIdByUserId
	Type string `form:"type"`
}

type getAuthorByIdResponse struct {
	Author types.Author `json:"author"`
}

var validParamTypesGetById [3]string = [3]string{"authorById", "authorByUserId", "authorIdByUserId"}

func (authorHandler *AuthorHandler) getAuthorDataById(baseCtx context.Context, controller gomstoolkit.RequestController, req *getAuthorByIdRequest) (*getAuthorByIdResponse, int, error) {
	id, err := uuid.Parse(req.Id)
	if err != nil {
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid id passed", Errors: []string{err.Error()}}
	}

	uc := usecase.AuthorsOperations{
		AuthorRepo:     authorHandler.AuthorRepository,
		RemoteServices: &authorHandler.AuthorService.Service.RemoteServices,
	}

	if req.Type == "" {
		req.Type = "authorById"
	}

	var author types.Author

	switch req.Type {
	case "authorById":
		author, err = uc.GetAuthorById(baseCtx, id)
	case "authorByUserId":
		author, err = uc.GetAuthorByUserId(baseCtx, id)
		if author.Id == uuid.Nil {
			err = repository.ErrNotFound
		}
	case "authorIdByUserId":
		authorID, localerr := uc.GetAuthorIdByUserId(baseCtx, id)
		err = localerr
		if authorID == uuid.Nil {
			err = repository.ErrNotFound
		}
		author.Id = authorID
	default:
		return nil, 400, &gomstoolkit.HTTPError{Code: 400, Message: "invalid type passed", Errors: []string{fmt.Sprintf("invalid type passed: %s (allowed are: %v)", req.Type, validParamTypesGetById)}}
	}

	if err == repository.ErrNotFound {
		return nil, 404, &gomstoolkit.HTTPError{Code: 404, Message: "no author with this id or user id found", Errors: []string{err.Error()}}
	}

	if err != nil {
		return nil, 500, &gomstoolkit.HTTPError{Code: 500, Message: "failed to get author: internal error", Errors: []string{err.Error()}}
	}

	return &getAuthorByIdResponse{Author: author}, 200, nil
}
