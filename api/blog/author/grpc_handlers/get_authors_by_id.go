package grpc_handlers

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/proto"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/use_case"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
)

func (s *AuthorGrpcServer) GetAuthorsById(ctx context.Context, authorIDs *pb.GetAuthorsByIdRequest) (*pb.AuthorResultList, error) {
	// TODO: GRPC_AUTH This is stupid and should be handled by some kind of correct middleware
	ctx = middleware.SetProto(ctx, "grpc")
	ctx = middleware.SkipProto(ctx, "grpc")

	ids := make([]uuid.UUID, len(authorIDs.GetUUIDs()))

	if len(ids) == 0 {
		return nil, fmt.Errorf("please pass at least one id to fetch")
	}

	for i, id := range authorIDs.GetUUIDs() {
		uid, err := uuid.Parse(id)
		if err != nil {
			return nil, fmt.Errorf("unparsable id passed: %w", err)
		}

		ids[i] = uid
	}

	uc := usecase.AuthorsOperations{
		AuthorRepo:     s.authorRepo,
		RemoteServices: &s.authorSvc.Service.RemoteServices,
	}

	authors, err := uc.GetAuthorsById(ctx, ids)
	if err != nil {
		return nil, fmt.Errorf("failed to fetch authors: %w", err)
	}

	authorsProto := make([]*pb.AuthorType, len(authors))
	for i, author := range authors {
		authorsProto[i] = types.NativeToProtoAuthor(&author)
	}

	result := &pb.AuthorResultList{
		Authors: authorsProto,
	}

	return result, nil
}
