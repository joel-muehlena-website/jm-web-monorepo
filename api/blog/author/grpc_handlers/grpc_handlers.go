package grpc_handlers

import (
	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/proto"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	gomstoolkit_cache "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/cache"
)

type AuthorGrpcServer struct {
	authorRepo repository.AuthorRepository
	authorSvc  *types.AuthorService
	cache      gomstoolkit_cache.Cache
	pb.UnimplementedAuthorServer
}

func NewGrpcHandler(authorSvc *types.AuthorService, cache gomstoolkit_cache.Cache, repo repository.AuthorRepository) *AuthorGrpcServer {
	uSvr := new(AuthorGrpcServer)
	uSvr.authorSvc = authorSvc
	uSvr.authorRepo = repo
	uSvr.cache = cache
	return uSvr
}
