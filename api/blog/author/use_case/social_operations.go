package usecase

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
)

type SocialOperations struct {
	SocialRepository repository.AuthorSocials
}

func (so *SocialOperations) Operate(baseCtx context.Context, authorID uuid.UUID, socials []types.AuthorSocial) error {
	if socials == nil || len(socials) == 0 {
		return nil
	}

	toDelete := make([]types.AuthorSocial, 0)
	toCreate := make([]types.AuthorSocial, 0)
	toUpdate := make([]types.AuthorSocial, 0)

	for _, s := range socials {
		social := s

		switch social.Modifier {
		case types.CREATE_SOCIAL:
			toCreate = append(toCreate, social)
		case types.DELETE_SOCIAL:
			toDelete = append(toDelete, social)
		case types.EDIT_SOCIAL:
			toUpdate = append(toUpdate, social)
		}
	}

	err := so.SocialRepository.Create(baseCtx, authorID, toCreate)
	if err != nil {
		return err
	}

	err = so.SocialRepository.Update(baseCtx, authorID, toUpdate)
	if err != nil {
		return err
	}

	err = so.SocialRepository.Delete(baseCtx, authorID, toDelete)
	if err != nil {
		return err
	}

	return nil
}

// Insert Socials into the database using provided [pgx.Tx]
// to control the rollback/commit flow from the calling function
//
// Returns true if something was inserted, false otherwise.
// If false is returned there is maybe an error
func insertSocials(baseCtx context.Context, tx pgx.Tx, authorID int, socials []types.AuthorSocial, allowFailure bool) (bool, error) {
	insertValues := make([]interface{}, 0)

	cnt := 1
	valueStr := ""
	for _, pair := range socials {
		insertValues = append(insertValues, authorID, pair.Name, pair.Value)

		if valueStr != "" {
			valueStr += ","
		}

		valueStr += fmt.Sprintf("($%d, $%d, $%d)", cnt, cnt+1, cnt+2)

		cnt += 3
	}

	if cnt == 1 {
		return false, nil
	}

	conflictVal := ""

	if allowFailure == true {
		conflictVal = " ON CONFLICT DO NOTHING"
	}

	insertSQL := fmt.Sprintf(`INSERT INTO author_social (authorID, name, value) VALUES %s RETURNING name, value%s`, valueStr, conflictVal)

	ctx, cancel := context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
	rows, err := tx.Query(ctx, insertSQL, insertValues...)
	defer cancel()

	if err != nil {
		return false, err
	}

	rows.Close()

	return true, nil
}

// Delete Socials from the database using provided [pgx.Tx]
// to control the rollback/commit flow from the calling function
//
// Returns true if something was removed, false otherwise.
// If false is returned there is maybe an error
func deleteSocials(baseCtx context.Context, tx pgx.Tx, authorID int, socials []types.AuthorSocial) (bool, error) {
	deleted := false

	deleteValues := make([]interface{}, 1)
	deleteValues[0] = authorID

	cnt := 2
	valueStr := "("
	for _, pair := range socials {
		deleteValues = append(deleteValues, pair.Name)

		if valueStr != "(" {
			valueStr += ","
		}

		valueStr += fmt.Sprintf("$%d", cnt)

		cnt++
	}

	valueStr += ")"

	if cnt == 2 {
		return false, nil
	}

	deleteSQL := fmt.Sprintf(`DELETE FROM author_social WHERE authorId = $1 AND name IN %s RETURNING name`, valueStr)

	ctx, cancel := context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
	rows, err := tx.Query(ctx, deleteSQL, deleteValues...)
	defer cancel()

	if err != nil {
		return false, err
	}

	if rows.Next() {
		deleted = true
	}

	rows.Close()

	return deleted, nil
}

// Update Socials in the database using provided [pgx.Tx]
// to control the rollback/commit flow from the calling function.
// Note that the authorId cannot be changed
//
// Returns true if something was updated, false otherwise.
// If false is returned there is maybe an error
func updateSocials(baseCtx context.Context, tx pgx.Tx, authorID int, socials []types.AuthorSocial) (bool, error) {
	inserted := false

	for _, social := range socials {

		if social.Name == "" {
			return false, fmt.Errorf("no social name provided but required")
		}

		setValue := ""
		params := make([]interface{}, 0)
		cnt := 1

		if social.NewName != "" {
			setValue += fmt.Sprintf("name = $%d", cnt)
			params = append(params, social.NewName)
			cnt++
		}

		if social.Value != "" {
			setValue += fmt.Sprintf("value = $%d", cnt)
			params = append(params, social.Value)
			cnt++
		}

		if cnt == 1 {
			continue
		}

		params = append(params, authorID, social.Name)

		sqlUpdate := fmt.Sprintf(`UPDATE author_social SET %s WHERE authorid = $%d AND name = $%d RETURNING name`, setValue, cnt, cnt+1)

		ctx, cancel := context.WithTimeout(baseCtx, db.DB_CTX_DEFAULT_TIMEOUT)
		rows, err := tx.Query(ctx, sqlUpdate, params...)
		defer cancel()

		if err != nil {
			return false, err
		}

		if rows.Next() {
			inserted = true
		}

		rows.Close()
	}

	return inserted, nil
}
