package usecase

import (
	"fmt"
	"net/url"
	"strings"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

// Parses the raw value of the Author struct socials into the desired [][types.AuthorSocial] type
//
// The returned error must be a [gomstoolkit.HTTPError]
func ParseSocials(raw []string) ([]types.AuthorSocial, error) {
	result := make([]types.AuthorSocial, 0)

	for _, social := range raw {
		data := strings.Split(social, "=")

		if len(data) != 2 {
			return nil, &gomstoolkit.HTTPError{
				Code:    400,
				Message: "Unexpected length of socials pair",
				Errors:  []string{fmt.Sprintf("Got length of %d but expected 2. Maybe there is an unencoded '=' ", len(data))},
			}
		}

		name, _ := url.QueryUnescape(data[0])
		value, _ := url.QueryUnescape(data[1])

		result = append(result, types.AuthorSocial{
			Name:  name,
			Value: value,
		})
	}

	return result, nil
}
