package usecase

import (
	"context"
	"errors"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/google/uuid"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-middleware/providers/prometheus"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	user_pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/proto"
	user_types "gitlab.com/joelMuehlena/jm-web-monorepo/api/user/types"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/telemetry"
)

func GetUsersForAuthors(baseCtx context.Context, serverUrl string, userIdList []uuid.UUID) ([]user_types.PublicUser, error) {
	metrics := grpc_prometheus.NewClientMetrics(grpc_prometheus.WithClientHandlingTimeHistogram())

	var opts []grpc.DialOption

	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	opts = append(opts, telemetry.GetGRPCClientHandler(), grpc.WithUnaryInterceptor(metrics.UnaryClientInterceptor()), grpc.WithStreamInterceptor(metrics.StreamClientInterceptor()))
	conn, err := grpc.Dial(serverUrl, opts...)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	client := user_pb.NewUserClient(conn)

	ctx, cancel := context.WithCancel(baseCtx)
	defer cancel()
	stream, err := client.GetUsersById(ctx)
	if err != nil {
		return nil, err
	}

	var result []user_types.PublicUser

	closeErr := make(chan error, 0)

	// send
	go func() {
		for _, id := range userIdList {
			zap.L().Debug("Requesting user", zap.Any("id", id))
			stream.Send(&user_pb.GetUserByIdRequest{UUID: id.String()})
		}
		err := stream.CloseSend()
		if err != nil {
			closeErr <- err
			return
		}
		closeErr <- nil
	}()

	// recv
	for {
		in, err := stream.Recv()

		if err == io.EOF {
			stream.CloseSend()
			break
		}

		if err != nil {
			stream.CloseSend()
			zap.L().Error("grpc error", zap.Error(err))
			return nil, err
		}

		if in.Errors != nil && len(in.Errors) > 0 {
			zap.L().Debug("user by id in.Errors", zap.Strings("errors", in.Errors))
			continue
		}

		zap.L().Debug("received user", zap.Any("in.User", in.User))

		result = append(result, *user_types.ProtoToNativePublicUser(in.User))
	}

	err = <-closeErr
	close(closeErr)

	if err != nil {
		zap.L().Error("Error closing grpc connection", zap.Error(err))
		return result, fmt.Errorf("grpc error: %w", err)
	}

	return result, nil
}

func MapUsersToAuthors(authors []types.Author, users []user_types.PublicUser) {
	for i, author := range authors {
		for _, user := range users {
			if author.UserID == user.Id {
				authors[i].FirstName = user.FirstName
				authors[i].LastName = user.LastName
				authors[i].Avatar = *user.Avatar
			}
		}
	}
}

func IsUserExisting(baseCtx context.Context, serverUrl string, userId uuid.UUID, pathSegments ...string) (bool, error) {
	type noop struct{}

	path := "v2/user"

	if len(pathSegments) > 0 {
		path = strings.Join(pathSegments, "/")
	}

	userServiceURL := fmt.Sprintf("%s/%s/%s", serverUrl, path, userId.String())

	ctx, cancel := context.WithTimeout(context.Background(), 7*time.Second)
	defer cancel()

	_, fRes, err := gomstoolkit.Fetch[noop](ctx, userServiceURL, nil)
	if err != nil && !errors.Is(err, gomstoolkit.ErrRequestNon200) {
		return false, err
	}

	return fRes.StatusCode == 200, nil
}
