package usecase

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"go.opentelemetry.io/otel"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/repository"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/utility"
)

type AuthorsOperations struct {
	AuthorRepo     repository.AuthorRepository
	RemoteServices *gomstoolkit.RemoteServices
}

func (uc *AuthorsOperations) GetAuthors(baseCtx context.Context, alias string) ([]types.Author, error) {
	ctx, span := otel.Tracer("").Start(baseCtx, "GetAuthors UseCase")
	defer span.End()

	var authors []types.Author
	var err error

	if alias != "" {
		authors, err = uc.AuthorRepo.GetByAlias(ctx, alias)
	} else {
		authors, err = uc.AuthorRepo.Get(ctx)
	}

	if err != nil {
		return nil, err
	}

	err = uc.GetAndMapUsersForAuthors(ctx, authors)

	if err != nil {
		return nil, err
	}

	FillNilFields(authors)

	return authors, nil
}

func (uc *AuthorsOperations) GetAuthorsById(baseCtx context.Context, ids []uuid.UUID) ([]types.Author, error) {
	authors, err := uc.AuthorRepo.GetByIds(baseCtx, ids)
	if err != nil {
		return nil, err
	}

	err = uc.GetAndMapUsersForAuthors(baseCtx, authors)

	if err != nil {
		return nil, err
	}

	FillNilFields(authors)

	return authors, nil
}

func (uc *AuthorsOperations) GetAuthorById(baseCtx context.Context, id uuid.UUID) (types.Author, error) {
	author, err := uc.AuthorRepo.GetById(baseCtx, id)
	if err != nil {
		return types.Author{}, err
	}
	authors := []types.Author{author}

	err = uc.GetAndMapUsersForAuthors(baseCtx, authors)

	if err != nil {
		return types.Author{}, err
	}

	FillNilFields(authors)

	return authors[0], nil
}

func (uc *AuthorsOperations) GetAuthorIdByUserId(baseCtx context.Context, id uuid.UUID) (uuid.UUID, error) {
	author, err := uc.AuthorRepo.GetByUserId(baseCtx, id)
	if err != nil {
		return uuid.Nil, err
	}

	return author.Id, nil
}

func (uc *AuthorsOperations) GetAuthorByUserId(baseCtx context.Context, id uuid.UUID) (types.Author, error) {
	author, err := uc.AuthorRepo.GetByUserId(baseCtx, id)
	if err != nil {
		return types.Author{}, err
	}
	authors := []types.Author{author}

	err = uc.GetAndMapUsersForAuthors(baseCtx, authors)

	if err != nil {
		return types.Author{}, err
	}

	FillNilFields(authors)

	return authors[0], nil
}

func (uc *AuthorsOperations) DeleteAuthor(baseCtx context.Context, id uuid.UUID) error {
	return uc.AuthorRepo.Delete(baseCtx, id)
}

func (uc *AuthorsOperations) CreateAuthor(baseCtx context.Context, author types.Author) (uuid.UUID, error) {
	author.Id = uuid.New()

	userURL := uc.RemoteServices.Get(types.USER_SVC_ID).GetUrlAuto()

	_, isProd := gomstoolkit.GetCurrentEnv("")
	path := []string{}

	if !isProd {
		path = append(path, "user")
	}

	exist, err := IsUserExisting(baseCtx, userURL, author.UserID, path...)

	if !exist || err != nil {
		zap.L().Error("Failed to check if user exists or user is not existing", zap.Error(err))
		return uuid.Nil, fmt.Errorf("failed to get existing user or user does not exist")
	}

	authorId, err := uc.AuthorRepo.Create(baseCtx, author)
	if err != nil {
		return uuid.Nil, err
	}

	return authorId, nil
}

func (uc *AuthorsOperations) UpdateAuthor(baseCtx context.Context, id uuid.UUID, author types.Author) error {
	author.Id = id
	return uc.AuthorRepo.Update(baseCtx, author)
}

func (uc *AuthorsOperations) GetAndMapUsersForAuthors(baseCtx context.Context, authors []types.Author) error {
	ctx, span := otel.Tracer("").Start(baseCtx, "GetAndMapUsersForAuthors")
	defer span.End()
	remoteCfg := uc.RemoteServices.Get(types.USER_SVC_ID_GRPC)

	if remoteCfg == nil {
		return fmt.Errorf("failed to get remote config for %s", types.USER_SVC_ID_GRPC)
	}

	userIDs := make([]uuid.UUID, len(authors))
	for i, author := range authors {
		userIDs[i] = author.UserID
	}

	users, err := GetUsersForAuthors(ctx, remoteCfg.GetUrlAuto(), userIDs)
	if err != nil {
		return fmt.Errorf("failed to query user service to get author names: %w", err)
	}

	MapUsersToAuthors(authors, users)

	return nil
}

func FillNilFields(authors []types.Author) {
	for i, author := range authors {
		if author.Biography == nil {
			authors[i].Biography = utility.NewPtrVal("")
		}

		if author.Website == nil {
			authors[i].Website = utility.NewPtrVal("")
		}

		if author.Socials == nil {
			authors[i].Socials = make([]types.AuthorSocial, 0)
		}
	}
}
