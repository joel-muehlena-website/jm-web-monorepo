package repository

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
)

var ErrNotFound = fmt.Errorf("Not found")

type AuthorRepository interface {
	Get(baseCtx context.Context) ([]types.Author, error)
	GetByAlias(baseCtx context.Context, alias string) ([]types.Author, error)
	GetById(baseCtx context.Context, id uuid.UUID) (types.Author, error)
	GetByUserId(baseCtx context.Context, id uuid.UUID) (types.Author, error)
	GetByIds(baseCtx context.Context, ids []uuid.UUID) ([]types.Author, error)
	Create(baseCtx context.Context, author types.Author) (uuid.UUID, error)
	Delete(baseCtx context.Context, id uuid.UUID) error
	Update(baseCtx context.Context, author types.Author) error
}
