package repository

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
)

type AuthorSocials interface {
	Update(baseCtx context.Context, authorID uuid.UUID, socials []types.AuthorSocial) error
	Create(baseCtx context.Context, authorID uuid.UUID, socials []types.AuthorSocial) error
	Delete(baseCtx context.Context, authorID uuid.UUID, socials []types.AuthorSocial) error
}
