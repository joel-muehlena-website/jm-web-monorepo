package main

import (
	"flag"
	"os"
	"time"

	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/handlers"
	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/proto"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/repository_impl"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	service "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	gomstoolkit_cache "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/cache"
	service_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
	gomstoolkit_util "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/util"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/utility"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/grpc_handlers"
)

func main() {
	configServerUrl := flag.String("config-server-url", types.CONFIG_SERVER_URL_DEV, "URL of the config server")
	configServerFilePath := flag.String("config-server-file-path", "blog/author.dev.yaml", "Path to the config file on the config server")
	useSvcEnv := flag.Bool("use-svc-env", true, "Use the JM_SVC_ENV environment variable")
	flag.Parse()

	authorSvc := types.AuthorService{}

	if *useSvcEnv && os.Getenv("JM_SVC_ENV") == "production" {
		*configServerUrl = types.CONFIG_SERVER_URL_PROD
		*configServerFilePath = "blog/author.prod.yaml"
	}

	result := gomstoolkit_util.FetchConfigServerConfig(*configServerUrl, *configServerFilePath)
	authorServiceConfig := utility.ParseConfig[types.AuthorServiceConfig](result)

	svc := service.New(service.WithServiceConfig(&authorServiceConfig.Service), service.WithLogger(&service.DefaultLoggerConfig))
	authorSvc.Service = svc

	pool := service_pg.NewPool(svc.Logger, authorServiceConfig.DBConfig)
	err := pool.Connect()
	if err != nil {
		svc.Logger.Fatal("Failed to connect to db", zap.Error(err))
	}
	defer pool.DBPool.Close()

	arp := repository_impl.NewAuthorRepositoryPostgres(pool)

	if httpRef, ok := svc.SubServiceCommander.Load("http"); ok {
		httpRef.SubService.(*service.Http).RegisterEndpoint(handlers.GetHandlers(&authorSvc, arp)...)
	} else {
		svc.Logger.Fatal("Failed to load http sub service")
	}

	cache := gomstoolkit_cache.NewBigCache(1 * time.Minute)
	err = cache.Init()
	if err != nil {
		svc.Logger.Fatal("Failed to init cache", zap.Error(err))
	}

	if grpcRef, ok := svc.SubServiceCommander.Load("grpc"); ok {
		grpcHandler := grpc_handlers.NewGrpcHandler(&authorSvc, cache, arp)
		service.RegisterGrpc[pb.AuthorServer](grpcRef.SubService.(*service.Grpc), pb.RegisterAuthorServer, grpcHandler)
	} else {
		svc.Logger.Fatal("Failed to load grpc sub service")
	}

	err = svc.Run()
	if err != nil {
		svc.Logger.Fatal("Failed to run author service", zap.Error(err))
	}

	svc.Logger.Info("Exiting author service application")
}
