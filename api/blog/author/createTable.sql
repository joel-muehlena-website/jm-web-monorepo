DROP TABLE IF EXISTS author_social;
DROP TABLE IF EXISTS author;

CREATE TABLE IF NOT EXISTS author (
    id uuid PRIMARY KEY NOT NULL,
    alias varchar(50) NOT NULL,
    biography text,
    website varchar,
    userID uuid NOT NULL,
    createdat timestamp,
    updatedat timestamp DEFAULT now()
);

CREATE TABLE IF NOT EXISTS author_social (
  authorID uuid NOT NULL,
  name varchar(255) NOT NULL,
  value text NOT NULL,
  PRIMARY KEY(authorID, name),
  CONSTRAINT author_fk FOREIGN KEY (authorID) REFERENCES author(id) ON DELETE CASCADE ON UPDATE RESTRICT
);

ALTER TABLE author ADD CONSTRAINT unique_alias UNIQUE(alias);
ALTER TABLE author ADD CONSTRAINT unique_userid UNIQUE(userID);

-- The following index creation was proposed by eversql
CREATE INDEX author_idx_id_alias_biogr_useri_websi_creat ON "author" ("id","alias","biography","userid","website","createdat");

DROP TRIGGER IF EXISTS preventUpdateCreatedAtForAuthorTrigger ON author;
DROP TRIGGER IF EXISTS setUpdatedAtOnUpdateForAuthorTrigger ON author;
DROP TRIGGER IF EXISTS setCreatedAtUpdatedAtForAuthorTrigger ON author;

CREATE TRIGGER preventUpdateCreatedAtForAuthorTrigger BEFORE UPDATE ON author
  FOR EACH ROW EXECUTE PROCEDURE preventUpdateCreatedAt();

CREATE TRIGGER setUpdatedAtOnUpdateForAuthorTrigger BEFORE UPDATE ON author
  FOR EACH ROW EXECUTE PROCEDURE setUpdatedAtOnUpdate();

CREATE TRIGGER setCreatedAtUpdatedAtForAuthorTrigger BEFORE INSERT ON author
  FOR EACH ROW EXECUTE PROCEDURE setCreatedAtAndUpdatedAt();