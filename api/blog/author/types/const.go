package types

const (
	CONFIG_SERVER_URL_DEV  = "http://localhost:8081/config"
	CONFIG_SERVER_URL_PROD = "http://config-server.jm-core.svc.cluster.local/config"
	PROD_USER_SVC_URL      = "http://user-service.jm-core.svc.cluster.local"
	DEV_USER_SVC_URL       = "http://localhost:3011"
	PROD_USER_SVC_URL_GRPC = "user-service.jm-core.svc.cluster.local:5555"
	DEV_USER_SVC_URL_GRPC  = "localhost:5555"

	USER_SVC_ID      = "user-service"
	USER_SVC_ID_GRPC = "user-service-grpc"
)
