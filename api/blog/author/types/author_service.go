package types

import (
	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	gomstoolkit_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
)

type AuthorService struct {
	Service *gomstoolkit.Service
}

type AuthorServiceConfig struct {
	Service  gomstoolkit.Config                  `yaml:"service"`
	DBConfig gomstoolkit_pg.PostgreSQLPoolConfig `yaml:"dbConfig"`
}
