package types

type AuthorSocialModifier int

const (
	CREATE_SOCIAL AuthorSocialModifier = iota
	EDIT_SOCIAL
	DELETE_SOCIAL

	// Can be set if nothing should be done with this social
	NOOP_SOCIAL
)

type AuthorSocial struct {
	Name  string `json:"name"`
	Value string `json:"value"`

	// Required if the modifier is EDIT_SOCIAL and the name should be updated
	NewName string `json:"newName,omitempty"`

	// Required to specify the requested operation on this social item
	// Defaults to create, but only on post, patch or put operations
	Modifier AuthorSocialModifier `json:"modifier,omitempty"`
}
