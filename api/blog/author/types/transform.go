package types

import (
	"google.golang.org/protobuf/types/known/timestamppb"

	"github.com/google/uuid"

	pb "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/proto"
)

func NativeToProtoAuthor(author *Author) *pb.AuthorType {
	return &pb.AuthorType{
		Id:        author.Id.String(),
		UserId:    author.UserID.String(),
		FirstName: author.FirstName,
		LastName:  author.LastName,
		Avatar:    author.Avatar,
		Alias:     author.Alias,
		Biography: *author.Biography,
		Website:   *author.Website,
		Socials:   NativeToProtoSocials(author.Socials),
		CreatedAt: timestamppb.New(author.CreatedAt),
		UpdatedAt: timestamppb.New(author.UpdatedAt),
	}
}

func NativeToProtoSocial(social *AuthorSocial) *pb.Social {
	return &pb.Social{
		Name:  social.Name,
		Value: social.Value,
	}
}

func NativePtrToProtoSocials(socials []*AuthorSocial) []*pb.Social {
	protoSocials := make([]*pb.Social, len(socials))

	for i, social := range socials {
		protoSocials[i] = &pb.Social{
			Name:  social.Name,
			Value: social.Value,
		}
	}

	return protoSocials
}

func NativeToProtoSocials(socials []AuthorSocial) []*pb.Social {
	protoSocials := make([]*pb.Social, len(socials))

	for i, social := range socials {
		protoSocials[i] = &pb.Social{
			Name:  social.Name,
			Value: social.Value,
		}
	}

	return protoSocials
}

func ProtoAuthorsToNatives(pAuthors []*pb.AuthorType) []Author {
	authors := make([]Author, len(pAuthors))

	for _, pAuthor := range pAuthors {
		author := ProtoAuthorToNative(pAuthor)
		authors = append(authors, *author)
	}

	return authors
}

func ProtoAuthorToNative(pAuthor *pb.AuthorType) *Author {
	return &Author{
		Id:        uuid.MustParse(pAuthor.Id),
		UserID:    uuid.MustParse(pAuthor.UserId),
		FirstName: pAuthor.FirstName,
		LastName:  pAuthor.LastName,
		Avatar:    pAuthor.Avatar,
		Alias:     pAuthor.Alias,
		Biography: &pAuthor.Biography,
		Website:   &pAuthor.Website,
		Socials:   ProtoSocialsToNative(pAuthor.Socials),
		CreatedAt: pAuthor.CreatedAt.AsTime(),
		UpdatedAt: pAuthor.UpdatedAt.AsTime(),
	}
}

func ProtoSocialToNative(pSocial *pb.Social) *AuthorSocial {
	return &AuthorSocial{
		Name:  pSocial.Name,
		Value: pSocial.Value,
	}
}

func ProtoSocialsToNativePtr(pSocials []*pb.Social) []*AuthorSocial {
	socials := make([]*AuthorSocial, len(pSocials))

	for i, pSocial := range pSocials {
		socials[i] = &AuthorSocial{
			Name:  pSocial.Name,
			Value: pSocial.Value,
		}
	}

	return socials
}

func ProtoSocialsToNative(pSocials []*pb.Social) []AuthorSocial {
	socials := make([]AuthorSocial, len(pSocials))

	for i, pSocial := range pSocials {
		socials[i] = AuthorSocial{
			Name:  pSocial.Name,
			Value: pSocial.Value,
		}
	}

	return socials
}
