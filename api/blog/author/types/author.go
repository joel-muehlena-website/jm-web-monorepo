package types

import (
	"time"

	"github.com/google/uuid"
)

type Author struct {
	Id        uuid.UUID      `json:"id"`
	UserID    uuid.UUID      `json:"userId" form:"userId" validate:"required"`
	FirstName string         `json:"firstName"`
	LastName  string         `json:"lastName"`
	Avatar    string         `json:"avatar"`
	Alias     string         `json:"alias" form:"alias" validate:"required,alphanum"`
	Biography *string        `json:"biography" form:"biography" validate:"omitempty,min=10"`
	Website   *string        `json:"website" form:"website" validate:"omitempty,url"`
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
	Socials   []AuthorSocial `json:"socials" form:"-"`

	// Used by forms because binding with "form"-arrays is difficult and stupid.
	// If this field is posted, Socials will be overwritten with those values.
	// Values must be passed as <key>=<value> pairs to be parsed correctly
	// Both parts of the pair should be url encoded
	// The raw part currently not supports modifiers for the socials.
	// So it's just possible to create new ones while creating an author or updating an author
	SocialsRaw []string `json:"-" form:"socials[]"`
}
