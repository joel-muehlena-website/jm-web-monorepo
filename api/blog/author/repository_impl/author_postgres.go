package repository_impl

import (
	"context"
	"encoding/json"
	"fmt"
	"math"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/repository_impl/pgx_gen"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	usecase "gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/use_case"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/log"

	gomstoolkit_db "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
	gomstoolkit_pg "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/middleware"
)

type authorRepositoryPostgres struct {
	DBConn     *gomstoolkit_pg.PostgreSQLPoolConnection
	db         *pgx_gen.Queries
	linkLogger *log.ZapTraceLinkLogger
}

func NewAuthorRepositoryPostgres(conn *gomstoolkit_pg.PostgreSQLPoolConnection) *authorRepositoryPostgres {
	repo := new(authorRepositoryPostgres)

	repo.DBConn = conn
	repo.db = pgx_gen.New(repo.DBConn.DBPool)
	repo.linkLogger = log.NewZapTraceLinkLogger(zap.L().Named("user-repo-pg"))

	return repo
}

func (arp *authorRepositoryPostgres) Get(baseCtx context.Context) ([]types.Author, error) {
	logger := arp.linkLogger.Ctx(baseCtx)

	rows, err := arp.db.ListAuthors(baseCtx, pgx_gen.ListAuthorsParams{
		Offset: 0,
		Limit:  math.MaxInt32,
	})
	if err != nil {
		return nil, err
	}

	transformed := make([]types.Author, 0, len(rows))
	for _, row := range rows {
		a := types.Author{
			Id:        row.ID,
			UserID:    row.Userid,
			Alias:     row.Alias,
			CreatedAt: row.Createdat.Time,
			UpdatedAt: row.Updatedat.Time,
			Biography: &row.Biography.String,
			Website:   &row.Website.String,
		}

		socials := make([]types.AuthorSocial, 0)
		err = json.Unmarshal(row.Socials, &socials)
		if err != nil {
			logger.Error("Failed to Unmarshal author social data", zap.Error(err), zap.String("raw", string(row.Socials)))
			continue
		}
		a.Socials = socials

		transformed = append(transformed, a)
	}

	return transformed, nil
}

func (arp *authorRepositoryPostgres) GetByAlias(baseCtx context.Context, alias string) ([]types.Author, error) {
	logger := arp.linkLogger.Ctx(baseCtx)

	rows, err := arp.db.ListAuthorsLikeAlias(baseCtx, pgx_gen.ListAuthorsLikeAliasParams{
		Alias:  alias,
		Offset: 0,
		Limit:  math.MaxInt32,
	})
	if err != nil {
		return nil, err
	}

	transformed := make([]types.Author, 0, len(rows))
	for _, row := range rows {
		a := types.Author{
			Id:        row.ID,
			UserID:    row.Userid,
			Alias:     row.Alias,
			CreatedAt: row.Createdat.Time,
			UpdatedAt: row.Updatedat.Time,
			Biography: &row.Biography.String,
			Website:   &row.Website.String,
		}

		socials := make([]types.AuthorSocial, 0)
		err = json.Unmarshal(row.Socials, &socials)
		if err != nil {
			logger.Error("Failed to Unmarshal author social data", zap.Error(err), zap.String("raw", string(row.Socials)))
			continue
		}
		a.Socials = socials

		transformed = append(transformed, a)
	}

	return transformed, nil
}

func (arp *authorRepositoryPostgres) GetById(baseCtx context.Context, id uuid.UUID) (types.Author, error) {
	logger := arp.linkLogger.Ctx(baseCtx)

	row, err := arp.db.GetAuthorById(baseCtx, id)
	if err != nil {
		return types.Author{}, nil
	}

	a := types.Author{
		Id:        row.ID,
		UserID:    row.Userid,
		Alias:     row.Alias,
		CreatedAt: row.Createdat.Time,
		UpdatedAt: row.Updatedat.Time,
		Biography: &row.Biography.String,
		Website:   &row.Website.String,
	}

	socials := make([]types.AuthorSocial, 0)
	err = json.Unmarshal(row.Socials, &socials)
	if err != nil {
		logger.Error("Failed to Unmarshal author social data", zap.Error(err), zap.String("raw", string(row.Socials)))
		return types.Author{}, err
	}
	a.Socials = socials

	return a, nil
}

func (arp *authorRepositoryPostgres) GetByUserId(baseCtx context.Context, id uuid.UUID) (types.Author, error) {
	logger := arp.linkLogger.Ctx(baseCtx)

	row, err := arp.db.GetAuthorByUserId(baseCtx, id)
	if err != nil {
		return types.Author{}, nil
	}

	a := types.Author{
		Id:        row.ID,
		UserID:    row.Userid,
		Alias:     row.Alias,
		CreatedAt: row.Createdat.Time,
		UpdatedAt: row.Updatedat.Time,
		Biography: &row.Biography.String,
		Website:   &row.Website.String,
	}

	socials := make([]types.AuthorSocial, 0)
	err = json.Unmarshal(row.Socials, &socials)
	if err != nil {
		logger.Error("Failed to Unmarshal author social data", zap.Error(err), zap.String("raw", string(row.Socials)))
		return types.Author{}, err
	}
	a.Socials = socials

	return a, nil
}

func (arp *authorRepositoryPostgres) GetByIds(baseCtx context.Context, ids []uuid.UUID) ([]types.Author, error) {
	logger := arp.linkLogger.Ctx(baseCtx)

	if ids == nil {
		return nil, fmt.Errorf("Passed nil id array")
	}

	rows, err := arp.db.ListAuthorsByMultipleIDs(baseCtx, pgx_gen.ListAuthorsByMultipleIDsParams{
		Offset: 0,
		Limit:  math.MaxInt32,
		Ids:    ids,
	})
	if err != nil {
		return nil, err
	}

	transformed := make([]types.Author, 0, len(rows))
	for _, row := range rows {
		a := types.Author{
			Id:        row.ID,
			UserID:    row.Userid,
			Alias:     row.Alias,
			CreatedAt: row.Createdat.Time,
			UpdatedAt: row.Updatedat.Time,
			Biography: &row.Biography.String,
			Website:   &row.Website.String,
		}

		socials := make([]types.AuthorSocial, 0)
		err = json.Unmarshal(row.Socials, &socials)
		if err != nil {
			logger.Error("Failed to Unmarshal author social data", zap.Error(err), zap.String("raw", string(row.Socials)))
			continue
		}
		a.Socials = socials

		transformed = append(transformed, a)
	}

	return transformed, nil
}

func (arp *authorRepositoryPostgres) Delete(baseCtx context.Context, id uuid.UUID) error {
	logger := arp.linkLogger.Ctx(baseCtx)

	if baseCtx.Value(middleware.UserShouldBeSelfKeyEndpoint) == nil && baseCtx.Value(middleware.UserPermissionsKeyEndpoint) == nil {
		return fmt.Errorf("no permissions provided to delete author")
	}

	checkSelf := false
	uid := uuid.UUID{}
	aid := uuid.UUID{}
	if baseCtx.Value(middleware.UserShouldBeSelfKeyEndpoint) != nil && baseCtx.Value(middleware.UserPermissionsKeyEndpoint) == nil {
		checkSelf = true
		uid = middleware.GetUserIdFromCtx(baseCtx)

		// TODO: smart up solution of user <-> author id mapping
		author, err := arp.GetByUserId(baseCtx, uid)
		if err != nil {
			return fmt.Errorf("failed to fetch author for logged in user id: %w", err)
		}

		aid = author.Id
	}
	logger.Debug("Should check self", zap.Bool("checkSelf", checkSelf), zap.String("uid", uid.String()), zap.String("aid", aid.String()))

	if checkSelf {
		if id != aid {
			return fmt.Errorf("without elevated permissions you can only delete yourself")
		}
	}

	return arp.db.DeleteAuthorByID(baseCtx, id)
}

func (arp *authorRepositoryPostgres) Create(baseCtx context.Context, author types.Author) (uuid.UUID, error) {
	tx, err := arp.DBConn.DBPool.BeginTx(baseCtx, pgx.TxOptions{})
	if err != nil {
		return uuid.Nil, err
	}

	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	defer cancel()
	defer gomstoolkit_pg.TxRollback(ctx, tx)

	db := arp.db.WithTx(tx)

	params := pgx_gen.CreateNewAuthorParams{
		ID:     author.Id,
		Alias:  author.Alias,
		Userid: author.UserID,
	}

	params.Biography.Scan(author.Biography)
	params.Website.Scan(author.Website)

	err = db.CreateNewAuthor(baseCtx, params)
	if err != nil {
		return uuid.Nil, fmt.Errorf("failed to create author: %w", err)
	}

	// Create socials
	suc := usecase.SocialOperations{
		SocialRepository: NewSocialRepositoryPostgres(tx),
	}

	err = suc.Operate(baseCtx, author.Id, author.Socials)
	if err != nil {
		return uuid.Nil, err
	}

	// Commit transaction to persist it
	ctx, cancel = context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	err = tx.Commit(ctx)
	cancel()
	if err != nil {
		return uuid.Nil, err
	}

	return author.Id, nil
}

func (arp *authorRepositoryPostgres) Update(baseCtx context.Context, author types.Author) error {
	logger := arp.linkLogger.Ctx(baseCtx)

	if baseCtx.Value(middleware.UserShouldBeSelfKeyEndpoint) == nil && baseCtx.Value(middleware.UserPermissionsKeyEndpoint) == nil {
		return fmt.Errorf("no permissions provided to update author")
	}

	checkSelf := false
	uid := uuid.UUID{}
	aid := uuid.UUID{}
	if baseCtx.Value(middleware.UserShouldBeSelfKeyEndpoint) != nil && baseCtx.Value(middleware.UserPermissionsKeyEndpoint) == nil {
		checkSelf = true
		uid = middleware.GetUserIdFromCtx(baseCtx)

		// TODO: smart up solution of user <-> author id mapping
		author, err := arp.GetByUserId(baseCtx, uid)
		if err != nil {
			return fmt.Errorf("failed to fetch author for logged in user id: %w", err)
		}

		aid = author.Id
	}
	logger.Debug("Should check self", zap.Bool("checkSelf", checkSelf), zap.String("uid", uid.String()), zap.String("aid", aid.String()))

	if checkSelf {
		if author.Id != aid {
			return fmt.Errorf("without elevated permissions you can only update yourself")
		}
	}

	tx, err := arp.DBConn.DBPool.BeginTx(baseCtx, pgx.TxOptions{})
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	defer cancel()
	defer gomstoolkit_pg.TxRollback(ctx, tx)

	db := arp.db.WithTx(tx)

	if author.Alias == "" && author.Biography == nil && author.Website == nil {
		return fmt.Errorf("please provide any data to change")
	}

	params := pgx_gen.UpdateAuthorParams{
		ID:    author.Id,
		Alias: author.Alias,
	}

	params.Biography.Scan(author.Biography)
	params.Website.Scan(author.Website)

	err = db.UpdateAuthor(baseCtx, params)
	if err != nil {
		return fmt.Errorf("failed to update author: %w", err)
	}

	// Alter socials
	suc := usecase.SocialOperations{
		SocialRepository: NewSocialRepositoryPostgres(tx),
	}

	err = suc.Operate(baseCtx, author.Id, author.Socials)
	if err != nil {
		return err
	}

	// Commit transaction to persist it
	ctx, cancel = context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	err = tx.Commit(ctx)
	cancel()
	if err != nil {
		return err
	}

	return nil
}
