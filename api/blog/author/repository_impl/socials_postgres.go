package repository_impl

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	"github.com/jackc/pgx/v5"

	"gitlab.com/joelMuehlena/jm-web-monorepo/api/blog/author/types"
	gomstoolkit_db "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
)

type socialRepositoryPostgres struct {
	tx pgx.Tx
}

func NewSocialRepositoryPostgres(tx pgx.Tx) *socialRepositoryPostgres {
	repo := new(socialRepositoryPostgres)

	repo.tx = tx

	return repo
}

func (srp *socialRepositoryPostgres) Delete(baseCtx context.Context, authorID uuid.UUID, socials []types.AuthorSocial) error {
	if len(socials) == 0 {
		return nil
	}

	deleteValues := make([]interface{}, 1)
	deleteValues[0] = authorID

	cnt := 2
	var valueStr string
	for _, pair := range socials {
		deleteValues = append(deleteValues, pair.Name)

		if valueStr != "" {
			valueStr += ","
		}

		valueStr += fmt.Sprintf("$%d", cnt)

		cnt++
	}

	if cnt == 2 {
		return nil
	}

	deleteSQL := fmt.Sprintf(`DELETE FROM author_social WHERE authorId = $1 AND name IN (%s) RETURNING name`, valueStr)

	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	_, err := srp.tx.Exec(ctx, deleteSQL, deleteValues...)
	defer cancel()

	if err != nil {
		return err
	}

	return nil
}

func (srp *socialRepositoryPostgres) Create(baseCtx context.Context, authorID uuid.UUID, socials []types.AuthorSocial) error {
	if len(socials) == 0 {
		return nil
	}

	insertValues := make([]interface{}, 0)
	insertStr := ""

	cnt := 1
	for _, social := range socials {

		if social.Modifier != types.CREATE_SOCIAL {
			return fmt.Errorf("a social with invalid modifier passed: %v", social)
		}

		insertStr += fmt.Sprintf("($%d, $%d, $%d)", cnt, cnt+1, cnt+2)

		insertValues = append(insertValues, authorID, social.Name, social.Value)

		cnt += 3
	}

	insertSQL := fmt.Sprintf(`INSERT INTO author_social (authorid, name, value) VALUES %s`, insertStr)

	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	_, err := srp.tx.Exec(ctx, insertSQL, insertValues...)
	defer cancel()

	if err != nil {
		return err
	}

	return nil
}

func (srp *socialRepositoryPostgres) Update(baseCtx context.Context, authorID uuid.UUID, socials []types.AuthorSocial) error {
	if len(socials) == 0 {
		return nil
	}

	for _, social := range socials {

		if social.Modifier != types.CREATE_SOCIAL {
			return fmt.Errorf("a social with invalid modifier passed: %v", social)
		}

		params := []interface{}{authorID, social.Name, social.Value}

		updateStr := ""
		if social.NewName != "" {
			params = append(params, social.NewName)
			updateStr = ", name = $4"
		}

		updateSQL := fmt.Sprintf(`UPDATE author_social SET value = $3 %s WHERE authorid = $1 AND name = $2`, updateStr)

		ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
		_, err := srp.tx.Exec(ctx, updateSQL, params...)
		defer cancel()

		if err != nil {
			return err
		}
	}

	return nil
}
