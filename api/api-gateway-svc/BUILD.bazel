load("@rules_go//go:def.bzl", "go_binary", "go_cross_binary", "go_library")
load("@rules_oci//oci:defs.bzl", "oci_image", "oci_image_index", "oci_push")
load("@rules_pkg//:pkg.bzl", "pkg_tar")
load("//api:archs.bzl", "ARCHS", "GO_PLATFORMS")
load("//bzl/compose:defs.bzl", "docker_compose_service")

go_library(
    name = "api-gateway-svc_lib",
    srcs = ["main.go"],
    importpath = "gitlab.com/joelMuehlena/jm-web-monorepo/api/api-gateway-svc",
    visibility = ["//visibility:private"],
    deps = [
        "//api-gateway",
        "//api/api-gateway-svc/middleware",
        "//go-ms-toolkit/util",
        "//lib/go/utility",
    ],
)

go_binary(
    name = "api-gateway-svc_base",
    basename = "api-gateway",
    embed = [":api-gateway-svc_lib"],
    visibility = ["//visibility:private"],
)

[go_cross_binary(
    name = "api-gateway-svc_{}_{}".format(
        p["os"],
        p["arch"],
    ),
    platform = p["platform"],
    target = ":api-gateway-svc_base",
    visibility = ["//visibility:public"],
) for p in GO_PLATFORMS]

[genrule(
    name = "bin_api-gateway-svc_{}_{}".format(
        p["os"],
        p["arch"],
    ),
    srcs = [":api-gateway-svc_{}_{}".format(
        p["os"],
        p["arch"],
    )],
    outs = ["api-gateway_{}_{}".format(
        p["os"],
        p["arch"],
    )],
    cmd = "cp $(location :api-gateway-svc_{os}_{arch}) $(location api-gateway_{os}_{arch})".format(
        arch = p["arch"],
        os = p["os"],
    ),
) for p in GO_PLATFORMS]

#################################
#          Containers           #
#################################

[pkg_tar(
    name = "go_bin_tar_linux_{}".format(arch),
    srcs = [
        ":bin_api-gateway-svc_linux_{}".format(arch),
        "//healthcheck:healthcheck_linux_{}".format(arch),
    ],
    visibility = ["//visibility:private"],
) for arch in ARCHS]

[oci_image(
    name = "svc_image_base_linux_{}".format(arch),
    base = "@cgr_static_linux_{}".format(arch),
    entrypoint = ["/api-gateway_linux_{}".format(arch)],
    env = {
        "GOGC": "400",
    },
    tars = [
        ":go_bin_tar_linux_{}".format(arch),
    ],
    user = "nonroot:nonroot",
    visibility = ["//visibility:private"],
) for arch in ARCHS]

oci_image_index(
    name = "svc_image_multi",
    images = [
        ":svc_image_base_linux_amd64",
        ":svc_image_base_linux_arm64",
    ],
    visibility = ["//visibility:private"],
)

oci_push(
    name = "push_gitlab",
    image = ":svc_image_multi",
    remote_tags = ["latest"],
    repository = "registry.gitlab.com/joel-muehlena-website/jm-web-monorepo/api-gateway",
)

oci_push(
    name = "push_local",
    image = ":svc_image_multi",
    remote_tags = ["latest"],
    repository = "localhost:5003/joel-muehlena-website/jm-web-monorepo/api-gateway",
)

#################################
#          Compose              #
#################################

oci_image(
    name = "svc_image_linux_amd64_docker",
    base = ":svc_image_base_linux_amd64",
    cmd = [
        "-config-server-url",
        "http://config-server:8081/config",
        "-config-server-file-path",
        "api-gateway/api-gateway.docker.yaml",
    ],
    visibility = ["//visibility:private"],
)

docker_compose_service(
    name = "compose",
    extra_hosts = ["host.docker.internal:host-gateway"],
    extra_name = "api-gateway",
    image = ":svc_image_linux_amd64_docker",
    labels = ["logging=promtail"],
    networks = ["api-bazel-net"],
    ports = [
        "8088:8080",
        "9100:9095",
    ],
    repo_tags = ["bazel-compose_api-gateway:latest"],
    restart = "on-failure",
    visibility = ["//visibility:public"],
)
