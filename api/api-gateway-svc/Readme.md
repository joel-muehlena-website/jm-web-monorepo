# API-GW Svc

WS example:

```go
cfg2 := api_gateway.EndpointsConfig{
  Name:   "test",
  Prefix: "wss",
  Endpoints: []api_gateway.EndpointConfig{
    {
      Path: "/1/demo",
      Upstream: api_gateway.UpstreamConfig{
        Service: "ws://socketsbay.com",
      },
      PathBehavior: api_gateway.PathBehavior{
        StripPrefix:      false,
        StripVersion:     false,
        AddTrailingSlash: true,
      },
      Enabled: nil,
      Type:    "ws",
      HandlerConfig: []api_gateway.HandlerConfig{
        {
          Method: "WS",
          Versions: []api_gateway.VersionConfig{
            {Path: "v2", PreHandlers: []string{"auth", "authorize"}, PostHandlers: []string{"extended_log"}},
          },
        },
      },
    },
  }
}
```
