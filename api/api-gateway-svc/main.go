package main

import (
	"flag"
	"os"

	api_gateway "gitlab.com/joelMuehlena/jm-web-monorepo/api-gateway"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/api-gateway-svc/middleware"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/utility"

	gomstoolkit_util "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/util"
)

const (
	CONFIG_SERVER_URL_PROD = "http://config-server.jm-core.svc.cluster.local/config"
	CONFIG_SERVER_URL_DEV  = "http://localhost:8081/config"
)

type Config struct {
	Config           api_gateway.Config `yaml:"gw_config"`
	MiddlewareConfig MiddlewareConfig   `yaml:"middlewareConfig"`
}

type MiddlewareConfig struct {
	JWTAuthVerifier JWTAuthVerifier `yaml:"jwtAuthVerifier"`
	RoleVerifier    RoleVerifier    `yaml:"roleVerifier"`
}

type JWTAuthVerifier struct {
	MetaUrl string `yaml:"metaUrl"`
}

type RoleVerifier struct {
	AuthUrl string `yaml:"authUrl"`
}

func main() {
	configServerUrl := flag.String("config-server-url", CONFIG_SERVER_URL_DEV, "URL of the config server")
	configServerFilePath := flag.String("config-server-file-path", "api-gateway/api-gateway.dev.yaml", "Path to the config file on the config server")
	useSvcEnv := flag.Bool("use-svc-env", true, "Use the JM_SVC_ENV environment variable")
	flag.Parse()

	if *useSvcEnv && os.Getenv("JM_SVC_ENV") == "production" {
		*configServerUrl = CONFIG_SERVER_URL_PROD
		*configServerFilePath = "api-gateway/api-gateway.prod.yaml"
	}

	result := gomstoolkit_util.FetchConfigServerConfig(*configServerUrl, *configServerFilePath)
	apiGatewayConfig := utility.ParseConfig[Config](result)

	apiGateway := api_gateway.New(&apiGatewayConfig.Config, *configServerUrl)

	apiGateway.MiddlewareRegistry().Use("jwt_auth_verifier", middleware.NewJWTAuthVerifier(apiGatewayConfig.MiddlewareConfig.JWTAuthVerifier.MetaUrl))
	apiGateway.MiddlewareRegistry().Use("role_verifier", middleware.NewRoleVerifier(apiGatewayConfig.MiddlewareConfig.RoleVerifier.AuthUrl))

	apiGateway.ListenAndServe()
}
