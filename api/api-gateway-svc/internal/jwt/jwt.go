package jwt

import (
	"context"
	"crypto"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/samber/lo"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.uber.org/atomic"
	"go.uber.org/zap"
)

type JWTVerifyProvider[ClaimType ClaimTypeConstraints, ClaimTypeP interface {
	ClaimTypeConstraints
	*ClaimType
}] interface {
	Init()
	Verify(tokenString string, parser *jwt.Parser) (ClaimType, error)
	IsEnabled() bool
	Shutdown(ctx context.Context)
}

type JWTLoader[ClaimType ClaimTypeConstraints, ClaimTypeP ClaimTypePointerConstraints[ClaimType]] struct {
	logger               *zap.Logger
	metaProvider         JWTVerifyProvider[ClaimType, ClaimTypeP]
	fileKeyProvider      JWTVerifyProvider[ClaimType, ClaimTypeP]
	remoteVerifyProvider JWTVerifyProvider[ClaimType, ClaimTypeP]
}

type ClaimTypeConstraints interface {
	jwt.Claims
}

type ClaimTypePointerConstraints[ClaimType ClaimTypeConstraints] interface {
	ClaimTypeConstraints
	*ClaimType
}

// TODO: Make dynamic
func NewJWTLoader[ClaimType ClaimTypeConstraints, ClaimTypeP ClaimTypePointerConstraints[ClaimType]](logger *zap.Logger, metaUrl string) *JWTLoader[ClaimType, ClaimTypeP] {
	loader := &JWTLoader[ClaimType, ClaimTypeP]{
		logger: logger,
		metaProvider: &metaProvider[ClaimType, ClaimTypeP]{
			logger:  logger.Named("meta-provider"),
			enabled: atomic.NewBool(false),
			metaURL: metaUrl,
		},
	}

	// Check meta endpoint for public key and refresh periodically
	// If meta or pubkey is not enabled check filesystem for public key
	// If no public key is found try auth endpoint to verify against auth server
	// else return error

	return loader
}

func (loader *JWTLoader[ClaimType, ClaimTypeP]) Init() {
	loader.metaProvider.Init()
}

func (loader *JWTLoader[ClaimType, ClaimTypeP]) Shutdown(ctx context.Context) {
	loader.metaProvider.Shutdown(ctx)
}

func (loader *JWTLoader[ClaimType, ClaimTypeP]) Verify(tokenString string, parser *jwt.Parser) (ClaimType, error) {
	var claims ClaimType
	var err error = fmt.Errorf("no provider enabled")
	if loader.metaProvider.IsEnabled() {
		claims, err = loader.metaProvider.Verify(tokenString, parser)
	}

	// TODO use other provider

	return claims, err
}

type metaProvider[ClaimType ClaimTypeConstraints, ClaimTypeP ClaimTypePointerConstraints[ClaimType]] struct {
	logger    *zap.Logger
	enabled   *atomic.Bool
	publicKey crypto.PublicKey
	keyLock   sync.RWMutex
	metaURL   string

	cancelRefresher context.CancelFunc
}

type MetaInfoResponse struct {
	Code int      `json:"code"`
	Meta MetaInfo `json:"meta"`
}

type MetaInfo struct {
	Authorizer map[string]interface{} `json:"authorizer"`
	Jwt        MetaInfoJWT            `json:"jwt"`
}

type MetaInfoJWT struct {
	Mode string                 `json:"mode"`
	Jwt  map[string]interface{} `json:"jwt"`
}

func (provider *metaProvider[ClaimType, ClaimTypeP]) Init() {
	getKey := func(ctx context.Context) error {
		// Get public key from meta endpoint
		client := &http.Client{
			Transport: otelhttp.NewTransport(http.DefaultTransport),
		}

		req, err := http.NewRequestWithContext(ctx, http.MethodGet, provider.metaURL, nil)
		if err != nil {
			return err
		}

		res, err := client.Do(req)
		if err != nil {
			return err
		}

		if res.StatusCode != 200 {
			return fmt.Errorf("meta endpoint returned non 200 status code: %v", res.StatusCode)
		}

		var data MetaInfoResponse

		err = json.NewDecoder(res.Body).Decode(&data)
		if err != nil {
			return err
		}

		if data.Meta.Jwt.Mode != "ED25519" {
			return fmt.Errorf("meta endpoint returned unsupported jwt mode: %v", data.Meta.Jwt.Mode)
		}

		parsedDERBytesI, ok := data.Meta.Jwt.Jwt["access_key_public_key"].([]interface{})

		if !ok {
			return fmt.Errorf("failed to parse public key from meta endpoint")
		}

		parsedDERBytes := lo.Map(parsedDERBytesI, func(d interface{}, _ int) byte {
			return byte(d.(float64))
		})

		publicKey, err := x509.ParsePKIXPublicKey(parsedDERBytes)
		if err != nil {
			return err
		}

		provider.keyLock.Lock()
		provider.publicKey = publicKey
		provider.keyLock.Unlock()
		provider.logger.Debug("Met public key", zap.Any("key", publicKey))

		return nil
	}

	err := getKey(context.Background())
	if err != nil {
		alreadyDisabled := !(provider.enabled.CompareAndSwap(true, false))
		provider.enabled.CompareAndSwap(true, false)
		provider.logger.Error("Failed to get public key from meta. Disabling meta provider", zap.Error(err), zap.Bool("alreadyDisabled", alreadyDisabled))
	} else {
		alreadyEnabled := !(provider.enabled.CompareAndSwap(false, true))
		provider.logger.Info("Got key from meta endpoint. Enabling provider if not yet done", zap.Bool("alreadyEnabled", alreadyEnabled))
	}

	ctx, cancel := context.WithCancel(context.Background())
	provider.cancelRefresher = cancel

	ticker := time.NewTicker(5 * time.Minute)
	go func() {
		for {
			select {
			case <-ticker.C:
				err := getKey(context.Background())
				if err != nil {
					alreadyDisabled := !(provider.enabled.CompareAndSwap(true, false))
					provider.enabled.CompareAndSwap(true, false)
					provider.logger.Error("Failed to get public key from meta. Disabling meta provider", zap.Error(err), zap.Bool("alreadyDisabled", alreadyDisabled))
				} else {
					alreadyEnabled := !(provider.enabled.CompareAndSwap(false, true))
					provider.logger.Info("Got key from meta endpoint. Enabling provider if not yet done", zap.Bool("alreadyEnabled", alreadyEnabled))
				}
			case <-ctx.Done():
				return
			}
		}
	}()
}

func (provider *metaProvider[ClaimType, ClaimTypeP]) Shutdown(ctx context.Context) {
	provider.cancelRefresher()
}

func (provider *metaProvider[ClaimType, ClaimTypeP]) Verify(tokenString string, parser *jwt.Parser) (ClaimType, error) {
	var claims ClaimType
	provider.keyLock.RLock()
	_, err := parser.ParseWithClaims(tokenString, ClaimTypeP(&claims), func(t *jwt.Token) (interface{}, error) {
		return provider.publicKey, nil
	})
	provider.keyLock.RUnlock()

	if err != nil {
		return claims, err
	}

	return claims, nil
}

func (provider *metaProvider[ClaimType, ClaimTypeP]) IsEnabled() bool {
	return provider.enabled.Load()
}

type (
	fileProvider         struct{}
	remoteVerifyProvider struct{}
)
