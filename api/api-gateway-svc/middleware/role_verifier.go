package middleware

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.uber.org/zap"

	api_gateway "gitlab.com/joelMuehlena/jm-web-monorepo/api-gateway"
)

type RoleVerifier struct {
	logger  *zap.Logger
	authUrl string
}

func NewRoleVerifier(authUrl string) *RoleVerifier {
	verifier := new(RoleVerifier)

	verifier.authUrl = authUrl

	return verifier
}

func (verifier *RoleVerifier) Init(logger *zap.Logger) {
	verifier.logger = logger.Named("role-verifier")
	verifier.logger.Info("Middleware initialized")
}

func (verifier *RoleVerifier) Handle(w http.ResponseWriter, r *http.Request, data api_gateway.MiddlewareData) (http.ResponseWriter, *http.Request, *api_gateway.MiddlewareError) {
	permissionI, ok := data["permission"]
	if !ok {
		return w, r, &api_gateway.MiddlewareError{
			HTTPErrorCode: 403,
			Err:           fmt.Errorf("required permission not provided"),
		}
	}

	permissionRequest, ok := permissionI.(string)
	if !ok {
		return w, r, &api_gateway.MiddlewareError{
			HTTPErrorCode: 403,
			Err:           fmt.Errorf("provided permission is not a string"),
		}
	}

	passAnywayI, ok := data["pass-anyway"]
	passAnyway := false
	if ok {
		passAnywayB, ok := passAnywayI.(bool)
		if ok {
			passAnyway = passAnywayB
		}

	}

	_, ok = r.Context().Value(JWTClaimContextTypeValue).(JWTClaim)

	if !ok {
		return w, r, &api_gateway.ErrNotAuthorized
	}

	client := http.Client{
		Transport: otelhttp.NewTransport(http.DefaultTransport),
	}
	req, err := http.NewRequestWithContext(r.Context(), http.MethodGet, verifier.authUrl, nil)
	if err != nil {
		return w, r, &api_gateway.ErrNotAuthorized
	}

	req.Header.Add("Authorization", r.Header.Get("Authorization"))

	permissionsQuery := url.Values{}
	permissionsQuery.Add("permission", permissionRequest)
	req.URL.RawQuery = permissionsQuery.Encode()

	res, err := client.Do(req)
	if err != nil {
		return w, r, &api_gateway.ErrNotAuthorized
	}
	d, _ := io.ReadAll(res.Body)
	verifier.logger.Debug("Authorized response", zap.Int("status", res.StatusCode), zap.String("body", string(d)))

	if res.StatusCode >= 500 {
		return w, r, &api_gateway.ErrNotAuthorized
	}

	if res.StatusCode != 200 && !passAnyway {
		return w, r, &api_gateway.ErrNotAuthorized
	} else if res.StatusCode != 200 && passAnyway {
		verifier.logger.Debug("Passing request but permissions is not verified due to pass-anyway flag being set")
		permissionRequest = "pass-anyway"
	}

	r.Header.Set("X-Permissions-Verified-For", permissionRequest)
	verifier.logger.Debug("Permissions verified", zap.String("permission", permissionRequest))

	return w, r, nil
}

func (verifier *RoleVerifier) Reload(ctx context.Context) {
}

func (verifier *RoleVerifier) Shutdown(ctx context.Context) {
}
