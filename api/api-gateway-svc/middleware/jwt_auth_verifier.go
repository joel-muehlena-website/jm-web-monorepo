package middleware

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"go.uber.org/zap"

	jwtLib "github.com/golang-jwt/jwt/v5"

	api_gateway "gitlab.com/joelMuehlena/jm-web-monorepo/api-gateway"
	"gitlab.com/joelMuehlena/jm-web-monorepo/api/api-gateway-svc/internal/jwt"
)

var ErrTokenInvalid = api_gateway.MiddlewareError{
	HTTPErrorCode: uint16(http.StatusUnauthorized),
	Err:           fmt.Errorf("the passed key is not valid. Maybe a refresh or login is required"),
}

var ErrTokenExpired = api_gateway.MiddlewareError{
	HTTPErrorCode: uint16(http.StatusUnauthorized),
	Err:           fmt.Errorf("the passed key is expired"),
}

type JWTClaimContextType string

const JWTClaimContextTypeValue JWTClaimContextType = "__JWTClaimContextType__"

type JWTClaim struct {
	jwtLib.RegisteredClaims
}

type JWTAuthVerifier struct {
	logger  *zap.Logger
	parser  *jwtLib.Parser
	jwt     *jwt.JWTLoader[JWTClaim, *JWTClaim]
	metaUrl string
}

func NewJWTAuthVerifier(metaUrl string) *JWTAuthVerifier {
	verifier := new(JWTAuthVerifier)

	verifier.metaUrl = metaUrl

	return verifier
}

func (verifier *JWTAuthVerifier) Init(logger *zap.Logger) {
	verifier.logger = logger.Named("jwt-auth-verifier")

	verifier.jwt = jwt.NewJWTLoader[JWTClaim, *JWTClaim](verifier.logger, verifier.metaUrl)
	verifier.jwt.Init()

	verifier.parser = jwtLib.NewParser(jwtLib.WithIssuedAt(), jwtLib.WithIssuer("ds-auth::iam"), jwtLib.WithValidMethods([]string{"EdDSA"}))

	verifier.logger.Info("Middleware initialized")
}

func (verifier *JWTAuthVerifier) Handle(w http.ResponseWriter, r *http.Request, data api_gateway.MiddlewareData) (http.ResponseWriter, *http.Request, *api_gateway.MiddlewareError) {
	claims, err := verifier.jwt.Verify(strings.TrimSpace(
		strings.Replace(r.Header.Get("Authorization"), "Bearer", "", 1),
	), verifier.parser)
	if err != nil {
		verifier.logger.Debug("Authentication verification failed", zap.Error(&ErrTokenInvalid))
		return w, r, &ErrTokenInvalid
	}

	r = r.WithContext(context.WithValue(r.Context(), JWTClaimContextTypeValue, claims))

	r.Header.Set("X-User-ID", claims.Subject)
	r.Header.Set("X-User-Authenticated", "true")

	verifier.logger.Debug("Authentication verification successful", zap.String("user-id", claims.Subject))
	return w, r, nil
}

func (verifier *JWTAuthVerifier) Reload(ctx context.Context) {
}

func (verifier *JWTAuthVerifier) Shutdown(ctx context.Context) {
	verifier.jwt.Shutdown(ctx)
}
