"""
Constants for all API relevant projects.
"""

# The required architectures and oses for the API services.
ARCHS = [
    "amd64",
    "arm64",
]

OSS = [
    "linux",
]

GO_PLATFORMS = [
    {
        "os": os,
        "arch": arch,
        "platform": "@rules_go//go/toolchain:{}_{}".format(os, arch),
    }
    for os in OSS
    for arch in ARCHS
]
