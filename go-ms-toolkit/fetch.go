package gomstoolkit

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"time"
)

const (
	HTTP_CONTENT_HEADER_JSON = "application/json"
	HTTP_CONTENT_HEADER_XML  = "application/xml"
	HTTP_CONTENT_HEADER_TEXT = "text/*"
	HTTP_CONTENT_HEADER_BIN  = "application/octet-stream"

	DEFAULT_FETCH_TIMEOUT = 7 * time.Second
)

var ErrRequestNon200 = fmt.Errorf("request not successful with non 200 code")

type FetchConfig struct {
	Method string
	Body   io.Reader
	Header map[string]string
}

type FetchResponse struct {
	StatusCode int
	Status     string

	// The untouched body of the response
	BodyRaw io.ReadCloser
}

var defaultFetchConfig = FetchConfig{
	Method: "GET",
	Body:   nil,
	Header: map[string]string{},
}

func DefaultFetchConfig() *FetchConfig {
	cfg := defaultFetchConfig
	return &cfg
}

type FetchSetter interface {
	SetData(r io.ReadCloser) error
}

// Performs a http request with the passed parameters and options.
// This Fetch function only supports parsable data types as response.
// E.g. JSON or XML if you want to handle the data a specific way use FetchWithBinding
// It is also possible to just fetch and the use the BodyRaw of the FetchResponse
//
// Returns the parsed response as result, as well as a generic response object
func Fetch[T any](ctx context.Context, url string, cfg *FetchConfig) (*T, FetchResponse, error) {
	result := new(T)

	if cfg == nil {
		cfg = DefaultFetchConfig()
	}

	client := &http.Client{}

	req, err := http.NewRequestWithContext(ctx, cfg.Method, url, cfg.Body)
	if err != nil {
		return nil, FetchResponse{}, err
	}

	for key, value := range cfg.Header {
		req.Header.Add(key, value)
	}

	res, err := client.Do(req)
	if err != nil {
		return nil, FetchResponse{}, err
	}

	if res.StatusCode >= 400 {
		return nil, FetchResponse{
			StatusCode: res.StatusCode,
			Status:     res.Status,
			BodyRaw:    res.Body,
		}, ErrRequestNon200
	}

	bodyCopy, bodyRaw, err := copyBody(res.Body)
	if err != nil {
		return nil, FetchResponse{}, err
	}

	res.Body = bodyCopy

	fetchResponse := FetchResponse{
		StatusCode: res.StatusCode,
		Status:     res.Status,
		BodyRaw:    bodyRaw,
	}

	switch res.Header.Get("Content-Type") {
	case HTTP_CONTENT_HEADER_JSON:
		err = json.NewDecoder(res.Body).Decode(result)
	case HTTP_CONTENT_HEADER_XML:
		err = xml.NewDecoder(res.Body).Decode(result)
	}

	// TODO test EOF error
	if err != nil && err != io.EOF {
		return nil, FetchResponse{}, err
	}

	return result, fetchResponse, nil
}

type Noop struct{}

// Performs a http request with the passed parameters and options.
// Only accepts types implementing the FetchSetter interface.
// So you can handle the data like you want
//
// Returns a generic response object
func FetchWithBinding[T FetchSetter](ctx context.Context, result T, url string, cfg *FetchConfig) (FetchResponse, error) {
	_, fetchResult, err := Fetch[Noop](ctx, url, cfg)
	if err != nil {
		return FetchResponse{}, err
	}

	r1, r2, err := copyBody(fetchResult.BodyRaw)
	if err != nil {
		return FetchResponse{}, err
	}

	fetchResult.BodyRaw = r1

	err = result.SetData(r2)

	if err != nil {
		return FetchResponse{}, err
	}

	return fetchResult, nil
}

func copyBody(b io.ReadCloser) (r1, r2 io.ReadCloser, err error) {
	if b == nil || b == http.NoBody {
		// No copying needed. Preserve the magic sentinel meaning of NoBody.
		return http.NoBody, http.NoBody, nil
	}
	var buf bytes.Buffer
	if _, err = buf.ReadFrom(b); err != nil {
		return nil, b, err
	}
	if err = b.Close(); err != nil {
		return nil, b, err
	}
	return io.NopCloser(&buf), io.NopCloser(bytes.NewReader(buf.Bytes())), nil
}
