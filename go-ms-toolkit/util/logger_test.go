package util

import (
	"path/filepath"
	"testing"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"

	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

func TestCreateLoggerCfg(t *testing.T) {
	expected := gomstoolkit.DefaultLoggerConfig
	logFilePath := filepath.Join(string(filepath.Separator), "var", "log", "jm-web-svc", "{NAME}.log")

	expected.LogOutputPaths = append(expected.LogOutputPaths, logFilePath)
	expected.LogErrorPaths = append(expected.LogErrorPaths, logFilePath)

	assert.Nil(t, deep.Equal(expected, *CreateLoggerCfgWithVarLog()))
}
