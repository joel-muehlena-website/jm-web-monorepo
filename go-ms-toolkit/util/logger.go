package util

import (
	"path/filepath"

	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

func CreateLoggerCfgWithVarLog() *gomstoolkit.LoggerConfig {
	loggerCfg := gomstoolkit.DefaultLoggerConfig
	logFilePath := filepath.Join(string(filepath.Separator), "var", "log", "jm-web-svc", "{NAME}.log")

	loggerCfg.LogOutputPaths = append(loggerCfg.LogOutputPaths, logFilePath)
	loggerCfg.LogErrorPaths = append(loggerCfg.LogErrorPaths, logFilePath)

	return &loggerCfg
}
