package util

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	configServerAddon "gitlab.com/joelMuehlena/jm-web-monorepo/utility/go-config-server-addon"
)

func TestFetchConfigServerConfig(t *testing.T) {
	t.Run("Should succeed", func(t *testing.T) {
		value := []byte(`port: 3000
		dbConfig:
		  general:
			username: 23`)

		jsonResp := configServerAddon.GitlabResponseJSON{
			ConfigB64Data: base64.StdEncoding.EncodeToString(value),
			FileEnding:    "yaml",
		}

		svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/octet-stream")
			w.WriteHeader(http.StatusOK)

			json.NewEncoder(w).Encode(jsonResp)
		}))
		defer svr.Close()

		result := FetchConfigServerConfig(svr.URL, "a/random/path")
		assert.Equal(t, value, result)
	})

	t.Run("Should panic on error", func(t *testing.T) {
		svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(404)
		}))
		defer svr.Close()

		assert.Panics(t, func() {
			FetchConfigServerConfig(svr.URL, "")
		})
	})
}
