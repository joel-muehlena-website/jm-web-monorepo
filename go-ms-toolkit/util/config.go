package util

import (
	configServerAddon "gitlab.com/joelMuehlena/jm-web-monorepo/utility/go-config-server-addon"
)

func FetchConfigServerConfig(configServerUrl string, pathToFile string) []byte {
	resultChannel := make(chan configServerAddon.Result, 1)
	configServerAddon.FetchConfig(configServerUrl, resultChannel, configServerAddon.WithFilePath(pathToFile), configServerAddon.WithLogging(configServerAddon.ERROR))

	result := <-resultChannel

	if result.Error != nil {
		panic("Error fetching config: " + result.Error.Error())
	}

	return result.Data
}
