package gomstoolkit

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/samber/lo"
	"go.opentelemetry.io/otel"
	"go.uber.org/zap"
)

type (
	EndpointHandler[Req any, Res any]           func(context.Context, *Req) (*Res, int, error)
	EndpointHandlerController[Req any, Res any] func(context.Context, RequestController, *Req) (*Res, int, error)
	AuthHandler                                 func(context.Context, *gin.Context) (context.Context, bool, error)
	AuthorizationHandler                        func(context.Context, *gin.Context) (context.Context, bool, error)
)

type EndpointContextKey string

type Method uint16

const (
	GET     Method = iota
	POST    Method = iota
	DELETE  Method = iota
	PATCH   Method = iota
	PUT     Method = iota
	OPTIONS Method = iota
)

func getMethodStringList() []string {
	return []string{
		"GET",
		"POST",
		"DELETE",
		"PATCH",
		"PUT",
		"OPTIONS",
	}
}

func (m Method) String() string {
	return GetMethodString(m)
}

func GetMethodString(op Method) string {
	opList := getMethodStringList()

	if int(op) > len(opList)-1 {
		panic("This Method is not supported")
	}

	return opList[op]
}

type Endpoint[Req any, Res any] struct {
	Path                string
	Handler             EndpointHandler[Req, Res]
	HandlerController   EndpointHandlerController[Req, Res]
	Method              Method
	Description         string
	AuthHandler         AuthHandler
	AuthorizationChain  []AuthorizationHandler
	RestrictContentType []string
}

type (
	EmptyRequest  struct{}
	EmptyResponse struct{}
)

type QueryParams map[string][]string

type RequestController struct {
	RequestRAW  *http.Request
	Writer      http.ResponseWriter
	QueryParams QueryParams
}

type Hitable interface {
	Hit(*gin.Context)
	GetMethod() Method
	GetPath() string
	Info(bool) string
}

func (endpoint *Endpoint[Req, Res]) Info(extended bool) string {
	info := fmt.Sprintf("Endpoint with path %s and HTTP Method %s", endpoint.Path, GetMethodString(endpoint.Method))

	if extended {

		authValue := "an"
		if endpoint.AuthHandler == nil {
			authValue = "no"
		}

		info += fmt.Sprintf(". The endpoint has %s auth handler and %d authorization funcs", authValue, len(endpoint.AuthorizationChain))

		if endpoint.Description != "" {
			info += fmt.Sprintf(": %s", endpoint.Description)
		}
	}

	return info
}

func (endpoint *Endpoint[Req, Res]) Hit(ginCtx *gin.Context) {
	ctx, span := otel.Tracer("").Start(ginCtx.Request.Context(), "endpoint handler")
	defer span.End()

	if endpoint.RestrictContentType != nil &&
		len(endpoint.RestrictContentType) > 0 &&
		!lo.Contains(endpoint.RestrictContentType, ginCtx.ContentType()) {
		ginCtx.AbortWithStatusJSON(400, &HTTPError{
			Code:    400,
			Message: "this content type is not allowed",
			Errors:  []string{fmt.Sprintf("The content type '%s' is not allowed for %s on %s. Allowed content types are: %s", ginCtx.ContentType(), ginCtx.Request.Method, ginCtx.Request.RequestURI, strings.Join(endpoint.RestrictContentType, ","))},
		})
		return
	}

	var err error = fmt.Errorf("Nothing parsed")

	if ginCtx.ContentType() == "multipart/form-data" {
		err = ginCtx.Request.ParseMultipartForm(1024 * 1024 * 10)
	} else {
		err = ginCtx.Request.ParseForm()
	}

	if err != nil {
		httpError := HTTPError{
			Code:    500,
			Message: "failed to parse form data",
			Errors:  []string{err.Error()},
		}

		ginCtx.AbortWithStatusJSON(500, httpError)
		return
	}

	if endpoint.AuthHandler != nil {
		_, span := otel.Tracer("").Start(ctx, "endpoint authentication handler")
		var success bool
		ctx, success, err = endpoint.AuthHandler(ctx, ginCtx)
		span.End()

		if !success {
			ginCtx.AbortWithStatusJSON(401, &HTTPError{
				Code:    401,
				Message: "Failed to authenticate",
				Errors:  []string{err.Error()},
			})
		}
	}

	if endpoint.AuthorizationChain != nil {
		var success bool
		for i, authFn := range endpoint.AuthorizationChain {
			_, span := otel.Tracer("").Start(ctx, fmt.Sprintf("endpoint authorization handler: %d", i))
			ctx, success, err = authFn(ctx, ginCtx)
			span.End()

			if !success {
				error := "Failed to authorize"
				if err != nil {
					error = err.Error()
				}

				ginCtx.AbortWithStatusJSON(403, &HTTPError{
					Code:    403,
					Message: "Failed to authorize",
					Errors:  []string{error},
				})

				return
			}
		}
	}

	request := new(Req)

	err = ginCtx.ShouldBind(request)
	if err != nil {

		httpError := HTTPError{
			Code:    500,
			Message: "failed to bind the request struct",
			Errors:  []string{err.Error()},
		}

		ginCtx.AbortWithStatusJSON(500, httpError)
		return
	}

	err = ginCtx.ShouldBindUri(request)
	if err != nil {

		httpError := HTTPError{
			Code:    500,
			Message: "failed to bind the request struct on the URI",
			Errors:  []string{err.Error()},
		}

		ginCtx.AbortWithStatusJSON(500, httpError)
		return
	}

	err = ginCtx.ShouldBindQuery(request)
	if err != nil {

		httpError := HTTPError{
			Code:    500,
			Message: "failed to bind the request struct on the query",
			Errors:  []string{err.Error()},
		}

		ginCtx.AbortWithStatusJSON(500, httpError)
		return
	}

	zap.S().Debugf("request: %v", request)

	var response *Res
	var code int

	if endpoint.HandlerController != nil {
		controller := RequestController{
			RequestRAW:  ginCtx.Request,
			Writer:      ginCtx.Writer,
			QueryParams: QueryParams(ginCtx.Request.URL.Query()),
		}

		response, code, err = endpoint.HandlerController(ctx, controller, request)
	} else {
		response, code, err = endpoint.Handler(ctx, request)
	}

	if err != nil {

		if hErr, ok := err.(*HTTPError); ok {
			hErr.SetDefaultsForEmptyFields()

			hErr.Code = code

			ginCtx.AbortWithStatusJSON(hErr.Code, hErr)
		} else {

			httpError := HTTPError{
				Code:    code,
				Message: err.Error(),
				Errors:  make([]string, 0),
			}

			ginCtx.AbortWithStatusJSON(httpError.Code, httpError)
		}

		return
	}

	ginCtx.JSON(code, gin.H{
		"code": code,
		"data": response,
	})
}

func (endpoint *Endpoint[Req, Res]) GetMethod() Method {
	return endpoint.Method
}

func (endpoint *Endpoint[Req, Res]) GetPath() string {
	return endpoint.Path
}
