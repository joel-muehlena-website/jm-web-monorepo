package gomstoolkit

import (
	"context"
	"errors"
	"fmt"

	"go.uber.org/zap"
)

var ErrShuttingDown = fmt.Errorf("Shutting down")

type (
	CommandType string

	Command struct {
		Command CommandType
		Ctx     context.Context
		Data    any
	}
	SubService interface {
		Init() (chan<- Command, error)
		IsInitialized() bool
		Run() error
		Shutdown(ctx context.Context)
	}

	SubServiceReference struct {
		SubService SubService
		Commander  chan<- Command
	}
)

func StartCommander(ctx context.Context, logger *zap.Logger, size int, handler func(command Command), onQuitError func(ctx context.Context)) (chan<- Command, context.CancelCauseFunc) {
	commander := make(chan Command, size)
	ctx, cancel := context.WithCancelCause(ctx)

	logger.Debug("Starting commander")

	go func() {
		defer logger.Debug("Stopping commander")

		for {
			select {
			case command := <-commander:
				handler(command)
			case <-ctx.Done():
				if ctx.Err() != nil && !errors.Is(context.Cause(ctx), ErrShuttingDown) {
					onQuitError(ctx)
				}
				return
			}
		}
	}()

	return commander, cancel
}
