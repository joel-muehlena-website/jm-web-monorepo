package gomstoolkit

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/metrics"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/probes"

	netpprof "net/http/pprof"

	"go.uber.org/atomic"
	"go.uber.org/zap"
)

type Monitoring struct {
	logger  *zap.Logger
	config  MonitoringConfig
	server  *http.Server
	metrics *metrics.Metrics
	probes  *probes.Probes

	initialized     *atomic.Bool
	running         *atomic.Bool
	cancelCommander context.CancelCauseFunc
}

var _ SubService = &Monitoring{}

func NewMonitoring(logger *zap.Logger, cfg MonitoringConfig) *Monitoring {
	if cfg.MetricsPrefix == "" {
		cfg.MetricsPrefix = "/metrics"
	}

	if cfg.ProbesPrefix == "" {
		cfg.ProbesPrefix = "/"
	}

	logger = logger.Named("monitoring")
	return &Monitoring{
		logger:      logger,
		config:      cfg,
		initialized: atomic.NewBool(false),
		running:     atomic.NewBool(false),
		metrics:     metrics.New(logger, metrics.Config{Port: cfg.Port, BindAddr: cfg.BindAddr, Endpoint: cfg.MetricsPrefix}),
		probes:      probes.New(logger, probes.Config{Port: cfg.Port, BindAddr: cfg.BindAddr}),
	}
}

func (monitoringSvc *Monitoring) Init() (chan<- Command, error) {
	monitoringSvc.initHttpServer()

	cmd, cancel := StartCommander(context.Background(), monitoringSvc.logger, 5, func(c Command) {
		switch c.Command {
		case HTTPCommandReload:
			monitoringSvc.logger.Info("Received reload command. Reloading server")
			monitoringSvc.stopServer(context.Background())
			monitoringSvc.initHttpServer()
			monitoringSvc.initialized.Store(true)
			monitoringSvc.startServer()
		case HTTPCommandStop:
			monitoringSvc.logger.Info("Received stop command. Stopping server")
			monitoringSvc.stopServer(context.Background())
		case HTTPCommandStart:
			monitoringSvc.logger.Info("Received start command. Starting server")
			monitoringSvc.initHttpServer()
			monitoringSvc.initialized.Store(true)
			monitoringSvc.startServer()
		}
	}, func(ctx context.Context) {
		monitoringSvc.logger.Error("Error in http commander context", zap.Error(ctx.Err()))
	})

	monitoringSvc.cancelCommander = cancel
	monitoringSvc.initialized.Store(true)

	return cmd, nil
}

func (monitoringSvc *Monitoring) Probes() *probes.Probes {
	return monitoringSvc.probes
}

func (monitoringSvc *Monitoring) initHttpServer() {
	mh := monitoringSvc.metrics.Handler()
	ph := monitoringSvc.probes.Handler()

	mux := http.NewServeMux()
	mux.Handle(monitoringSvc.config.MetricsPrefix, mh)
	if monitoringSvc.config.EnabledProfiling {
		mux.HandleFunc("/debug/pprof/", netpprof.Index)
		mux.HandleFunc("/debug/pprof/cmdline", netpprof.Cmdline)
		mux.HandleFunc("/debug/pprof/profile", netpprof.Profile)
		mux.HandleFunc("/debug/pprof/symbol", netpprof.Symbol)
		mux.HandleFunc("/debug/pprof/trace", netpprof.Trace)
	}
	mux.Handle(monitoringSvc.config.ProbesPrefix, ph)

	monitoringSvc.server = &http.Server{
		Addr:    fmt.Sprintf("%s:%d", monitoringSvc.config.BindAddr, monitoringSvc.config.Port),
		Handler: mux,
	}
}

func (monitoringSvc *Monitoring) IsInitialized() bool {
	return monitoringSvc.initialized.Load()
}

func (monitoringSvc *Monitoring) Shutdown(ctx context.Context) {
	monitoringSvc.cancelCommander(ErrShuttingDown)
	monitoringSvc.stopServer(ctx)
}

func (monitoringSvc *Monitoring) stopServer(ctx context.Context) error {
	if !monitoringSvc.IsInitialized() || monitoringSvc.server == nil {
		monitoringSvc.logger.Debug("Tried to stop a not initialized http server")
		return ErrHTTPNotInitialized
	}

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	err := monitoringSvc.server.Shutdown(ctx)
	if err != nil {
		monitoringSvc.logger.Error("Error shutting down http server", zap.Error(err))
		return err
	}

	// Revoking http initialization
	monitoringSvc.initialized.Store(false)

	return nil
}

func (monitoringSvc *Monitoring) startServer() error {
	if monitoringSvc.running.Load() {
		monitoringSvc.logger.Debug("Tried to start an already running http server")
		return ErrHTTPAlreadyRunning
	}

	go func() {
		defer monitoringSvc.running.Store(false)
		monitoringSvc.logger.Info("Trying to start server", zap.String("bindAddr", monitoringSvc.config.BindAddr), zap.Uint16("port", monitoringSvc.config.Port))

		monitoringSvc.running.Store(true)

		if err := monitoringSvc.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			monitoringSvc.logger.Error("Http server error", zap.Error(err), zap.String("bindAddr", monitoringSvc.config.BindAddr), zap.Uint16("port", monitoringSvc.config.Port))
		} else if err != nil && errors.Is(err, http.ErrServerClosed) {
			monitoringSvc.logger.Info("Http server closed gracefully")
		}
	}()

	return nil
}

func (monitoringSvc *Monitoring) Run() error {
	if !monitoringSvc.IsInitialized() {
		return ErrHTTPNotInitialized
	}

	return monitoringSvc.startServer()
}
