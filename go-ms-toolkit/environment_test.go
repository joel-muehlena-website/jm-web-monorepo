package gomstoolkit

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetCurrentEnv(t *testing.T) {
	assert := assert.New(t)

	jmSvcEnv := DEFAULT_ENV_NAME

	t.Run("Should return PRODUCTION if set to production/prod", func(t *testing.T) {
		err := os.Setenv(jmSvcEnv, "PROD")
		assert.Nil(err)
		env, isProd := GetCurrentEnv(jmSvcEnv)
		assert.True(isProd)
		assert.Equal(ENV_PRODUCTION, env)

		err = os.Setenv(jmSvcEnv, "prod")
		assert.Nil(err)
		env, isProd = GetCurrentEnv(jmSvcEnv)
		assert.True(isProd)
		assert.Equal(ENV_PRODUCTION, env)

		err = os.Setenv(jmSvcEnv, "PRODUCTION")
		assert.Nil(err)
		env, isProd = GetCurrentEnv(jmSvcEnv)
		assert.Equal(ENV_PRODUCTION, env)
		assert.True(isProd)

		err = os.Setenv(jmSvcEnv, "production")
		assert.Nil(err)
		env, isProd = GetCurrentEnv(jmSvcEnv)
		assert.Equal(ENV_PRODUCTION, env)
		assert.True(isProd)
	})

	t.Run("Should return TEST if set to test", func(t *testing.T) {
		err := os.Setenv(jmSvcEnv, "TEST")
		assert.Nil(err)
		env, isProd := GetCurrentEnv(jmSvcEnv)

		assert.False(isProd)
		assert.Equal(ENV_TEST, env)

		err = os.Setenv(jmSvcEnv, "test")
		assert.Nil(err)
		env, isProd = GetCurrentEnv(jmSvcEnv)

		assert.Equal(ENV_TEST, env)
		assert.False(isProd)
	})

	t.Run("Should return DEVELOPMENT if not set or not set to test or production/prod", func(t *testing.T) {
		err := os.Setenv(jmSvcEnv, "dev")
		assert.Nil(err)
		env, isProd := GetCurrentEnv(jmSvcEnv)

		assert.False(isProd)
		assert.Equal(ENV_DEVELOPMENT, env)

		err = os.Unsetenv(jmSvcEnv)
		assert.Nil(err)
		env, isProd = GetCurrentEnv(jmSvcEnv)

		assert.Equal(ENV_DEVELOPMENT, env)
		assert.False(isProd)
	})
}

func TestEnv(t *testing.T) {
	assert := assert.New(t)

	jmSvcEnv := "JM_SVC_ENV"

	t.Run("Should return PRODUCTION if set to production/prod", func(t *testing.T) {
		err := os.Setenv(jmSvcEnv, "prod")
		assert.Nil(err)
		svc := New()
		assert.Equal(ENV_PRODUCTION, svc.Env())

		err = os.Setenv(jmSvcEnv, "production")
		assert.Nil(err)
		svc = New()
		assert.Equal(ENV_PRODUCTION, svc.Env())
	})

	t.Run("Should return TEST if set to test", func(t *testing.T) {
		err := os.Setenv(jmSvcEnv, "test")
		assert.Nil(err)
		svc := New()
		assert.Equal(ENV_TEST, svc.Env())
	})

	t.Run("Should return DEVELOPMENT if not set or not set to test or production/prod", func(t *testing.T) {
		err := os.Setenv(jmSvcEnv, "dev")
		assert.Nil(err)
		svc := New()
		assert.Equal(ENV_DEVELOPMENT, svc.Env())

		err = os.Unsetenv(jmSvcEnv)
		assert.Nil(err)
		svc = New()
		assert.Equal(ENV_DEVELOPMENT, svc.Env())
	})
}

func TestIsProd(t *testing.T) {
	assert := assert.New(t)

	jmSvcEnv := "JM_SVC_ENV"

	t.Run("Should set prod to true if JM_SVC_ENV env var is set to production/prod", func(t *testing.T) {
		err := os.Setenv(jmSvcEnv, "production")
		assert.Nil(err)
		svc := New()

		assert.True(svc.IsProd())

		err = os.Setenv(jmSvcEnv, "prod")
		assert.Nil(err)
		svc = New()
		assert.True(svc.IsProd())
	})

	t.Run("Should set prod to false if JM_SVC_ENV env var is set", func(t *testing.T) {
		err := os.Setenv(jmSvcEnv, "dev")
		assert.Nil(err)
		svc := New()

		assert.False(svc.IsProd())

		err = os.Unsetenv(jmSvcEnv)
		assert.Nil(err)
		svc = New()

		assert.False(svc.IsProd())
	})
}
