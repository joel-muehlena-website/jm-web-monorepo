package gomstoolkit

import (
	"github.com/google/uuid"

	resource_registrator "gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/resource-registrator"
)

type ConfigOptions struct {
	Name                  string
	ID                    uuid.UUID
	HttpPort              uint16
	LoggerConfig          *LoggerConfig
	ServiceConfig         *Config
	Resources             resource_registrator.Resources
	ResourceServerAddress string
}

// Type which defines a config option function
type ConfigOption func(*ConfigOptions) error

var defaultConfigOptions ConfigOptions = ConfigOptions{
	Name:                  "",
	ID:                    uuid.UUID{},
	HttpPort:              0,
	LoggerConfig:          nil,
	ServiceConfig:         nil,
	Resources:             make(resource_registrator.Resources, 0),
	ResourceServerAddress: "",
}

// Applies a give list of MicroservicOption to the optins struct
func (opt *ConfigOptions) parseOptions(options ...ConfigOption) error {
	for _, op := range options {
		err := op(opt)
		if err != nil {
			return err
		}
	}

	return nil
}

func WithHttpPort(port uint16) ConfigOption {
	return func(mo *ConfigOptions) error {
		mo.HttpPort = port
		return nil
	}
}

func WithName(name string) ConfigOption {
	return func(mo *ConfigOptions) error {
		mo.Name = name
		return nil
	}
}

func WithID(id uuid.UUID) ConfigOption {
	return func(mo *ConfigOptions) error {
		mo.ID = id
		return nil
	}
}

func WithLogger(cfg *LoggerConfig) ConfigOption {
	return func(mo *ConfigOptions) error {
		mo.LoggerConfig = cfg
		return nil
	}
}

func WithResources(resourceServerAddress string, resources resource_registrator.Resources) ConfigOption {
	return func(mo *ConfigOptions) error {
		mo.Resources = resources
		mo.ResourceServerAddress = resourceServerAddress
		return nil
	}
}

func WithServiceConfig(cfg *Config) ConfigOption {
	return func(mo *ConfigOptions) error {
		mo.ServiceConfig = cfg
		return nil
	}
}
