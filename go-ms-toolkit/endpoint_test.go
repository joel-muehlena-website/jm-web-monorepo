package gomstoolkit

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"
)

func TestGetMethodStringList(t *testing.T) {
	options := []string{
		"GET",
		"POST",
		"DELETE",
		"PATCH",
		"PUT",
		"OPTIONS",
	}

	assert.Equal(t, options, getMethodStringList())
}

func TestGetMethodString(t *testing.T) {
	t.Parallel()

	t.Run("Should return GET", func(t *testing.T) {
		assert.Equal(t, "GET", GetMethodString(GET))
	})

	t.Run("Should return POST", func(t *testing.T) {
		assert.Equal(t, "POST", GetMethodString(POST))
	})

	t.Run("Should return DELETE", func(t *testing.T) {
		assert.Equal(t, "DELETE", GetMethodString(DELETE))
	})

	t.Run("Should return PATCH", func(t *testing.T) {
		assert.Equal(t, "PATCH", GetMethodString(PATCH))
	})

	t.Run("Should return PUT", func(t *testing.T) {
		assert.Equal(t, "PUT", GetMethodString(PUT))
	})

	t.Run("Should return OPTIONS", func(t *testing.T) {
		assert.Equal(t, "OPTIONS", GetMethodString(OPTIONS))
	})

	t.Run("Should panic if option is out if range", func(t *testing.T) {
		assert.Panics(t, func() {
			GetMethodString(Method((^uint16(0) >> 1)))
		})
	})
}

type (
	testReq struct {
		Param    int    `uri:"param"`
		Test     string `form:"test" json:"test"`
		TestBind int    `form:"testBind" json:"testBind"`
	}
	testRes struct {
		Success bool   `json:"success"`
		Value   int    `json:"value"`
		Test    string `json:"test"`
	}
)

var testEndpoint = Endpoint[testReq, testRes]{
	Path:        "/test/:param",
	Handler:     nil,
	Method:      POST,
	Description: "A test function",
}

func TestEndpointInfo(t *testing.T) {
	t.Parallel()

	t.Run("Should get info", func(t *testing.T) {
		info := testEndpoint.Info(false)

		expected := fmt.Sprintf("Endpoint with path %s and HTTP Method %s", testEndpoint.Path, GetMethodString(testEndpoint.Method))

		assert.Equal(t, expected, info)
	})

	t.Run("Should get extended info", func(t *testing.T) {
		info := testEndpoint.Info(true)

		expected := fmt.Sprintf("Endpoint with path %s and HTTP Method %s. The endpoint has %s auth handler and %d authorization funcs: %s", testEndpoint.Path, GetMethodString(testEndpoint.Method), "no", 0, testEndpoint.Description)

		assert.Equal(t, expected, info)
	})

	t.Run("Should get extended info with authentication and authorization handler", func(t *testing.T) {
		endpoint := testEndpoint

		endpoint.AuthHandler = func(ctx *gin.Context) (bool, error) { return true, nil }
		endpoint.AuthorizationChain = []AuthorizationHandler{
			func(ctx *gin.Context) (bool, error) { return true, nil },
			func(ctx *gin.Context) (bool, error) { return true, nil },
		}

		info := endpoint.Info(true)

		expected := fmt.Sprintf("Endpoint with path %s and HTTP Method %s. The endpoint has %s auth handler and %d authorization funcs: %s", testEndpoint.Path, GetMethodString(testEndpoint.Method), "an", 2, testEndpoint.Description)

		assert.Equal(t, expected, info)
	})

	t.Run("Should not get extended info if description is empty", func(t *testing.T) {
		internalTestEndpoint := testEndpoint
		internalTestEndpoint.Description = ""

		info := internalTestEndpoint.Info(true)

		expected := fmt.Sprintf("Endpoint with path %s and HTTP Method %s. The endpoint has %s auth handler and %d authorization funcs", testEndpoint.Path, GetMethodString(testEndpoint.Method), "no", 0)

		assert.Equal(t, expected, info)
	})
}

func TestEndpointHit(t *testing.T) {
	gin.SetMode(gin.TestMode)
	assert := assert.New(t)
	test_param := 2

	setup := func(handler EndpointHandler[testReq, testRes], param string, body io.Reader, isJSON bool, endpoint *Endpoint[testReq, testRes]) (*httptest.ResponseRecorder, *gin.Context) {
		testEndpoint := testEndpoint

		if endpoint != nil {
			testEndpoint = *endpoint
		}

		if handler != nil {
			testEndpoint.Handler = handler
		} else {
			testEndpoint.Handler = func(r *testReq) (*testRes, int, error) {
				return nil, 500, fmt.Errorf("Default handler")
			}
		}

		if param == "" {
			param = strconv.Itoa(test_param)
		}

		w := httptest.NewRecorder()
		c, engine := gin.CreateTestContext(w)

		req := httptest.NewRequest(http.MethodPost, strings.Replace(testEndpoint.Path, ":param", param, 1), body)

		if isJSON {
			req.Header.Set("Content-Type", "application/json")
		} else {
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		}

		c.Request = req

		engine.Handle(req.Method, testEndpoint.Path, func(ctx *gin.Context) {
			testEndpoint.Hit(ctx)
		})

		engine.ServeHTTP(w, req)
		return w, c
	}

	t.Parallel()

	t.Run("Should return error", func(t *testing.T) {
		t.Run("If content type is not allowed", func(t *testing.T) {
			endpoint := testEndpoint

			endpoint.Path = "/test"

			expected := HTTPError{
				Code:    400,
				Message: "this content type is not allowed",
				Errors:  []string{fmt.Sprintf("The content type %s is not allowed for %s on %s", "application/x-www-form-urlencoded", "POST", endpoint.GetPath())},
			}

			endpoint.RestrictContentType = []string{"application/json"}

			recorder, _ := setup(nil, "", nil, false, &endpoint)
			assert.Equal(400, recorder.Code)
			hErr := HTTPError{}

			err := json.NewDecoder(recorder.Body).Decode(&hErr)
			assert.Nil(err)

			assert.Nil(deep.Equal(expected, hErr))
		})

		t.Run("Should return 500 if form parsing failed", func(t *testing.T) {
			// TODO create empty form body set request body = nil
		})

		t.Run("Should return error (401) if auth handler fails", func(t *testing.T) {
			expected := HTTPError{
				Code:    401,
				Message: "Failed to authenticate",
				Errors: []string{
					`not authenticated`,
				},
			}

			endpoint := testEndpoint

			endpoint.AuthHandler = func(ctx *gin.Context) (bool, error) { return false, fmt.Errorf("not authenticated") }

			recorder, _ := setup(nil, "", nil, false, &endpoint)
			assert.Equal(401, recorder.Code)
			hErr := HTTPError{}

			err := json.NewDecoder(recorder.Body).Decode(&hErr)
			assert.Nil(err)

			assert.Nil(deep.Equal(expected, hErr))
		})

		t.Run("Should return error (403) if authorization chain fails", func(t *testing.T) {
			expected := HTTPError{
				Code:    403,
				Message: "Failed to authorize",
				Errors: []string{
					`not authorized`,
				},
			}

			endpoint := testEndpoint

			endpoint.AuthorizationChain = []AuthorizationHandler{
				func(ctx *gin.Context) (bool, error) { return false, fmt.Errorf("not authorized") },
			}

			recorder, _ := setup(nil, "", nil, false, &endpoint)
			assert.Equal(403, recorder.Code)
			hErr := HTTPError{}

			err := json.NewDecoder(recorder.Body).Decode(&hErr)
			assert.Nil(err)

			assert.Nil(deep.Equal(expected, hErr))
		})

		t.Run("Should return 500 if failed to bind uri", func(t *testing.T) {
			expected := HTTPError{
				Code:    500,
				Message: "failed to bind the request struct on the URI",
				Errors: []string{
					`strconv.ParseInt: parsing "false_value": invalid syntax`,
				},
			}

			recorder, _ := setup(nil, "false_value", nil, false, nil)
			assert.Equal(500, recorder.Code)
			hErr := HTTPError{}

			err := json.NewDecoder(recorder.Body).Decode(&hErr)
			assert.Nil(err)

			assert.Nil(deep.Equal(expected, hErr))
		})

		t.Run("Should return 500 if failed to bind", func(t *testing.T) {
			expected := HTTPError{
				Code:    500,
				Message: "failed to bind the request struct",
				Errors: []string{
					`strconv.ParseInt: parsing "false_value": invalid syntax`,
				},
			}

			form := url.Values{}
			form.Add("testBind", "false_value")
			recorder, _ := setup(nil, "", strings.NewReader(form.Encode()), false, nil)

			assert.Equal(500, recorder.Code)
			hErr := HTTPError{}

			err := json.NewDecoder(recorder.Body).Decode(&hErr)
			assert.Nil(err)

			assert.Nil(deep.Equal(expected, hErr))
		})

		t.Run("Should return same error if is http error", func(t *testing.T) {
			expected := HTTPError{
				Code:    500,
				Message: "an error ocurred",
				Errors:  make([]string, 0),
			}

			handler := func(r *testReq) (*testRes, int, error) {
				return nil, 500, &expected
			}

			form := url.Values{}
			form.Add("test", "test_string")
			recorder, _ := setup(handler, "", strings.NewReader(form.Encode()), false, nil)

			assert.Equal(500, recorder.Code)

			hErr := HTTPError{}

			err := json.NewDecoder(recorder.Body).Decode(&hErr)
			assert.Nil(err)

			assert.Nil(deep.Equal(expected, hErr))
		})

		t.Run("Should create and return http error if is no http error", func(t *testing.T) {
			handler := func(r *testReq) (*testRes, int, error) {
				return nil, 500, fmt.Errorf("an error ocurred")
			}

			form := url.Values{}
			form.Add("test", "test_string")
			recorder, _ := setup(handler, "", strings.NewReader(form.Encode()), false, nil)

			assert.Equal(500, recorder.Code)

			expected := HTTPError{
				Code:    500,
				Message: "an error ocurred",
				Errors:  make([]string, 0),
			}

			hErr := HTTPError{}

			err := json.NewDecoder(recorder.Body).Decode(&hErr)
			assert.Nil(err)

			assert.Nil(deep.Equal(expected, hErr))
		})
	})

	t.Run("Should succeed", func(t *testing.T) {
		t.Parallel()

		t.Run("Should pass auth handler", func(t *testing.T) {
			endpoint := testEndpoint

			endpoint.AuthHandler = func(ctx *gin.Context) (bool, error) { return true, nil }

			handler := func(r *testReq) (*testRes, int, error) {
				return &testRes{Success: true}, 200, nil
			}

			recorder, _ := setup(handler, "", nil, false, &endpoint)
			assert.Equal(200, recorder.Code)
			hErr := HTTPError{}

			err := json.NewDecoder(recorder.Body).Decode(&hErr)
			assert.Nil(err)
		})

		t.Run("Should pass authorization handler", func(t *testing.T) {
			endpoint := testEndpoint

			endpoint.AuthorizationChain = []AuthorizationHandler{
				func(ctx *gin.Context) (bool, error) { return true, nil },
				func(ctx *gin.Context) (bool, error) { return true, nil },
			}

			handler := func(r *testReq) (*testRes, int, error) {
				return &testRes{Success: true}, 200, nil
			}

			recorder, _ := setup(handler, "", nil, false, &endpoint)
			assert.Equal(200, recorder.Code)
			hErr := HTTPError{}

			err := json.NewDecoder(recorder.Body).Decode(&hErr)
			assert.Nil(err)
		})

		t.Run("Should pass both auth and authorization handler types", func(t *testing.T) {
			endpoint := testEndpoint

			endpoint.AuthHandler = func(ctx *gin.Context) (bool, error) { return true, nil }

			endpoint.AuthorizationChain = []AuthorizationHandler{
				func(ctx *gin.Context) (bool, error) { return true, nil },
				func(ctx *gin.Context) (bool, error) { return true, nil },
			}

			handler := func(r *testReq) (*testRes, int, error) {
				return &testRes{Success: true}, 200, nil
			}

			recorder, _ := setup(handler, "", nil, false, &endpoint)
			assert.Equal(200, recorder.Code)
			hErr := HTTPError{}

			err := json.NewDecoder(recorder.Body).Decode(&hErr)
			assert.Nil(err)
		})

		t.Run("Should return 200 and result with form data", func(t *testing.T) {
			resp := testRes{
				Success: true,
				Value:   4,
			}

			handler := func(r *testReq) (*testRes, int, error) {
				resp.Test = r.Test

				return &resp, 200, nil
			}

			form := url.Values{}
			form.Add("test", "test_string")
			recorder, _ := setup(handler, "", strings.NewReader(form.Encode()), false, nil)

			assert.Equal(200, recorder.Code)

			type httpResponse struct {
				Code int     `json:"code"`
				Data testRes `json:"data"`
			}

			actual := httpResponse{}

			err := json.NewDecoder(recorder.Body).Decode(&actual)
			assert.Nil(err)

			assert.Equal(200, actual.Code)
			assert.Equal("test_string", actual.Data.Test)
			assert.Nil(deep.Equal(resp, actual.Data))
		})

		t.Run("Should return 200 and result with json", func(t *testing.T) {
			resp := testRes{
				Success: true,
				Value:   4,
			}

			handler := func(r *testReq) (*testRes, int, error) {
				resp.Test = r.Test

				return &resp, 200, nil
			}

			data := struct {
				Test string `json:"test"`
			}{
				Test: "test msg",
			}

			b := new(bytes.Buffer)
			json.NewEncoder(b).Encode(data)

			recorder, _ := setup(handler, "", b, true, nil)

			assert.Equal(200, recorder.Code)

			type httpResponse struct {
				Code int     `json:"code"`
				Data testRes `json:"data"`
			}

			actual := httpResponse{}

			err := json.NewDecoder(recorder.Body).Decode(&actual)
			assert.Nil(err)

			assert.Equal(200, actual.Code)
			assert.Equal("test msg", actual.Data.Test)
			assert.Nil(deep.Equal(resp, actual.Data))
		})
	})
}

func TestEndpointGetMethod(t *testing.T) {
	assert.Equal(t, testEndpoint.Method, testEndpoint.GetMethod())
}

func TestEndpointGetPath(t *testing.T) {
	assert.Equal(t, testEndpoint.Path, testEndpoint.GetPath())
}
