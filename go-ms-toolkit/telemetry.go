package gomstoolkit

import (
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/telemetry"
)

func initTelemetry(svc *Service) (*telemetry.Telemetry, error) {
	telCfg := svc.initialConfig.TelemetryConfig

	if telCfg.Name == "" {
		telCfg.Name = svc.Name
	}

	if telCfg.ID == "" {
		telCfg.ID = svc.ID.String()
	}

	tel := telemetry.New(telCfg, svc.Logger)

	err := tel.Init()

	return tel, err
}
