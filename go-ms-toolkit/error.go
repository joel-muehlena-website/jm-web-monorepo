package gomstoolkit

import (
	"fmt"

	"github.com/go-test/deep"
)

type HTTPError struct {
	Code    int      `json:"code"`
	Message string   `json:"message"`
	Errors  []string `json:"errors"`
}

func (httpError *HTTPError) Error() string {
	errorString := fmt.Sprintf("There was an HTTP Error with code %d and the message %s and the following errors: \n", httpError.Code, httpError.Message)

	for _, err := range httpError.Errors {
		errorString += fmt.Sprintf("\t %s", err)
	}

	return errorString
}

func (httpError *HTTPError) Is(target error) bool {
	httpErrorTarget, ok := target.(*HTTPError)

	if !ok {
		return false
	}

	return httpErrorTarget.Code == httpError.Code &&
		httpErrorTarget.Message == httpError.Message &&
		deep.Equal(httpErrorTarget.Errors, httpError.Errors) == nil
}

func (httpError *HTTPError) SetDefaultsForEmptyFields() {
	if httpError.Code == 0 {
		httpError.Code = 500
	}

	if httpError.Message == "" {
		httpError.Message = "Internal Server error"
	}

	if httpError.Errors == nil {
		httpError.Errors = make([]string, 0)
	}
}
