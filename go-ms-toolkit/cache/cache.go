package cache

type Cache interface {
	Init() error
	Get(key string) ([]byte, error)
	Set(key string, data []byte) error
	Has(key string) bool
	Delete(key string) error
}
