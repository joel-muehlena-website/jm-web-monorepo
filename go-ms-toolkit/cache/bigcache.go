package cache

import (
	"time"

	"github.com/allegro/bigcache/v3"
)

type BigCache struct {
	cache    *bigcache.BigCache
	eviction time.Duration
}

func NewBigCache(eviction time.Duration) *BigCache {
	cache := new(BigCache)

	cache.cache = nil
	cache.eviction = eviction

	return cache
}

func (bCache *BigCache) Init() error {
	config := bigcache.DefaultConfig(bCache.eviction)

	config.MaxEntrySize = 300

	cache, initErr := bigcache.NewBigCache(config)
	if initErr != nil {
		return initErr
	}

	bCache.cache = cache
	return nil
}

func (bCache *BigCache) Get(key string) ([]byte, error) {
	return bCache.cache.Get(key)
}

func (bCache *BigCache) Set(key string, data []byte) error {
	return bCache.cache.Set(key, data)
}

func (bCache *BigCache) Has(key string) bool {
	_, err := bCache.cache.Get(key)
	return err == nil
}

func (bCache *BigCache) Delete(key string) error {
	return bCache.cache.Delete(key)
}
