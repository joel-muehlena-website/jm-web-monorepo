package cache

import (
	"testing"
	"time"

	"github.com/allegro/bigcache/v3"
	"github.com/stretchr/testify/assert"
)

func TestNewBigCache(t *testing.T) {
}

func createTestCache(t *testing.T) *BigCache {
	cache, err := bigcache.NewBigCache(bigcache.DefaultConfig(10 * time.Minute))

	assert.Nil(t, err)

	return &BigCache{
		cache: cache,
	}
}

func TestInit(t *testing.T) {
	cache := createTestCache(t)

	err := cache.Init()
	assert.Nil(t, err)
}

type testDataT struct {
	key   string
	value string
}

var testData = []testDataT{
	{key: "test", value: "test"},
	{key: "test2", value: "test2"},
}

func TestGet(t *testing.T) {
	cache := createTestCache(t)

	for _, data := range testData {
		err := cache.cache.Set(data.key, []byte(data.value))
		assert.Nil(t, err)
		res, err := cache.Get(data.key)
		assert.Nil(t, err)
		assert.Equal(t, data.value, string(res))
	}
}

func TestSet(t *testing.T) {
	cache := createTestCache(t)

	for _, data := range testData {
		err := cache.Set(data.key, []byte(data.value))
		assert.Nil(t, err)
		res, err := cache.cache.Get(data.key)
		assert.Nil(t, err)
		assert.Equal(t, data.value, string(res))
	}
}

func TestHas(t *testing.T) {
	cache := createTestCache(t)

	type testDataT struct {
		key      string
		value    string
		expected bool
	}

	testData := []testDataT{
		{key: "test", value: "test", expected: true},
		{key: "test2", expected: false},
	}

	for _, data := range testData {
		if data.expected {
			err := cache.cache.Set(data.key, []byte(data.value))
			assert.Nil(t, err)
			res := cache.Has(data.key)
			assert.True(t, res)
		} else {
			res := cache.Has(data.key)
			assert.False(t, res)
		}
	}
}

func TestDelete(t *testing.T) {
	cache := createTestCache(t)

	for _, data := range testData {
		err := cache.Set(data.key, []byte(data.value))
		assert.Nil(t, err)
		_, err = cache.cache.Get(data.key)
		assert.Nil(t, err)
		cache.Delete(data.key)
		_, err = cache.cache.Get(data.key)
		assert.Error(t, err)
	}
}
