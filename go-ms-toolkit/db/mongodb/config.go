package mongodb

import (
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
)

type MongoDBConfig struct {
	// the main db config
	DBConfig db.DBConfig `yaml:"general"`
	URI      string      `yaml:"uri"`
}
