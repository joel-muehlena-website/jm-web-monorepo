package mongodb

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
	"go.opentelemetry.io/contrib/instrumentation/go.mongodb.org/mongo-driver/mongo/otelmongo"
	"go.uber.org/zap"
)

// TODO create more config options
func New(logger *zap.Logger, cfg MongoDBConfig) (*mongo.Client, error) {
	logger = logger.Named("mongodb")

	if cfg.DBConfig.InitialConnectionRetry == 0 {
		cfg.DBConfig.InitialConnectionRetry = 3
	}

	if cfg.DBConfig.ConnectionTimeout == 0 {
		cfg.DBConfig.ConnectionTimeout = 5
	}

	if cfg.URI == "" {
		cfg.URI = fmt.Sprintf("mongodb+srv://%s", cfg.DBConfig.Host)
	}

	apiOpts := options.ServerAPI(options.ServerAPIVersion1)
	opt := options.Client().
		ApplyURI(cfg.URI).
		SetAuth(options.Credential{
			Username: cfg.DBConfig.Username,
			Password: cfg.DBConfig.Password,
		}).
		SetRetryWrites(true).
		SetWriteConcern(writeconcern.New(writeconcern.WMajority())).
		SetServerAPIOptions(apiOpts).
		SetMonitor(otelmongo.NewMonitor())

	err := opt.Validate()
	if err != nil {
		logger.Error("Failed to create a valid mongodb config", zap.Error(err))
		return nil, err
	}

	var client *mongo.Client
	for i := 0; i < cfg.DBConfig.InitialConnectionRetry; i++ {
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(cfg.DBConfig.ConnectionTimeout)*time.Second)
		defer cancel()

		client, err = mongo.Connect(ctx, opt)
		if err != nil {
			logger.Error("Error connecting to mongodb database... Retry...", zap.Error(err))
			continue
		}

		ctxPing, cancelPing := context.WithTimeout(context.Background(), time.Duration(cfg.DBConfig.ConnectionTimeout)*time.Second)
		defer cancelPing()
		err = client.Ping(ctxPing, nil)

		if err != nil {
			logger.Error("Failed to ping the mongodb database", zap.String("host", cfg.DBConfig.Host), zap.Int("try", i+1), zap.Error(err))
			continue
		}

		break
	}

	if client == nil {
		logger.Error("Client is still nil after connecting")
		return nil, fmt.Errorf("client nil after connect")
	}

	logger.Debug("Connected to db")

	return client, err
}
