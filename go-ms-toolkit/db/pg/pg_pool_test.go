package pg

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"

	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
)

var defaultPoolCfg PostgreSQLPoolConfig = PostgreSQLPoolConfig{
	MinConns:        1,
	MaxConns:        2,
	MaxConnLifetime: time.Minute,
	MaxConnIdleTime: time.Minute,
	DBConfig: db.DBConfig{
		Username:               "testUser",
		Password:               "testPassword",
		Host:                   "testHost.com",
		InitialConnectionRetry: 2,
		ConnectionTimeout:      2,
	},
}

func TestConfigureDBPool(t *testing.T) {
	t.Parallel()

	t.Run("load tls cert", func(t *testing.T) {
		t.Run("should configure the pool to not load a certificate on default", func(t *testing.T) {
			t.Parallel()

			conn := PostgreSQLPoolConnection{Config: defaultPoolCfg}
			poolCfg := pgxpool.Config{ConnConfig: &pgx.ConnConfig{}}
			conn.configure(&poolCfg)
			assert.Nil(t, poolCfg.ConnConfig.TLSConfig)
		})

		t.Run("should configure the pool to load a certificate", func(t *testing.T) {
			t.Parallel()

			caPEM := createTestCertificate(t)

			// Create a tmp dir
			dir, err := ioutil.TempDir("./", "pemData")
			assert.Nil(t, err)
			defer os.RemoveAll(dir)

			// Create a tmp file
			file, err := ioutil.TempFile(dir, "TestSQLCA.pem")
			assert.Nil(t, err)
			defer os.Remove(file.Name())
			fileName := file.Name()

			// write the test pem to that file
			_, err = file.Write(caPEM.Bytes())
			assert.Nil(t, err)
			file.Close()

			conn := PostgreSQLPoolConnection{Config: defaultPoolCfg}
			conn.Config.CAPath = fileName
			poolCfg := pgxpool.Config{ConnConfig: &pgx.ConnConfig{}}
			conn.configure(&poolCfg)

			CACert, err := ioutil.ReadFile(fileName)
			assert.Nil(t, err)

			CACertPool := x509.NewCertPool()
			CACertPool.AppendCertsFromPEM(CACert)

			tlsConfig := &tls.Config{
				RootCAs:            CACertPool,
				InsecureSkipVerify: true,
			}

			assert.Nil(t, deep.Equal(*tlsConfig, *poolCfg.ConnConfig.TLSConfig))
		})

		t.Run("should return error for loading cert", func(t *testing.T) {
			t.Parallel()

			// Create a tmp dir
			dir, err := ioutil.TempDir("./", "pemData")
			assert.Nil(t, err)
			defer os.RemoveAll(dir)

			conn := PostgreSQLPoolConnection{Config: defaultPoolCfg}
			conn.Config.CAPath = dir + "/testFile.pem"
			poolCfg := pgxpool.Config{ConnConfig: &pgx.ConnConfig{}}

			err = conn.configure(&poolCfg)
			assert.NotNil(t, err)
		})
	})
}

func TestDBConnection(t *testing.T) {
	t.Parallel()

	t.Run("should connect to postgres", func(t *testing.T) {
		if os.Getenv("POSTGRES_TEST_ENABLED") != "true" {
			t.Skip("Postgres testing not enabled. Please set the env var POSTGRES_TEST_ENABLED to `true`. You need also to set the postgres driver env variables:\n\t- PGHOST\n\t- PGPORT\n\t- PGDATABASE\n\t- PGUSER\n")
			return
		}

		var buffer bytes.Buffer
		logger, writer := createTestLogger(&buffer)

		conn := NewPool(logger, defaultPoolCfg)
		assert.Nil(t, conn.Connect())
		writer.Flush()
	})

	t.Run("should fail connect to postgres", func(t *testing.T) {
		t.Parallel()

		var buffer bytes.Buffer
		logger, writer := createTestLogger(&buffer)

		conn := NewPool(logger, defaultPoolCfg)

		assert.NotNil(t, conn.Connect())
		writer.Flush()
	})

	t.Run("should create pool if lazy connect is enabled", func(t *testing.T) {
		t.Parallel()

		var buffer bytes.Buffer
		logger, writer := createTestLogger(&buffer)

		conn := NewPool(logger, defaultPoolCfg)
		conn.Config.LazyConnect = true
		assert.Nil(t, conn.Connect())
		writer.Flush()
	})

	t.Run("should create pool if lazy connect fallback is enabled", func(t *testing.T) {
		t.Parallel()

		var buffer bytes.Buffer
		logger, writer := createTestLogger(&buffer)

		conn := NewPool(logger, defaultPoolCfg)
		conn.Config.LazyConnectFallback = true
		assert.Nil(t, conn.Connect())
		writer.Flush()
	})
}
