package pg

import (
	"context"
	"errors"

	"github.com/jackc/pgx/v5"
	"go.uber.org/zap"

	gomstoolkit_db "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
)

// Usually used for defer the rollback of a transaction
func TxRollback(baseCtx context.Context, tx pgx.Tx) {
	ctx, cancel := context.WithTimeout(baseCtx, gomstoolkit_db.DB_CTX_DEFAULT_TIMEOUT)
	err := tx.Rollback(ctx)
	if err != nil {
		if !errors.Is(err, pgx.ErrTxClosed) {
			zap.L().Error("Failed to rollback tx", zap.Error(err))
		}
	}

	cancel()
}
