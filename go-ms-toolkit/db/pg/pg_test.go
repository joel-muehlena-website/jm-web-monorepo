package pg

import (
	"bufio"
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func TestCreateConnectionString(t *testing.T) {
	cfg := defaultPoolCfg

	t.Parallel()

	t.Run("should create string with just username password and host", func(t *testing.T) {
		t.Parallel()

		s := createPostgreSQLConnectionString(&cfg.DBConfig, "")

		assert.Contains(t, s, "user="+cfg.DBConfig.Username)
		assert.Contains(t, s, "password="+cfg.DBConfig.Password)
		assert.Contains(t, s, "host="+cfg.DBConfig.Host)
	})

	t.Run("should create string with just username password host and port", func(t *testing.T) {
		t.Parallel()

		cfg := cfg
		cfg.DBConfig.Port = 2345

		s := createPostgreSQLConnectionString(&cfg.DBConfig, "")

		assert.Contains(t, s, "user="+cfg.DBConfig.Username)
		assert.Contains(t, s, "password="+cfg.DBConfig.Password)
		assert.Contains(t, s, "host="+cfg.DBConfig.Host)
		assert.Contains(t, s, "port="+strconv.Itoa(int(cfg.DBConfig.Port)))
	})

	t.Run("should create string with just username password host and database", func(t *testing.T) {
		t.Parallel()

		cfg := cfg
		cfg.DBConfig.Database = "testdb"

		s := createPostgreSQLConnectionString(&cfg.DBConfig, "")

		assert.Contains(t, s, "user="+cfg.DBConfig.Username)
		assert.Contains(t, s, "password="+cfg.DBConfig.Password)
		assert.Contains(t, s, "host="+cfg.DBConfig.Host)
		assert.Contains(t, s, "dbname="+cfg.DBConfig.Database)
	})

	t.Run("should create string with just username password host and sslmode", func(t *testing.T) {
		t.Parallel()

		cfg := cfg
		cfg.CAPath = "./testCA.pem"

		s := createPostgreSQLConnectionString(&cfg.DBConfig, cfg.CAPath)

		assert.Contains(t, s, "user="+cfg.DBConfig.Username)
		assert.Contains(t, s, "password="+cfg.DBConfig.Password)
		assert.Contains(t, s, "host="+cfg.DBConfig.Host)
		assert.Contains(t, s, "sslmode=verify-full")
	})

	t.Run("should create string with all", func(t *testing.T) {
		t.Parallel()

		cfg := cfg
		cfg.DBConfig.Port = 2345
		cfg.DBConfig.Database = "testdb"
		cfg.CAPath = "./testCA.pem"

		s := createPostgreSQLConnectionString(&cfg.DBConfig, cfg.CAPath)

		assert.Contains(t, s, "user="+cfg.DBConfig.Username)
		assert.Contains(t, s, "password="+cfg.DBConfig.Password)
		assert.Contains(t, s, "host="+cfg.DBConfig.Host)
		assert.Contains(t, s, "port="+strconv.Itoa(int(cfg.DBConfig.Port)))
		assert.Contains(t, s, "dbname="+cfg.DBConfig.Database)
		assert.Contains(t, s, "sslmode=verify-full")
	})
}

func TestDBConnErr(t *testing.T) {
	t.Parallel()

	var buffer bytes.Buffer

	logger, writer := createTestLogger(&buffer)

	t.Run("should panic on called", func(t *testing.T) {
		t.Parallel()

		dbConnErr(logger, "Test Error", fmt.Errorf("An error occured"))
		writer.Flush()
		fmt.Printf("buffer: %s\n", buffer.String())
	})
}

func createTestCertificate(t *testing.T) *bytes.Buffer {
	ca := &x509.Certificate{
		SerialNumber: big.NewInt(2019),
		Subject: pkix.Name{
			Organization:  []string{"Company, INC."},
			Country:       []string{"US"},
			Province:      []string{""},
			Locality:      []string{"San Francisco"},
			StreetAddress: []string{"Golden Gate Bridge"},
			PostalCode:    []string{"94016"},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(10, 0, 0),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
	}

	caPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	assert.Nil(t, err)

	caBytes, err := x509.CreateCertificate(rand.Reader, ca, ca, &caPrivKey.PublicKey, caPrivKey)
	assert.Nil(t, err)

	caPEM := new(bytes.Buffer)
	pem.Encode(caPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: caBytes,
	})

	return caPEM
}

func createTestLogger(buffer *bytes.Buffer) (*zap.SugaredLogger, *bufio.Writer) {
	encoder := zapcore.NewConsoleEncoder(zap.NewDevelopmentEncoderConfig())
	writer := bufio.NewWriter(buffer)
	logger := zap.New(zapcore.NewCore(encoder, zapcore.AddSync(writer), zap.DebugLevel))

	return logger.Sugar(), writer
}
