package pg

import (
	"time"

	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
)

type PostgreSQLPoolConfig struct {
	// the main db config
	DBConfig db.DBConfig `yaml:"general"`

	MinConns        int32 `yaml:"minConns"`
	MaxConns        int32 `yaml:"maxConns"`
	MaxConnLifetime time.Duration
	MaxConnIdleTime time.Duration

	// Path to a ca file for tls connection to the database
	CAPath string `yaml:"caPath"`
}
