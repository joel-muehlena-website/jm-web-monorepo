package pg

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/jackc/pgx/v5/tracelog"
	pgxUUID "github.com/vgarvardt/pgx-google-uuid/v5"
	"go.uber.org/zap"
)

type PgxPoolIface interface {
	Query(context.Context, string, ...interface{}) (pgx.Rows, error)
	QueryRow(context.Context, string, ...interface{}) pgx.Row
	Acquire(ctx context.Context) (*pgxpool.Conn, error)
	Begin(ctx context.Context) (pgx.Tx, error)
	BeginTx(ctx context.Context, txOptions pgx.TxOptions) (pgx.Tx, error)
	Exec(ctx context.Context, sql string, arguments ...any) (commandTag pgconn.CommandTag, err error)
	Close()
}

type PostgreSQLPoolConnection struct {
	DBPool PgxPoolIface
	Config PostgreSQLPoolConfig
	Logger *zap.Logger
}

func NewPool(logger *zap.Logger, cfg PostgreSQLPoolConfig) *PostgreSQLPoolConnection {
	poolConn := new(PostgreSQLPoolConnection)

	logger = logger.Named("pg-pool")

	poolConn.Logger = logger
	poolConn.Config = cfg
	poolConn.DBPool = nil
	return poolConn
}

func (poolConn *PostgreSQLPoolConnection) configure(poolCfg *pgxpool.Config) error {
	if poolConn.Config.MinConns == 0 {
		poolCfg.MinConns = 2
	} else {
		poolCfg.MinConns = poolConn.Config.MinConns
	}

	if poolConn.Config.MaxConns == 0 {
		poolCfg.MaxConns = 10
	} else {
		poolCfg.MaxConns = poolConn.Config.MaxConns
	}

	if poolConn.Config.MaxConnLifetime == 0 {
		poolCfg.MaxConnLifetime = 15 * time.Minute
	} else {
		poolCfg.MaxConnLifetime = poolConn.Config.MaxConnLifetime
	}

	if poolConn.Config.MaxConnIdleTime == 0 {
		poolCfg.MaxConnIdleTime = 30 * time.Minute
	} else {
		poolCfg.MaxConnIdleTime = poolConn.Config.MaxConnIdleTime
	}

	// poolCfg.LazyConnect = poolConn.Config.LazyConnect

	if poolConn.Config.CAPath != "" {
		tlsConfig, err := createTLSConfig(poolConn.Config.CAPath)
		if err != nil {
			return err
		}

		poolCfg.ConnConfig.Config.TLSConfig = tlsConfig
	}

	poolCfg.AfterConnect = func(c1 context.Context, c2 *pgx.Conn) error {
		pgxUUID.Register(c2.TypeMap())
		poolConn.Logger.Info("Connected to db")
		return nil
	}

	poolCfg.BeforeConnect = func(c context.Context, cc *pgx.ConnConfig) error {
		poolConn.Logger.Info("Trying to establish a database connection")
		return nil
	}

	return nil
}

// Create a connection pool for the postgresql database
func (poolConn *PostgreSQLPoolConnection) Connect() error {
	poolCfg, err := pgxpool.ParseConfig(createPostgreSQLConnectionString(&poolConn.Config.DBConfig, poolConn.Config.CAPath))

	logger := poolConn.Logger

	poolCfg.ConnConfig.Tracer = &MultiQueryTracer{
		Tracers: []pgx.QueryTracer{NewZapLogTracer(logger, tracelog.LogLevelWarn), NewOTELTracer()},
	}

	if err != nil {
		logger.Error("Failed to create database cfg", zap.Error(err))
		return err
	}

	err = poolConn.configure(poolCfg)
	if err != nil {
		logger.Error("Failed to configure database", zap.Error(err))
		return err
	}

	connectionTimeout := time.Second * time.Duration(poolConn.Config.DBConfig.ConnectionTimeout)
	dbpool, err := connectToPostgreSQLPool(connectionTimeout, poolCfg)
	poolConn.DBPool = dbpool

	ctx, cancel := context.WithTimeout(context.Background(), connectionTimeout)
	defer cancel()
	con, err := poolConn.DBPool.Acquire(ctx)
	if err != nil {
		dbpool.Close()
		return fmt.Errorf("Failed to aquire conn: %w", err)
	}

	ctx, cancel = context.WithTimeout(context.Background(), connectionTimeout)
	defer cancel()
	err = con.Ping(ctx)

	if err != nil {
		return fmt.Errorf("Failed to ping conn: %w", err)
	}

	con.Release()

	return nil
}

func connectToPostgreSQLPool(timeout time.Duration, cfg *pgxpool.Config) (*pgxpool.Pool, error) {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	dbpool, err := pgxpool.NewWithConfig(ctx, cfg)
	defer cancel()
	return dbpool, err
}
