package pg

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"strconv"

	"github.com/jackc/pgx/v5/pgconn"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
)

// create the connection string for the pgx driver
func createPostgreSQLConnectionString(dbConfig *db.DBConfig, caPath string) string {
	connString := fmt.Sprintf(
		"user=%s password=%s host=%s",
		dbConfig.Username,
		dbConfig.Password,
		dbConfig.Host,
	)

	if dbConfig.Port != 0 {
		connString += " port=" + strconv.Itoa(int(dbConfig.Port))
	}

	if dbConfig.Database != "" {
		connString += " dbname=" + dbConfig.Database
	}

	if caPath != "" {
		connString += " sslmode=verify-full"
	}

	return connString
}

// loads the pem certificate from the given path and creates a tls.Config with it as CertPool
func createTLSConfig(certPath string) (*tls.Config, error) {
	CACert, err := ioutil.ReadFile(certPath)
	if err != nil {
		return nil, fmt.Errorf("failed to load server certificate: %w", err)
	}

	CACertPool := x509.NewCertPool()
	CACertPool.AppendCertsFromPEM(CACert)

	tlsConfig := &tls.Config{
		RootCAs:            CACertPool,
		InsecureSkipVerify: true,
	}

	return tlsConfig, nil
}

func dbConnErr(logger *zap.Logger, msg string, err error) {
	logger.Error(msg, zap.Error(err), zap.Bool("isTimeout", pgconn.Timeout(err)))
}
