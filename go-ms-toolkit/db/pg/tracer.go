package pg

import (
	"context"

	"github.com/exaring/otelpgx"
	zapadapter "github.com/jackc/pgx-zap"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/tracelog"
	"go.uber.org/zap"
)

type MultiQueryTracer struct {
	Tracers []pgx.QueryTracer
}

func (m *MultiQueryTracer) TraceQueryStart(ctx context.Context, conn *pgx.Conn, data pgx.TraceQueryStartData) context.Context {
	for _, t := range m.Tracers {
		ctx = t.TraceQueryStart(ctx, conn, data)
	}

	return ctx
}

func (m *MultiQueryTracer) TraceQueryEnd(ctx context.Context, conn *pgx.Conn, data pgx.TraceQueryEndData) {
	for _, t := range m.Tracers {
		t.TraceQueryEnd(ctx, conn, data)
	}
}

func NewOTELTracer() pgx.QueryTracer {
	return otelpgx.NewTracer()
}

func NewZapLogTracer(logger *zap.Logger, lvl tracelog.LogLevel) pgx.QueryTracer {
	zapLogger := zapadapter.NewLogger(logger)

	return &tracelog.TraceLog{
		Logger:   zapLogger,
		LogLevel: lvl,
	}
}
