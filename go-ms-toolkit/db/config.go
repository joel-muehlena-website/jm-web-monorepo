package db

import (
	"sync"
	"time"
)

const DB_CTX_DEFAULT_TIMEOUT = 7 * time.Second

type DBConfig struct {
	Username               string `yaml:"username"`
	Password               string `yaml:"password"`
	Port                   uint16 `yaml:"port"`
	Host                   string `yaml:"host"`
	Database               string `yaml:"database"`
	ConnectionTimeout      int    `yaml:"connectionTimeout"`
	InitialConnectionRetry int    `yaml:"initialConnectionRetry"`
	PanicOnFail            bool   `yaml:"panicOnFail"` // TODO implement in PG
}

var (
	dbMutex          sync.Mutex
	supportedDBTypes map[string]bool = map[string]bool{"postgresql": true}
)

func SupportedDBTypes() []string {
	ret := make([]string, len(supportedDBTypes))

	for key := range supportedDBTypes {
		ret = append(ret, key)
	}

	return ret
}
