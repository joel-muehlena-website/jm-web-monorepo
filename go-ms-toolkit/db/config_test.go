package db

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSupportedDBTypes(t *testing.T) {
	types := SupportedDBTypes()

	shouldContain := []string{"postgresql"}

	for _, key := range shouldContain {
		found := false
		for _, actual := range types {
			if actual == key {
				found = true
				break
			}
		}

		assert.True(t, found, "Expected to contain %s", key)
	}
}
