package middleware

import (
	"context"

	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
)

const (
	setProtoKey = "X-Proto"

	setProtoKeyId gomstoolkit.EndpointContextKey = gomstoolkit.EndpointContextKey(setProtoKey)
)

func SetProto(ctx context.Context, name string) context.Context {
	return context.WithValue(ctx, setProtoKeyId, name)
}

func Proto(ctx context.Context) string {
	if ctx.Value(setProtoKeyId) == nil {
		return ""
	}

	return ctx.Value(setProtoKeyId).(string)
}
