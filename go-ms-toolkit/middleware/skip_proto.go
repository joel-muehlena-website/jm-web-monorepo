package middleware

import (
	"context"

	"go.uber.org/zap"

	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/log"
)

const (
	skipProtoKey = "X-Proto-Skipped"

	skipProtoKeyId gomstoolkit.EndpointContextKey = gomstoolkit.EndpointContextKey(skipProtoKey)
)

func SkipProto(ctx context.Context, name string) context.Context {
	// FIXME: This is a hack to allow bypassing the authorization chain for grpc calls or other protocols
	if Proto(ctx) == name {
		logger := log.NewZapTraceLinkLogger(zap.L())
		logger.Ctx(ctx).Warn("Skipping protocol and bypassing authorization chain for repository depending implementations.", zap.String("protocol", name))

		ctx := context.WithValue(ctx, UserPermissionsKeyEndpoint, []string{"Skipped-By-Proto-Check"})

		return context.WithValue(ctx, skipProtoKeyId, name)
	}

	return ctx
}
