package middleware

import (
	"context"
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"go.uber.org/zap"

	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/log"
)

const (
	userIDKey            = "X-User-ID"
	userAuthenticatedKey = "X-User-Authenticated"

	userIdKeyEndpoint gomstoolkit.EndpointContextKey = gomstoolkit.EndpointContextKey(userIDKey)
)

var ErrNotAuthenticated = fmt.Errorf("not authenticated and therefore not authorized")

func IsAuthenticated(ctx context.Context, ginCtx *gin.Context) (context.Context, bool, error) {
	logger := log.NewZapTraceLinkLogger(zap.L().Named("http-middleware.is_authenticated")).Ctx(ctx)

	if ginCtx.GetHeader(userAuthenticatedKey) != "true" {
		logger.Debug("User not authenticated: no user authenticated header set")
		return ctx, false, ErrNotAuthenticated
	}

	if ginCtx.GetHeader(userIDKey) == "" {
		logger.Debug("User not authenticated: no user id provided in header")
		return ctx, false, ErrNotAuthenticated
	}

	ctx = context.WithValue(ctx, userIdKeyEndpoint, ginCtx.GetHeader(userIDKey))

	logger.Debug("User authenticated", zap.String("user-id", ginCtx.GetHeader(userIDKey)))
	return ctx, true, nil
}

func GetUserIdFromCtx(ctx context.Context) uuid.UUID {
	return uuid.MustParse(ctx.Value(userIdKeyEndpoint).(string))
}
