package middleware

import (
	"context"
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/samber/lo"
	"go.uber.org/zap"

	gomstoolkit "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/log"
)

const (
	UserPermissionsKey                                        = "X-Permissions-Verified-For"
	UserPermissionsKeyEndpoint gomstoolkit.EndpointContextKey = gomstoolkit.EndpointContextKey(UserPermissionsKey)

	UserShouldBeSelfKey                                        = "X-User-Should-Be-Self-By"
	UserShouldBeSelfKeyEndpoint gomstoolkit.EndpointContextKey = gomstoolkit.EndpointContextKey(UserShouldBeSelfKey)
)

func IsPermittedForRole(role string) gomstoolkit.AuthorizationHandler {
	return func(ctx context.Context, ginCtx *gin.Context) (context.Context, bool, error) {
		logger := log.NewZapTraceLinkLogger(zap.L().Named("http-middleware.is_permitted_for_role")).Ctx(ctx)

		if !lo.Contains(strings.Split(ginCtx.GetHeader(UserPermissionsKey), " "), role) {
			logger.Debug("User not permitted for role", zap.String("role", role), zap.String("permissions", ginCtx.GetHeader(UserPermissionsKey)))
			return ctx, false, fmt.Errorf("wrong permissions verified, expected to contain %s but got %s", role, ginCtx.GetHeader(UserPermissionsKey))
		}

		ctx = context.WithValue(ctx, UserPermissionsKeyEndpoint, []string{role})
		logger.Debug("User permitted for role", zap.String("role", role))

		return ctx, true, nil
	}
}

func IsPermittedForAtLeastOnOfRoles(roles ...string) gomstoolkit.AuthorizationHandler {
	return func(ctx context.Context, ginCtx *gin.Context) (context.Context, bool, error) {
		logger := log.NewZapTraceLinkLogger(zap.L().Named("http-middleware.is_permitted_for_role")).Ctx(ctx)
		for _, role := range roles {
			if lo.Contains(strings.Split(ginCtx.GetHeader(UserPermissionsKey), " "), role) {
				logger.Debug("User permitted for role", zap.String("role", role), zap.Strings("ofRoles", roles))
				ctx = context.WithValue(ctx, UserPermissionsKeyEndpoint, []string{role})
				return ctx, true, nil
			}
		}

		logger.Debug("User not permitted for any of the roles", zap.Strings("roles", roles), zap.String("permissions", ginCtx.GetHeader(UserPermissionsKey)))
		return ctx, false, fmt.Errorf("wrong permissions verified, expected to contain on of [%s] but got %s", strings.Join(roles, ", "), ginCtx.GetHeader(UserPermissionsKey))
	}
}

func IsPermittedForAllOfRoles(roles ...string) gomstoolkit.AuthorizationHandler {
	return func(ctx context.Context, ginCtx *gin.Context) (context.Context, bool, error) {
		logger := log.NewZapTraceLinkLogger(zap.L().Named("http-middleware.is_permitted_for_all_roles")).Ctx(ctx)
		for _, role := range roles {
			if !lo.Contains(strings.Split(ginCtx.GetHeader(UserPermissionsKey), " "), role) {
				logger.Debug("User not permitted for one of roles", zap.Strings("ofRoles", roles), zap.String("role", role), zap.String("permissions", ginCtx.GetHeader(UserPermissionsKey)))
				return ctx, false, fmt.Errorf("wrong permissions verified, expected to contain all of [%s] but got %s", strings.Join(roles, ", "), ginCtx.GetHeader(UserPermissionsKey))
			}
		}

		ctx = context.WithValue(ctx, UserPermissionsKeyEndpoint, roles)
		logger.Debug("User permitted for all of the roles", zap.Strings("roles", roles))

		return ctx, true, nil
	}
}

func ShouldBeSelf(key string) gomstoolkit.AuthorizationHandler {
	return func(ctx context.Context, ginCtx *gin.Context) (context.Context, bool, error) {
		logger := log.NewZapTraceLinkLogger(zap.L().Named("http-middleware.should_be_self")).Ctx(ctx)
		logger.Debug("User should be himself", zap.String("key", key))
		return context.WithValue(ctx, UserShouldBeSelfKeyEndpoint, key), true, nil
	}
}

func Or(evaluateAll bool, handlers ...gomstoolkit.AuthorizationHandler) gomstoolkit.AuthorizationHandler {
	return func(ctx context.Context, ginCtx *gin.Context) (context.Context, bool, error) {
		logger := log.NewZapTraceLinkLogger(zap.L().Named("http-middleware.or").With(zap.Bool("evaluateAll", evaluateAll))).Ctx(ctx)

		atLeastOneOk := false
		updatedContext := ctx
		for i, handler := range handlers {
			newCtx, ok, err := handler(updatedContext, ginCtx)
			if ok && !evaluateAll {
				logger.Debug("At least one handler returned ok", zap.Int("index", i))
				return newCtx, ok, err
			} else if ok && evaluateAll {
				logger.Debug("At least one handler returned ok", zap.Int("index", i))
				atLeastOneOk = true
				updatedContext = newCtx
			}
		}

		if atLeastOneOk {
			logger.Debug("Or successful")
			return updatedContext, true, nil
		}

		logger.Debug("Or failed")
		return updatedContext, false, nil
	}
}
