package gomstoolkit

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"golang.org/x/exp/constraints"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"
)

func TestDefaultFetchConfig(t *testing.T) {
	cfg := DefaultFetchConfig()

	assert.NotNil(t, cfg)
	assert.Nil(t, deep.Equal(*cfg, defaultFetchConfig))

	t.Log(cfg)
	t.Log(&defaultFetchConfig)

	if cfg == &defaultFetchConfig {
		t.Log("Expected structs to have different addresses")
		t.FailNow()
	}
}

func TestFetch(t *testing.T) {
	assert := assert.New(t)

	createTestServer := func(handler http.Handler) *httptest.Server {
		s := httptest.NewServer(handler)
		return s
	}

	t.Run("Should fetch with default config if cfg is nil", func(t *testing.T) {
		testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			assert.Equal("GET", r.Method)
		}))
		defer testServer.Close()

		Fetch[Noop](context.Background(), testServer.URL, nil)
	})

	t.Run("Should fetch with config if cfg is not nil", func(t *testing.T) {
		testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			assert.Equal("POST", r.Method)
			assert.Equal("header", r.Header.Get("test"))
		}))
		defer testServer.Close()

		Fetch[Noop](context.Background(), testServer.URL, &FetchConfig{Method: "POST", Header: map[string]string{"test": "header"}})
	})

	t.Run("Should fetch correctly", func(t *testing.T) {
		t.Run("GET", func(t *testing.T) {
			testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				assert.Equal("GET", r.Method)
			}))
			defer testServer.Close()

			Fetch[Noop](context.Background(), testServer.URL, &FetchConfig{Method: "GET"})
		})

		t.Run("POST", func(t *testing.T) {
			testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				assert.Equal("POST", r.Method)
			}))
			defer testServer.Close()

			Fetch[Noop](context.Background(), testServer.URL, &FetchConfig{Method: "POST"})
		})

		t.Run("DELETE", func(t *testing.T) {
			testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				assert.Equal("DELETE", r.Method)
			}))
			defer testServer.Close()

			Fetch[Noop](context.Background(), testServer.URL, &FetchConfig{Method: "DELETE"})
		})
	})

	type testResponse struct {
		Value int    `json:"value" xml:"value"`
		Msg   string `json:"msg" xml:"msg"`
	}

	tst := testResponse{
		Value: 2,
		Msg:   "Msg",
	}

	t.Run("Should parse response", func(t *testing.T) {
		t.Run("JSON", func(t *testing.T) {
			testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Content-Type", HTTP_CONTENT_HEADER_JSON)

				err := json.NewEncoder(w).Encode(tst)
				assert.Nil(err)
			}))
			defer testServer.Close()

			res, fRes, err := Fetch[testResponse](context.Background(), testServer.URL, nil)
			assert.Nil(err)
			assert.Nil(deep.Equal(&tst, res))
			assert.Equal(200, fRes.StatusCode)
		})

		t.Run("XML", func(t *testing.T) {
			testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Content-Type", HTTP_CONTENT_HEADER_XML)

				err := xml.NewEncoder(w).Encode(tst)
				assert.Nil(err)
			}))
			defer testServer.Close()

			res, fRes, err := Fetch[testResponse](context.Background(), testServer.URL, nil)
			assert.Nil(err)
			assert.Nil(deep.Equal(&tst, res))
			assert.Equal(200, fRes.StatusCode)
		})
	})

	t.Run("Should return error", func(t *testing.T) {
		t.Run("If new request fails", func(t *testing.T) {
			invalidURL := "http://test.de:%%"
			_, _, err := Fetch[Noop](context.Background(), invalidURL, nil)
			assert.NotNil(err)
			assert.Contains(err.Error(), "invalid port \":%%\" after host")
		})

		t.Run("If do fails", func(t *testing.T) {
			invalidURL := "http://novalid.loca:34532"
			_, _, err := Fetch[Noop](context.Background(), invalidURL, nil)
			assert.NotNil(err)
			assert.Contains(err.Error(), "no such host")
		})

		t.Run("If copy body fails", func(t *testing.T) {
			// Not quite sure yet how to test properly
		})

		t.Run("If decoding fails", func(t *testing.T) {
			testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Content-Type", HTTP_CONTENT_HEADER_JSON)

				w.Write([]byte(`{"name":what}`))
			}))
			defer testServer.Close()

			_, _, err := Fetch[testResponse](context.Background(), testServer.URL, nil)
			assert.NotNil(err)
			assert.Contains(err.Error(), "invalid character 'w'")
		})

		t.Run("If status code is non 200 (should contain FetchResponse)", func(t *testing.T) {
			t.Parallel()

			codes := []int{400, 500}

			for _, code := range codes {
				testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(code)
					w.Write([]byte("Error"))
				}))
				defer testServer.Close()

				_, fRes, err := Fetch[testResponse](context.Background(), testServer.URL, nil)
				assert.NotNil(err)
				assert.True(errors.Is(err, ErrRequestNon200))
				assert.Equal(code, fRes.StatusCode)
			}
		})
	})
}

type testFetchSetter struct {
	TestS       string
	FailSetData bool
}

func (s *testFetchSetter) SetData(r io.ReadCloser) error {
	if s.FailSetData {
		return fmt.Errorf("fail set data")
	}

	res, err := io.ReadAll(r)
	if err != nil {
		return err
	}

	s.TestS = string(res)

	return nil
}

func TestFetchWithBinding(t *testing.T) {
	assert := assert.New(t)
	text := "Text"

	createTestServer := func(handler http.Handler) *httptest.Server {
		s := httptest.NewServer(handler)
		return s
	}

	t.Run("Should set data with custom binding func", func(t *testing.T) {
		testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("Content-Type", HTTP_CONTENT_HEADER_TEXT)

			w.Write([]byte(text))
		}))
		defer testServer.Close()

		testSetter := testFetchSetter{}

		_, err := FetchWithBinding(context.Background(), &testSetter, testServer.URL, nil)

		assert.Nil(err)

		assert.Equal(text, testSetter.TestS)
	})

	t.Run("Should still have the BodyRaw set correctly", func(t *testing.T) {
		testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.Header().Add("Content-Type", HTTP_CONTENT_HEADER_TEXT)

			w.Write([]byte(text))
		}))
		defer testServer.Close()

		testSetter := testFetchSetter{}

		fRes, err := FetchWithBinding(context.Background(), &testSetter, testServer.URL, nil)
		assert.Nil(err)

		bdy, err := io.ReadAll(fRes.BodyRaw)
		assert.Nil(err)
		assert.Equal(text, string(bdy))
	})

	t.Run("Should return error", func(t *testing.T) {
		t.Run("If fetch fail", func(t *testing.T) {
			invalidURL := "http://test.de:%%"

			testSetter := testFetchSetter{}

			_, err := FetchWithBinding(context.Background(), &testSetter, invalidURL, nil)
			assert.NotNil(err)
			assert.Contains(err.Error(), "invalid port \":%%\" after host")
		})

		t.Run("If set data fails", func(t *testing.T) {
			testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Content-Type", HTTP_CONTENT_HEADER_TEXT)

				w.Write([]byte(text))
			}))
			defer testServer.Close()

			testSetter := testFetchSetter{
				FailSetData: true,
			}

			_, err := FetchWithBinding(context.Background(), &testSetter, testServer.URL, nil)

			assert.NotNil(err)

			assert.Contains(err.Error(), "fail set data")
		})

		t.Run("If copyBody fails", func(t *testing.T) {
			// Not quite sure yet how to test properly
		})
	})
}

type testReader struct {
	failClose bool
	failRead  bool
	readVal   string
}

func (r *testReader) Close() error {
	if r.failClose {
		return fmt.Errorf("Fail close")
	}

	return nil
}

func min[T constraints.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}

func (r *testReader) Read(b []byte) (n int, err error) {
	if r.failRead {
		return 0, fmt.Errorf("Fail read")
	}

	minVal := min(len(r.readVal), len(b))

	for i := 0; i < minVal; i++ {
		b[i] = r.readVal[i]
	}
	return minVal, io.EOF
}

func TestCopyBody(t *testing.T) {
	assert := assert.New(t)

	t.Run("Should return no body if body is nil", func(t *testing.T) {
		r1, r2, err := copyBody(nil)

		assert.Nil(err)
		assert.Equal(http.NoBody, r1)
		assert.Equal(http.NoBody, r2)
	})

	t.Run("Should copy the body and return two reader with the same bytes", func(t *testing.T) {
		val := "Test"
		buf := bytes.NewBufferString(val)
		r1, r2, err := copyBody(io.NopCloser(buf))

		assert.Nil(err)

		r1Res, err := io.ReadAll(r1)
		assert.Nil(err)
		assert.Equal(val, string(r1Res))

		r2Res, err := io.ReadAll(r2)
		assert.Nil(err)
		assert.Equal(val, string(r2Res))
	})

	t.Run("Should return error", func(t *testing.T) {
		t.Run("While reading reader", func(t *testing.T) {
			tr := &testReader{failRead: true}

			r1, r2, err := copyBody(tr)

			assert.Nil(r1)
			assert.Equal(tr, r2)
			assert.NotNil(err)
			assert.Contains(strings.ToLower(err.Error()), "fail read")
		})

		t.Run("While closing reader", func(t *testing.T) {
			tr := &testReader{failClose: true}

			r1, r2, err := copyBody(tr)

			assert.Nil(r1)
			assert.Equal(tr, r2)
			assert.NotNil(err)
			assert.Contains(strings.ToLower(err.Error()), "fail close")
		})
	})
}
