package gomstoolkit

import (
	"errors"
	"fmt"
	"testing"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"
)

func TestHttpErrorSetDefaultFields(t *testing.T) {
	t.Parallel()

	t.Run("Should set all fields", func(t *testing.T) {
		err := &HTTPError{}
		err.SetDefaultsForEmptyFields()

		assert.Nil(t, deep.Equal(err, &HTTPError{
			Code:    500,
			Errors:  make([]string, 0),
			Message: "Internal Server error",
		}))
	})

	t.Run("Should set specific field", func(t *testing.T) {
		t.Parallel()

		t.Run("Should set only Code", func(t *testing.T) {
			msg := "Message"
			errs := []string{"An error"}

			err := &HTTPError{Message: msg, Errors: errs}
			err.SetDefaultsForEmptyFields()

			assert.Nil(t, deep.Equal(err, &HTTPError{
				Code:    500,
				Errors:  errs,
				Message: msg,
			}))
		})

		t.Run("Should set only message", func(t *testing.T) {
			code := 300
			errs := []string{"An error"}

			err := &HTTPError{Code: code, Errors: errs}
			err.SetDefaultsForEmptyFields()

			assert.Nil(t, deep.Equal(err, &HTTPError{
				Code:    code,
				Errors:  errs,
				Message: "Internal Server error",
			}))
		})

		t.Run("Should set only errors", func(t *testing.T) {
			code := 300
			msg := "Message"

			err := &HTTPError{Code: code, Message: msg}
			err.SetDefaultsForEmptyFields()

			assert.Nil(t, deep.Equal(err, &HTTPError{
				Code:    code,
				Errors:  make([]string, 0),
				Message: msg,
			}))
		})
	})
}

func TestHttpErrorIs(t *testing.T) {
	code := 300
	errs := []string{"An error"}
	msg := "Message"

	err := &HTTPError{Code: code, Errors: errs, Message: msg}

	assert.False(t, errors.Is(err, &HTTPError{}))
	assert.True(t, errors.Is(err, &HTTPError{Code: code, Errors: errs, Message: msg}))

	assert.False(t, errors.Is(err, errors.New("test")))
}

func TestHttpErrorError(t *testing.T) {
	code := 300
	errs := []string{"An error"}
	msg := "Message"

	err := &HTTPError{Code: code, Errors: errs, Message: msg}
	actual := err.Error()

	expected := fmt.Sprintf("There was an HTTP Error with code %d and the message %s and the following errors: \n", err.Code, err.Message)

	for _, err := range err.Errors {
		expected += fmt.Sprintf("\t %s", err)
	}

	assert.Equal(t, expected, actual)
}
