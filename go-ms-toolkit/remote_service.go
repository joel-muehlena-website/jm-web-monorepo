package gomstoolkit

import (
	"fmt"
	"sync"

	"go.uber.org/zap/zapcore"
)

type RemoteServiceConfig interface {
	GetUrl(env Environment) string

	// Same as GetUrl(Environment) but should detect the Env automatically
	GetUrlAuto() string
}

type RemoteServices struct {
	lock sync.RWMutex
	data map[string]RemoteServiceConfig
}

func (rs *RemoteServices) Add(name string, svc RemoteServiceConfig) error {
	rs.lock.RLock()
	_, ok := rs.data[name]
	rs.lock.RUnlock()

	if ok {
		return fmt.Errorf("Cannot override existing entry. Please use AddOverride.")
	}

	rs.lock.Lock()
	rs.data[name] = svc
	rs.lock.Unlock()

	return nil
}

func (rs *RemoteServices) AddOverride(name string, svc RemoteServiceConfig) {
	rs.lock.Lock()
	rs.data[name] = svc
	rs.lock.Unlock()
}

func (rs *RemoteServices) Remove(name string) {
	rs.lock.Lock()
	delete(rs.data, name)
	rs.lock.Unlock()
}

func (rs *RemoteServices) Get(name string) RemoteServiceConfig {
	rs.lock.RLock()
	svc, ok := rs.data[name]
	rs.lock.RUnlock()

	if !ok {
		return nil
	}

	return svc
}

type GenericRemoteServicesConfig struct {
	Name     string                       `json:"name" yaml:"name" toml:"name"`
	Services []GenericRemoteServiceConfig `json:"services" yaml:"services" toml:"services"`
}

func (svcCfg GenericRemoteServicesConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("name", svcCfg.Name)
	enc.AddArray("services", zapcore.ArrayMarshalerFunc(func(enc zapcore.ArrayEncoder) error {
		for _, s := range svcCfg.Services {
			enc.AppendObject(s)
		}
		return nil
	}))

	return nil
}

func (svcCfg GenericRemoteServicesConfig) ToGenericRemoteService() *GenericRemoteService {
	svc := new(GenericRemoteService)

	for _, s := range svcCfg.Services {
		env := ParseEnvFromString(s.Env)

		if env == ENV_DEVELOPMENT {
			svc.Dev_url = s.Url
		}
		if env == ENV_TEST {
			svc.Test_url = s.Url
		}
		if env == ENV_PRODUCTION {
			svc.Prod_url = s.Url
		}
	}

	return svc
}

type GenericRemoteServiceConfig struct {
	Env string `json:"env" yaml:"env" toml:"env"`
	Url string `json:"url" yaml:"url" toml:"url"`
}

func (cfg GenericRemoteServiceConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("env", cfg.Env)
	enc.AddString("url", cfg.Url)

	return nil
}

type GenericRemoteService struct {
	Prod_url string
	Dev_url  string
	Test_url string
}

func (svc *GenericRemoteService) GetUrl(env Environment) string {
	if env == ENV_DEVELOPMENT {
		return svc.Dev_url
	}

	if env == ENV_TEST {
		return svc.Test_url
	}

	return svc.Prod_url
}

func (svc *GenericRemoteService) GetUrlAuto() string {
	env, _ := GetCurrentEnv(DEFAULT_ENV_NAME)
	return svc.GetUrl(env)
}
