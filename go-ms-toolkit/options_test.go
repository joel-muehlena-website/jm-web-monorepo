package gomstoolkit

import (
	"testing"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"
)

func TestWithHttpPort(t *testing.T) {
	port := 8908

	expected := ConfigOptions{
		HttpPort: uint16(port),
	}

	fn := WithHttpPort(uint16(port))

	actual := ConfigOptions{}
	err := fn(&actual)
	assert.Nil(t, err)

	assert.Nil(t, deep.Equal(expected, actual))
}

func TestWithName(t *testing.T) {
	name := "test-svc"

	expected := ConfigOptions{
		Name: name,
	}

	fn := WithName(name)

	actual := ConfigOptions{}
	err := fn(&actual)
	assert.Nil(t, err)

	assert.Nil(t, deep.Equal(expected, actual))
}

func TestWithID(t *testing.T) {
	id := "dsd67988as6dx87y6sd"

	expected := ConfigOptions{
		ID: id,
	}

	fn := WithID(id)

	actual := ConfigOptions{}
	err := fn(&actual)
	assert.Nil(t, err)

	assert.Nil(t, deep.Equal(expected, actual))
}

func TestLogger(t *testing.T) {
	loggerCfg := LoggerConfig{
		LogLevel:       "info",
		LogOutputPaths: []string{"test"},
		LogErrorPaths:  []string{"test"},
		UseForGin:      false,
		Enabled:        true,
	}

	expected := ConfigOptions{
		LoggerConfig: &loggerCfg,
	}

	fn := WithLogger(&loggerCfg)

	actual := ConfigOptions{}
	err := fn(&actual)
	assert.Nil(t, err)

	assert.Equal(t, expected.LoggerConfig, actual.LoggerConfig)
	assert.Nil(t, deep.Equal(expected, actual))
}

func TestParseOptions(t *testing.T) {
	port := uint16(3022)
	name := "test-name"
	id := "fsd78689xc7v"
	loggerCfg := LoggerConfig{
		LogLevel:       "info",
		LogOutputPaths: []string{"test"},
		LogErrorPaths:  []string{"test"},
		UseForGin:      false,
		Enabled:        true,
	}

	expected := ConfigOptions{
		HttpPort:     port,
		Name:         name,
		ID:           id,
		LoggerConfig: &loggerCfg,
	}

	actual := ConfigOptions{}

	err := actual.parseOptions(WithID(id), WithHttpPort(port), WithName(name), WithLogger(&loggerCfg))
	assert.Nil(t, err)

	assert.Nil(t, deep.Equal(expected, actual))
}
