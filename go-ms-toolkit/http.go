package gomstoolkit

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	gzap "github.com/gin-contrib/zap"
	"github.com/samber/lo"

	"github.com/gin-gonic/gin"
	"go.uber.org/atomic"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/telemetry"
)

var (
	ErrHTTPNotInitialized = fmt.Errorf("HTTP server not initialized")
	ErrHTTPAlreadyRunning = fmt.Errorf("HTTP server already running")
)

type Http struct {
	endpoints []Hitable
	router    *gin.Engine
	logger    *zap.Logger
	config    HttpConfig
	server    *http.Server

	initialized     *atomic.Bool
	running         *atomic.Bool
	cancelCommander context.CancelCauseFunc
}

var _ SubService = &Http{}

func NewHttp(logger *zap.Logger, cfg HttpConfig) *Http {
	gin.SetMode(gin.ReleaseMode)

	router := gin.New()
	router.HandleMethodNotAllowed = true
	router.RedirectTrailingSlash = true
	router.SetTrustedProxies(nil)

	router.Use(GinZapLogger(logger), gzap.RecoveryWithZap(logger, true))
	// TODO
	router.Use(telemetry.GetGinMiddleware("TODO"))

	return &Http{
		endpoints:   make([]Hitable, 0),
		logger:      logger.Named("http"),
		config:      cfg,
		router:      router,
		initialized: atomic.NewBool(false),
		running:     atomic.NewBool(false),
	}
}

const (
	HTTPCommandReload CommandType = "http_reload"
	HTTPCommandStop   CommandType = "http_stop"
	HTTPCommandStart  CommandType = "http_start"
)

func (httpSvc *Http) AttachInternalApi(commanders map[string]chan<- Command) {
	internalApiGroup := httpSvc.router.Group("/api/internal")

	internalApiGroup.GET("/healthz", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{
			"status": "ok",
		})
	})

	type CommandRequest struct {
		Service string                 `json:"service"`
		Command string                 `json:"command"`
		Data    map[string]interface{} `json:"data,omitempty"`
	}

	internalApiGroup.GET("/command", func(ctx *gin.Context) {
		ctx.JSON(200, gin.H{
			"status":   "ok",
			"commands": lo.Keys(commanders),
		})
	})

	internalApiGroup.POST("/command", func(ctx *gin.Context) {
		var commandRequest CommandRequest
		err := ctx.BindJSON(&commandRequest)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": fmt.Sprintf("failed to bind json: %s", err.Error()),
			})
			return
		}

		if commandRequest.Command == "" {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "command is empty",
			})
			return
		}

		if commandRequest.Service == "" {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "service is empty",
			})
			return
		}

		command := Command{
			Command: CommandType(commandRequest.Command),
			Ctx:     ctx.Request.Context(),
			Data:    commandRequest.Data,
		}

		svcCommander, ok := commanders[commandRequest.Service]

		if !ok {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"error": "service not found",
			})
			return
		}

		svcCommander <- command

		ctx.JSON(http.StatusOK, gin.H{
			"status": "ok",
		})
	})
}

func (httpSvc *Http) Init() (chan<- Command, error) {
	httpSvc.initHttpServer()

	cmd, cancel := StartCommander(context.Background(), httpSvc.logger, 5, func(c Command) {
		switch c.Command {
		case HTTPCommandReload:
			httpSvc.logger.Info("Received reload command. Reloading server")
			httpSvc.stopServer(context.Background())
			httpSvc.initHttpServer()
			httpSvc.initialized.Store(true)
			httpSvc.startServer()
		case HTTPCommandStop:
			httpSvc.logger.Info("Received stop command. Stopping server")
			httpSvc.stopServer(context.Background())
		case HTTPCommandStart:
			httpSvc.logger.Info("Received start command. Starting server")
			httpSvc.initHttpServer()
			httpSvc.initialized.Store(true)
			httpSvc.startServer()
		}
	}, func(ctx context.Context) {
		httpSvc.logger.Error("Error in http commander context", zap.Error(ctx.Err()))
	})

	httpSvc.cancelCommander = cancel
	httpSvc.initialized.Store(true)

	return cmd, nil
}

func (httpSvc *Http) initHttpServer() {
	httpSvc.server = &http.Server{
		Addr:    fmt.Sprintf("%s:%d", httpSvc.config.BindAddr, httpSvc.config.Port),
		Handler: httpSvc.router,
	}
}

func (httpSvc *Http) IsInitialized() bool {
	return httpSvc.initialized.Load()
}

func (httpSvc *Http) Shutdown(ctx context.Context) {
	httpSvc.cancelCommander(ErrShuttingDown)
	httpSvc.stopServer(ctx)
}

func (httpSvc *Http) RegisterEndpoint(endpoints ...Hitable) {
	for _, endpoint := range endpoints {
		httpSvc.endpoints = append(httpSvc.endpoints, endpoint)

		httpSvc.logger.Debug("Registered endpoint", zap.String("endpointInfo", endpoint.Info(true)))

		handler := Handler{
			Endpoint: endpoint,
		}

		httpSvc.router.Handle(GetMethodString(endpoint.GetMethod()), endpoint.GetPath(), handler.genericHandler)
	}
}

func (httpSvc *Http) stopServer(ctx context.Context) error {
	if !httpSvc.IsInitialized() || httpSvc.server == nil {
		httpSvc.logger.Debug("Tried to stop a not initialized http server")
		return ErrHTTPNotInitialized
	}

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	err := httpSvc.server.Shutdown(ctx)
	if err != nil {
		httpSvc.logger.Error("Error shutting down http server", zap.Error(err))
		return err
	}

	// Revoking http initialization
	httpSvc.initialized.Store(false)

	return nil
}

func (httpSvc *Http) startServer() error {
	if httpSvc.running.Load() {
		httpSvc.logger.Debug("Tried to start an already running http server")
		return ErrHTTPAlreadyRunning
	}

	go func() {
		defer httpSvc.running.Store(false)
		httpSvc.logger.Info("Trying to start server", zap.String("bindAddr", httpSvc.config.BindAddr), zap.Uint16("port", httpSvc.config.Port))

		httpSvc.running.Store(true)

		if err := httpSvc.server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			httpSvc.logger.Error("Http server error", zap.Error(err), zap.String("bindAddr", httpSvc.config.BindAddr), zap.Uint16("port", httpSvc.config.Port))
		} else if err != nil && errors.Is(err, http.ErrServerClosed) {
			httpSvc.logger.Info("Http server closed gracefully")
		}
	}()

	return nil
}

func (httpSvc *Http) Run() error {
	if !httpSvc.IsInitialized() {
		return ErrHTTPNotInitialized
	}

	return httpSvc.startServer()
}

type Handler struct {
	Endpoint Hitable
}

func (handler *Handler) genericHandler(ctx *gin.Context) {
	handler.Endpoint.Hit(ctx)
}
