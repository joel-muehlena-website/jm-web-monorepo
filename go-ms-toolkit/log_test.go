package gomstoolkit

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zapcore"
)

func TestGetLevelFromString(t *testing.T) {
	expected := map[string]zapcore.Level{
		"debug": zapcore.DebugLevel,
		"info":  zapcore.InfoLevel,
		"warn":  zapcore.WarnLevel,
		"error": zapcore.ErrorLevel,
		"panic": zapcore.PanicLevel,
		"false": zapcore.InfoLevel,
		"test":  zapcore.InfoLevel,
	}

	for key, value := range expected {
		assert.Equal(t, getLevelFromString(key), value)
	}
}

func TestCreateLogDir(t *testing.T) {
	tmpdir := t.TempDir()
	assert := assert.New(t)

	cleanUp := func(dir string) {
		err := os.RemoveAll(dir)
		assert.Nil(err)
	}

	t.Run("Should return false if folder already exist", func(t *testing.T) {
		dir, err := os.MkdirTemp(tmpdir, "test")
		defer cleanUp(dir)
		assert.Nil(err)

		isCreated, err := CreateLogDir(dir)
		assert.Nil(err)
		assert.False(isCreated)
	})

	t.Run("Should return true and create dir", func(t *testing.T) {
		path := filepath.Join(tmpdir, "test")
		isCreated, err := CreateLogDir(path)
		assert.Nil(err)

		defer cleanUp(path)

		assert.True(isCreated)
	})
}
