package gomstoolkit

import (
	"fmt"
	"reflect"

	"github.com/go-playground/validator/v10"
)

type InvalidType struct {
	expected reflect.Kind
	actual   reflect.Kind
}

func (t *InvalidType) Error() string {
	return fmt.Sprintf("you passed an invalid type! Expected: %s Got %s", t.expected, t.actual)
}

func Validate[T any](data *T) ([]string, error) {
	if reflect.ValueOf(*data).Kind() != reflect.Struct {
		return nil, &InvalidType{expected: reflect.Struct, actual: reflect.ValueOf(*data).Kind()}
	}

	validate := validator.New()
	errStr := make([]string, 0)

	err := validate.Struct(data)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			errStr = append(errStr, fmt.Sprintf("Error in field %s with value %v. Requirements to satisfy: %s", err.StructField(), err.Value(), err.ActualTag()))
		}

		return errStr, nil
	}

	return errStr, nil
}
