package gomstoolkit

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"dario.cat/mergo"
	"github.com/google/uuid"
	"github.com/puzpuzpuz/xsync/v3"
	"github.com/samber/lo"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/probes"
	resource_registrator "gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/resource-registrator"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/telemetry"
	resource_proto "gitlab.com/joelMuehlena/jm-web-monorepo/lib/rust/ds-auth/proto/resource"
)

const DEFAULT_PORT uint16 = 3000

type ApplicationService interface {
	SetupEndpoints() []Hitable
}

type Service struct {
	globalLock sync.RWMutex

	Ws            *Ws
	Logger        *zap.Logger
	Name          string
	ID            uuid.UUID
	isProd        bool
	env           Environment
	initialConfig Config
	Resources     resource_registrator.ApplicationResources

	probeSubService *probes.Probes

	// TODO sync.Map
	SubServiceCommander *xsync.MapOf[string, SubServiceReference]

	// Holds remote services which can be saved by name and
	// the used in the application
	RemoteServices RemoteServices

	Telemetry *telemetry.Telemetry
}

func New(options ...ConfigOption) *Service {
	svc := new(Service)

	svc.SubServiceCommander = xsync.NewMapOf[string, SubServiceReference]()

	svc.env, svc.isProd = GetCurrentEnv(DEFAULT_ENV_NAME)
	svc.RemoteServices = RemoteServices{
		data: make(map[string]RemoteServiceConfig, 0),
	}

	cfg := defaultConfigOptions

	err := cfg.parseOptions(options...)
	if err != nil {
		panic("failed to parse config options")
	}

	var nullUUID uuid.UUID
	if cfg.ID == nullUUID {
		svc.ID = uuid.New()
	} else {
		svc.ID = cfg.ID
	}

	svc.applyConfig(&cfg)

	svc.Logger = nil

	svc.initLogger()

	printApplicationLogHeadline(svc.Logger)

	tel, err := initTelemetry(svc)
	if err != nil {
		// TODO: Add retry mechanism
		svc.Logger.Fatal("Tel error", zap.Error(err))
	}
	svc.Telemetry = tel

	var http *Http
	commanders := make(map[string]chan<- Command, 0)

	// TODO register into probes & metrics

	if svc.initialConfig.Monitoring.Enabled {
		monitoring := NewMonitoring(svc.Logger, svc.initialConfig.Monitoring)
		monitoringCommander, err := monitoring.Init()
		if err != nil {
			svc.Logger.Fatal("Failed to init monitoring server", zap.Error(err))
		}

		svc.probeSubService = monitoring.Probes()

		commanders["monitoring"] = monitoringCommander

		svc.SubServiceCommander.Store("monitoring", SubServiceReference{
			SubService: monitoring,
			Commander:  monitoringCommander,
		})
	}

	if svc.initialConfig.HttpConfig.Enabled {
		http = NewHttp(svc.Logger, svc.initialConfig.HttpConfig)
		httpCommander, err := http.Init()
		if err != nil {
			svc.Logger.Fatal("Failed to init http server", zap.Error(err))
		}

		commanders["http"] = httpCommander

		svc.SubServiceCommander.Store("http", SubServiceReference{
			SubService: http,
			Commander:  httpCommander,
		})
	}

	if svc.initialConfig.GrpcConfig.Enabled {
		grpc := NewGRPC(svc.Logger, svc.initialConfig.GrpcConfig)
		grpcCommander, err := grpc.Init()
		if err != nil {
			svc.Logger.Fatal("Failed to init grpc server", zap.Error(err))
		}

		commanders["grpc"] = grpcCommander

		svc.SubServiceCommander.Store("grpc", SubServiceReference{
			SubService: grpc,
			Commander:  grpcCommander,
		})
	}

	svc.Ws = &Ws{}
	svc.Ws.Init(&svc.initialConfig.WSConfig)

	if http != nil {
		http.AttachInternalApi(commanders)
	}

	return svc
}

func (svc *Service) registerResources() {
	if len(svc.Resources.Resources) == 0 {
		svc.Logger.Info("No resources defined")
		return
	}

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	opts = append(opts, telemetry.GetGRPCClientHandler())
	conn, err := grpc.Dial(svc.Resources.ResourceServerAddress, opts...)
	if err != nil {
		svc.Logger.Error("Failed to connect to resource server", zap.Error(err))
		return
	}
	defer conn.Close()

	idString := svc.ID.String()

	for _, resource := range svc.Resources.Resources {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		res, err := resource_registrator.Register(svc.Logger, ctx, &resource_proto.RegisterRequest{
			ServiceName:    svc.Name,
			ServiceId:      idString,
			HealthCheckUrl: svc.Resources.HealthCheckUrl,
			Resource: &resource_proto.Resource{
				Name:    resource.Name,
				Domain:  resource.Domain,
				Actions: resource.Actions,
				Attributes: &resource_proto.Attributes{
					Constraints: lo.MapEntries(resource.Attributes.Constraints, func(key string, value resource_registrator.ResourceConstraint) (string, *resource_proto.Constraint) {
						return key, &resource_proto.Constraint{}
					}),
				},
			},
		}, conn)
		cancel()
		if err != nil {
			svc.Logger.Error("Failed to register resource "+resource.Name, zap.Error(err), zap.Any("resource", resource))
			continue
		}

		svc.Logger.Info("Registered resource "+resource.Name, zap.Error(err), zap.String("referenceId", res.ReferenceId), zap.Any("resource", resource))

		svc.Resources.Claims[resource.Name] = res.ReferenceId
	}
}

func (svc *Service) unregisterResources() {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))
	opts = append(opts, telemetry.GetGRPCClientHandler())
	conn, err := grpc.Dial(svc.Resources.ResourceServerAddress, opts...)
	if err != nil {
		svc.Logger.Error("Failed to connect to resource server", zap.Error(err))
		return
	}
	defer conn.Close()

	idString := svc.ID.String()

	for resourceName, referenceId := range svc.Resources.Claims {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		res, err := resource_registrator.Unregister(svc.Logger, ctx, &resource_proto.UnregisterRequest{
			ServiceName:  svc.Name,
			ServiceId:    idString,
			ResourceName: resourceName,
			ReferenceId:  referenceId,
		}, conn)
		cancel()
		if err != nil || !res.Success {
			svc.Logger.Error("Failed to unregister resource "+resourceName, zap.Error(err), zap.String("referenceId", referenceId))
			continue
		}

		delete(svc.Resources.Claims, resourceName)
	}
}

// TODO refactor configuration
func (svc *Service) applyConfig(cfg *ConfigOptions) {
	svcConfig := new(Config)

	svcConfig.LoggerConfig = DefaultLoggerConfig

	if cfg.ServiceConfig != nil {
		svcConfig = cfg.ServiceConfig

		err := mergo.Merge(&svcConfig.LoggerConfig, cfg.LoggerConfig)
		if err != nil {
			fmt.Printf("Error merging logger cfg: %v", err)
		}
	}

	if cfg.HttpPort != 0 {
		svcConfig.HttpConfig.Port = cfg.HttpPort
	}

	if cfg.Name != "" {
		svcConfig.Name = cfg.Name
	}

	if cfg.LoggerConfig != nil {
		svcConfig.LoggerConfig = *cfg.LoggerConfig
	}

	if len(cfg.Resources) > 0 && cfg.ResourceServerAddress != "" {
		svcConfig.Resources = resource_registrator.ResourceConfig{
			Resources:             cfg.Resources,
			ResourceServerAddress: cfg.ResourceServerAddress,
		}
	}

	svc.initialConfig = *svcConfig
	svc.Name = svc.initialConfig.Name

	svc.Resources = resource_registrator.ApplicationResources{
		Resources:             svc.initialConfig.Resources.Resources,
		ResourceServerAddress: svc.initialConfig.Resources.ResourceServerAddress,
		Claims:                make(map[string]string, 0),
	}

	for _, remoteService := range svc.initialConfig.RemoteServices {
		svc.RemoteServices.Add(remoteService.Name, remoteService.ToGenericRemoteService())
	}
}

func (svc *Service) Run() error {
	defer func() {
		if r := recover(); r != nil {
			svc.Logger.Error("Recovered from panic in main loop", zap.Any("panic", r))
			svc.Shutdown(context.Background())
		}
	}()

	svc.Logger.Debug("Starting gomstoolkit service", zap.Object("config", &svc.initialConfig))

	if !svc.initialConfig.GrpcConfig.Enabled && !svc.initialConfig.HttpConfig.Enabled && !svc.initialConfig.WSConfig.Enabled {
		svc.Logger.Fatal("Please configure at least one service of grpc, http, ws. Terminating")
	}

	if svc.initialConfig.Monitoring.Enabled {
		if ref, ok := svc.SubServiceCommander.Load("monitoring"); ok {
			err := ref.SubService.Run()
			if err != nil {
				return err
			}
		} else {
			return fmt.Errorf("failed to load monitoring sub service")
		}
	}

	if svc.initialConfig.HttpConfig.Enabled {
		if ref, ok := svc.SubServiceCommander.Load("http"); ok {
			err := ref.SubService.Run()
			if err != nil {
				return err
			}
		} else {
			return fmt.Errorf("failed to load http sub service")
		}
	}

	if svc.initialConfig.GrpcConfig.Enabled {
		if ref, ok := svc.SubServiceCommander.Load("grpc"); ok {
			err := ref.SubService.Run()
			if err != nil {
				return err
			}
		} else {
			return fmt.Errorf("failed to load grpc sub service")
		}
	}

	if svc.initialConfig.WSConfig.Enabled {
		go svc.Ws.Run()
	}

	svc.registerResources()

	svc.probeSubService.SetStartup(true)

	// Wait for CTRL+C, Kill or TERM (Docker) to quit
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGKILL, syscall.SIGTERM)
	sig := <-c

	svc.Logger.Info("Shutting down", zap.String("signal", sig.String()))
	svc.Shutdown(context.Background())

	return nil
}

func (svc *Service) Shutdown(ctx context.Context) {
	defer svc.Logger.Sync()

	svc.SubServiceCommander.Range(func(key string, value SubServiceReference) bool {
		svc.Logger.Debug("Shutting down subservice: " + key)
		value.SubService.Shutdown(ctx)
		return true
	})

	svc.Telemetry.Shutdown(ctx)

	svc.unregisterResources()
}

func (svc *Service) parseVarList(valueList []string) {
	for i, entry := range valueList {
		valueList[i] = svc.parseVars(entry)
	}
}

func (svc *Service) parseVars(value string) string {
	allowedVars := []string{"NAME", "ID", "HTTP_PORT"}

	entry := value

	for _, allowedVar := range allowedVars {
		var newS string = ""
		switch allowedVar {
		case "NAME":
			newS = strings.ReplaceAll(entry, "{"+allowedVar+"}", svc.Name)
		case "ID":
			newS = strings.ReplaceAll(entry, "{"+allowedVar+"}", svc.ID.String())
		case "HTTP_PORT":
			newS = strings.ReplaceAll(entry, "{"+allowedVar+"}", strconv.Itoa(int(svc.initialConfig.HttpConfig.Port)))
		}

		if newS != "" && newS != entry {
			entry = newS
		}

	}

	return entry
}

func (svc *Service) initLogger() {
	loggerCfg := &svc.initialConfig.LoggerConfig

	svc.parseVarList(loggerCfg.LogOutputPaths)
	svc.parseVarList(loggerCfg.LogErrorPaths)

	for _, path := range loggerCfg.LogOutputPaths {
		_, err := CreateLogDir(path)
		if err != nil {
			log.Fatal("Failed to create log dir: " + err.Error())
		}
	}

	for _, path := range loggerCfg.LogErrorPaths {
		_, err := CreateLogDir(path)
		if err != nil {
			log.Fatal("Failed to create log dir: " + err.Error())
		}
	}

	zapCfg := zap.Config{
		OutputPaths:      loggerCfg.LogOutputPaths,
		ErrorOutputPaths: loggerCfg.LogErrorPaths,
		EncoderConfig:    loggerCfg.EncoderConfig,
		Encoding:         loggerCfg.Encoder,
		Level:            zap.NewAtomicLevelAt(getLevelFromString(loggerCfg.LogLevel)),
	}

	zapCfg.InitialFields = map[string]interface{}{"service_name": svc.Name, "service_id": svc.ID.String()}

	if !loggerCfg.Enabled {
		zapCfg.OutputPaths = make([]string, 0)
		zapCfg.ErrorOutputPaths = make([]string, 0)
		zapCfg.Level = zap.NewAtomicLevelAt(zap.FatalLevel)
		zapCfg.Encoding = "console"
	}

	var err error
	svc.Logger, err = zapCfg.Build()

	if err != nil {
		panic("can't initialize zap logger: " + err.Error())
	}

	svc.Logger = svc.Logger.Named("service")

	svc.Logger.Sync()

	// Replace global logger
	// Ignoring zap recommendations here on purpose because it is easier than passing the logger everywhere
	zap.ReplaceGlobals(svc.Logger)
}

func printApplicationLogHeadline(logger *zap.Logger) {
	logger.Info("Copyright (c) 2023 Joel Rene Mühlena")
	logger.Info("All Rights Reserved")
	logger.Info("")
	logger.Info("Starting application with microservice toolkit as base")
}
