package gomstoolkit

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInvalidTypeError(t *testing.T) {
	invalidTypeErr := InvalidType{
		expected: reflect.Array,
		actual:   reflect.Ptr,
	}

	expected := fmt.Sprintf("you passed an invalid type! Expected: %s Got %s", invalidTypeErr.expected, invalidTypeErr.actual)

	assert.Equal(t, expected, invalidTypeErr.Error())
}

func TestValidate(t *testing.T) {
	assert := assert.New(t)

	type testType struct {
		TestField1 string `validate:"required"`
		TestField2 string `validate:"required"`
	}

	t.Run("Should return error if data is no struct", func(t *testing.T) {
		val := 12
		iErr := InvalidType{
			expected: reflect.Struct,
			actual:   reflect.Int,
		}

		res, err := Validate(&val)

		assert.NotNil(err)
		assert.Nil(res)
		assert.EqualError(err, iErr.Error())
	})

	t.Run("Should return list of errors if data is invalid", func(t *testing.T) {
		expected := map[testType][]string{
			{}: {
				fmt.Sprintf("Error in field %s with value %v. Requirements to satisfy: %s", "TestField1", "", "required"),
				fmt.Sprintf("Error in field %s with value %v. Requirements to satisfy: %s", "TestField2", "", "required"),
			},
			{TestField1: "test"}: {
				fmt.Sprintf("Error in field %s with value %v. Requirements to satisfy: %s", "TestField2", "", "required"),
			},
			{TestField2: "test2"}: {
				fmt.Sprintf("Error in field %s with value %v. Requirements to satisfy: %s", "TestField1", "", "required"),
			},
		}

		for key, value := range expected {
			res, err := Validate(&key)

			assert.Nil(err)
			assert.Equal(len(value), len(res))

			for i, val := range res {
				assert.Equal(value[i], val)
			}
		}
	})

	t.Run("Should return empty list if validation succeeds", func(t *testing.T) {
		testVal := testType{
			TestField1: "test",
			TestField2: "test2",
		}

		res, err := Validate(&testVal)

		assert.Nil(err)
		assert.Equal(0, len(res))
	})
}
