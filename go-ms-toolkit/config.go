package gomstoolkit

import (
	resource_registrator "gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/resource-registrator"

	"go.uber.org/zap/zapcore"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/telemetry"
)

type Config struct {
	Id              string                              `yaml:"id" toml:"id"`
	Name            string                              `yaml:"name" toml:"name"`
	HttpConfig      HttpConfig                          `yaml:"http" toml:"http"`
	GrpcConfig      GrpcConfig                          `yaml:"grpc" toml:"grpc"`
	WSConfig        WSConfig                            `yaml:"ws" toml:"ws"`
	MetricsConfig   MetricsConfig                       `yaml:"metrics" toml:"metrics"`
	LoggerConfig    LoggerConfig                        `yaml:"logging" toml:"logging"`
	TelemetryConfig telemetry.Config                    `yaml:"telemetry" toml:"telemetry"`
	RemoteServices  RemoteServicesConfig                `yaml:"remoteServices" toml:"remoteServices"`
	Resources       resource_registrator.ResourceConfig `yaml:"resources" toml:"resources"`
	Monitoring      MonitoringConfig                    `yaml:"monitoring" toml:"monitoring"`
}

type MonitoringConfig struct {
	Enabled          bool   `yaml:"enabled" toml:"enabled" json:"enabled"`
	EnabledProfiling bool   `yaml:"enabledProfiling" toml:"enabledProfiling" json:"enabledProfiling"`
	Port             uint16 `yaml:"port" toml:"port" json:"port"`
	BindAddr         string `yaml:"address" toml:"address" json:"address"`
	MetricsPrefix    string `yaml:"metricsPrefix" toml:"metricsPrefix" json:"metricsPrefix"`
	ProbesPrefix     string `yaml:"probesPrefix" toml:"probesPrefix" json:"probesPrefix"`
}

type RemoteServicesConfig []GenericRemoteServicesConfig

func (remoteServices RemoteServicesConfig) MarshalLogArray(enc zapcore.ArrayEncoder) error {
	for _, svc := range remoteServices {
		enc.AppendObject(svc)
	}

	return nil
}

func (cfg *Config) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddString("id", cfg.Id)
	enc.AddString("name", cfg.Name)
	enc.AddObject("http", &cfg.HttpConfig)
	enc.AddObject("grpc", &cfg.GrpcConfig)
	enc.AddObject("ws", &cfg.WSConfig)
	enc.AddObject("metrics", &cfg.MetricsConfig)
	enc.AddObject("telemetry", &TelemetryConfigZap{cfg.TelemetryConfig})
	enc.AddArray("remoteServices", cfg.RemoteServices)
	enc.AddReflected("resources", &cfg.Resources)
	// enc.AddObject("logging", &cfg.LoggerConfig)

	return nil
}

type TelemetryConfigZap struct {
	telemetry.Config
}

func (tcz *TelemetryConfigZap) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddBool("enabled", tcz.Enabled)
	enc.AddString("name", tcz.Name)
	enc.AddBool("useCollector", tcz.UseCollector)
	enc.AddReflected("collector", tcz.CollectorConfig)
	return nil
}

type HttpConfig struct {
	Enabled   bool      `yaml:"enabled" toml:"enabled" json:"enabled"`
	Port      uint16    `yaml:"port" toml:"port" json:"port"`
	BindAddr  string    `yaml:"address" toml:"address" json:"address"`
	TLSConfig TLSConfig `yaml:"tlsConfig" toml:"tlsConfig" json:"tlsConfig"`
}

func (cfg *HttpConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddBool("enabled", cfg.Enabled)
	enc.AddUint16("port", cfg.Port)
	enc.AddString("bindAddr", cfg.BindAddr)
	enc.AddObject("tls", &cfg.TLSConfig)

	return nil
}

type GrpcConfig struct {
	Enabled          bool      `yaml:"enabled" toml:"enabled" json:"enabled"`
	Port             uint16    `yaml:"port" toml:"port" json:"port"`
	BindAddr         string    `yaml:"address" toml:"address" json:"address"`
	TLSConfig        TLSConfig `yaml:"tlsConfig" toml:"tlsConfig" json:"tlsConfig"`
	ServerReflection *bool     `yaml:"serverReflection" toml:"serverReflection" json:"serverReflection"`
}

func (cfg *GrpcConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	reflectionEnabled := true
	if cfg.ServerReflection != nil {
		reflectionEnabled = *cfg.ServerReflection
	}

	enc.AddBool("enabled", cfg.Enabled)
	enc.AddUint16("port", cfg.Port)
	enc.AddString("bindAddr", cfg.BindAddr)
	enc.AddObject("tls", &cfg.TLSConfig)
	enc.AddBool("serverReflection", reflectionEnabled)
	return nil
}

type WSConfig struct {
	Enabled   bool      `yaml:"enabled" toml:"enabled" json:"enabled"`
	Port      uint16    `yaml:"port" toml:"port" json:"port"`
	BindAddr  string    `yaml:"address" toml:"address" json:"address"`
	TLSConfig TLSConfig `yaml:"tlsConfig" toml:"tlsConfig" json:"tlsConfig"`
}

func (cfg *WSConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddBool("enabled", cfg.Enabled)
	enc.AddUint16("port", cfg.Port)
	enc.AddString("bindAddr", cfg.BindAddr)
	enc.AddObject("tls", &cfg.TLSConfig)
	return nil
}

type MetricsConfig struct{}

func (cfg *MetricsConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	return nil
}

type TLSConfig struct {
	Rotation bool `yaml:"rotation" toml:"rotation" json:"rotation"`
}

func (cfg *TLSConfig) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	enc.AddBool("rotation", cfg.Rotation)
	return nil
}
