package gomstoolkit

import (
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc/metadata"
)

// TODO: Replace with log library

const RFC3339_TIME_FORMAT string = "2006-01-02T15:04:05-0700"

func CustomTimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.UTC().Format(RFC3339_TIME_FORMAT))
}

func GinZapLogger(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		// some evil middlewares modify this values
		path := c.Request.URL.Path
		query := c.Request.URL.RawQuery
		c.Next()

		end := time.Now()
		latency := end.Sub(start)

		if len(c.Errors) > 0 {
			// Append error field if this is an erroneous request.
			for _, e := range c.Errors.Errors() {
				logger.Error(e)
			}
		} else {
			fields := []zapcore.Field{
				zap.Int("status", c.Writer.Status()),
				zap.String("method", c.Request.Method),
				zap.String("path", path),
				zap.String("query", query),
				zap.String("ip", c.ClientIP()),
				zap.String("user-agent", c.Request.UserAgent()),
				zap.Duration("latency", latency),
				zap.String("time", end.Format(RFC3339_TIME_FORMAT)),
			}
			logger.Info(path, fields...)
		}
	}
}

func ZapMap2FieldsString(m map[string]string) []zap.Field {
	fields := make([]zap.Field, 0, len(m))
	for k, v := range m {
		fields = append(fields, zap.String(k, v))
	}
	return fields
}

type ZapGrpcMetadata struct {
	m metadata.MD
}

func ZapMap2FieldsGrpcMetadata(m metadata.MD) *ZapGrpcMetadata {
	md := new(ZapGrpcMetadata)
	md.m = m
	return md
}

func (md *ZapGrpcMetadata) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	for k, v := range md.m {
		enc.AddString(k, strings.Join(v, ","))
	}

	return nil
}

type LoggerConfig struct {
	LogLevel       string                `yaml:"logLevel" toml:"logLevel"`
	LogOutputPaths []string              `yaml:"stdoutPaths" toml:"stdoutPaths"`
	LogErrorPaths  []string              `yaml:"stderrPaths" toml:"stderrPaths"`
	Encoder        string                `yaml:"encoder" toml:"encoder"`
	EncoderConfig  zapcore.EncoderConfig `yaml:"encoderCfg" toml:"encoderCfg"`
	UseForGin      bool                  `yaml:"useForGin" toml:"useForGin"`
	Enabled        bool                  `yaml:"enabled" toml:"enabled"`
}

var DefaultLoggerConfig LoggerConfig = LoggerConfig{
	Enabled:        true,
	UseForGin:      true,
	LogLevel:       "debug",
	LogOutputPaths: []string{"stdout"},
	LogErrorPaths:  []string{"stderr"},
	Encoder:        "json",
	EncoderConfig: zapcore.EncoderConfig{
		MessageKey:    "message",
		LevelKey:      "level",
		EncodeLevel:   zapcore.LowercaseLevelEncoder,
		TimeKey:       "time",
		EncodeTime:    CustomTimeEncoder,
		CallerKey:     "caller",
		EncodeCaller:  zapcore.FullCallerEncoder,
		NameKey:       "name",
		EncodeName:    zapcore.FullNameEncoder,
		StacktraceKey: "stacktrace",
		FunctionKey:   "function",
	},
}

func getLevelFromString(level string) zapcore.Level {
	var lvl zapcore.Level

	switch level {
	case "debug":
		lvl = zapcore.DebugLevel
	case "info":
		lvl = zapcore.InfoLevel
	case "warn":
		lvl = zapcore.WarnLevel
	case "error":
		lvl = zapcore.ErrorLevel
	case "panic":
		lvl = zapcore.PanicLevel
	default:
		lvl = zapcore.InfoLevel
	}

	return lvl
}

func CreateLogDir(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return false, nil
	}

	if os.IsNotExist(err) {
		dir := filepath.Dir(path)
		if err = os.MkdirAll(dir, os.ModePerm); err != nil {
			return false, err
		}

		return true, nil
	}

	return false, err
}
