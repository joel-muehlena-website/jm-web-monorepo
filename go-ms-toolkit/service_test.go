package gomstoolkit

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap/zapcore"
)

func TestNew(t *testing.T) {
	assert := assert.New(t)

	t.Parallel()

	t.Run("Should create with default", func(t *testing.T) {
		svc := New()

		assert.NotNil(svc.SubServiceCommander["http"].SubService.(*Http).endpoints)
		assert.NotNil(svc.SubServiceCommander["http"].SubService.(*Http).server)
		assert.NotNil(svc.Logger)
		assert.Equal("default-svc", svc.Name)
		assert.Equal(3000, svc.Port)
		assert.LessOrEqual(1, len(svc.ID))
	})

	t.Run("Should create with options", func(t *testing.T) {
		svc := New(WithHttpPort(3032), WithID("anID"), WithName("test-svc"))

		assert.NotNil(svc.SubServiceCommander["http"].SubService.(*Http).endpoints)
		assert.NotNil(svc.SubServiceCommander["http"].SubService.(*Http).server)
		assert.NotNil(svc.Logger)
		assert.Equal("test-svc", svc.Name)
		assert.Equal(3032, svc.Port)
		assert.LessOrEqual(1, len(svc.ID))
		assert.Equal("anID", svc.ID)
	})
}

func TestApplyConfig(t *testing.T) {
	assert := assert.New(t)

	lCfg := DefaultLoggerConfig
	lCfg.LogLevel = "debug"

	cfg := ConfigOptions{
		Name:         "test-name",
		HttpPort:     3043,
		ID:           "csd879ff",
		LoggerConfig: &lCfg,
	}

	t.Parallel()

	t.Run("Should apply with custom logger cfg", func(t *testing.T) {
		svc := Service{}
		svc.applyConfig(&cfg)

		assert.Equal(cfg.Name, svc.Name)
		assert.Equal(int(cfg.HttpPort), svc.Port)
		assert.Equal(cfg.ID, svc.ID)
		assert.Nil(deep.Equal(lCfg, svc.initialConfig.LoggerConfig))
	})

	t.Run("Should apply default logger cfg if nil", func(t *testing.T) {
		svc := Service{}
		cfg.LoggerConfig = nil
		svc.applyConfig(&cfg)

		assert.Equal(cfg.Name, svc.Name)
		assert.Equal(int(cfg.HttpPort), svc.Port)
		assert.Equal(cfg.ID, svc.ID)
		assert.Nil(deep.Equal(DefaultLoggerConfig, svc.initialConfig.LoggerConfig))
	})
}

func TestParseVarList(t *testing.T) {
	svc := Service{
		Name: "test-name",
		ID:   "789yxc789aw",
		Port: 2389,
	}

	testStrings := map[string][2][]any{
		"this_is_a_%s_test":                {{"{NAME}"}, {svc.Name}},
		"this_is_a_%s_%s_%s_test":          {{"{NAME}", "{ID}", "{PORT}"}, {svc.Name, svc.ID, strconv.Itoa(svc.Port)}},
		"this_is_a_%s_%s_%s_test_2":        {{"{NAME}", "{NAME}", "{PORT}"}, {svc.Name, svc.Name, strconv.Itoa(svc.Port)}},
		"this_is_a_%s_%s_%s_%s_%s_%s_test": {{"{NAME}", "{NAME}", "{ID}", "{ID}", "{PORT}", "{PORT}"}, {svc.Name, svc.Name, svc.ID, svc.ID, strconv.Itoa(svc.Port), strconv.Itoa(svc.Port)}},
	}

	testData := make(map[string]string, 0)

	for s, vals := range testStrings {
		builtString := fmt.Sprintf(s, vals[0]...)
		testData[builtString] = fmt.Sprintf(s, vals[1]...)
	}

	testDataSlice := make([]string, 0)
	testDataSliceResult := make([]string, 0)

	for val := range testData {
		testDataSlice = append(testDataSlice, val)
		testDataSliceResult = append(testDataSliceResult, val)
	}

	svc.parseVarList(testDataSliceResult)

	for i, actual := range testDataSlice {

		val, ok := testData[actual]
		assert.True(t, ok)
		assert.Equal(t, val, testDataSliceResult[i])
	}
}

func TestParseVars(t *testing.T) {
	svc := Service{
		Name: "test-name",
		ID:   "789yxc789aw",
		Port: 2389,
	}

	testStrings := map[string][2][]any{
		"this_is_a_%s_test":                {{"{NAME}"}, {svc.Name}},
		"this_is_a_%s_%s_%s_test":          {{"{NAME}", "{ID}", "{PORT}"}, {svc.Name, svc.ID, strconv.Itoa(svc.Port)}},
		"this_is_a_%s_%s_%s_test_2":        {{"{NAME}", "{NAME}", "{PORT}"}, {svc.Name, svc.Name, strconv.Itoa(svc.Port)}},
		"this_is_a_%s_%s_%s_%s_%s_%s_test": {{"{NAME}", "{NAME}", "{ID}", "{ID}", "{PORT}", "{PORT}"}, {svc.Name, svc.Name, svc.ID, svc.ID, strconv.Itoa(svc.Port), strconv.Itoa(svc.Port)}},
	}

	testData := make(map[string]string, 0)

	for s, vals := range testStrings {
		builtString := fmt.Sprintf(s, vals[0]...)
		testData[builtString] = fmt.Sprintf(s, vals[1]...)
	}

	for key, value := range testData {
		actual := svc.parseVars(key)
		assert.Equal(t, value, actual)
	}
}

func TestInitLogger(t *testing.T) {
	t.Parallel()

	t.Run("Should be disabled if configured so", func(t *testing.T) {
		svc := New(WithLogger(&LoggerConfig{Enabled: false}))

		assert.False(t, svc.initialConfig.LoggerConfig.Enabled)
		assert.NotNil(t, svc.Logger)
		assert.NotNil(t, svc.Logger.Core().Enabled(zapcore.InfoLevel))
	})

	t.Run("Should panic if logger init fails", func(t *testing.T) {
		lCfg := DefaultLoggerConfig
		lCfg.Enabled = false

		assert.Panics(t, func() { New(WithLogger(&LoggerConfig{Encoder: "false_val", Enabled: true})) })
	})
}

func TestGenericHandler(t *testing.T) {
	assert := assert.New(t)

	testEndpoint := Endpoint[testReq, testRes]{
		Path:        "/test",
		Handler:     nil,
		Description: "A test",
		Method:      GET,
	}

	h := Handler{
		Endpoint: &testEndpoint,
	}

	testEndpoint.Handler = func(r *testReq) (*testRes, int, error) {
		return &testRes{}, 200, nil
	}

	w := httptest.NewRecorder()
	c, engine := gin.CreateTestContext(w)

	req := httptest.NewRequest(http.MethodPost, testEndpoint.Path, nil)

	c.Request = req

	engine.Handle(req.Method, testEndpoint.Path, func(ctx *gin.Context) {
		h.genericHandler(ctx)
	})

	engine.ServeHTTP(w, req)

	assert.Equal(200, w.Code)
}

func TestRegisterEndpoint(t *testing.T) {
	assert := assert.New(t)
	svc := New()

	type testReq struct{}
	type testRes struct{}

	testEndpoint := Endpoint[testReq, testRes]{
		Path:        "/test",
		Handler:     nil,
		Description: "A test",
		Method:      GET,
	}

	svc.SubServiceCommander["http"].SubService.(*Http).RegisterEndpoint(&testEndpoint)

	assert.Equal(1, len(svc.SubServiceCommander["http"].SubService.(*Http).endpoints))
	assert.Nil(deep.Equal(svc.SubServiceCommander["http"].SubService.(*Http).endpoints, []Hitable{&testEndpoint}))
	assert.Equal(1, len(svc.SubServiceCommander["http"].SubService.(*Http).server.Routes()))

	route := svc.SubServiceCommander["http"].SubService.(*Http).server.Routes()[0]

	assert.Equal(GetMethodString(testEndpoint.Method), route.Method)
	assert.Equal(testEndpoint.Path, route.Path)
	assert.NotNil(route.HandlerFunc)
}
