package gomstoolkit

import (
	"os"
	"strings"
)

type Environment uint8

const (
	ENV_DEVELOPMENT Environment = iota
	ENV_PRODUCTION
	ENV_TEST
)

const DEFAULT_ENV_NAME = "JM_SVC_ENV"

func ParseEnvFromString(env string) Environment {
	env = strings.ToLower(env)

	if env == "production" || env == "prod" {
		return ENV_PRODUCTION
	}

	if env == "test" {
		return ENV_TEST
	}

	return ENV_DEVELOPMENT
}

func (env Environment) String() string {
	if env == ENV_PRODUCTION {
		return "production"
	}

	if env == ENV_TEST {
		return "test"
	}

	return "development"
}

// Checks the env for envVarName and determines the current environment
// depending on its value.
//
// Defaults to DEVELOPMENT
func GetCurrentEnv(envVarName string) (env Environment, isProd bool) {
	isProd = false
	env = ENV_DEVELOPMENT

	if envVarName == "" {
		envVarName = DEFAULT_ENV_NAME
	}

	envVar := os.Getenv(envVarName)
	envVar = strings.ToLower(envVar)

	if envVar == "production" || envVar == "prod" {
		isProd = true
		env = ENV_PRODUCTION
	}

	if envVar == "test" {
		isProd = false
		env = ENV_TEST
	}

	return
}

func (svc *Service) Env() Environment {
	return svc.env
}

func (svc *Service) IsProd() bool {
	return svc.isProd
}
