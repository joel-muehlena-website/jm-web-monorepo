package gomstoolkit

import (
	"context"
	"fmt"
	"net"

	"go.uber.org/atomic"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-middleware/providers/prometheus"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/telemetry"
)

type Grpc struct {
	logger          *zap.Logger
	server          *grpc.Server
	cfg             GrpcConfig
	isRegistered    bool
	enableTelemetry bool

	initialized     *atomic.Bool
	cancelCommander context.CancelCauseFunc
}

var _ SubService = &Grpc{}

func NewGRPC(logger *zap.Logger, cfg GrpcConfig) *Grpc {
	metrics := grpc_prometheus.NewServerMetrics(grpc_prometheus.WithServerHandlingTimeHistogram())

	var opts []grpc.ServerOption

	opts = append(opts, telemetry.GetGRPCServerHandler(), grpc.StreamInterceptor(metrics.StreamServerInterceptor()), grpc.UnaryInterceptor(metrics.UnaryServerInterceptor()))

	server := grpc.NewServer(opts...)

	if cfg.ServerReflection == nil || *cfg.ServerReflection {
		reflection.Register(server)
	}

	return &Grpc{
		logger:      logger.Named("grpc"),
		server:      server,
		cfg:         cfg,
		initialized: atomic.NewBool(false),
	}
}

func (grpcSvc *Grpc) Init() (chan<- Command, error) {
	cmd, cancel := StartCommander(context.Background(), grpcSvc.logger, 5, func(c Command) {
		switch c.Command {
		}
	}, func(ctx context.Context) {
		grpcSvc.logger.Error("Error in grpc commander context", zap.Error(ctx.Err()))
	})

	grpcSvc.cancelCommander = cancel

	return cmd, nil
}

func (grpcSvc *Grpc) Shutdown(ctx context.Context) {
	grpcSvc.cancelCommander(ErrShuttingDown)
	grpcSvc.server.GracefulStop()
}

func (grpcSvc *Grpc) IsInitialized() bool {
	return grpcSvc.initialized.Load()
}

func RegisterGrpc[T any](grpcSvc *Grpc, regFn func(*grpc.Server, T), regSvc T) {
	grpcSvc.isRegistered = true
	regFn(grpcSvc.server, regSvc)
}

func (grpcSvc *Grpc) Run() error {
	if !grpcSvc.isRegistered {
		grpcSvc.logger.Fatal("no grpc service registered yet")
	}

	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", grpcSvc.cfg.BindAddr, grpcSvc.cfg.Port))
	if err != nil {
		grpcSvc.logger.Fatal("Failed to listen ", zap.String("bindAddr", grpcSvc.cfg.BindAddr), zap.Uint16("port", grpcSvc.cfg.Port), zap.Error(err))
	}

	grpcSvc.logger.Info("Started tcp listener for grpc", zap.String("bindAddr", grpcSvc.cfg.BindAddr), zap.Uint16("port", grpcSvc.cfg.Port))

	go func() {
		grpcSvc.logger.Info("Try starting grpc server", zap.String("bindAddr", grpcSvc.cfg.BindAddr), zap.Uint16("port", grpcSvc.cfg.Port))
		err = grpcSvc.server.Serve(lis)
		if err != nil {
			grpcSvc.logger.Fatal("Failed to start grpc server", zap.Error(err))
		}
	}()

	return nil
}
