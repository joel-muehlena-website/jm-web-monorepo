load("@bazel_skylib//lib:paths.bzl", "paths")

def _make_build_folder_impl(ctx):
    tmp = ctx.actions.declare_file("tmp_XXXXX.tmp")

    base = tmp.dirname.replace(ctx.bin_dir.path, "")
    base = paths.normalize(base)
    base = base[1:]

    ctx.actions.write(
        output = tmp,
        content = "",
    )

    all_outputs = []
    for key, value in ctx.attr.folder_map.items():
        all_input_files = [
            f
            for f in key.files.to_list()
        ]

        for f in all_input_files:
            path = f.short_path
            path = path.replace(ctx.build_file_path.removesuffix("BUILD.bazel"), "")
            path = path.replace(base, "")
            path = paths.join(value, path)

            out = ctx.actions.declare_file(path)

            all_outputs.append(out)
            ctx.actions.run_shell(
                outputs = [out],
                inputs = depset([f]),
                arguments = [f.path, out.path],
                # This is what we're all about here. Just a simple 'cp' command.
                # Copy the input to CWD/f.basename, where CWD is the package where
                # the copy_filegroups_to_this_package rule is invoked.
                # (To be clear, the files aren't copied right to where your BUILD
                # file sits in source control. They are copied to the 'shadow tree'
                # parallel location under `bazel info bazel-bin`)
                command = 'cp -r "$1" "$2"',
            )

    return [
        DefaultInfo(
            files = depset(all_outputs),
            runfiles = ctx.runfiles(files = all_outputs),
        ),
    ]

make_build_folder = rule(
    implementation = _make_build_folder_impl,
    attrs = {
        "out": attr.string(default = "build"),
        "folder_map": attr.label_keyed_string_dict(),
        "copy_to_base": attr.bool(default = False),
    },
)
