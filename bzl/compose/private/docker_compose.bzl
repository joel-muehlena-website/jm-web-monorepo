load("//bzl/compose/private:provider.bzl", "DockerComposeServiceInfo")
load("//bzl/compose/private/util:compose_svc_dict.bzl", "clean_service_dict")
load("@bazel_skylib//rules:common_settings.bzl", "BuildSettingInfo")

def _impl(ctx):
    should_destroy = ctx.attr._destroy[BuildSettingInfo].value
    should_stop = ctx.attr._stop[BuildSettingInfo].value
    files = []

    svc_dict = {}

    image_names = []

    import_cmd = "#!/usr/bin/env bash\nset -o pipefail -o errexit -o nounset"

    import_cmd = ""
    for target in ctx.attr.targets:
        service_files = target[DefaultInfo].files.to_list()
        if len(service_files) != 1:
            fail("Unexpected length of tar files for docker-compose service: " + str(len(service_files)))

        files = files + service_files

        service = target[DockerComposeServiceInfo]

        image_names.append(service.image)

        name = target.label.name
        if service.extra_name != "":
            name = service.extra_name

        # Types: https://github.com/compose-spec/compose-go/blob/master/types/types.go
        svc_dict[name] = {
            "image": service.image,
            "ports": service.ports,
            "networks": service.networks,
            "environment": service.environment,
            "healthcheck": service.healthcheck,
            "volumes": service.volumes,
            "restart": service.restart,
            "labels": service.labels,
            "extra_hosts": service.extra_hosts,
        }

        import_cmd = import_cmd + "\ndata=$(cat %s | docker load)\nINFO \"${data} from %s\"" % (service_files[0].short_path, service_files[0].path)

    svc_dict = clean_service_dict(svc_dict)

    import_image_file = ctx.actions.declare_file(ctx.label.name + ".import.sh")

    # Create import file with data to import the image tars
    ctx.actions.expand_template(
        template = ctx.file._import_sh_tpl,
        output = import_image_file,
        is_executable = True,
        substitutions = {
            "{{import_cmd}}": import_cmd,
        },
    )

    yq = ctx.toolchains["@aspect_bazel_lib//lib:yq_toolchain_type"]

    compose_file_builder = ctx.actions.declare_file("compose-builder.sh")
    ctx.actions.expand_template(
        template = ctx.file._compose_sh_tpl,
        output = compose_file_builder,
        is_executable = True,
        substitutions = {
            "{{yq_path}}": yq.yqinfo.bin.path,
            "{{base_file_path}}": ctx.file.base.short_path,
        },
    )

    compose_file = ctx.actions.declare_file("docker-compose.bazel." + ctx.label.name + ".yaml")
    ctx.actions.run(
        inputs = depset([ctx.file.base]),
        outputs = [compose_file],
        tools = [yq.yqinfo.bin],
        arguments = [compose_file.path, json.encode(svc_dict)],
        progress_message = "Building docker-compose file",
        executable = compose_file_builder,
    )

    # Create executable file which first imports all images and then runs docker compose
    ctx.actions.expand_template(
        template = ctx.file._exec_sh_tpl,
        output = ctx.outputs.executable,
        is_executable = True,
        substitutions = {
            "{{target_name}}": ctx.label.name,
            "{{importer_path}}": import_image_file.short_path,
            "{{compose_file_path}}": compose_file.short_path,
            "{{should_destroy}}": "true" if should_destroy else "false",
            "{{should_stop}}": "true" if should_stop else "false",
            "{{image_names}}": " ".join(image_names),
        },
    )

    return [
        DefaultInfo(
            files = depset([compose_file] + ctx.files.data),
            runfiles = ctx.runfiles([import_image_file, compose_file] + files + ctx.files.data),
        ),
    ]

docker_compose = rule(
    implementation = _impl,
    attrs = {
        "targets": attr.label_list(allow_files = False, providers = [DockerComposeServiceInfo]),
        "base": attr.label(mandatory = True, allow_single_file = [".yaml", ".yml"]),
        "data": attr.label_list(allow_files = True),
        "_exec_sh_tpl": attr.label(
            default = "execute.sh.tpl",
            allow_single_file = True,
        ),
        "_import_sh_tpl": attr.label(
            default = "import.sh.tpl",
            allow_single_file = True,
        ),
        "_compose_sh_tpl": attr.label(
            default = "compose_builder.sh.tpl",
            allow_single_file = True,
        ),
        "_destroy": attr.label(
            default = "//bzl/compose/config:destroy",
        ),
        "_stop": attr.label(
            default = "//bzl/compose/config:stop",
        ),
    },
    executable = True,
    toolchains = ["@aspect_bazel_lib//lib:yq_toolchain_type"],
)
