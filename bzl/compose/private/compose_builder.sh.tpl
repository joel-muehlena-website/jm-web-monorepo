#!/usr/bin/env bash

set -o pipefail -o errexit -o nounset

function INFO() {
  log "\e[32mINFO\e[0m $1"
}

function ERROR() {
  log "\e[31mERROR\e[0m $1"
}

function log() {
  echo -e "[$(date -Iseconds)] $1"
}

readonly YQ_PATH="{{yq_path}}"
readonly BASE_FILE_PATH="{{base_file_path}}"

INFO "Running bazel docker compose file generator for $BASE_FILE_PATH to $1"

# Convert the passed json to yaml (bazel just has a json module for parsing dicts)
INFO "Converting json to yaml"
yamlConverted=$(echo -e "$2" | ${YQ_PATH} -P)

INFO "Merging base compose file and custom service spec"
echo -e "$yamlConverted" | ${YQ_PATH} eval-all 'select(fileIndex == 0) * {"services": select(fileIndex == 1)}' ${BASE_FILE_PATH} - > $1