"""This module contains functions to work with docker compose service spec dictionaries."""

def clean_service_dict(svc_dict):
    """
    Removes empty keys from compose spec service dictionary

    Args:
      svc_dict: Dictionary with docker copose service spec

    Returns:
      cleaned service dict
    """
    for svc in svc_dict:
        item = svc_dict[svc]
        if len(item["networks"]) == 0:
            item.pop("networks", None)

        if len(item["environment"]) == 0:
            item.pop("environment", None)

        if item["healthcheck"] == None or len(item["healthcheck"]) == 0:
            item.pop("healthcheck", None)

        if len(item["volumes"]) == 0:
            item.pop("volumes", None)

        if len(item["ports"]) == 0:
            item.pop("ports", None)

        if len(item["extra_hosts"]) == 0:
            item.pop("extra_hosts", None)

    return svc_dict
