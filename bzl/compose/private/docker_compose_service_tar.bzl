load("@bazel_skylib//rules:common_settings.bzl", "BuildSettingInfo")
load("//bzl/compose/private:provider.bzl", "DockerComposeServiceInfo")

def _impl(ctx):
    extra_env = ctx.attr._env_flag[BuildSettingInfo].value

    new_env = {}
    new_env.update(ctx.attr.environment)

    if len(extra_env) % 2 != 0:
        fail("Env flag must have even number of elements (target for env, env value[key=value]): %s" % extra_env)

    for i in range(0, len(extra_env) - 1):
        target = extra_env[i]
        v = extra_env[i + 1]

        if ctx.label == Label(target):
            new_env[v.split("=")[0]] = v.split("=")[1]

        i += 2

    return [
        DefaultInfo(files = ctx.attr.tar[DefaultInfo].files),
        DockerComposeServiceInfo(tar = ctx.attr.tar, extra_name = ctx.attr.extra_name, image = ctx.attr.image, ports = ctx.attr.ports, networks = ctx.attr.networks, environment = new_env, healthcheck = ctx.attr.healthcheck, volumes = ctx.attr.volumes, restart = ctx.attr.restart, labels = ctx.attr.labels, extra_hosts = ctx.attr.extra_hosts),
    ]

docker_compose_service_tar = rule(
    implementation = _impl,
    attrs = {
        "tar": attr.label(),
        "extra_name": attr.string(default = ""),
        "image": attr.string(mandatory = True),
        "ports": attr.string_list(default = []),
        "networks": attr.string_list(default = []),
        "environment": attr.string_dict(default = {}),
        "healthcheck": attr.string_dict(default = {}),
        "volumes": attr.string_list(default = []),
        "restart": attr.string(default = "no"),
        "labels": attr.string_list(default = []),
        "extra_hosts": attr.string_list(default = []),
        "_env_flag": attr.label(
            default = "//bzl/compose/config:env",
        ),
    },
    provides = [
        DefaultInfo,
        DockerComposeServiceInfo,
    ],
)
