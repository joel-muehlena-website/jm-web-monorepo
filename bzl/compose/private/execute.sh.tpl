#!/usr/bin/env bash

set -o pipefail -o errexit -o nounset

function INFO() {
  log "\e[32mINFO\e[0m $1"
}

function ERROR() {
  log "\e[31mERROR\e[0m $1"
}

function WARN() {
  log "\e[33mWARN\e[0m $1"
}

function log() {
  echo -e "[$(date -Iseconds)] $1"
}

readonly SHOULD_DESTROY={{should_destroy}}
readonly SHOULD_STOP={{should_stop}}

INFO "Running bazel docker compose integration"
INFO "Target name {{target_name}}"

if [[ $SHOULD_DESTROY == "false" ]] && [[ $SHOULD_STOP == "false" ]] ; then
  INFO "Starting docker image importing for {{importer_path}}"
  {{importer_path}}
fi


if [[ $SHOULD_DESTROY == "true" ]]; then
  INFO "Destroying docker compose"
  docker compose -f {{compose_file_path}} down --volumes

  INFO "Removing images {{image_names}}"

  for image_name in {{image_names}}; do
    INFO "Removing image $image_name"

    image_ids=$(docker images -q $image_name)
    if [[ -z $image_ids ]]; then
      WARN "Image $image_name not found. Maybe already removed?"
      continue
    fi

    docker rmi $image_ids > /dev/null 2>&1 || ERROR "Failed to remove image $image_name"
  done

elif [[ $SHOULD_STOP == "true" ]]; then
  INFO "Stopping docker compose"
  docker compose -f {{compose_file_path}} stop
else
  INFO "Starting docker compose"
  docker compose -f {{compose_file_path}} up --remove-orphans
fi