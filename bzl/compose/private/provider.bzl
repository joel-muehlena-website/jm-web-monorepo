"""
This module contains the definitions of custom providers for the compose rules.
"""

def _init_docker_compose_service_info(*, tar, image, extra_name = "", ports = [], networks = [], environment = {}, healthcheck = None, volumes = [], restart = "no", labels = [], extra_hosts = []):
    return {
        "tar": tar,
        "extra_name": extra_name,
        "image": image,
        "ports": ports,
        "networks": networks,
        "environment": environment,
        "healthcheck": healthcheck,
        "volumes": volumes,
        "restart": restart,
        "labels": labels,
        "extra_hosts": extra_hosts,
    }

DockerComposeServiceInfo, _new_docker_compose_service_info = provider(
    doc = "Information about a docker-compose service",
    fields = {
        "tar": "The oci tarball to use for this service",
        "extra_name": "Indicates if an other name should be used for this service",
        "image": "The remote or local image to use for this service",
        "ports": "The ports to expose",
        "networks": "The networks to use",
        "environment": "The environment variables to set",
        "healthcheck": "The healthcheck to use",
        "volumes": "The volumes to use",
        "restart": "The restart policy to use",
        "labels": "The labels to use",
        "extra_hosts": "The extra hosts to use",
    },
    init = _init_docker_compose_service_info,
)
