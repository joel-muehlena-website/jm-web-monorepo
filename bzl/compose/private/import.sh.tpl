#!/usr/bin/env bash

set -o pipefail -o errexit -o nounset

function INFO() {
  log "\e[32mINFO\e[0m $1"
}

function ERROR() {
  log "\e[31mERROR\e[0m $1"
}

function log() {
  echo -e "[$(date -Iseconds)] $1"
}

{{import_cmd}}

INFO "Importing done!"
