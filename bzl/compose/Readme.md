# Compose rules for bazel

```shell
# Destroy is evaluated before stop, so if both are defined destroy will be executed

bazel run //api:api-compose

bazel run --//bzl/compose/config:destroy //api/compose:api-compose

bazel run --//bzl/compose/config:stop //api/compose:api-compose

bazel run --//bzl/compose/config:env="//config-server/cmd:compose GITLAB_API_TOKEN=<TOKEN>" //api/compose:api-compose
```
