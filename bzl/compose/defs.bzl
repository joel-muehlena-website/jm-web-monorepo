"""
Rule defs for docker-compose.
"""

load("@rules_oci//oci:defs.bzl", "oci_tarball")
load("//bzl/compose/private:docker_compose_service_tar.bzl", _docker_compose_service_tar = "docker_compose_service_tar")
load("//bzl/compose/private:docker_compose.bzl", _docker_compose = "docker_compose")

def docker_compose_service(name, image, repo_tags, **kwargs):
    """
    Macro wrapper arround [docker_compose_service_tar](#docker_compose_service_tar).

    Macro wrapping docker_compose_service_tar which uses oci_tarball to create a tarball of the provided image.
    Same functionality like using docker_compose_service_tar with a tarball created by oci_tarball. But saves some line in the build file.

    Args:
        name: Name of the service
        image: Labels of the oci_image
        repo_tags: List of tags for the tarball (docker image) which is created by oci_tarball
        **kwargs: Additional arguments passed to [docker_compose_service_tar](#docker_compose_service_tar)
    """

    OCI_TAR_NAME_ENDING = "_oci_tar"

    oci_tarball(
        name = name + OCI_TAR_NAME_ENDING,
        image = image,
        repo_tags = repo_tags,
    )

    docker_compose_service_tar(image = repo_tags[0], name = name, tar = name + OCI_TAR_NAME_ENDING, **kwargs)

docker_compose = _docker_compose

# TODO fix healthcheck (test is an array)
docker_compose_service_tar = _docker_compose_service_tar
