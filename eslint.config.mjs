// @ts-check

import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
import reactRecommended from 'eslint-plugin-react/configs/recommended.js';
import jsxRuntime from "eslint-plugin-react/configs/jsx-runtime.js"
import globals from 'globals';
import reactHooks from 'eslint-plugin-react-hooks';

export default tseslint.config(
  {
    ignores: [
      ".yarn",
      "build",
      ".pnp.cjs",
      ".pnp.loader.mjs",
      "eslint.config.mjs",
      "**/*.css",
      "**/*.scss",
      "**/*.sass",
      "**/*.less",
      "**/*.json",
      "**/*.test.*",
      "**/*.spec.*",
    ],
  },
  eslint.configs.recommended,
  {
    rules: {
      // TODO: Enforce by setting to error
      "no-restricted-exports": ["warn", {
        "restrictDefaultExports": { "direct": true }
      }],
      "prefer-arrow-callback": ["error"],
      "prefer-promise-reject-errors": ["error"],
    }
  },
  {
    files: ['**/*.{js,mjs,cjs,jsx,mjsx,ts,tsx,mtsx}'],
    ...reactRecommended,
    settings: {
      "react": {
        "version": "detect"
      }
    }
  },
  {
    files: ['**/*.{js,mjs,cjs,jsx,mjsx,ts,tsx,mtsx}'],
    ...jsxRuntime
  },
  {
    files: ['**/*.{js,mjs,cjs,jsx,mjsx,ts,tsx,mtsx}'],
    plugins: {
      "react-hooks": reactHooks,
    },
    // @ts-ignore - Not yet correctly setup 100% for new eslint format by facebook: https://github.com/facebook/react/issues/28313
    rules: {
      ...reactHooks.configs.recommended.rules,
    },
  },
  {
    files: ['**/*.{js,mjs,cjs,jsx,mjsx,ts,tsx,mtsx}'],
    languageOptions: {
      globals: {
        ...globals.serviceworker,
        ...globals.browser,
      },
    },
  },
  ...tseslint.configs.recommended, // FIXME: Replace with recommendedTypeChecked -- not working with bazel atm
  // ...tseslint.configs.recommendedTypeChecked,
  // {
  //   languageOptions: {
  //     parserOptions: {
  //       project: ["tsconfig.json"],
  //       tsconfigRootDir: import.meta.dirname
  //     }
  //   },
  // },
  {
    rules: {
      "require-await": ["warn"]
      //"@typescript-eslint/require-await": ["error"] // TODO: Enable for typed checked
    }
  }
);
