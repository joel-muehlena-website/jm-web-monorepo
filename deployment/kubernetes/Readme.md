# Bootstrap

```bash
export GITLAB_TOKEN=<gh-token>

flux bootstrap gitlab \
  --token-auth=false \
  --hostname=gitlab.com \
  --owner=joel-muehlena-website \
  --repository=jm-web-monorepo \
  --branch=main \
  --path=deployment/kubernetes/clusters/jm-hetzner

kubectl apply -f deployment/kubernetes/clusters/jm-hetzner/kubeseal.yaml

flux reconcile kustomization flux-sealed-secrets  

kubeseal --fetch-cert \
--controller-name=sealed-secrets-controller \
--controller-namespace=sealed-secrets > deployment/kubernetes/clusters/jm-hetzner/pub-sealed-secrets.pem

# See https://docs.gitlab.com/ee/user/clusters/agent/gitops/flux_tutorial.html#install-agentk for raw agent-token-secret.yaml
kubeseal --format=yaml --cert=deployment/kubernetes/clusters/jm-hetzner/pub-sealed-secrets.pem \
< deployment/kubernetes/clusters/jm-hetzner/gitlab-kas/agent-token-secret.yaml > deployment/kubernetes/clusters/jm-hetzner/gitlab-kas/agent-token-secret-sealed.yaml

kubectl apply -f deployment/kubernetes/clusters/jm-hetzner/gitlab-kas.yaml

kubectl apply -f deployment/kubernetes/clusters/jm-hetzner/cluster-setup.yaml
```

```bash
# Encrypt
sops --age=age1ar67sr7d2shhkqurh0zkzqfmgrz8xjpyac7fmt60ds4rxfeg7s9qefz4k3 --encrypt --encrypted-regex '^(data|stringData)$' --in-place deployment/kubernetes/namespaces/minio-main-tenant/main-tenant-secret.yaml


# Decrypt
# Add password to ~/.config/sops/age/keys.txt
sops --decrypt --in-place deployment/kubernetes/namespaces/minio-main-tenant/main-tenant-secret.yaml

```