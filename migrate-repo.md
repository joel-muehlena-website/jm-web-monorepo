# How to migrate a git repo

NOTICE: **CHECK IF THE REPO IS PUBLIC IF NOT THERE ARE MAYBE STILL KEYS WHICH HAVE TO BE REMOVED**

**Do not push old tags**

The following steps should be performed to migrate a existing repo with history into this monorepo. For new projects to further steps are required.

```bash
cd <repo to migrate>
git pull
git mv <your content> /new/dest/in/monorepo
git commit -m "Monorepo prep"
git filter-repo --tag-rename '':'<name>-'
cd <repo to migrate into>
git remote add source-repo /path/to/repo/to/migrate
git fetch source-repo
git branch source-repo remotes/source-repo/main
git merge source-repo --allow-unrelated-histories
git branch -d source-repo
git remote remove source-repo
```
