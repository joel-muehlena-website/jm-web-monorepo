use actix_cors::Cors;
use actix_web::http;
use anyhow::bail;
use serde::Deserialize;
use std::{fs, path::PathBuf};
use thiserror::Error;

use crate::custom_user_service::UserServiceConfig;

#[derive(Deserialize, Debug)]
pub struct ServerConfig {
    #[serde(flatten)]
    pub config: AppConfig,
    pub logging: LoggingConfig,
    pub iam: ds_auth::config::Config,
    pub user_service: UserServiceConfig,
}

#[derive(Deserialize, Debug)]
pub struct LoggingConfig {
    level: String,
}

#[derive(Deserialize, Debug)]
pub struct AppConfig {
    pub server: HTTPServerConfig,
}

#[derive(Deserialize, Debug)]
pub struct HTTPServerConfig {
    #[serde(default = "_default_bind_addr")]
    pub bind_addr: String,

    #[serde(default = "_default_port")]
    pub port: u16,

    pub cors: CorsConfig,
}

#[derive(Deserialize, Clone, Debug)]
pub struct CorsConfig {
    pub allowed_origins: Vec<String>,

    #[serde(default = "_default_allowed_methods")]
    pub allowed_methods: Vec<String>,

    #[serde(default = "_default_allowed_headers")]
    pub allowed_headers: Vec<String>,

    #[serde(default)]
    pub exposed_headers: Vec<String>,

    #[serde(default = "_default_allow_credentials")]
    pub allow_credentials: bool,

    #[serde(default = "_default_max_age")]
    pub max_age: usize,

    #[serde(skip_serializing, skip_deserializing)]
    parsed_methods: Vec<http::Method>,
    #[serde(skip_serializing, skip_deserializing)]
    parsed_headers: Vec<http::header::HeaderName>,
}

impl CorsConfig {
    pub fn validate(&mut self) -> Result<(), anyhow::Error> {
        self.parsed_methods = CorsConfig::parse_methods(&self.allowed_methods)?;
        self.parsed_headers = CorsConfig::parse_headers(&self.allowed_headers)?;

        Ok(())
    }

    pub fn get_cors(&self) -> Cors {
        let allowed_origins = self.allowed_origins.clone();

        let mut cors = Cors::default()
            .allowed_origin_fn(move |origin, _req_head| {
                let origin = match origin.to_str() {
                    Ok(origin) => String::from(origin),
                    Err(_) => {
                        return false;
                    }
                };

                if allowed_origins.contains(&origin) {
                    return true;
                }

                false
            })
            .allowed_methods(self.parsed_methods.clone())
            .allowed_headers(self.parsed_headers.clone())
            .max_age(self.max_age);

        if self.allow_credentials {
            cors = cors.supports_credentials();
        }

        cors
    }

    fn parse_methods(methods_str: &Vec<String>) -> Result<Vec<http::Method>, anyhow::Error> {
        let mut methods: Vec<http::Method> = Vec::new();

        for raw_method in methods_str {
            let method = http::Method::from_bytes(raw_method.as_bytes());
            let method = match method {
                Ok(method) => method,
                Err(err) => {
                    bail!("Failed to parse HTTP method ({}): {}", raw_method, err);
                }
            };
            methods.push(method);
        }

        Ok(methods)
    }

    fn parse_headers(
        headers: &Vec<String>,
    ) -> Result<Vec<http::header::HeaderName>, anyhow::Error> {
        let mut header_vec: Vec<http::header::HeaderName> = Vec::new();

        for raw_header in headers {
            let header_name = http::header::HeaderName::from_bytes(raw_header.as_bytes());
            let header_name = match header_name {
                Ok(header_name) => header_name,
                Err(err) => {
                    bail!("Failed to parse header name ({}): {}", raw_header, err);
                }
            };
            header_vec.push(header_name);
        }

        Ok(header_vec)
    }
}

const fn _default_allow_credentials() -> bool {
    true
}

const fn _default_max_age() -> usize {
    3600
}

fn _default_allowed_methods() -> Vec<String> {
    vec![
        String::from("GET"),
        String::from("POST"),
        String::from("PATCH"),
        String::from("DELETE"),
    ]
}

fn _default_allowed_headers() -> Vec<String> {
    vec![
        http::header::AUTHORIZATION.as_str().to_string(),
        http::header::ACCEPT.as_str().to_string(),
        http::header::CONTENT_TYPE.as_str().to_string(),
    ]
}

const fn _default_port() -> u16 {
    8080
}

fn _default_bind_addr() -> String {
    "0.0.0.0".to_string()
}

#[derive(Error, Debug)]
pub enum ConfigError {
    #[error("Failed to load config file")]
    LoadFileError(#[from] std::io::Error),
    #[error("Failed to parse config file: {0:?}")]
    ParseError(#[from] toml::de::Error),
}

pub fn parse_config<CfgType: for<'de> toml::macros::Deserialize<'de>>(
    filename: &PathBuf,
) -> Result<CfgType, ConfigError> {
    let contents: String = fs::read_to_string(filename)?;
    let config: CfgType = toml::from_str(&contents)?;

    Ok(config)
}
