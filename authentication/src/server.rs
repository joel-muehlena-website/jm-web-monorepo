use std::sync::{Arc, RwLock};

use ds_auth::{IAMLock, IAM};

pub struct Server {
    pub iam: IAMLock,
}

impl Server {
    pub fn new(iam: IAM) -> Self {
        Server {
            iam: Arc::new(RwLock::new(iam)),
        }
    }
}
