use actix_web::{middleware, web, App, HttpResponse, HttpServer};
use anyhow::bail;
use clap::Parser;
use opentelemetry::global;
use std::{io::ErrorKind, path::PathBuf, sync::Arc};
use tokio;
use tracing_actix_web::TracingLogger;

use tracing::{info, trace};

mod cli;
mod config;
mod custom_user_service;
mod grpc;
mod server;
mod tracer;

use crate::custom_user_service::JMUserService;

use config::{parse_config, ServerConfig};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let cli = cli::Cli::parse();

    match &cli.command {
        cli::Commands::Run(run) => run_auth_server(&run.config).await,
    }
}

async fn run_auth_server(config_path: &PathBuf) -> anyhow::Result<()> {
    let mut config: ServerConfig = parse_config(config_path)?;

    tracer::init_tracer()?;

    info!("Started digital signage server");
    info!("Copyright 2023 Joel Mühlena");
    trace!("Loaded config: {:?}", config);

    let user_service = JMUserService::new(config.user_service);

    let iam = ds_auth::IAM::new_with_user_service(config.iam, Arc::new(user_service))
        .expect("Failed to create iam");

    iam.init().await;

    let metrics = iam.metrics.clone();

    let server = server::Server::new(iam);
    let iam_data = web::Data::new(server.iam);

    config.config.server.cors.validate()?;

    let http_res = HttpServer::new(move || {
        let cors = config.config.server.cors.get_cors();

        // TODO: Create dedicated server for metrics & health checks

        App::new()
            .wrap(TracingLogger::default())
            .wrap(middleware::NormalizePath::trim())
            .wrap(cors)
            .app_data(iam_data.clone())
            .app_data(web::Data::new(metrics.clone()))
            .service(
                web::scope("/auth")
                    .service(ds_auth::api::authorize)
                    .service(ds_auth::api::verify)
                    .service(ds_auth::api::refresh)
                    .service(ds_auth::api::meta::get)
                    .service(ds_auth::api::authorized::authorized),
            )
            .route("/metrics", web::get().to(ds_auth::metrics::metrics_handler))
            .route("/", web::get().to(HttpResponse::Ok))
    })
    .bind((config.config.server.bind_addr, config.config.server.port))?
    .run()
    .await;

    info!("Shutting down");

    global::shutdown_tracer_provider();

    match http_res {
        Ok(_) => Ok(()),
        Err(e) => match e.kind() {
            ErrorKind::AddrInUse => {
                bail!("Address already in use. Is the server already running?")
            }
            _ => bail!(e),
        },
    }
}
