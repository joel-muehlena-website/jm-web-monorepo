use std::str::FromStr;

use async_trait::async_trait;
use ds_auth::{
    meta::{Meta, MetaInfo},
    models::User,
    user_service::{Meta as UserServiceMeta, UserService, UserServiceError},
};
use serde::Deserialize;
use tracing::debug;

use crate::grpc::user_service;

#[derive(Deserialize, Debug)]
pub struct UserServiceConfig {
    url: String,
}

pub struct JMUserService {
    config: UserServiceConfig,
}

impl JMUserService {
    pub fn new(config: UserServiceConfig) -> Self {
        JMUserService { config }
    }
}

#[async_trait]
impl UserService for JMUserService {
    async fn get_user(&self, email: &String) -> Result<User, UserServiceError> {
        let mut client = user_service::user_client::UserClient::connect(self.config.url.clone())
            .await
            .map_err(|e| UserServiceError::FailedToFetchUser(Box::from(e)))?;

        let request = tonic::Request::new(user_service::GetUsersRequest {
            limit: Some(1),
            offset: None,
            options: Some(user_service::GetUserOptions {
                email: email.clone(),
            }),
        });

        let response = client
            .get_full_users(request)
            .await
            .map_err(|e| UserServiceError::FailedToFetchUser(Box::from(e)))?;

        debug!("User by email: {:?}", response);

        let user = match response.into_inner().users.first() {
            Some(u) => u.clone(),
            None => return Err(UserServiceError::UserNotFound),
        };

        let public_user = user.public_user.unwrap();

        Ok(User {
            email: public_user.email,
            firstname: public_user.first_name.clone(),
            lastname: public_user.last_name.clone(),
            username: public_user.username.clone(),
            id: uuid::Uuid::from_str(public_user.id.as_str()).unwrap(),
            locale: Some("".to_string()),
            password: Some(user.password),
            picture: Some(public_user.avatar),
        })
    }

    async fn get_user_by_id(&self, uid: uuid::Uuid) -> Result<User, UserServiceError> {
        let mut client = user_service::user_client::UserClient::connect(self.config.url.clone())
            .await
            .map_err(|e| UserServiceError::FailedToFetchUser(Box::from(e)))?;

        let request = tonic::Request::new(user_service::GetUserByIdRequest {
            uuid: uid.to_string(),
        });

        let response = client
            .get_full_user_by_id(request)
            .await
            .map_err(|e| UserServiceError::FailedToFetchUser(Box::from(e)))?;

        debug!("User by id: {:?}", response);

        let user = response.into_inner();

        let public_user = user.public_user.unwrap();

        Ok(User {
            email: public_user.email,
            firstname: public_user.first_name.clone(),
            lastname: public_user.last_name.clone(),
            username: public_user.username.clone(),
            id: uuid::Uuid::from_str(public_user.id.as_str()).unwrap(),
            locale: Some("".to_string()),
            password: Some(user.password),
            picture: Some(public_user.avatar),
        })
    }
}

impl MetaInfo for JMUserService {
    fn meta(&self) -> Box<dyn Meta> {
        Box::new(UserServiceMeta {})
    }
}
