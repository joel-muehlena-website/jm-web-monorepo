use std::path::PathBuf;

use clap::{Args, Parser, Subcommand};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = false)]
pub struct Cli {
    #[command(subcommand)]
    pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    Run(Run),
}

#[derive(Args)]
pub struct Run {
    /// Path to the config file
    #[arg(short, long)]
    pub config: PathBuf,
}
