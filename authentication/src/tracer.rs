use opentelemetry::{global, KeyValue};
use opentelemetry_otlp::WithExportConfig;
use opentelemetry_sdk::{propagation::TraceContextPropagator, runtime, Resource};
use tracing_subscriber::{filter, prelude::*, Registry};

pub fn init_tracer() -> anyhow::Result<()> {
    let fmt_layer = tracing_subscriber::fmt::layer().with_filter(filter::LevelFilter::DEBUG);

    let tracer = opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_exporter(
            opentelemetry_otlp::new_exporter()
                .tonic()
                .with_endpoint("http://localhost:4317")
                .with_timeout(std::time::Duration::from_secs(5)),
        )
        .with_trace_config(
            opentelemetry_sdk::trace::config().with_resource(Resource::new(vec![KeyValue::new(
                opentelemetry_semantic_conventions::resource::SERVICE_NAME,
                "authentication-service",
            )])),
        )
        .install_batch(runtime::Tokio)?;

    global::set_tracer_provider(tracer.provider().unwrap());
    global::set_text_map_propagator(TraceContextPropagator::new());
    let telemetry = tracing_opentelemetry::layer().with_tracer(tracer);

    let subscriber = Registry::default().with(fmt_layer).with(telemetry);

    tracing::subscriber::set_global_default(subscriber)?;

    Ok(())
}
