#!/bin/bash

out_dir="${1:-/etc/iam/config/keys}"

openssl genpkey -algorithm ed25519 -out $out_dir/jwt_access_private.pem
openssl genpkey -algorithm ed25519 -out $out_dir/jwt_refresh_private.pem

openssl pkey -in $out_dir/jwt_access_private.pem -pubout -out $out_dir/jwt_access_public.pem
openssl pkey -in $out_dir/jwt_refresh_private.pem -pubout -out $out_dir/jwt_refresh_public.pem

openssl pkey -in $out_dir/jwt_access_private.pem -outform DER -out $out_dir/jwt_access_private.der
openssl pkey -in $out_dir/jwt_refresh_private.pem -outform DER -out $out_dir/jwt_refresh_private.der

# kubectl create secret generic auth-server-ed-keys --dry-run=client -o yaml \
#     --from-file=$out_dir/jwt_access_private.pem \
#     --from-file=$out_dir/jwt_access_public.pem \
#     --from-file=$out_dir/jwt_access_private.der \
#     --from-file=$out_dir/jwt_refresh_private.pem \
#     --from-file=$out_dir/jwt_refresh_public.pem \
#     --from-file=$out_dir/jwt_refresh_private.der

echo "Keys generated in $out_dir"
echo ""

echo "Run the following command to create a k8s secret with the generated keys:"
echo ""
echo "kubectl create secret generic auth-server-ed-keys --dry-run=client -o yaml \\"
echo "    --from-file=$out_dir/jwt_access_private.pem \\"
echo "    --from-file=$out_dir/jwt_access_public.pem \\"
echo "    --from-file=$out_dir/jwt_access_private.der \\"
echo "    --from-file=$out_dir/jwt_refresh_private.pem \\"
echo "    --from-file=$out_dir/jwt_refresh_public.pem \\"
echo "    --from-file=$out_dir/jwt_refresh_private.der"