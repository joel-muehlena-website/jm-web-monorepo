package configServerAddon

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
)

type LogLevel uint8

const (
	INFO  LogLevel = iota // Log level for printing only info messages
	ERROR LogLevel = iota // Log level for printing info messages and all error messages
)

// Result passed back by the fetch function
type Result struct {
	Data  []byte
	Error error
}

type GitlabResponseJSON struct {
	ConfigB64Data string `json:"configB64Data"`
	FileEnding    string `json:"fileEnding"`
}

func errorAbort(err error, c chan<- Result, opts *configOptions) {
	if err == nil {
		err = fmt.Errorf("unexpected error")
	}

	if opts.isLog && opts.logLevel == ERROR {
		if opts.zap != nil {
			opts.zap.Sugar().Errorf("[CONFIG SERVER ADDON ERROR] %w", err)
		} else {
			println("[CONFIG SERVER ADDON ERROR]", err.Error())
		}
	}

	c <- Result{make([]byte, 0), err}
}

// Fetch the config from the provided config server url
func FetchConfig(configServerUrl string, c chan<- Result, options ...ConfigFetchOption) {
	opts := &configOptions{}

	for _, op := range options {
		err := op(opts)
		if err != nil {
			errorAbort(err, c, opts)
			return
		}
	}

	if opts.filePath == "" && opts.fileName == "" {
		errorAbort(fmt.Errorf("please create with a file path or a file name"), c, opts)
		return
	}

	var request *http.Request
	var err error

	if opts.isCtx {
		request, err = http.NewRequestWithContext(opts.ctx, "GET", configServerUrl, nil)
	} else {
		request, err = http.NewRequest("GET", configServerUrl, nil)
	}

	if err != nil {
		errorAbort(err, c, opts)
		return
	}

	query := request.URL.Query()

	if opts.filePath != "" {
		query.Add("filePath", url.QueryEscape(opts.filePath))
	} else {
		if opts.fileName != "" {

			fn := opts.fileName

			if opts.env != "" {
				fn += ":" + opts.env
			}

			query.Add("filename", url.QueryEscape(fn))
		}

		if opts.fileEnding != "" {
			query.Add("gitlabFileEnding", opts.fileEnding)
		}
	}

	request.URL.RawQuery = query.Encode()
	httpClient := &http.Client{}
	resp, err := httpClient.Do(request)
	if err != nil {
		errorAbort(err, c, opts)
		return
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		err = fmt.Errorf("unexpected response code %d", resp.StatusCode)

		errorAbort(err, c, opts)
		return
	}

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		errorAbort(err, c, opts)
		return
	}

	jsonResp := GitlabResponseJSON{}
	err = json.Unmarshal(bodyBytes, &jsonResp)
	if err != nil {
		errorAbort(err, c, opts)
		return
	}

	b64Decode, err := base64.StdEncoding.DecodeString(jsonResp.ConfigB64Data)
	if err != nil {
		errorAbort(err, c, opts)
		return
	}

	if opts.isSave {

		filename := opts.fileName

		if opts.env != "" {
			filename += "." + opts.env
		}

		if opts.filePath != "" {
			filename = filepath.Base(opts.filePath)
			filename = strings.TrimSuffix(filename, path.Ext(filename))
		}

		pathToWrite := filepath.Join(opts.savePath, filename+"."+jsonResp.FileEnding)

		wBytes, err := saveFile(pathToWrite, b64Decode)
		if err != nil {
			errorAbort(err, c, opts)
			return
		}

		if opts.isSave && opts.logLevel == INFO {
			if opts.zap != nil {
				opts.zap.Sugar().Infof("[CONFIG SERVER ADDON INFO] Provided PATH: %s", pathToWrite)
				opts.zap.Sugar().Infof("[CONFIG SERVER ADDON INFO] Saved file. Wrote %d Bytes", wBytes)
			} else {
				println("[CONFIG SERVER ADDON INFO] Provided PATH: " + pathToWrite)
				println("[CONFIG SERVER ADDON INFO] Saved file. Wrote " + strconv.Itoa(wBytes) + " Bytes")
			}
		}
	}

	c <- Result{b64Decode, nil}
}

func saveFile(path string, data []byte) (int, error) {
	err := os.MkdirAll(filepath.Dir(path), os.ModePerm)
	if err != nil {
		return 0, err
	}

	f, err := os.Create(path)
	if err != nil {
		return 0, err
	}

	defer f.Close()

	wBytes, err := f.Write(data)
	if err != nil {
		return 0, err
	}

	return wBytes, nil
}
