package configServerAddon

import (
	"context"

	"go.uber.org/zap"
)

// Type which defines a config option function
type ConfigFetchOption func(*configOptions) error

type configOptions struct {
	filePath   string
	fileName   string
	fileEnding string
	env        string
	isSave     bool
	savePath   string
	isLog      bool
	logLevel   LogLevel
	zap        *zap.Logger
	ctx        context.Context
	isCtx      bool
}

// Fetch the config with a direct file path.
// Overrides all other naming attributes
func WithFilePath(filePath string) ConfigFetchOption {
	return func(co *configOptions) error {
		co.filePath = filePath
		return nil
	}
}

// Fetch the config with a custom file ending.
func WithFileEnding(fileEnding string) ConfigFetchOption {
	return func(co *configOptions) error {
		co.fileEnding = fileEnding
		return nil
	}
}

// The name of the file to fetch
func WithFileName(fileName string) ConfigFetchOption {
	return func(co *configOptions) error {
		co.fileName = fileName
		return nil
	}
}

// The env of the file to fetch. Will be appended between the filename and the file ending.
// E.g. dev or prod
func WithEnv(envName string) ConfigFetchOption {
	return func(co *configOptions) error {
		co.env = envName
		return nil
	}
}

// Saves the returned data to the given path
func WithSave(savePath string) ConfigFetchOption {
	return func(co *configOptions) error {
		co.isSave = true
		co.savePath = savePath
		return nil
	}
}

// Enables the log output
func WithLogging(lvl LogLevel) ConfigFetchOption {
	return func(co *configOptions) error {
		co.isLog = true
		co.logLevel = lvl
		return nil
	}
}

// Logs with a provided zap logger
func WithZap(zap *zap.Logger, lvl LogLevel) ConfigFetchOption {
	return func(co *configOptions) error {
		co.isLog = true
		co.logLevel = lvl
		co.zap = zap
		return nil
	}
}

// Cretae http requets with context
func WithContext(ctx context.Context) ConfigFetchOption {
	return func(co *configOptions) error {
		co.ctx = ctx
		co.isCtx = true
		return nil
	}
}
