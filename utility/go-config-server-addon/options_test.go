package configServerAddon

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestWithFilePath(t *testing.T) {
	op := WithFilePath("path.de/path?test.txt")

	co := configOptions{}
	err := op(&co)
	assert.Nil(t, err)
	assert.Equal(t, "path.de/path?test.txt", co.filePath)
}

func TestWithFileEnding(t *testing.T) {
	op := WithFileEnding("yaml")

	co := configOptions{}
	err := op(&co)
	assert.Nil(t, err)
	assert.Equal(t, "yaml", co.fileEnding)
}

func TestWithFileName(t *testing.T) {
	op := WithFileName("testFile")

	co := configOptions{}
	err := op(&co)
	assert.Nil(t, err)
	assert.Equal(t, "testFile", co.fileName)
}

func TestWithEnv(t *testing.T) {
	op := WithEnv("test")

	co := configOptions{}
	err := op(&co)
	assert.Nil(t, err)
	assert.Equal(t, "test", co.env)
}

func TestWithSave(t *testing.T) {
	op := WithSave("/etc/cfg/test")

	co := configOptions{}
	err := op(&co)
	assert.Nil(t, err)
	assert.Equal(t, "/etc/cfg/test", co.savePath)
	assert.True(t, co.isSave)
}

func TestWithLogging(t *testing.T) {
	op := WithLogging(ERROR)

	co := configOptions{}
	err := op(&co)
	assert.Nil(t, err)
	assert.Equal(t, ERROR, co.logLevel)
	assert.True(t, co.isLog)
}

func TestWithZap(t *testing.T) {
	lg, err := zap.NewDevelopmentConfig().Build()
	assert.Nil(t, err)

	op := WithZap(lg, ERROR)

	co := configOptions{}
	err = op(&co)
	assert.Nil(t, err)
	assert.Equal(t, ERROR, co.logLevel)
	assert.True(t, co.isLog)
	assert.NotNil(t, co.zap)
	assert.Equal(t, lg, co.zap)
}

func TestWithContext(t *testing.T) {
	ctx, _ := context.WithCancel(context.Background())

	op := WithContext(ctx)

	co := configOptions{}
	err := op(&co)
	assert.Nil(t, err)
	assert.Equal(t, ctx, co.ctx)
	assert.True(t, co.isCtx)
}
