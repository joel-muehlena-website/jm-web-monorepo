package configServerAddon

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

func TestErrorAbort(t *testing.T) {
	tErr := fmt.Errorf("an error ocurred")

	t.Run("Should log if log is enabled", func(t *testing.T) {
		ch := make(chan Result, 1)
		errorAbort(tErr, ch, &configOptions{isLog: true, logLevel: ERROR})

		select {
		case res := <-ch:
			assert.NotNil(t, res.Error)
			assert.Equal(t, 0, len(res.Data))
			assert.Equal(t, "an error ocurred", res.Error.Error())
		default:
			assert.Fail(t, "No error")
		}
	})

	t.Run("Should log to zap if enabled", func(t *testing.T) {
		ch := make(chan Result, 1)

		zp, err := zap.NewDevelopmentConfig().Build()
		assert.Nil(t, err)

		errorAbort(tErr, ch, &configOptions{isLog: true, logLevel: ERROR, zap: zp})

		select {
		case res := <-ch:
			assert.NotNil(t, res.Error)
			assert.Equal(t, 0, len(res.Data))
			assert.Equal(t, "an error ocurred", res.Error.Error())
		default:
			assert.Fail(t, "No error")
		}
	})

	t.Run("Should return default error if err is nil", func(t *testing.T) {
		ch := make(chan Result, 1)
		errorAbort(nil, ch, &configOptions{})

		select {
		case res := <-ch:
			assert.NotNil(t, res.Error)
			assert.Equal(t, 0, len(res.Data))
			assert.Equal(t, "unexpected error", res.Error.Error())
		default:
			assert.Fail(t, "No error")
		}
	})

	t.Run("Should return error as result", func(t *testing.T) {
		ch := make(chan Result, 1)
		errorAbort(tErr, ch, &configOptions{})

		select {
		case res := <-ch:
			assert.NotNil(t, res.Error)
			assert.Equal(t, 0, len(res.Data))
			assert.Equal(t, "an error ocurred", res.Error.Error())
		default:
			assert.Fail(t, "No error")
		}
	})
}

func TestFetchConfig(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.URL.RawQuery, "filename=test_timeout") {
			time.Sleep(3 * time.Second)
			w.WriteHeader(200)
		} else if strings.Contains(r.URL.RawQuery, "filename=test_300") {
			w.WriteHeader(300)
		} else if strings.Contains(r.URL.RawQuery, "filename=test_404") {
			w.WriteHeader(404)
		} else {

			w.WriteHeader(200)

			data := "Test data"

			jsonResp := GitlabResponseJSON{
				ConfigB64Data: base64.StdEncoding.EncodeToString([]byte(data)),
				FileEnding:    "yaml",
			}

			json.NewEncoder(w).Encode(jsonResp)

			w.WriteHeader(200)
		}
	}))

	t.Cleanup(func() {
		server.Close()
	})

	t.Parallel()

	t.Run("Should return error", func(t *testing.T) {
		t.Parallel()

		t.Run("Should return error if no path is set", func(t *testing.T) {
			ch := make(chan Result, 1)
			FetchConfig(server.URL, ch)

			select {
			case res := <-ch:
				assert.NotNil(t, res.Error)
				assert.Equal(t, 0, len(res.Data))
				assert.Equal(t, "please create with a file path or a file name", res.Error.Error())
			default:
				assert.Fail(t, "Expected result")
			}
		})

		t.Run("Should timeout a request with context", func(t *testing.T) {
			ch := make(chan Result, 1)
			ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
			defer cancel()
			FetchConfig(server.URL, ch, WithContext(ctx), WithFileName("test_timeout"))

			select {
			case res := <-ch:
				assert.NotNil(t, res.Error)
				assert.Equal(t, 0, len(res.Data))
				assert.Contains(t, res.Error.Error(), "context deadline exceeded")
			default:
				assert.Fail(t, "Expected result")
			}
		})

		t.Run("Should get an unexpected status code == 300", func(t *testing.T) {
			ch := make(chan Result, 1)
			FetchConfig(server.URL, ch, WithFileName("test_300"))

			select {
			case res := <-ch:
				assert.NotNil(t, res.Error)
				assert.Equal(t, 0, len(res.Data))
				assert.Equal(t, "unexpected response code 300", res.Error.Error())
			default:
				assert.Fail(t, "Expected result")
			}
		})

		t.Run("Should get an unexpected status code > 300", func(t *testing.T) {
			ch := make(chan Result, 1)
			FetchConfig(server.URL, ch, WithFileName("test_404"))

			select {
			case res := <-ch:
				assert.NotNil(t, res.Error)
				assert.Equal(t, 0, len(res.Data))
				assert.Equal(t, "unexpected response code 404", res.Error.Error())
			default:
				assert.Fail(t, "Expected result")
			}
		})
	})

	t.Run("Should succeed", func(t *testing.T) {
		ch := make(chan Result, 1)
		FetchConfig(server.URL, ch, WithFileName("test_success"), WithFileEnding("yaml"), WithEnv("test"))

		select {
		case res := <-ch:
			assert.Nil(t, res.Error)
			assert.Equal(t, "Test data", string(res.Data))
		default:
			assert.Fail(t, "Expected result")
		}
	})

	t.Run("Should succeed with save", func(t *testing.T) {
		t.Parallel()

		tmpDir := t.TempDir()
		savePath := filepath.Join(tmpDir, "test_success.test.yaml")

		ch := make(chan Result, 1)
		FetchConfig(server.URL, ch, WithFileName("test_success"), WithFileEnding("yaml"), WithEnv("test"), WithSave(tmpDir))

		select {
		case res := <-ch:
			assert.Nil(t, res.Error)
			assert.Equal(t, "Test data", string(res.Data))
			assert.FileExists(t, savePath)
		default:
			assert.Fail(t, "Expected result")
		}
	})

	t.Run("Should succeed with save and filepath", func(t *testing.T) {
		t.Parallel()

		tmpDir := t.TempDir()
		savePath := filepath.Join(tmpDir, "test_success_fp.test.yaml")

		ch := make(chan Result, 1)
		FetchConfig(server.URL, ch, WithFilePath("test_success_fp.test.yaml"), WithSave(tmpDir))

		select {
		case res := <-ch:
			assert.Nil(t, res.Error)
			assert.Equal(t, "Test data", string(res.Data))
			assert.FileExists(t, savePath)
		default:
			assert.Fail(t, "Expected result")
		}
	})

	t.Run("Should succeed with save and zap logger", func(t *testing.T) {
		t.Parallel()

		zp, err := zap.NewDevelopmentConfig().Build()
		assert.Nil(t, err)

		tmpDir := t.TempDir()
		savePath := filepath.Join(tmpDir, "test_success.test.yaml")

		ch := make(chan Result, 1)
		FetchConfig(server.URL, ch, WithZap(zp, INFO), WithFileName("test_success"), WithFileEnding("yaml"), WithEnv("test"), WithSave(tmpDir))

		select {
		case res := <-ch:
			assert.Nil(t, res.Error)
			assert.Equal(t, "Test data", string(res.Data))
			assert.FileExists(t, savePath)
		default:
			assert.Fail(t, "Expected result")
		}
	})
}

func TestSaveFile(t *testing.T) {
	dir := t.TempDir()

	t.Run("Should save file without errors", func(t *testing.T) {
		path := filepath.Join(dir, "testFolder", "test.txt")

		data := []byte("Test data")

		bytes, err := saveFile(path, data)

		assert.Nil(t, err)
		assert.Equal(t, len(data), bytes)

		_, err = os.Stat(path)
		assert.Nil(t, err)
	})
}
