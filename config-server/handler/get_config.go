package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"strings"

	"go.uber.org/zap"

	config_server "gitlab.com/joelMuehlena/jm-web-monorepo/config-server/config-server"

	"github.com/gin-gonic/gin"
)

type ResponseData struct {
	ConfigB64Data string `json:"configB64Data"`
	FileEnding    string `json:"fileEnding"`
}

type HttpHandler struct {
	ConfigServer *config_server.ConfigServer
	ProjectId    string
	Branch       string
	Logger       *zap.Logger
}

const (
	QUERY_PARAM_FILE_PATH   = "filePath"
	QUERY_PARAM_FILE_NAME   = "filename"
	QUERY_PARAM_FILE_ENDING = "gitlabFileEnding"
)

func (handler *HttpHandler) HandleGetConfigRequest(ctx *gin.Context) {
	gitlabFileEnding := ctx.Query(QUERY_PARAM_FILE_ENDING)
	if gitlabFileEnding == "" {
		gitlabFileEnding = "properties"
	}

	fullPathName, err := url.QueryUnescape(ctx.Query(QUERY_PARAM_FILE_PATH))
	if err != nil {
		handler.Logger.Error("Failed to unescape query param", zap.String("query-param", QUERY_PARAM_FILE_PATH), zap.Error(err))
		ctx.JSON(500, gin.H{"error": err})
		return
	}

	if fullPathName == "" {
		handler.Logger.Debug("No full path was provided. Constructing from query params")

		fileName, err := url.QueryUnescape(ctx.Query(QUERY_PARAM_FILE_NAME))
		if err != nil {
			handler.Logger.Error("Failed to unescape query param", zap.String("query-param", QUERY_PARAM_FILE_NAME), zap.Error(err))
			ctx.JSON(500, gin.H{"error": err})
			return
		}

		if fileName == "" {
			handler.Logger.Debug("No file name was provided")
			ctx.JSON(400, gin.H{"error": "Please provide a filename"})
			return
		}

		fileNameSplit := strings.Split(fileName, ":")
		fileName = fileNameSplit[0]
		env := ""
		if len(fileNameSplit) > 1 {
			env = fileNameSplit[1]
		}

		fullPathName = fmt.Sprintf("%s.%s.%s", fileName, env, gitlabFileEnding)
	}

	handler.Logger.Debug("Config path", zap.String("path", fullPathName))

	data, err := handler.getConfig(fullPathName)
	if err != nil {
		handler.Logger.Error("Failed to get config", zap.Error(err))
		if errors.Is(err, ErrNoConfigFile) {
			ctx.JSON(404, gin.H{"error": "Config file not found"})
			return
		}

		ctx.JSON(500, gin.H{"error": err})
		return
	}

	data.FileEnding = gitlabFileEnding

	handler.Logger.Debug("Inserting data into cache")
	dataToCacheBytes, err := json.Marshal(&data)
	if err != nil {
		handler.Logger.Error("Failed to marshal data for cache", zap.Error(err))
	}
	err = handler.ConfigServer.InsertDataToConfigCache(fullPathName, string(dataToCacheBytes))
	if err != nil {
		handler.Logger.Error("Failed to insert data into cache", zap.Error(err))
	}

	ctx.JSON(200, data)
}

var ErrNoConfigFile = fmt.Errorf("no config file found")

func (handler *HttpHandler) getConfig(filePath string) (ResponseData, error) {
	isData, data := handler.ConfigServer.GetDataFromConfigCache(filePath)

	if isData {
		handler.Logger.Info("Got data from cache", zap.String("filePath", filePath))
		dataStruct := ResponseData{}

		err := json.Unmarshal([]byte(data), &dataStruct)
		if err != nil {
			return ResponseData{}, err
		}

		return dataStruct, nil
	}

	handler.Logger.Info("No data in cache. Getting from gitlab", zap.String("filePath", filePath))
	gitlabData := config_server.GitlabRequest{FileName: filePath, Branch: handler.Branch, ProjectId: handler.ProjectId, Token: handler.ConfigServer.AccessToken(), GitlabUrl: "https://gitlab.com"}

	b64Data, err := gitlabData.GetGitlabFile()
	if err != nil {
		return ResponseData{}, err
	}

	if b64Data == "" {
		return ResponseData{}, ErrNoConfigFile
	}

	dataStruct := ResponseData{ConfigB64Data: b64Data}

	return dataStruct, nil
}
