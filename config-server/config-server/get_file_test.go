package config_server

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetGitlabFile(t *testing.T) {

	assert := assert.New(t)

	createTestServer := func(handler http.Handler) *httptest.Server {
		svr := httptest.NewServer(handler)
		return svr
	}

	defaultGR := GitlabRequest{Token: "test_token", FileName: "test_file.md", Branch: "main", ProjectId: "342323"}

	t.Parallel()

	t.Run("Should get data", func(t *testing.T) {
		t.Parallel()

		resp := GitlabFileResponse{
			Content: "test_content",
		}

		testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			err := json.NewEncoder(w).Encode(resp)
			assert.Nil(err)
		}))
		defer testServer.Close()

		gr := defaultGR
		gr.GitlabUrl = testServer.URL

		data, err := gr.GetGitlabFile()
		assert.Nil(err)
		assert.NotNil(data)
		assert.Equal(resp.Content, data)
	})

	t.Run("Should add PRIVATE-TOKEN header", func(t *testing.T) {
		t.Parallel()

		testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			assert.Equal(defaultGR.Token, r.Header.Get("PRIVATE-TOKEN"))

			w.WriteHeader(200)
		}))
		defer testServer.Close()

		gr := defaultGR
		gr.GitlabUrl = testServer.URL

		gr.GetGitlabFile()
	})

	t.Run("Should return error", func(t *testing.T) {

		t.Parallel()

		t.Run("If new request fails", func(t *testing.T) {
			t.Parallel()

			gr := defaultGR
			gr.GitlabUrl = "https://test.local:%%"

			data, err := gr.GetGitlabFile()
			assert.Equal("", data)
			assert.NotNil(err)
			assert.Contains(err.Error(), "invalid port")
		})

		t.Run("If do fails", func(t *testing.T) {
			t.Parallel()

			gr := defaultGR
			gr.GitlabUrl = "https://notvalid.local:43231"

			data, err := gr.GetGitlabFile()
			assert.Equal("", data)
			assert.NotNil(err)
			assert.Contains(err.Error(), "dial tcp:")
		})

		t.Run("If json decode fails", func(t *testing.T) {
			t.Parallel()

			testServer := createTestServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Content-Type", "application/json")

				w.Write([]byte(`{"name":what}`))
			}))
			defer testServer.Close()

			gr := defaultGR
			gr.GitlabUrl = testServer.URL
			data, err := gr.GetGitlabFile()

			assert.Equal("", data)
			assert.NotNil(err)
			assert.Contains(err.Error(), "invalid character 'w'")
		})

	})

}
