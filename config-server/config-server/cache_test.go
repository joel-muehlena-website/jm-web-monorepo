package config_server

import (
	"testing"

	"github.com/allegro/bigcache/v3"
	"github.com/stretchr/testify/assert"
)

func TestInitCache(t *testing.T) {
	t.Run("Should init cache without error", func(t *testing.T) {
		svr := ConfigServer{}
		err := svr.InitCache(nil)

		assert.Nil(t, err)
		assert.NotNil(t, svr.cache)
	})

	t.Run("Should init cache with custom cfg", func(t *testing.T) {
		svr := ConfigServer{}
		err := svr.InitCache(&bigcache.Config{Shards: 4})

		assert.Nil(t, err)
		assert.NotNil(t, svr.cache)
	})

	t.Run("Should return error if bigcache init fails", func(t *testing.T) {
		svr := ConfigServer{}
		err := svr.InitCache(&bigcache.Config{Shards: 3})

		assert.NotNil(t, err)
		assert.Nil(t, svr.cache)
	})
}

func TestGetDataFromConfigCache(t *testing.T) {
	svr := ConfigServer{}
	err := svr.InitCache(nil)
	assert.Nil(t, err)

	svr.cache.Set("test", []byte("value"))

	t.Run("Should get item", func(t *testing.T) {
		isfound, data := svr.GetDataFromConfigCache("test")

		assert.True(t, isfound)
		assert.Equal(t, "value", data)
	})

	t.Run("Should fail to get item", func(t *testing.T) {
		isfound, data := svr.GetDataFromConfigCache("notThere")

		assert.False(t, isfound)
		assert.Equal(t, "", data)
	})

}

func TestInsertDataToConfigCache(t *testing.T) {
	svr := ConfigServer{}
	err := svr.InitCache(nil)
	assert.Nil(t, err)

	t.Run("Should insert item and return no error", func(t *testing.T) {
		err = svr.InsertDataToConfigCache("test", "value")
		assert.Nil(t, err)

		data, err := svr.cache.Get("test")
		assert.Nil(t, err)

		assert.Equal(t, "value", string(data))
	})

	t.Run("Should insert item and return an error", func(t *testing.T) {
		t.Log("Cannot produce an error currently")
	})
}
