package config_server

import (
	"fmt"
	"os"

	"github.com/allegro/bigcache/v3"
	"go.uber.org/zap"
)

type ConfigServer struct {
	Port         uint16
	gitlabToken  string
	Branch       string
	ProjectId    string
	logger       *zap.Logger
	cache        *bigcache.BigCache
	disableCache bool
}

type ConfigServerConfig struct {
	GitlabToken    string
	GitlabTokenEnv string
	Branch         string
	ProjectId      string
	DisableCache   bool
}

var (
	ErrNoProjectID   = fmt.Errorf("no project id specified")
	ErrEmptyBranch   = fmt.Errorf("empty branch specified")
	ErrNoGitlabToken = fmt.Errorf("no gitlab token specified or set in env var GITLAB_API_TOKEN (or custom env). Maybe see config-server help")
)

func New(logger *zap.Logger, port uint16, cfg ConfigServerConfig) (*ConfigServer, error) {
	svr := &ConfigServer{
		Port:         port,
		logger:       logger,
		ProjectId:    cfg.ProjectId,
		Branch:       cfg.Branch,
		disableCache: cfg.DisableCache,
	}

	if cfg.ProjectId == "" {
		return nil, ErrNoProjectID
	}

	if cfg.Branch == "" {
		return nil, ErrEmptyBranch
	}

	if cfg.GitlabToken != "" {
		svr.gitlabToken = cfg.GitlabToken
		logger.Debug("Setting gitlab token from config", zap.String("token", svr.gitlabToken))
	} else {
		envVar := "GITLAB_API_TOKEN"

		if cfg.GitlabTokenEnv != "" {
			envVar = cfg.GitlabTokenEnv
		}

		tokenEnv := os.Getenv(envVar)

		svr.gitlabToken = tokenEnv
		logger.Debug("Setting gitlab token from env", zap.String("token", svr.gitlabToken), zap.String("env_var", envVar))

		if svr.gitlabToken == "" {
			return nil, ErrNoGitlabToken
		}
	}

	logger.Info("Config server created", zap.String("project_id", svr.ProjectId), zap.String("branch", svr.Branch))

	return svr, nil
}

func (svr *ConfigServer) AccessToken() string {
	return svr.gitlabToken
}
