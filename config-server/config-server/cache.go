package config_server

import (
	"context"
	"time"

	"github.com/allegro/bigcache/v3"
)

type CacheHolder interface {
	InsertDataToConfigCache(string, string) (bool, string)
	GetDataFromConfigCache(string) error
}

func (svr *ConfigServer) InitCache(cfg *bigcache.Config) error {
	config := bigcache.Config{
		Shards:             1024,
		LifeWindow:         15 * time.Minute,
		CleanWindow:        10 * time.Minute,
		HardMaxCacheSize:   5 * 1024,
		OnRemove:           nil,
		OnRemoveWithReason: nil,
	}

	if cfg != nil {
		config = *cfg
	}

	if svr.disableCache {
		config.LifeWindow = 0
		config.CleanWindow = 5 * time.Second
		config.HardMaxCacheSize = 128
		config.Shards = 2
	}

	cache, err := bigcache.New(context.Background(), config)
	if err != nil {
		return err
	}

	svr.cache = cache

	return nil
}

func (svr *ConfigServer) GetDataFromConfigCache(str string) (bool, string) {
	entry, err := svr.cache.Get(str)
	if err == nil {
		return true, string(entry)
	} else {
		return false, ""
	}
}

func (svr *ConfigServer) InsertDataToConfigCache(key string, data string) error {
	return svr.cache.Set(key, []byte(data))
}
