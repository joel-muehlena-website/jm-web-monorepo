package config_server

import (
	"os"
	"os/exec"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	assert := assert.New(t)
	testToken := "example_token"

	shouldCrash := os.Getenv("FLAG_CRASH")

	t.Parallel()

	// Only run if crashing is not set
	if shouldCrash == "" {
		t.Run("Should set gitlab token from cfg", func(t *testing.T) {
			t.Parallel()

			svr := New(3000, ConfigServerConfig{GitlabToken: "test_token", ProjectId: "123", Branch: "main"})

			assert.Equal("test_token", svr.gitlabToken)
		})

		t.Run("Should set gitlab token from env var", func(t *testing.T) {
			t.Parallel()

			t.Run("Preset", func(t *testing.T) {
				t.Parallel()

				err := os.Setenv("GITLAB_API_TOKEN", testToken)
				assert.Nil(err)

				svr := New(3000, ConfigServerConfig{ProjectId: "123", Branch: "main"})

				assert.Equal(testToken, svr.gitlabToken)
			})

			t.Run("Custom", func(t *testing.T) {
				t.Parallel()

				customVar := "CUSTOM_VAR"
				err := os.Setenv(customVar, testToken)
				assert.Nil(err)

				svr := New(3000, ConfigServerConfig{GitlabTokenEnv: customVar, ProjectId: "123", Branch: "main"})
				assert.Equal(testToken, svr.gitlabToken)
			})
		})
	}

	t.Run("Should exit if no gitlab access token is set", func(t *testing.T) {
		// Run the crashing code when FLAG is set
		if shouldCrash == "1" {
			err := os.Unsetenv("GITLAB_API_TOKEN")
			assert.Nil(err)
			New(3000, ConfigServerConfig{ProjectId: "123", Branch: "main"})
			return
		}
		// Run the test in a subprocess
		cmd := exec.Command(os.Args[0], "-test.run=TestNew")
		cmd.Env = append(os.Environ(), "FLAG_CRASH=1")
		err := cmd.Run()

		// Cast the error as *exec.ExitError and compare the result
		e, ok := err.(*exec.ExitError)
		expectedErrorString := "exit status 1"
		assert.Equal(true, ok)
		assert.Equal(expectedErrorString, e.Error())
	})

	t.Run("Should exit if no project id is set", func(t *testing.T) {
		assert.Panics(func() {
			New(3000, ConfigServerConfig{})
		})
	})

	t.Run("Should exit if no branch is set", func(t *testing.T) {
		assert.Panics(func() {
			New(3000, ConfigServerConfig{ProjectId: "123"})
		})
	})
}

func TestAccessToken(t *testing.T) {
	t.Parallel()

	testVal := "example_token"
	svr := ConfigServer{gitlabToken: testVal}

	assert.Equal(t, testVal, svr.AccessToken())
}
