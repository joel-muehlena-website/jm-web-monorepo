package config_server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"go.uber.org/zap"
)

type GitlabRequest struct {
	FileName  string
	Branch    string
	ProjectId string
	Token     string
	GitlabUrl string
}

type GitlabFileResponse struct {
	Content string `json:"content"`
}

var ErrNon200Response = fmt.Errorf("non 200 response")

func (gr *GitlabRequest) GetGitlabFile() (string, error) {
	url := gr.GitlabUrl + "/api/v4/projects/" + gr.ProjectId + "/repository/files/" + url.QueryEscape(gr.FileName) + "?ref=" + gr.Branch

	zap.L().Debug("Requesting file from GitLab", zap.String("url", url))

	client := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	req.Header.Add("PRIVATE-TOKEN", gr.Token)

	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 200 {
		return "", fmt.Errorf("failed to fetch GitLab file %d: %w", resp.StatusCode, ErrNon200Response)
	}

	var respJSON GitlabFileResponse
	err = json.NewDecoder(resp.Body).Decode(&respJSON)
	if err != nil {
		return "", err
	}

	return respJSON.Content, nil
}
