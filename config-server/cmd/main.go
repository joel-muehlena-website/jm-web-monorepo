package main

import (
	"flag"
	"fmt"
	"time"

	config_server "gitlab.com/joelMuehlena/jm-web-monorepo/config-server/config-server"
	"gitlab.com/joelMuehlena/jm-web-monorepo/config-server/handler"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/log"

	"go.uber.org/zap"

	ginzap "github.com/gin-contrib/zap"

	"github.com/gin-gonic/gin"
)

func main() {
	port := flag.Uint("port", 8081, "The port on which the config server should run")
	gitlabToken := flag.String("token", "", "The access token for gitlab (requires read_repository)")
	branch := flag.String("branch", "master", "The branch to pull the data from")
	projectId := flag.String("project_id", "", "The id of the gitlab project where the files are stored")
	gitlabEnvVarName := flag.String("gitlabTokenEnv", "", "The name of the env var which holds the access token for gitlab")
	disableCache := flag.Bool("disableCache", false, "Disable the cache")
	loggerEncoder := flag.String("loggerEncoder", "json", "The encoder to use for the logger")
	useLoggerColors := flag.Bool("useLoggerColors", false, "Use colors for the logger")
	logLevel := flag.String("logLevel", "info", "The log level to use")

	flag.Parse()

	logConfig := log.Config{
		UseServiceNameAsBaseName: true,
		LogLevel:                 *logLevel,
		UseLevelColors:           *useLoggerColors,
		Encoder:                  *loggerEncoder,
	}

	logger, err := log.New("config-server", logConfig, func(placeholder string) string { return "" }, nil)
	if err != nil {
		panic(err)
	}

	logger.Info("Starting jm config server. © Copyright 2024 Joel Rene Mühlena")

	if *disableCache {
		logger.Warn("Cache is disabled, this may lead to worse performance and API rate limits on gitlab")
	}

	svr, err := config_server.New(logger, uint16(*port), config_server.ConfigServerConfig{GitlabToken: *gitlabToken, GitlabTokenEnv: *gitlabEnvVarName, ProjectId: *projectId, Branch: *branch, DisableCache: *disableCache})
	if err != nil {
		logger.Fatal("Failed to create config server", zap.Error(err))
	}
	run(svr)
}

func run(svr *config_server.ConfigServer) {
	svr.InitCache(nil)

	handler := handler.HttpHandler{
		ConfigServer: svr,
		Branch:       svr.Branch,
		ProjectId:    svr.ProjectId,
		Logger:       zap.L().Named("http-handler"),
	}

	router := gin.New()
	ginzap.RecoveryWithZap(zap.L().Named("gin"), true)
	router.Use(ginzap.GinzapWithConfig(zap.L().Named("gin"), &ginzap.Config{
		TimeFormat: time.RFC3339,
		UTC:        true,
		SkipPaths:  []string{"/healthz"},
	}))

	router.RedirectTrailingSlash = true

	router.GET("/healthz", func(ctx *gin.Context) {
		ctx.Status(200)
	})

	router.GET("/config", handler.HandleGetConfigRequest)

	zap.L().Info("Try starting server", zap.Uint16("port", svr.Port))

	err := router.Run(fmt.Sprintf(":%d", svr.Port))
	if err != nil {
		zap.L().Fatal("Error while running server", zap.Error(err))
	}
}
