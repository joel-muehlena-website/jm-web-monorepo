// Description: A simple healthcheck tool that checks if an endpoint is ready to receive traffic.
// Could e.g. used for depends check in docker-compose, where curl or similar is not available.
package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
)

func main() {
	host := flag.String("host", "localhost", "The host of the endpoint to check")
	port := flag.Uint("port", 80, "The port of the host of the endpoint to check")
	expectedStatusCode := flag.Int("expected-status", 200, "The expected status codes of the endpoint to check")
	endpoint := flag.String("endpoint", "/ready", "The path of the endpoint to check")
	verbose := flag.Bool("verbose", false, "If verbose mode should be enabled")
	flag.Parse()

	if verbose != nil && *verbose {
		fmt.Printf("Checking http://%s:%d%s\n", *host, *port, *endpoint)
	}

	res, err := http.Get(fmt.Sprintf("http://%s:%d%s", *host, *port, *endpoint))
	if err != nil {
		if verbose != nil && *verbose {
			fmt.Printf("Error: %s\n", err)
		}
		os.Exit(1)
	}

	if res.StatusCode != *expectedStatusCode {
		if verbose != nil && *verbose {
			fmt.Printf("Error (not %d status): %s\n", *expectedStatusCode, res.Status)
		}
		os.Exit(1)
	}

	if verbose != nil && *verbose {
		fmt.Printf("Success: %s\n", res.Status)
	}
}
