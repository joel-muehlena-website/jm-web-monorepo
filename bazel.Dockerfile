FROM ubuntu:22.04

ARG BAZELISK_VERSION=v1.14.0

RUN apt-get update && apt-get upgrade -y && apt install curl git python3 python3-pip -y
RUN curl -L https://github.com/bazelbuild/bazelisk/releases/download/${BAZELISK_VERSION}/bazelisk-linux-amd64 -o bazelisk
RUN mv bazelisk /usr/local/bin
RUN chmod a+x /usr/local/bin/bazelisk
RUN ln -s /usr/local/bin/bazelisk /usr/local/bin/bazel

WORKDIR /wdir

CMD ["/bin/bash"]
