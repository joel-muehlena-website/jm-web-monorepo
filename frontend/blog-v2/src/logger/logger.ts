import {Logger} from "tslog"

export enum LogLevel {
    SILLY, TRACE, DEBUG, INFO, WARN, ERROR, FATAL
}

const parseLogLevel = (): LogLevel => {
    if (process.env.LOG_LEVEL !== undefined) {
        try {
            let applicationLogLevelNumber = parseInt(process.env.LOG_LEVEL)
            switch (applicationLogLevelNumber) {
                case 0:
                    return LogLevel.SILLY
                case 1:
                    return LogLevel.TRACE
                case 2:
                    return LogLevel.DEBUG
                case 3:
                    return LogLevel.INFO
                case 4:
                    return LogLevel.WARN
                case 5:
                    return LogLevel.ERROR
                case 6:
                    return LogLevel.FATAL
                default:
                    return LogLevel.TRACE
            }
        } catch (error) {
            console.error("Failed to parse LOG_LEVEL env variable", process.env.LOG_LEVEL, error)
        }
    }

    return LogLevel.TRACE
}

const applicationLogLevel = parseLogLevel()

export const LOGGER = new Logger({
    name: "blog",
    minLevel: applicationLogLevel
});
