import React from 'react'
import Link from 'next/link'
import { ExceptionPage } from './../components/ExceptionPage/exceptionPage'

const message = 'Ooops, the page you requested was not found'


export const NotFound404 = () => {

  return (
    <ExceptionPage statusCode={404} message={message}>
      <p>Ensure you entered a valid URL or just go to <Link href="/posts" about='Navigates to all posts'
        className="text-blue-700 tracking-wider font-bold text-">all
        posts</Link></p>
    </ExceptionPage>
  )
}

export default NotFound404