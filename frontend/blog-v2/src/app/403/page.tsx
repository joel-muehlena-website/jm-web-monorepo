// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import Link from "next/link";
import {ExceptionPage} from "./../../components/ExceptionPage/exceptionPage";

const message = "Not enough permissions to access this page"

const Page403 = () => {
    return (
        <ExceptionPage statusCode={403} message={message}>
            <p>Login or gather more permissions. Alternative go to <Link href="/posts" about='Navigates to all posts'
                                                                         className="text-blue-700 tracking-wider font-bold text-">all
                posts</Link></p>
        </ExceptionPage>
    );
};

export default Page403;