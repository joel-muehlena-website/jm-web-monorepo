// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { AnimatedBG } from "./../../../components/AnimatedBG";
import { DefaultNavigation } from "./../../../components/Header/defaultNavigation";
import { useServerSession } from "./../../../components/hooks/server/useServerSession";

export default async function PostLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const session = await useServerSession(false);

  return (
    <div>
      <DefaultNavigation session={session} />
      <div className="text-center w-full h-screen grid content-center font-jm-montserrat bg-scroll text-white">
        <AnimatedBG />

        <div className="-translate-y-44">
          <h1 className="text-8xl tracking-wide select-none">My Blog</h1>
          <h2 className="text-3xl mt-6 select-none">
            Here is everything interesting in my life written down
          </h2>
        </div>
      </div>
      <main>
        {children}
      </main>
    </div>
  );
}
