"use client";

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { useEffect } from "react";
import { LOGGER } from "../../../logger";

const Error = ({ error, reset }: { error: Error & { digest?: string; }, reset: () => void }) => {
  useEffect(() => {
    LOGGER.error("There was an error fetching all post initial data:", error.message, error.digest);
  }, [error]);

  return (
    <div className="grid content-center px-24 py-8">
      <p className="text-2xl">
        We&apos;re sorry but the server failed to load and render the posts. Maybe a <button
          onClick={() => reset()}
          className="inline font-bold bg-[#34495e] rounded-lg text-white py-2 px-6 shadow-3xl">reset</button> helps
        to resolve the problem.
        Otherwise try to <button onClick={() => window.location.reload()}
          className="inline font-bold bg-[#34495e] rounded-lg text-white py-2 px-6 shadow-3xl">reload</button> the
        page. In each case we are trying to fix the problem as soon as possible.
        <br />

        <p className="mt-6">Thank you for your patience.</p>

        <hr className="my-10" />

        Your blog Team
      </p>
      <small className="mt-8 italic inline-block">Digest: {error.digest}</small>
    </div>
  );
};

export default Error;
