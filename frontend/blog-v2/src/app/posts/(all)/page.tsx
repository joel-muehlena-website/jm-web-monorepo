// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { Suspense } from "react";
import { AllPosts } from "./../../../components/AllPosts/allPosts";
import { HTTPClient, HTTPRequest } from "../../../http";
import { HTTP_INTERCEPTOR_MANAGER, HTTPHandler, HTTPInterceptor } from "./../../../http/interceptor";
import { LOGGER } from "./../../../logger/logger";
import { retry } from "rxjs";
import { AllPostLoader } from "../../../components/AllPosts/allPostsLoader";

class TestInterceptorPre implements HTTPInterceptor {
  intercept(req: HTTPRequest, next: HTTPHandler) {
    LOGGER.info("Test interceptor pre");
    return next.handle(req);
  }

}

class TestInterceptorPrePost implements HTTPInterceptor {
  intercept(req: HTTPRequest, next: HTTPHandler) {
    LOGGER.info("Test interceptor prepost");
    return next.handle(req).pipe(retry(3));
  }

}

// HTTP_INTERCEPTOR_MANAGER.add(new TestInterceptorPre())
// HTTP_INTERCEPTOR_MANAGER.add(new HTTPErrorInterceptor((error: any) => {
//   LOGGER.error("Error in HTTP request", error.message);
// }))
// HTTP_INTERCEPTOR_MANAGER.add(new TestInterceptorPrePost())

const client = new HTTPClient(HTTP_INTERCEPTOR_MANAGER);

export default function Home() {

  // client.GET<{
  //   userId: number,
  //   id: number,
  //   title: string,
  //   completed: boolean
  // }>("https://jsonplaceholder.typicode.com/todos/1").subscribe(s => LOGGER.info("Test data", s));

  // client.GET<{
  //   userId: number,
  //   id: number,
  //   title: string,
  //   completed: boolean
  // }>("https://jsonplaceholder.typicode.com/todoss/1").subscribe({
  //   next: s => LOGGER.info("Test data 2", s),
  //   error: e => LOGGER.error("Test data 2 error", e.message),
  // });

  return (
    <Suspense fallback={<AllPostLoader />}>
      <AllPosts filter={{ limit: 250 }} />
    </Suspense>
  );
}
