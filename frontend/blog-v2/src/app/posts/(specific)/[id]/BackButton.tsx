"use client";

import {useRouter} from "next/navigation";
import React from "react";

interface Props {
    headerChangeRef: React.RefObject<HTMLElement>;
}

export const BackButton = ({headerChangeRef}: Props) => {
    const router = useRouter();

    return (
        <div
            className={`h-20 fixed top-0 z-10 left-0 w-full grid content-center justify-start bg-gradient-to-r from-[#f37335] to-[#fdc830]`}
        >
            <button
                onClick={() => router.back()}
                className="font-jm-roboto text-3xl cursor-pointer ml-4 flex justify-center items-center bg-transparent text-white hover:-translate-y-[0.2rem] hover:scale-[1.05] transition-all duration-300 ease-in-out"
            >
                <i className="las la-arrow-left mr-3"></i>
                <span>Back</span>
            </button>
        </div>
    );
};
