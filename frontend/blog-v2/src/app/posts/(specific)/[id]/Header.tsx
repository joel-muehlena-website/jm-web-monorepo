"use client";

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { useDebugView } from "../../../../components/DebugView/context";
import { BackButton } from "./BackButton";
import { useEffect, useRef } from "react";

interface Props {
  title: string;
}

export const Header = ({ title }: Props) => {
  const headerChangeRef = useRef<HTMLDivElement | null>(null);
  const { addDebugNode, removeDebugNode } = useDebugView();

  useEffect(() => {
    const node = (<div>Current opened post: <span>{title}</span></div>)

    addDebugNode(node)

    return () => {
      removeDebugNode(node)
    }
  }, [title, addDebugNode, removeDebugNode])

  return (
    <header
      className="flex min-h-[80vh] bg-gradient-to-r items-center justify-center from-[#f37335] to-[#fdc830]"
      ref={headerChangeRef}
    >
      <BackButton headerChangeRef={headerChangeRef} />
      <h1 className="text-7xl w-full px-10 hyphens-auto break-words font-bold h-fit text-white font-jm-montserrat">
        {title}
      </h1>
    </header>
  );
};
