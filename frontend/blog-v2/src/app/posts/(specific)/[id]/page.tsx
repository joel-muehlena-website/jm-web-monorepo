// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { getAuthor, getPost } from "./../../../../queries";
import type { Metadata } from "next";
import { Suspense } from "react";
import Link from "next/link";
import { Header } from "./Header";
import { isDateEqual } from "./../../../../utility/isDateEqual";
import { FormattedDate } from "./../../../../components/FormattedDate";
import { SinglePost } from "./../../../../components/SinglePost";
import { LoadingSpinner } from "./../../../../components/LoadingSpinner";
import Image from "next/image";
import { useServerSession } from "./../../../../components/hooks/server/useServerSession";

import styles from "./page.module.sass";


interface Props {
  params: { id: string };
}

export async function generateMetadata({
  params: { id },
}: Props): Promise<Metadata> {
  const post = await getPost(id);
  return {
    title: `${post.title} - ${post.authorLastName}`,
    authors: { name: `${post.authorFirstName} ${post.authorLastName}` },
    abstract: post.description,
    category: post.category,
  };
}

const PostByIdPage = async ({ params }: Props) => {
  const post = await getPost(params.id);
  const author = await getAuthor(post.authorId);
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const session = await useServerSession(false);

  return (
    <div>
      <Header title={post.title} />
      {session.user?.id === author.userId && (
        <div className="fixed bottom-0 right-0 m-4">
          <Link href={`/post/edit/${post.id}`} className="text-4xl font-bold text-[#f37335] z-[100]">
            Edit
          </Link>
        </div>
      )}
      <div
        className="min-h-[7vh] h-fit bg-[#ecf0f1] flex justify-between items-center px-[6rem] text-3xl font-jm-montserrat">
        <div>
          {!isDateEqual(
            new Date(post.createdAt),
            new Date(post.updatedAt)
          ) ? (
            <FormattedDate
              date={post.updatedAt}
              className="font-light"
            />
          ) : (
            <FormattedDate
              date={post.createdAt}
              className="font-light"
            />
          )}
        </div>
        <div className="font-light py-4 w-1/4">
          <div>Category: <span className="italic">{post.category}</span></div>
          <div>Tags: <span className="italic">{post.tags.join(", ")}</span></div>
        </div>
      </div>
      <main
        className="mt-16 grid gap-4 grid-cols-post grid-rows-1 justify-items-center justify-center pb-16 hyphens-auto">
        <div
          className={`text-3xl min-w-0 w-full font-jm-roboto col-span-1 col-start-2 col-end-2 break-words text-justify hyphens-auto ${styles.mdx}`}
        >
          <Suspense fallback={<LoadingSpinner className="mt-2" />}>
            <SinglePost content={post.content} />
          </Suspense>
        </div>
        <div
          className="w-full min-w-0 col-span-1 col-start-3 pr-4 col-end-3 flex flex-col items-center sticky top-32 self-start">
          <Image
            src={post.authorAvatar ?? "x"}
            height={80}
            width={80}
            alt={`Avatar of ${post.authorFirstName} ${post.authorLastName}`}
            className="rounded-[50%] object-cover border-[orange] border-2 object-center h-[10rem] w-[10rem]"
          />
          <Link href={`/author/${post.authorId}`} className="text-2xl mt-4">
            {post.authorFirstName} {post.authorLastName}
          </Link>
          <p className="text-xl mt-2 break-words hyphens-auto text-justify">
            {author.biography === undefined || author.biography === ""
              ? "No biography"
              : author.biography}
          </p>
        </div>
      </main>
    </div>
  );
};

export default PostByIdPage;
