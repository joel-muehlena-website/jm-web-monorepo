"use client"

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import Link from "next/link"
import { ExceptionPage } from "../../../../components/ExceptionPage/exceptionPage"
import { LOGGER } from "../../../../logger"
import { FetchError } from "../../../../queries"

const errorMessage = "Error loading this post."

export default function Error({
  error,
  reset,
}: {
  error: Error & { digest?: string }
  reset: () => void
}) {
  let fetchError: FetchError;
  try {
    fetchError = JSON.parse(error.message) as FetchError
  } catch (err) {
    LOGGER.trace("Failed to parse error message", err)
    fetchError = new FetchError(error.message, 500, null);
  }

  console.log(error.message, fetchError)

  return (
    <ExceptionPage statusCode={fetchError?.status ?? 500} message={fetchError?.response?.message ?? errorMessage}>
      <>
        Try to&nbsp;
        <button onClick={() => reset()}
          className="inline font-bold bg-[#34495e] rounded-lg text-white py-2 px-6 shadow-3xl">refresh
        </button>
        &nbsp;
        or to&nbsp;
        <button onClick={() => window.location.reload()}
          className="inline font-bold bg-[#34495e] rounded-lg text-white py-2 px-6 shadow-3xl">reload
        </button>
        &nbsp;
        otherwise&nbsp;<Link href="/"
          className="inline font-bold bg-[#34495e] rounded-lg text-white py-3 px-6 shadow-3xl">go
          home</Link>
      </>
      <p>Error(s): <span>{fetchError?.response?.errors.join(", ") ?? error.message}</span></p>
      <p className="">Ensure you entered a valid URL.</p>
      <p className="mt-8 font-bold">Other reasons for this error could be:</p>
      <ol className="list-decimal list-inside">
        <li className="pt-4 italic">The post was recently deleted</li>
      </ol>
      <small className="mt-8 italic inline-block">Digest: {error.digest}</small>
    </ExceptionPage>
  )
}