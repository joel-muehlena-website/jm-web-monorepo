'use client'

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {LOGGER} from "./../../../logger/logger";
import {Editor, FormData} from "./../../../components/Editor/editor";
import {Post} from "./../../../types/post";
import {createPost} from "./../../../queries/client";
import {useHttpInterceptorManager} from "./../../../context/httpInterceptor";
import {useCallback, useState} from "react";

export const CreatePostClientHandler = ({authorId}: { authorId: Post['authorId'] }) => {
    const {manager} = useHttpInterceptorManager();

    const [submitSuccess, setSubmitSuccess] = useState<string | null>(null);
    const [submitError, setSubmitError] = useState<string | null>(null);
    const [currentReset, setCurrentReset] = useState<NodeJS.Timeout | null>(null);

    const resetState = useCallback(() => {
        const id = setTimeout(() => {
            if (currentReset !== null) {
                clearTimeout(currentReset);
            }

            setSubmitSuccess(null);
            setSubmitError(null);
        }, 60 * 1000)

        setCurrentReset(id);
    }, [currentReset]);

    const onSubmitData = useCallback(async (data: FormData) => {
        LOGGER.debug("Submitted data", data);
        const res = await createPost(
            manager,
            {
                authorId: authorId,
                description: data.content.substring(0, data.content.length < 100 ? data.content.length : 100) + "...",
                title: data.title,
                content: data.content,
                category: data.newCategory !== undefined && data.newCategory.length > 0 ? data.newCategory : data.category.value,
                tags: data.tags.split(",").map((tag) => tag.trim())
            }
        );

        if (!res.ok) {
            LOGGER.error("Error while creating post", res.error);
            setSubmitError(res.error?.message ?? "Unknown error");
            setSubmitSuccess(null);

            resetState();
            return;
        }

        LOGGER.debug("Successfully created post", res);
        setSubmitSuccess(res.data.data.id);
        setSubmitError(null);

        resetState();
    }, [manager, authorId, resetState])

    return (
        <Editor onSubmit={onSubmitData} success={submitSuccess} error={submitError}/>
    );
}