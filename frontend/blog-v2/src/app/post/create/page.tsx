// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {LOGGER} from "./../../../logger/logger";
import {CreatePostClientHandler} from "./clientHandler";
import {getAuthorIdByUserId} from "./../../../queries";
import {useServerAuthorization} from "./../../../components/hooks/server/useAuthorization";

const CreatePostPage = async () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const {user} = await useServerAuthorization({permission: "jm_blog::BlogPost::Create::Allow"})

    const authorId = await getAuthorIdByUserId(user!.id);
    LOGGER.trace("Author id: ", authorId);

    return (
        <div className="bg-[#34495e]">
            <h1 className="text-4xl text-center text-white mb-8 pt-4">Create post</h1>
            <CreatePostClientHandler authorId={authorId}/>
        </div>
    );
};

export default CreatePostPage;
