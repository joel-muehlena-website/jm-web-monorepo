// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { LOGGER } from "./../../../logger/logger";
import { AllPosts } from "./../../../components/AllPosts/allPosts";
import { useServerAuthorization } from "./../../../components/hooks/server/useAuthorization";
import { getAuthorIdByUserId } from "./../../../queries";
import { Suspense } from "react";
import { AllPostLoader } from "../../../components/AllPosts/allPostsLoader";


const MyPostsPage = async () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const { user } = await useServerAuthorization({ permission: "jm_blog::BlogPost::Create::Allow" })

  const authorId = await getAuthorIdByUserId(user!.id);
  LOGGER.trace("Author id: ", authorId);

  return (
    <Suspense fallback={<AllPostLoader count={4} />}>
      <AllPosts filter={{ authorId, limit: 250 }} enableAuthorMode={true} />
    </Suspense>
  );
};

export default MyPostsPage;