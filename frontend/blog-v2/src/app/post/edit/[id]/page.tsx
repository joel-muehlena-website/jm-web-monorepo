// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {LOGGER} from "./../../../../logger/logger";
import {EditPostClientHandler} from "./clientHandler";
import {getAuthorIdByUserId, getPost} from "./../../../../queries";
import {useServerAuthorization} from "./../../../../components/hooks/server/useAuthorization";

type Props = {
    params: {
        id: string
    }
}

const EditPostPage = async ({params: {id: postID}}: Props) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const {user} = await useServerAuthorization({permission: "jm_blog::BlogPost::Create::Allow"})

    const authorId = await getAuthorIdByUserId(user!.id);
    LOGGER.trace("Author id: ", authorId);

    const post = await getPost(postID);

    return (
        <div className="bg-[#34495e]">
            <h1 className="text-4xl text-center text-white mb-8 pt-4">Edit post</h1>
            <EditPostClientHandler authorId={authorId} post={post}/>
        </div>
    );
};

export default EditPostPage;
