import React from "react";

import { Montserrat, Roboto } from "next/font/google";

import "./globals.sass";
import { Metadata } from "next";
import { ServiceProvider } from "./../service/provider";
import { Footer } from "./../components/Footer/footer";
import { DebugView } from "./../components/DebugView/DebugView";
import { useServerSession } from "./../components/hooks/server/useServerSession";
import { Providers } from "./../context/providers";
import { ConfigService } from "../service/config.service";

const roboto = Roboto({
  weight: ["300", "400", "700"],
  subsets: ["latin"],
  fallback: ["sans-serif"],
  variable: "--font-roboto",
});

const montserrat = Montserrat({
  weight: ["300", "400", "700"],
  subsets: ["latin"],
  fallback: ["Roboto", "sans-serif"],
  variable: "--font-montserrat",
});

export const metadata: Metadata = {
  title: "Blog - Joel Mühlena",
};

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const { configService } = ServiceProvider.getInstance().services;
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const session = await useServerSession(false);

  return (
    <html lang="en">
      <head>
        <link
          rel="stylesheet"
          href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css"
        />
      </head>
      <body
        className={`${roboto.className} ${montserrat.variable} ${roboto.variable}`}
      >
        <Providers authURLBase={ConfigService.config().auth.urlBase}>
          <div>{children}</div>
          <DebugView enabled={process.env.NODE_ENV !== "production"}>
            <div className="inline-block">
              Auth state:{" "}
              <span
                className={`${
                  session.isAuthenticated ? "bg-green-600" : "bg-red-600"
                }`}
              >
                {session.isAuthenticated ? "Auth" : "No auth"}
              </span>
            </div>
          </DebugView>
          <Footer
            isAuthenticated={session.isAuthenticated}
            loginUrl={configService.appLoginUrl}
          />
        </Providers>
      </body>
    </html>
  );
}
