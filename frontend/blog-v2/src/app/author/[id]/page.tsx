// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { Suspense } from "react";
import { AllPosts } from "./../../../components/AllPosts/allPosts";
import { AuthorHeader } from "./authorHeader";
import { DefaultNavigation } from "../../../components/Header/defaultNavigation";
import { useServerSession } from "../../../components/hooks/server/useServerSession";
import { AllPostLoader } from "../../../components/AllPosts/allPostsLoader";
import { LoadingSpinner } from "../../../components/LoadingSpinner";


const AuthorPage = async ({ params: { id: authorId } }: { params: { id: string } }) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const session = await useServerSession(false)

  return (
    <>
      <DefaultNavigation session={session} />
      <div className="grid place-content-center">
        <Suspense fallback={<LoadingSpinner className="mt-2 py-16" />}>
          <AuthorHeader authorID={authorId} />
        </Suspense>
      </div>
      <div className="px-4">
        <hr />
      </div>
      <div>
        <Suspense fallback={<AllPostLoader count={3} />}>
          <AllPosts filter={{ authorId, limit: 250 }} />
        </Suspense>
      </div>
    </>
  )
};

export default AuthorPage;
