// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { isValidUrl } from "../../../utility/isValidUrl";
import { getAuthor } from "./../../../queries";

export const AuthorHeader = async ({ authorID }: { authorID: string }) => {
  const author = await getAuthor(authorID);

  return (
    <div className="px-4 py-4 mt-4">
      {author !== undefined && (
        <div className="flex flex-col gap-6">
          <div className="text-start px-14 mb-4">
            <h1 className="inline-block text-5xl tracking-wide relative after:content-[''] after:block after:absolute after:w-full after:h-2 after:bg-gradient-to-r after:from-[#f37335] after:to-[#fdc830] after:rounded-lg after:-bottom-1 -z-[1] after:left-0">{author.firstName} {author.lastName} {author.alias && (
              <span>({author.alias})</span>)}</h1>
          </div>
          <div className="text-justify px-14">
            <p className="break-words hyphens-auto">{author.biography ?? "No bio"}</p>
          </div>
          <div className="align-self-center px-14">
            <h2 className="font-bold text-2xl">Links</h2>
            <ul>
              {author.socials.map(social => (
                <li key={social.name}><span
                  className="font-bold">{social.name}:</span> {isValidUrl(social.value) ? (
                    <a href={social.value} className="text-blue-500" target="_blank"
                      referrerPolicy="no-referrer">{social.value}</a>) : social.value}</li>
              ))}
            </ul>
          </div>
        </div>
      )}
    </div>
  )
}