// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";

const HealthzPage = () => {
  return (
    <div className="grid justify-center font-bold text-4xl items-center h-full p-20">
      200 - Healthy
    </div>
  );
};

export default HealthzPage;
