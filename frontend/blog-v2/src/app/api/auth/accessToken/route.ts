import { NextRequest, NextResponse } from "next/server";
import { format } from "date-fns";
import { applicationConfig } from "../../../../ApplicationConfig";
import { LOGGER } from "../../../../logger";

export type ServerAccessTokenSetter = {
  accessToken: string,
  cookieOpts: {
    expires: Date,
  }
}

export async function POST(req: NextRequest) {
  const { accessToken, cookieOpts: { expires } } = await req.json() as ServerAccessTokenSetter;

  const res = NextResponse.json({ msg: "Access token was set. Expires at: " + format(new Date(expires), "dd-MM-yyyy hh:mm:ss") }, { status: 200 })
  LOGGER.debug("Setting access token cookie on server side. Expires at: " + format(new Date(expires), "dd-MM-yyyy hh:mm:ss"));
  res.cookies.set(applicationConfig.auth.cookie.accessTokenName, accessToken, {
    sameSite: applicationConfig.auth.cookie.sameSite,
    httpOnly: true,
    expires: new Date(expires),
    secure: applicationConfig.auth.cookie.secure,
    domain: applicationConfig.auth.cookie.domain,
  });

  return res
}