import { NextRequest, NextResponse } from "next/server";
import { addDays, format } from "date-fns";
import { applicationConfig } from "../../../../ApplicationConfig";
import { LOGGER } from "../../../../logger";

export type ServerAccessTokenSetter = {
  refreshToken: string,
}

export async function POST(req: NextRequest) {
  const { refreshToken } = await req.json() as ServerAccessTokenSetter;

  const expires = addDays(new Date(), 15);

  const res = NextResponse.json({ msg: "Refresh token was set. Expires at: " + format(new Date(expires), "dd-MM-yyyy hh:mm:ss") }, { status: 200 })
  LOGGER.debug("Setting access token cookie on server side. Expires at: " + format(new Date(expires), "dd-MM-yyyy hh:mm:ss"));
  res.cookies.set("refresh_token", refreshToken, {
    sameSite: applicationConfig.auth.cookie.sameSite,
    httpOnly: true,
    expires: new Date(expires),
    secure: applicationConfig.auth.cookie.secure,
    domain: applicationConfig.auth.cookie.domain,
  });

  return res
}