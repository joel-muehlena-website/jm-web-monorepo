import {AuthService} from "./../service/auth.service";
import {HTTPHandler, HTTPInterceptor, HTTPRequest} from "./../http"

export class AuthInterceptor implements HTTPInterceptor {
    constructor(private authService: AuthService) {
    }

    intercept(req: HTTPRequest, next: HTTPHandler) {
        const headers = new Headers(req.options.headers ?? {});
        headers.set("Authorization", `Bearer ${this.authService.accessToken.value}`);
        req.options.headers = headers;
        req.options.credentials = "include";

        return next.handle(req);
    }
}