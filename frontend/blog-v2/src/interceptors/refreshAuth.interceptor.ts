import {AuthService} from "../service/auth.service";
import {HTTPError, HTTPHandler, HTTPInterceptor, HTTPRequest} from "../http"
import {catchError, switchMap, throwError} from "rxjs";
import {LOGGER} from "./../logger";

export class RefreshAuthInterceptor implements HTTPInterceptor {
    private logger = LOGGER.getSubLogger({name: "refresh-interceptor"});

    constructor(private authService: AuthService) {
    }

    intercept(req: HTTPRequest, next: HTTPHandler) {

        this.logger.debug("Intercepting request for refreshing", req);

        return next.handle(req).pipe(catchError((e) => {
            if (!(e instanceof HTTPError)) {
                return throwError(() => e);
            }

            const error: HTTPError = e;
            if (error.status !== 401 && error.status !== 403) {
                return throwError(() => error);
            }

            this.logger.debug("Got 401/403 response. Trying to refresh access token");

            return this.authService.refresh().pipe(switchMap(refreshed => {
                if (!refreshed) {
                    return throwError(() => new Error("Failed to refresh access token in interceptor"));
                }

                return this.authService.accessToken.pipe(switchMap(accessToken => {
                    if (accessToken === "") return throwError(() => new Error("Access token is empty after refresh"));

                    this.logger.trace("Successfully refreshed access token. Retrying request", accessToken);

                    const headers = new Headers(req.options.headers ?? {});
                    headers.set("Authorization", `Bearer ${accessToken}`);
                    req.options.headers = headers;
                    req.options.credentials = "include";

                    return next.handle(req)
                }))
            }));
        }));
    }
}