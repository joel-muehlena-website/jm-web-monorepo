// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {MDXRemote} from "next-mdx-remote/rsc";

import {MarkdownCodeBlock} from "./../components/MarkdownCodeBlock";

export const SinglePost = ({content}: { content: string; }) => {
    return (
        <MDXRemote
            source={content}
            options={{parseFrontmatter: true, mdxOptions: {jsx: false}}}
            components={{
                code(props) {
                    return (
                        <MarkdownCodeBlock
                            className={props.className}
                            // eslint-disable-next-line react/no-children-prop
                            children={[props.children]}
                        />
                    );
                },
            }}
        />
    )
}