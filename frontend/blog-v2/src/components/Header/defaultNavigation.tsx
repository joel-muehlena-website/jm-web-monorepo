// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {ServerSession} from "./../../service/server-auth.service";
import {asyncFilter} from "./../../utility/asyncFilter";
import Link from "next/link";

export interface Route {
    name: string;
    a?: string;
    link?: string;
    conditions?: Array<(session: ServerSession) => Promise<boolean>>;
}

const isAuthenticatedGuard = async (session: ServerSession) => {
    return session.isAuthenticated
}
const hasRoleGuard = async (session: ServerSession) => {
    return true
}

const routes: Array<Route> = [
    {name: "Homepage", a: "https://joel.muehlena.de"},
    {name: "Blog", link: "/"},
    {name: "Create Post", link: "/post/create", conditions: [isAuthenticatedGuard, hasRoleGuard]},
    {name: "$$NAME$$ posts", link: "/post/my", conditions: [isAuthenticatedGuard, hasRoleGuard]},
];

const doesRouteMeetConditions = async (route: Route, session: ServerSession) => {
    if (route.conditions) {
        for (const condition of route.conditions) {
            const if_ = await condition(session);
            if (!if_) return false;
        }
    }
    return true;
}

export const DefaultNavigation = async ({session}: { session: ServerSession }) => {
    return (
        <header
            className="h-20 top-0 left-0 right-0 sticky w-full grid content-center items-start bg-[#00000066] z-[200]">
            <nav className="flex gap-7 content-center ml-8">
                {(await asyncFilter(routes, session, doesRouteMeetConditions)).map((route) => (
                    <span className="text-2xl link-hover text-white" key={route.name}>
            {route.a ? (
                <a href="https://joel.muehlena.de">{route.name.replace("$$NAME$$", session.user?.firstName ?? "Anonymous")}</a>
            ) : (
                <Link
                    href={route.link ?? "/"}>{route.name.replace("$$NAME$$", session.user?.firstName + "s'" ?? "Anonymous'")}</Link>
            )}
          </span>
                ))}
            </nav>
        </header>
    );
};
