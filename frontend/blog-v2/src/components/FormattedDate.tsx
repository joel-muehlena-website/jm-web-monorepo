import {getMonthString} from "./../utility/getMonthString";
import React from "react";

export const FormattedDate: React.FunctionComponent<{
    date: Date | string;
    className?: string;
}> = ({date, className: passedClassName}) => {
    let da = new Date(date);
    let day = da.getDate();
    let month = getMonthString(da.getMonth() + 1);
    let year = da.getFullYear();

    return (
        <span className={`jm-postDate ${passedClassName ?? ""}`}>
      {month} {day}, {year}
    </span>
    );
};
