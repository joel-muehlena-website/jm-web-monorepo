import React, {ReactNode} from "react"

export type Props = {
    statusCode: number
    message: string
    children?: ReactNode
}

export const ExceptionPage = ({message, statusCode, children}: Props) => {

    return (
        <div className="grid content-center justify-center text-center min-h-page">
            <h1 className="text-7xl font-bold">{statusCode}</h1>
            <p className="text-2xl">{message}</p>
            {children !== undefined && (
                <div className="mt-12 text-lg">
                    {children}
                </div>
            )}
        </div>
    )
}