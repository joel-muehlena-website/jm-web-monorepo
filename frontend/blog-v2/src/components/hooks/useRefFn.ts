import {MutableRefObject, useRef} from "react";

const NULL_REF = {};

export const useRefFn = <T>(init: () => T) => {
    const ref = useRef<T | typeof NULL_REF>(NULL_REF);
    if (ref.current === NULL_REF) {
        ref.current = init();
    }

    return ref as MutableRefObject<T>;
}