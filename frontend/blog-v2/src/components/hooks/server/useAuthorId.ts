import {LOGGER} from "./../../../logger/logger";
import {getAuthorIdByUserId} from "./../../../queries";
import {useServerSession} from "./useServerSession";


export const useAuthorId = async (): Promise<string | undefined> => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const session = await useServerSession(false);

    if (!session.isAuthenticated) {
        LOGGER.error("User is not authenticated, author id cannot be determined");
        return undefined;
    }

    if (session.user === null) {
        LOGGER.error("User is null, but is authenticated, author id cannot be determined");
        return undefined;
    }

    const authorId = await getAuthorIdByUserId(session.user.id);
    LOGGER.trace("Author id: ", authorId);

    return authorId;
}