import {LOGGER} from "./../../../logger";
import {ServiceProvider} from "./../../../service/provider"
import {ServerSession} from "./../../../service/server-auth.service";
import {redirect} from "next/navigation";

export const useServerSession = async (enableLoginRedirect = true): Promise<ServerSession> => {
    const {authService} = ServiceProvider.getInstance().services;
    const configService = ServiceProvider.getInstance().services.configService;

    const session = await authService.getServerSession()

    if (!session.isAuthenticated) {
        LOGGER.debug("useServerSession hook: User is not authenticated");
        if (enableLoginRedirect) redirect(configService.appLoginUrl);
        return session;
    }

    if (session.user === null) {
        LOGGER.error("useServerSession hook: User is null, but is authenticated");
        if (enableLoginRedirect) redirect(configService.appLoginUrl);
        return session;
    }

    // TODO FIX setting cookie for access token
    // const res = await fetch(`${configService.get("rootUrl") as string}/api/auth/accessToken`, {
    //   method: "POST",
    //   credentials: "same-origin",
    //   mode: "same-origin",
    //   cache: "no-store",
    //   headers: {
    //     "Content-Type": "application/json"
    //   },
    //   body: JSON.stringify({
    //     accessToken: session.accessToken, cookieOpts: {
    //       expires: session.user?.expiresAtUTC,
    //       secure: false,
    //     }
    //   } as ServerAccessTokenSetter)
    // })

    // if (res.status !== 200) {
    //   LOGGER.error("Get Server Session: Error setting access token", res.status);
    // } else {
    //   LOGGER.trace("Get Server Session: Setting access token", res.headers.get("Set-Cookie"));
    //   const resj = await res.json()
    //   LOGGER.trace("Get Server Session: Setting access token", resj);
    // }

    return session;
}