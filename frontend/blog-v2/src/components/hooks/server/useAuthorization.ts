import { firstValueFrom } from "rxjs";
import { useServerSession } from "./useServerSession";
import { ServiceProvider } from "./../../../service/provider";
import { LOGGER } from "./../../../logger/logger";
import { AuthService, User } from "./../../../service/auth.service";
import { redirect } from "next/navigation";
import { RedirectType } from "next/dist/client/components/redirect";
import { ConfigService } from "../../../service/config.service";

export type UseServerAuthorizationParams = {
  permission: string;
  enableLoginRedirect?: boolean;
  enableNoPermRedirect?: boolean;
}

export const useServerAuthorization = async ({
  enableLoginRedirect = false,
  enableNoPermRedirect = true,
  permission
}: UseServerAuthorizationParams): Promise<{
  isAuthorized: boolean,
  user: User | null
}> => {
  if (enableNoPermRedirect) enableLoginRedirect = false;

  const configService = ServiceProvider.getInstance().services.configService;
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const session = await useServerSession(enableLoginRedirect);

  if (!session.isAuthenticated) {
    if (enableNoPermRedirect) {
      LOGGER.debug("useServerAuthorization hook: User is not authenticated, redirecting to 403 page");
      redirect("/403", RedirectType.push);
    }

    return { isAuthorized: false, user: null };
  }

  let isAuthorized = false;

  try {
    isAuthorized = await firstValueFrom(AuthService.isAuthorized(ConfigService.config().auth.urlBase, session.accessToken, permission));
  } catch (error) {
    LOGGER.error("useServerAuthorization error: ", error)
  }

  if (!isAuthorized && enableNoPermRedirect) {
    LOGGER.debug("useServerAuthorization hook: User is not authorized, redirecting to 403 page");
    redirect("/403", RedirectType.push);
  }

  return { isAuthorized, user: session.user };
};