// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {DEFAULT_CANCEL_TEXT, DEFAULT_CONFIRM_TEXT, Props} from "./props";

export const ModalPortalInner = ({children, cancelText, confirmText, onCancel, onConfirm, show}: Props) => {

    if (!show) {
        return <></>
    }

    return (
        <div className="fixed top-0 left-0 right-0 bottom-0 z-[40000] grid place-content-center bg-[rgba(0,0,0,0.46)]">
            <div className="bg-white px-10 py-5 rounded-md">
                <div>{children}</div>
                <div className="flex gap-4 mt-5">
                    <button className="w-24 text-center rounded-md cursor-pointer py-1 text-white bg-green-600"
                            onClick={() => onConfirm === undefined ? () => {
                            } : onConfirm()}>{confirmText ?? DEFAULT_CONFIRM_TEXT}</button>
                    <button className="w-24 text-center rounded-md cursor-pointer py-1 text-white bg-red-600"
                            onClick={() => onCancel === undefined ? () => {
                            } : onCancel()}>{cancelText ?? DEFAULT_CANCEL_TEXT}</button>
                </div>
            </div>
        </div>
    )
};