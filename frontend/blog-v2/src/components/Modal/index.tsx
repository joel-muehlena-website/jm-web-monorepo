// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
export * from "./Modal"