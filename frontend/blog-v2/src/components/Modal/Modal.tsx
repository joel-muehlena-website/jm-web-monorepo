"use client"

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {createPortal} from "react-dom"
import {ModalPortalInner} from "./ModalPortalInner"
import {useEffect, useState} from "react"
import {Props} from "./props"


export const Modal = ({children, show, ...rest}: Props) => {
    const [prevOverflow, setPrevOverflow] = useState("auto")

    useEffect(() => {
        const initialOverflow = document.body.style.overflow
        if (show) {
            setPrevOverflow(initialOverflow)
            document.body.style.overflow = "hidden"
        } else {
            document.body.style.overflow = prevOverflow
        }

        return () => {
            document.body.style.overflow = initialOverflow
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [show])


    if (typeof window !== "object") return null

    return (
        createPortal(<ModalPortalInner {...rest} show={show}>{children}</ModalPortalInner>, document.body)
    )
}