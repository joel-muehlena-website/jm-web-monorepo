import {ReactNode} from "react"

export const DEFAULT_CONFIRM_TEXT = "Ok"
export const DEFAULT_CANCEL_TEXT = "Cancel"

export type Props = {
    show: boolean
    children: ReactNode
    confirmText?: string
    cancelText?: string
    onConfirm?: () => void
    onCancel?: () => void
}
