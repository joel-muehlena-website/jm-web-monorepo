"use client";

import React, { useEffect, useMemo, useState } from "react";
import { format } from "date-fns";
import { usePathname, useSearchParams } from "next/navigation";

export const Footer = ({
  isAuthenticated,
  loginUrl,
}: {
  isAuthenticated: boolean;
  loginUrl: string;
}) => {
  const currentYear = useMemo(() => format(new Date(), "yyyy"), []);
  const [pageURL, setPageURL] = useState<URL | null>(null);
  const [pathAppendix, setPathAppendix] = useState<string>("");
  const pathname = usePathname();
  const searchParams = useSearchParams();

  useEffect(() => {
    setPageURL(new URL(window.location.href));
  }, []);

  useEffect(() => {
    let p = pathname;

    const paramString = searchParams.toString();

    if (paramString !== "") {
      p += "?" + paramString;
    }

    setPathAppendix(p);
  }, [pathname, searchParams]);

  return (
    <footer className="h-footer flex justify-center items-center">
      &copy; {currentYear} by Joel Mühlena{" "}
      {!isAuthenticated && (
        <>
          &nbsp;&mdash;&nbsp;
          <a
            href={`${loginUrl}?redirect_url=${encodeURIComponent(
              pageURL?.origin + pathAppendix
            )}`}
          >
            Login
          </a>
        </>
      )}
    </footer>
  );
};
