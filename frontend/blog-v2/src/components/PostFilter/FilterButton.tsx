"use client";

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {animated, useSpring} from "@react-spring/web";

type Props = {
    toggleFilter: () => void;
};

export const FilterButton = ({toggleFilter}: Props) => {
    const [springs, api] = useSpring(() => ({
        from: {rotate: 0, y: 0},
    }));

    return (
        <animated.button
            className="cursor-pointer fixed bottom-8 right-8 h-24 w-24 bg-[#34495e] rounded-full text-white grid items-center justify-center align-middle shadow-md transition-shadow hover:shadow-lg"
            style={springs}
            onClick={toggleFilter}
            onMouseEnter={() => api.start({y: -10, rotate: 20})}
            onMouseLeave={() => api.start({y: 0, rotate: 0})}
        >
            <i className="las la-filter text-[4.5rem] mt-4"></i>
        </animated.button>
    );
};
