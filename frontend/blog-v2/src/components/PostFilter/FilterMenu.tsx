// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {animated, useSpring, useTransition} from "@react-spring/web";
import {useEffect, useState} from "react";
import {FilterOptions} from "./PostFilter";
import {Post} from "./../../types/post";
import {useFilter} from "./useFilter";

type Props = {
    isOpen: boolean;
    toggleFilter: () => void;
    applyFilter: (opts: FilterOptions) => void;
    posts: Post[];
};

type BodyStyle = {
    height: string;
    overflow: string;
};

export const FilterMenu = ({
                               isOpen,
                               toggleFilter,
                               applyFilter,
                               posts,
                           }: Props) => {
    const [internalIsOpen, setInternalIsOpen] = useState<boolean[]>([]);
    const [bodyValues, setBodyValues] = useState<BodyStyle | null>(null);

    const {
        active,
        setFilter,
        authors,
        tags,
        categories,
        earliestDate,
        latestDate,
    } = useFilter(posts);

    useEffect(() => {
        if (isOpen) {
            setInternalIsOpen([true]);
            setBodyValues({
                height: document.body.style.height,
                overflow: document.body.style.overflow,
            });
            document.body.style.height = "100vh";
            document.body.style.overflow = "hidden";
            menuWrapperSpringsApi.start({
                from: {
                    opacity: 0,
                },
                to: {
                    opacity: 1,
                },
            });
        } else {
            if (bodyValues != null) {
                document.body.style.height = bodyValues.height;
                document.body.style.overflow = bodyValues.overflow;
            }

            menuWrapperSpringsApi.start({reverse: true});

            setInternalIsOpen([]);
            setBodyValues(null);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isOpen]);

    const menuSprings = useTransition(internalIsOpen, {
        from: {
            opacity: 0,
            y: 1600,
        },
        enter: {
            opacity: 1,
            y: 0,
        },
        leave: {
            opacity: 1,
            y: 1600,
        },
    });

    const [menuWrapperSprings, menuWrapperSpringsApi] = useSpring(() => ({}));

    return menuSprings((style, _) => (
        <animated.div
            className="w-full h-screen bg-[#000000a6] fixed bottom-0 left-0 right-0 top-0 z-[1000] grid items-end"
            style={menuWrapperSprings}
        >
            <animated.div
                style={style}
                className="w-full h-[95vh] rounded-tl-xl rounded-tr-xl flex flex-col gap-10 bg-[#ecf0f1]"
            >
        <span className="place-self-center text-[3.5rem] h-fit">
          <i className="las la-grip-lines cursor-grab"></i>
        </span>

                <section className="bg-white px-5 py-5">
                    <h2 className="text-[2.5rem] font-bold mb-8">Active filters</h2>
                    <div></div>
                </section>
                <section className="bg-white px-5 py-5">
                    <h2 className="text-[2.5rem] font-bold mb-8">Authors</h2>
                    <div className="pb-6">
                        {authors.map((a) => (
                            <span
                                key={a}
                                className="px-10 py-4 text-2xl bg-[#34495e] text-white cursor-pointer"
                                onClick={() => setFilter("author", a, "add")}
                            >
                {a}
              </span>
                        ))}
                    </div>
                </section>
                <section className="bg-white px-5 py-5">
                    <h2 className="text-[2.5rem] font-bold mb-8">Date</h2>
                    <div></div>
                </section>
                <section className="bg-white px-5 py-5">
                    <h2 className="text-[2.5rem] font-bold mb-8">Tags</h2>
                    <div></div>
                </section>
                <section className="bg-white px-5 py-5">
                    <h2 className="text-[2.5rem] font-bold mb-8">Categories</h2>
                    <div></div>
                </section>
                <section className="flex justify-evenly bg-white fixed bottom-0 left-0 right-0 py-8">
                    <button
                        className="px-10 py-4 border-black border-solid border-[3px] text-2xl font-bold w-48 transition-all hover:bg-black hover:text-white"
                        onClick={toggleFilter}
                    >
                        Cancel
                    </button>
                    <button
                        className="px-10 py-4 text-2xl font-bold w-48 text-white border-black border-solid border-[3px] bg-black transition-all hover:bg-transparent hover:text-black"
                        onClick={() => {
                            applyFilter({});
                        }}
                    >
                        Apply
                    </button>
                </section>
            </animated.div>
        </animated.div>
    ));
};
