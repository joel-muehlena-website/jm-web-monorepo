"use client";

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {Post} from "./../../types/post";
import {FilterButton} from "./FilterButton";
import {useCallback, useState} from "react";
import {FilterMenu} from "./FilterMenu";

type Props = { posts: Post[]; onApply: () => void };

export type FilterOptions = Partial<{
    authorFilter: Set<string>;
    categoryFilter: Set<string>;
    tagFilter: Set<string>;
    dateFilter: Partial<{
        from: Date;
        to: Date;
    }>;
}>;

export const PostFilter = ({posts}: Props) => {
    const [isFilterOpen, setIsFilterOpen] = useState<boolean>(false);

    const toggleFilter = useCallback(() => {
        setIsFilterOpen(!isFilterOpen);
    }, [setIsFilterOpen, isFilterOpen]);

    const applyFilter = useCallback((filterOpts: FilterOptions) => {
    }, []);

    return (
        <>
            <FilterButton toggleFilter={toggleFilter}/>
            <FilterMenu
                isOpen={isFilterOpen}
                toggleFilter={toggleFilter}
                applyFilter={applyFilter}
                posts={posts}
            />
        </>
    );
};
