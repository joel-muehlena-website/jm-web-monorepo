// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {Post} from "./../../types/post";
import {useEffect, useState} from "react";
import {FilterOptions} from "./PostFilter";

type SetFilterOptions = {
    filterType: "author" | "category" | "tag" | "from" | "to";
    value: string | Date;
    op: "add" | "remove";
};

export const useFilter = (posts: Post[]) => {
    const [active, setActive] = useState<FilterOptions>();

    const [authors, setAuthors] = useState<Array<string>>([]);
    const [tags, setTags] = useState<Array<string>>([]);
    const [categories, setCategories] = useState<Array<string>>([]);
    const [earliestDate, setEarliestDate] = useState<Date>(new Date());
    const [latestDate, setLatestDate] = useState<Date>(new Date());

    useEffect(() => {
        if (posts === undefined || !Array.isArray(posts)) return;

        const authors = new Set<string>();
        const tags = new Set<string>();
        const categories = new Set<string>();
        let earliestDate = new Date();
        let latestDate = new Date();

        posts.forEach((post) => {
            authors.add(`${post.authorFirstName} ${post.authorLastName}`);

            post.tags.forEach((tag) => {
                tags.add(tag);
            });

            categories.add(post.category);

            if (new Date(post.createdAt) < earliestDate)
                earliestDate = new Date(post.createdAt);

            if (new Date(post.createdAt) > latestDate)
                latestDate = new Date(post.createdAt);
        });

        setAuthors(Array.from(authors));
        setCategories(Array.from(categories));
        setTags(Array.from(tags));
        setEarliestDate(earliestDate);
        setLatestDate(latestDate);
    }, [posts]);

    const setFilter = (
        type: SetFilterOptions["filterType"],
        value: SetFilterOptions["value"],
        op: SetFilterOptions["op"]
    ) => {
        // TODO
    };

    return {
        active,
        setFilter,
        authors,
        tags,
        categories,
        earliestDate,
        latestDate,
    };
};
