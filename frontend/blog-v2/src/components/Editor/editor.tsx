'use client'

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {useCallback, useEffect, useState} from "react";
import {Controller, ControllerFieldState, ControllerRenderProps, useForm, UseFormStateReturn} from "react-hook-form"
import {TagInput} from "../Form/tagInput";
import {LOGGER} from "./../../logger/logger";
import Select, {ContainerProps, GroupBase, SingleValue} from "react-select";
import {from} from "rxjs";
import {getCategories} from "./../../queries/queries";
import {useRouter} from "next/navigation";
import {Modal} from "../Modal";
import Link from "next/link";

type Option = {
    label: string;
    value: string;
}

export type FormData = {
    title: string;
    content: string;
    category: Option;
    tags: string;
    newCategory: string;
}

type Category = Option;

const SET_NEW_CATEGORY_VALUE = "__set_new_category";

export const Editor = ({initData, onSubmit: onSubmitCB, success, error, isNew = false}: {
    initData?: FormData,
    onSubmit: (formData: FormData) => Promise<void>,
    success: string | null,
    error: string | null,
    isNew?: boolean
}) => {
    const router = useRouter()
    const {handleSubmit, register, setValue, control, ...formController} = useForm<FormData>();
    const [items, setItems] = useState<Array<Category>>([]);
    const [defaultSelectValue, setDefaultSelectValue] = useState<Option | undefined>(undefined);
    const [isNewCategoryEnabled, setIsNewCategoryEnabled] = useState<boolean>(false);

    const [isInitialSet, setIsInitialSet] = useState<boolean>(false);

    const [showModal, setShowModal] = useState<boolean>(false);

    const modalConfirm = () => {
        router.back
        router.push("/posts")
    }
    const modalCancel = () => {
        setShowModal(false);
    }

    // Initial fetch of categories (only if no init data was set & no entries are already set)
    useEffect(() => {
        const sub = from(getCategories()).subscribe(categories => setItems(categories.map(cat => {
            return {label: cat, value: cat}
        })));

        console.log(error)

        return () => sub.unsubscribe();
    }, [error]);

    // Set init data if provided
    useEffect(() => {
        if (initData === undefined || isInitialSet) return;

        LOGGER.trace("Initializing editor with init data", initData);
        setValue("title", initData.title);
        setValue("content", initData.content);
        setValue("category", initData.category);
        setValue("tags", initData.tags);

        LOGGER.trace("Initializing default category option", {label: initData.category, value: initData.category});
        setDefaultSelectValue(initData.category);

        setIsInitialSet(true);
    }, [initData, setValue, isInitialSet]);

    useEffect(() => {
        register("tags", {required: true});
    }, [register]);

    const onSubmit = useCallback((data: FormData) => {
        onSubmitCB(data)
    }, [onSubmitCB]);

    const onTagChange = useCallback((tags: Array<string>) => setValue("tags", tags.join(",")), [setValue]);

    const onCategoryChange = useCallback((value: string) => {
        if (value === SET_NEW_CATEGORY_VALUE) {
            setValue("newCategory", "");
            setIsNewCategoryEnabled(true)
        } else {
            setIsNewCategoryEnabled(prevState => {
                if (prevState) {
                    setValue("newCategory", "");
                }

                return false
            })
        }
    }, [setValue]);

    const renderSelect = useCallback(({field: {onChange, ...rest}}: {
        field: ControllerRenderProps<FormData, "category">;
        fieldState: ControllerFieldState;
        formState: UseFormStateReturn<FormData>;
    }) => {
        return (
            <Select
                isLoading={items.length === 0}
                isClearable
                blurInputOnSelect
                closeMenuOnSelect
                closeMenuOnScroll
                defaultValue={defaultSelectValue ?? null}
                onChange={(val: SingleValue<Category>) => {
                    if (!val || val === undefined) return;
                    onChange(val);
                    onCategoryChange(val.value)
                }}
                menuPlacement="auto"
                options={[{label: "New category", value: SET_NEW_CATEGORY_VALUE}, ...items]}
                className="w-full text-lg rounded-lg"
                classNames={{
                    control: (_: ContainerProps<Category, false, GroupBase<Category>>) => {
                        return "py-1 rounded-lg"
                    }
                }}
                {...rest}
            />
        )
    }, [defaultSelectValue, items, onCategoryChange]);

    return (
        <div>
            {success !== null && success !== "" && success !== undefined && <div
                className="w-[95%] bg-green-500 text-white text-2xl p-4 rounded-lg m-[0_auto] mb-6">Successfully {isNew ? "created" : "updated"} post! <Link
                href={"/posts/" + success} className="text-blue-500">Open it</Link></div>}
            {error !== null && error !== "" && error !== undefined &&
                <div className="w-[95%] bg-red-500 text-white text-2xl p-4 rounded-lg m-[0_auto] mb-6">Error while
                    creating post: {error}</div>}

            <Modal onCancel={modalCancel} onConfirm={modalConfirm} show={showModal}><p className="text-justify">Are you
                sure you want to leave this page? Unsaved changed may be lost!</p></Modal>
            <form onSubmit={handleSubmit(onSubmit)} className="w-[95%] h-[90vh] flex flex-col m-center gap-7">
                <div><input className="w-full text-3xl shadow-xl rounded-lg px-3 py-2"
                            type="text" {...register("title", {required: true, minLength: 6})} /></div>
                <div className="flex flex-col h-3/6">
                    <textarea className="h-full text-xl shadow-xl rounded-lg px-4 py-4"  {...register("content", {
                        required: true,
                        minLength: 50
                    })} />
                </div>
                <div>
                    <Controller
                        name="category"
                        control={control}
                        rules={{required: true}}
                        defaultValue={defaultSelectValue}
                        render={renderSelect}
                    />

                    {isNewCategoryEnabled &&
                        <input className="w-full px-3 py-3 text-lg rounded-lg mt-4"
                               type="text" {...register("newCategory", {required: true, minLength: 6})} />
                    }

                </div>

                <TagInput onChange={onTagChange}
                          defaultValue={initData?.tags.split(",").map(el => el.trim()) ?? undefined}/>
                <div className="flex flex-row gap-4">
                    <input type="submit" value="Submit"
                           className="text-white border-white border-solid hover:cursor-pointer"/>
                    <input type="button" value="Discard"
                           className="text-white border-white border-solid hover:cursor-pointer"
                           onClick={() => setShowModal(true)}/>
                </div>
            </form>
        </div>
    );
};