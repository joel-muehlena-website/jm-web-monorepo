// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { FormattedDate } from "./../../components/FormattedDate";
import { isDateEqual } from "./../../utility/isDateEqual";
import { Post } from "./../../types/post";
import Link from "next/link";
import Image from "next/image";
import { useCallback, useState } from "react";
import { Modal } from "../Modal";
import { deletePost } from "./../../queries/client";
import { LOGGER } from "../../logger";
import { useHttpInterceptorManager } from "../../context/httpInterceptor";

export const PostPreview = ({ post, enableAuthorMode, onRemove }: {
  post: Post,
  enableAuthorMode: boolean,
  onRemove: (id: string) => void
}) => {
  const [showModal, setShowModal] = useState<boolean>(false);
  const { manager: interceptorManager } = useHttpInterceptorManager();

  const onDelete = useCallback(() => {
    setShowModal(true);
  }, [])

  const modalConfirm = async () => {
    try {
      const res = await deletePost(interceptorManager, post.id);
      if (!res.ok) {
        LOGGER.error("Error while deleting post", res);
        setShowModal(false);
        return;
      }
      LOGGER.info("Successfully deleted post", post.id, res);
      onRemove(post.id);
    } catch (err) {
      LOGGER.error("Error while deleting post", err);
    }
    setShowModal(false);
  }
  const modalCancel = () => {
    setShowModal(false);
  }

  return (
    <div className="px-10 py-8 hover:shadow-xl transition-all hover:scale-[0.98]">
      <Modal onCancel={modalCancel} onConfirm={modalConfirm} show={showModal}><p className="text-justify">Are you
        sure you want to delete the post &quot;{post.title}&quot;</p></Modal>
      <Link href={`/posts/${post.id}`}>
        <div className="flex flex-col text-xl">
          <p className="tracking-wider font-jm-montserrat text-2xl font-light">
            {post.category}
          </p>
          <h2 className="font-bold text-4xl mt-5 mb-5">{post.title}</h2>
          <p className="text-2xl font-light">{post.description}</p>
          <p className="italic font-bold text-xl flex items-center mt-6 mb-6">
            <span className="mr-6">
              by {post.authorFirstName} {post.authorLastName}
            </span>
            {post.authorAvatar && (
              <Image
                src={post.authorAvatar}
                height={50}
                width={50}
                alt={`Avatar of ${post.authorFirstName} ${post.authorLastName}`}
                className="rounded-[50%] object-cover border-[orange] border-2 object-center h-[5rem] w-[5rem]"
              />
            )}
          </p>
          <p>
            {!isDateEqual(new Date(post.createdAt), new Date(post.updatedAt)) ? (
              <FormattedDate date={post.updatedAt} className="text-xl" />
            ) : (
              <FormattedDate date={post.createdAt} className="text-xl" />
            )}
          </p>
          <div className="mt-5 flex gap-5 flex-wrap">
            {post.tags.map((tag) => (
              <div
                key={tag}
                className="bg-[#34495e] rounded-lg text-white py-2 px-4 shadow-3xl text-xl font-jm-montserrat font-light"
              >
                {tag}
              </div>
            ))}
          </div>
        </div>
      </Link>
      {
        enableAuthorMode && (
          <div className="mt-5 flex gap-5">
            <button><Link href={`/post/edit/${post.id}`} target="_blank">Edit</Link></button>
            <button onClick={onDelete}>Delete</button>
          </div>
        )
      }
    </div>
  );
};
