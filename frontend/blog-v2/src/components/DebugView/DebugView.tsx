'use client'

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {useState} from "react";
import {useDebugView} from "./context";

export const DebugView = ({
                              children,
                              enabled = true
                          }: {
    children: React.ReactNode;
    enabled?: boolean;
}) => {
    const [isOpen, setIsOpen] = useState<boolean>(false);

    if (!enabled) return null;

    return (
        <>
            {isOpen &&
                <ViewInternal close={() => setIsOpen(false)}>{children}</ViewInternal>
            }
            <button
                className="fixed left-5 bottom-5 h-[3rem] w-[3rem] rounded-[50%] bg-red-800 cursor-pointer text-sm text-white z-[5001]"
                onClick={() => setIsOpen(!isOpen)}>Toggle
            </button>
        </>
    );
};

const ViewInternal = ({
                          children,
                          close
                      }: {
    children: React.ReactNode;
    close: () => void;
}) => {
    const {nodes: clientNodes} = useDebugView();

    return (
        <div className="fixed bottom-0 left-0 right-0 w-screen h-screen z-[5000] bg-gray-800 text-white">
            <i className="las la-times fixed top-6 right-6 text-white text-6xl cursor-pointer"
               onClick={() => close()}></i>
            <h1 className="text-5xl text-center underline-offset-4 underline mt-8">Debug view</h1>
            <div className="text-xl px-10">
                <div className="flex flex-col items-start justify-start">
                    <h2 className="underline text-3xl mb-6">Server state</h2>
                    {children}
                </div>
                <hr className="my-10"/>
                <div>
                    <h2 className="underline text-3xl mb-6">Client state</h2>
                    {clientNodes.length === 0 ? <p>No nodes</p> : clientNodes}
                </div>
            </div>
        </div>
    )
}