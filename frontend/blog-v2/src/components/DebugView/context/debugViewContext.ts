'use client'

import {createContext, ReactNode} from "react";

export type DebugViewContext = {
    nodes: ReactNode[]
    addDebugNode: (node: ReactNode) => void
    removeDebugNode: (node: ReactNode) => void
}

export const DefaultDebugViewContextValue: DebugViewContext = {
    nodes: [],
    addDebugNode: (node) => {
    },
    removeDebugNode: (node) => {
    }
}

export const DebugViewContext = createContext<DebugViewContext>(DefaultDebugViewContextValue);