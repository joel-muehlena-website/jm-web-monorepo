'use client'
import {ReactNode, useCallback, useMemo, useState} from "react"
 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {DebugViewContext} from "./debugViewContext";

export const DebugViewProvider = ({children}: { children: ReactNode }) => {
    const [nodes, setNodes] = useState<Array<ReactNode>>([])

    const addDebugNode = useCallback((node: ReactNode) => {
        setNodes(nodes => [...nodes, node])
    }, []);

    const removeDebugNode = useCallback((node: ReactNode) => {
        setNodes(nodes => nodes.filter(n => n !== node))
    }, []);

    const contextValue = useMemo<DebugViewContext>(() => ({
        nodes: nodes,
        addDebugNode,
        removeDebugNode
    }), [nodes, addDebugNode, removeDebugNode])

    return (
        <DebugViewContext.Provider value={contextValue}>
            {children}
        </DebugViewContext.Provider>
    )
};