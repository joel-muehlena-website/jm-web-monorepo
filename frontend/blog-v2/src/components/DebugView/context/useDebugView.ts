'use client'
import {useContext} from "react"
import {DebugViewContext} from "./debugViewContext"

export const useDebugView = () => {
    const context = useContext(DebugViewContext)

    if (context === undefined || context === null) {
        throw new Error("debugView must be used within a DebugViewContextProvider")
    }

    return context;
}