"use client";

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {MainBG} from "./MainBG";

export const AnimatedBG = () => {
    return (
        <div className="absolute -z-10 top-0 left-0 right-0 bottom-0">
            <MainBG height={1} width={1}/>
        </div>
    );
};
