"use client";

import React, {FunctionComponent} from "react";
import {Prism as SyntaxHighlighter} from "react-syntax-highlighter";
import {darcula} from "react-syntax-highlighter/dist/esm/styles/prism";

export const MarkdownCodeBlock: FunctionComponent<{
    inline?: boolean;
    className?: string;
    children: React.ReactNode & React.ReactNode[];
}> = ({inline, className, children, ...props}) => {
    const match = /language-(\w+)/.exec(className || "");

    return !inline && match ? (
        <SyntaxHighlighter
            // eslint-disable-next-line react/no-children-prop
            children={String(children).replace(/\n$/, "")}
            language={match[1]}
            style={darcula}
            {...props}
        />
    ) : (
        <code className={className} {...props}>
            {children}
        </code>
    );
};
