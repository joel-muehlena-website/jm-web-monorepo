// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {getPosts} from "./../../queries";
import {OptionalFilterOptions} from "./../../types/post";
import {AllPostsInternal} from "./allPostsInternal";

type Props = {
    filter?: OptionalFilterOptions
    enableAuthorMode?: boolean
}

export const AllPosts = async ({filter, enableAuthorMode = false}: Props) => {
    const posts = await getPosts(filter ?? {});

    return (
        <div className="py-4 px-16">
            {posts !== undefined && Array.isArray(posts) && (
                <AllPostsInternal initialPosts={posts} enableAuthorMode={enableAuthorMode}/>
            )}
        </div>
    );
};

export default AllPosts;
