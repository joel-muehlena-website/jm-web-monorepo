"use client";

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {useState} from "react";
import {Post} from "./../../types/post";
import {PostPreview} from "../PostPreview/postPreview";

type Props = {
    initialPosts: Post[];
    enableAuthorMode: boolean;
};

export const AllPostsInternal = ({initialPosts, enableAuthorMode}: Props) => {
    const [posts, setPosts] = useState<Post[]>(initialPosts);

    const onFilterApply = () => {
    };

    const onRemovePost = (id: string) => {
        setPosts(posts.filter(p => p.id !== id));
    }

    return (
        <>
            {posts.map((post, i) => (
                <div
                    className={`${i < posts.length - 1 ? "border-b-[1px]" : ""
                    } border-b-black`}
                    key={post.id}
                >
                    <PostPreview post={post} key={i} enableAuthorMode={enableAuthorMode} onRemove={onRemovePost}/>
                </div>
            ))}
            {/*PostFilter posts={initialPosts} onApply={onFilterApply} />*/}
        </>
    );
};
