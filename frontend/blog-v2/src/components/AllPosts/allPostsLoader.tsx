// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import ContentLoader from "react-content-loader"

export const AllPostLoader = ({count = 1}: { count?: number }) => {
    const mt = count > 1 ? '-7rem' : '';

    return (
        <>
            {
                Array.from(Array(count).keys()).map((cnt, i) => (
                    <ContentLoader
                        key={cnt}
                        speed={2}
                        style={{height: '30rem', marginTop: i > 0 ? mt : 0}}
                        viewBox="0 0 550 230"
                        backgroundColor="#f3f3f3"
                        foregroundColor="#ecebeb"
                    >

                        <rect x="40" y="32" rx="0" ry="0" width="26" height="11"/>
                        <rect x="40" y="55" rx="0" ry="0" width="188" height="17"/>
                        <rect x="40" y="80" rx="0" ry="0" width="375" height="14"/>
                        <rect x="40" y="114" rx="0" ry="0" width="60" height="10"/>
                        <rect x="40" y="145" rx="0" ry="0" width="68" height="9"/>
                        <rect x="40" y="163" rx="0" ry="0" width="51" height="23"/>
                        <rect x="60" y="169" rx="0" ry="0" width="4" height="1"/>
                        <rect x="86" y="179" rx="0" ry="0" width="2" height="3"/>
                        <rect x="88" y="182" rx="0" ry="0" width="2" height="2"/>
                        <rect x="97" y="163" rx="0" ry="0" width="50" height="23"/>
                        <circle cx="124" cy="118" r="19"/>
                        <rect x="30" y="196" rx="0" ry="0" width="100%" height="4"/>


                        {count === 1 &&
                            <>
                                <rect x="40" y="213" rx="0" ry="0" width="62" height="11"/>
                            </>
                        }

                    </ContentLoader>
                ))
            }
        </>
    )
}