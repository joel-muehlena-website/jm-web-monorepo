'use client'

 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {ChangeEvent, KeyboardEvent, useCallback, useEffect, useState} from "react";

type Props = {
    onChange: (tags: Array<string>) => void;
    defaultValue?: Array<string>;
}

export const TagInput = ({onChange: onTagChange, defaultValue}: Props) => {
    const [tags, setTags] = useState<string[]>([]);
    const [newTag, setNewTag] = useState<string>("");

    useEffect(() => {
        if (defaultValue && defaultValue !== undefined) {
            onTagChange(defaultValue)
            const set = new Set(defaultValue)
            setTags(Array.from(set))
        }
    }, [defaultValue, onTagChange]);

    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
        let val = e.target.value;

        if (val.endsWith(" ") || val.endsWith(",")) {
            val = val.trim();
            if (val === "") return;

            const part = val.substring(0, val.length);

            if (!tags.includes(part)) {
                setTags([...tags, part]);
            }

            setNewTag("");
            return;
        }

        setNewTag(val);
    };

    const onKeyUp = useCallback((e: KeyboardEvent<HTMLInputElement>) => {
        if (e.key === "Enter" && newTag !== "") {
            e.preventDefault();

            if (!tags.includes(newTag)) {
                setTags([...tags, newTag.trim()]);
            }

            setNewTag("");
            return;
        }

        if (e.key === "Backspace" && newTag === "" && e.repeat === false) {
            e.preventDefault();
            setTags(tags.slice(0, tags.length - 1));
        }
    }, [newTag, tags]);

    const removeTag = (tag: string) => {
        setTags(tags.filter((t) => t !== tag));
    };

    useEffect(() => {
        onTagChange(tags);
    }, [tags, onTagChange]);

    return (
        <div className="flex gap-1 flex-wrap border-solid border-2 border-black py-2 px-1 rounded-lg" onKeyUp={onKeyUp}>
            {tags.map((tag) => (
                <div key={tag}
                     className="bg-[#34495e] rounded-lg text-white py-2 px-4 shadow-3xl text-xl font-jm-montserrat flex justify-between gap-0 items-center">
                    {tag} <i className="las la-times ml-4 cursor-pointer" onClick={() => removeTag(tag)}/>
                </div>
            ))}
            <input type="text" placeholder="Add a tag" onChange={onChange} value={newTag}
                   className="rounded-lg py-2 px-4 shadow-3xl text-lg font-jm-montserrat border-none outline-none bg-gray-500"/>
        </div>
    )
};