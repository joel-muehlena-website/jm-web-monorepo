'use client'

import React from "react"
import {DNA} from 'react-loader-spinner'

export const LoadingSpinner = ({className}: { className?: string }) => {
    return (
        <div className={`m-center grid justify-center ${className ?? ""}`}>
            <DNA
                visible={true}
                height="90"
                width="90"
            />
        </div>
    )
}