export type ResponseTypes = "json" | "text";

export interface IHTTPError {
    res?: Response;
}

export class HTTPError implements IHTTPError, Error {
    public name: string;
    public message: string;
    public stack?: string | undefined;
    public cause?: unknown;

    public res?: Response | undefined;
    public status: number;
    public statusText: string;

    constructor(message: string, cause?: unknown, res?: Response) {
        this.name = "HTTPError";
        this.message = message;
        this.cause = cause;
        this.res = res;
        this.status = res?.status ?? 500;
        this.statusText = res?.statusText ?? "Error";
    }
}

export type HTTPResponse<T = {}> = {
    data: T;
    ok: boolean;
    status: number;
    statusText: string;
    error: HTTPError | null;
}