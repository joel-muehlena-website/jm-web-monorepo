import {ResponseTypes} from "./response";

export class HTTPRequest {
    method: string;
    url: string;
    responseType: ResponseTypes;
    options: RequestInit

    constructor(method: string, url: string, options: RequestInit = {}, responseType: ResponseTypes = "json") {
        this.method = method;
        this.url = url;
        this.options = options;
        this.responseType = responseType;
    }

    private _allowedResponseCodes: Set<number> = new Set([200, 201, 204]);

    get allowedResponseCodes(): number[] {
        return Array.from(this._allowedResponseCodes.values());
    }

    allowResponseCode(code: number): HTTPRequest {
        this._allowedResponseCodes.add(code);
        return this;
    }

    allowResponseCodes(codes: number[]): HTTPRequest {
        this._allowedResponseCodes = new Set([...Array.from(this._allowedResponseCodes.values()), ...codes]);
        return this;
    }

    disallowResponseCode(code: number): HTTPRequest {
        this._allowedResponseCodes.delete(code);
        return this;
    }

    disallowResponseCodes(codes: number[]): HTTPRequest {
        for (let code of codes) {
            this._allowedResponseCodes.delete(code);
        }

        return this;
    }
}