import {catchError, Observable, throwError} from "rxjs";
import {HTTPRequest} from "./request";
import {LOGGER} from "./../logger/logger";
import {HTTPResponse} from "./response";

export class HTTPHandler {
    constructor(private _self: HTTPInterceptor | undefined, private _next: HTTPHandler | null) {
    }

    set next(next: HTTPHandler) {
        this._next = next;
    }

    handle(req: HTTPRequest): Observable<HTTPResponse<unknown>> {
        LOGGER.trace("HTTPHandler.handle", req.method, req.url, req.options.headers, req.options.credentials, req.options.body);

        if (this._self === undefined) {
            return throwError(() => new Error("No interceptor set. Maybe you wanted to override the handle method?"))
        }

        if (this._next === null) {
            return throwError(() => new Error("No next handler set. Maybe you wanted to override the handle method?"))
        }

        return this._self.intercept(req, this._next);
    }
}

export interface HTTPInterceptor {
    intercept(req: HTTPRequest, next: HTTPHandler): Observable<HTTPResponse<unknown>>;
}

export class HTTPErrorInterceptor implements HTTPInterceptor {
    constructor(private errorHandler: (error: any) => void) {
    }

    intercept(req: HTTPRequest, next: HTTPHandler): Observable<HTTPResponse<unknown>> {
        return next.handle(req).pipe(
            catchError(error => {
                this.errorHandler(error);
                return throwError(() => error);
            })
        );
    }
}

export class HTTPInterceptorChain {
    public static build(interceptors: Array<HTTPInterceptor>, reqHandler: HTTPHandler): HTTPHandler {
        // Early return if we don't have any interceptors and just want to handle the request
        if (interceptors.length === 0) {
            return reqHandler;
        }

        // Build the chain of handlers
        // Each handler must now its next handler
        const handlers: Array<HTTPHandler> = [];
        for (let i = 0; i < interceptors.length; i++) {
            const handler = new HTTPHandler(interceptors[i], null);
            handlers.push(handler);
            if (i == 0) continue;

            handlers[i - 1].next = handler;
        }

        // The last handler must call the handler which does the actual request
        handlers[handlers.length - 1].next = reqHandler
        return handlers[0];
    }
}

export class HTTPInterceptorManager {
    private _allowOverride = false;
    private _interceptors: Map<string, HTTPInterceptor> = new Map();
    private _order: Array<string> = [];

    allowOverride(allowOverride: boolean) {
        this._allowOverride = allowOverride;
    }

    isAllowOverride(): boolean {
        return this._allowOverride;
    }

    order(): Array<string> {
        return this._order;
    }

    setOrder(order: Array<string>) {
        for (let o of order) {
            if (!this._interceptors.has(o)) {
                throw new Error(`Interceptor ${o} does not exist. Please register first`);
            }
        }

        this._order = order;
    }

    register(name: string, interceptor: HTTPInterceptor): boolean {
        if (!this._allowOverride && this._interceptors.has(name)) {
            return false;
        }

        this._order.push(name);
        this._interceptors.set(name, interceptor);
        return true;
    }

    unregister(name: string): void {
        this._interceptors.delete(name);
    }

    getInterceptorNames(): String[] {
        return Array.from(this._interceptors.keys());
    }

    getInterceptors(): HTTPInterceptor[] {
        const interceptors: Array<HTTPInterceptor> = [];

        for (let o of this._order) {
            interceptors.push(this._interceptors.get(o) as HTTPInterceptor);
        }

        return interceptors;
    }

    getInterceptorsMap(): Map<string, HTTPInterceptor> {
        return this._interceptors;
    }
}

export const HTTP_INTERCEPTOR_MANAGER = new HTTPInterceptorManager();