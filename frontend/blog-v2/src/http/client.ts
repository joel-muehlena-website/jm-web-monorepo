import {LOGGER} from "../logger/logger"
import {catchError, firstValueFrom, from, map, Observable, switchMap, throwError} from "rxjs"
import {HTTPHandler, HTTPInterceptorChain, HTTPInterceptorManager} from "./interceptor";
import {HTTPError, HTTPResponse, ResponseTypes} from "./response";
import {HTTPRequest} from "./request";

function wrap<T>(d: T): Promise<T> {
    return new Promise((res) => res(d));
}

export type RequestResponse<T> = Observable<HTTPResponse<T>>

export class HTTPClient extends HTTPHandler {

    public static readonly supportedContentTypes = ["application/json", "text/plain"];

    private readonly logger = LOGGER.getSubLogger({name: "http-client"});

    constructor(private interceptors: HTTPInterceptorManager) {
        // Create an HTTPHandler, where the interceptor is not defined and the next handler is null
        // This is done because **this** is the last handler in the chain
        super(undefined, null);
    }

    GET<T = {}>(url: string, responseType: ResponseTypes = "json"): RequestResponse<T> {
        return this.request<T>('GET', url, {}, responseType);
    }

    POST<T = {}>(url: string, data: RequestInit = {}, responseType: ResponseTypes = "json"): RequestResponse<T> {
        return this.request<T>('POST', url, data, responseType);
    }

    PATCH<T = {}>(url: string, data: RequestInit = {}, responseType: ResponseTypes = "json"): RequestResponse<T> {
        return this.request<T>('PATCH', url, data, responseType);
    }

    DELETE<T = {}>(url: string, data: RequestInit = {}, responseType: ResponseTypes = "json"): RequestResponse<T> {
        return this.request<T>('DELETE', url, data, responseType);
    }

    handle({method, options, responseType, url, allowedResponseCodes}: HTTPRequest): Observable<HTTPResponse<unknown>> {
        LOGGER.trace("HTTPHandler.handle for real request", method, url, options);

        const request = fetch(url, {
            ...options,
            method,
        })

        const $observer: Observable<HTTPResponse<unknown>> = from(request).pipe(
            switchMap<Response, Promise<HTTPResponse<unknown>> | Observable<never>>((response) => {

                if (!response.ok && !allowedResponseCodes.includes(response.status)) {
                    return throwError(() => new HTTPError(`The response code of the http request was ${response.status} (${response.statusText})`, "Response not ok", response));
                }

                if (!allowedResponseCodes.includes(response.status)) {
                    return throwError(() => new HTTPError(`The response code of the http request was ${response.status}. But only ${allowedResponseCodes.join(", ")} are allowed`, "Response code not allowed", response));
                }

                const contentTypeRaw = response.headers.get('content-type');

                if (contentTypeRaw === null || contentTypeRaw === "") {
                    return new Promise((res) => res({
                        data: null,
                        ok: response.ok,
                        status: response.status,
                        statusText: response.statusText,
                        error: null,
                    } as HTTPResponse<unknown>))
                }

                const contentTypeOptions = contentTypeRaw.split(";");
                const contentType = contentTypeOptions.shift()!;

                if (!HTTPClient.supportedContentTypes.includes(contentType)) {
                    return throwError(() => new HTTPError(`Unsupported content type: ${contentType}. Supported are: ${HTTPClient.supportedContentTypes.join(", ")}`, "Unsupported content type in response", response));
                }

                let data: Observable<any> | null = null;
                if (responseType === "json" && contentType.includes("application/json")) {
                    data = from(response.json())
                } else if (responseType === "text" && contentType.includes("text")) {
                    data = from(response.text());
                }

                if (data !== null) {
                    return firstValueFrom(data.pipe(map(d => {
                        return {
                            data: d,
                            ok: response.ok,
                            status: response.status,
                            statusText: response.statusText,
                            error: null,
                        } as HTTPResponse<unknown>;
                    })));
                }

                return throwError(() => new Error(`Unexpected error. Should be unreachable!`));
            }),
            catchError((error) => {
                this.logger.trace("There was an error with an HTTP request", {method, url, options}, error);
                return throwError(() => error);
            })
        )

        return $observer;
    }

    private request<T>(method: string, url: string, config: RequestInit, responseType: ResponseTypes): Observable<HTTPResponse<T>> {
        this.logger.debug("Creating new request", method, url, config);
        const handlerChainList = HTTPInterceptorChain.build(this.interceptors.getInterceptors(), this);

        return handlerChainList.handle(new HTTPRequest(method, url, config, responseType)) as Observable<HTTPResponse<T>>;
    }

    private requestWithHTTPRequest<T>(req: HTTPRequest): Observable<T> {
        this.logger.debug("Creating new request", req);
        const handlerChainList = HTTPInterceptorChain.build(this.interceptors.getInterceptors(), this);

        return handlerChainList.handle(req) as Observable<T>;
    }

}