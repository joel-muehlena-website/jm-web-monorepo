import { DeepReadonly } from "ts-essentials"

export type ApplicationConfig = {
  api: {
    gateway: {
      url: string
    },
    requests: {
      defaultRevalidateTime: number
    }
  },
  auth: {
    cookie: {
      domain: string;
      secure: boolean;
      sameSite: "strict" | "lax" | "none";
      accessTokenName: string;
    },
    urlBase: string;
    loginUrl: string;
  }
}

const prodConfig: DeepReadonly<ApplicationConfig> = {
  api: {
    gateway: {
      url: process.env.GATEWAY_URL ?? "https://api-v2.joel.muehlena.de"
    },
    requests: {
      defaultRevalidateTime: 5000
    }
  },
  auth: {
    cookie: {
      domain: process.env.COOKIE_DOMAIN ?? "blog-v2.joel.muehlena.de",
      secure: true,
      sameSite: "strict",
      accessTokenName: "next_ac_43235"
    },
    loginUrl: process.env.LOGIN_URL ?? "https://login.joel.muehlena.de",
    urlBase: process.env.AUTH_URL_BASE ?? "https://auth2.joel.muehlena.de"
  }
}

const devConfig: DeepReadonly<ApplicationConfig> = {
  api: {
    gateway: {
      url: process.env.GATEWAY_URL ?? "http://localhost:8088"
    },
    requests: {
      defaultRevalidateTime: 0
    }
  },
  auth: {
    cookie: {
      domain: process.env.COOKIE_DOMAIN ?? "localhost",
      secure: false,
      sameSite: "strict",
      accessTokenName: "next_ac_43235__dev"
    },
    loginUrl: process.env.LOGIN_URL ?? "http://localhost:8180",
    urlBase: process.env.AUTH_URL_BASE ?? "http://localhost:8080"
  }
}

export const applicationConfig: DeepReadonly<ApplicationConfig> = process.env.NODE_ENV === "production" ? prodConfig : devConfig;