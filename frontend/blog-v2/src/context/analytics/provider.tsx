"use client";
import { ReactNode, useMemo, useState } from "react";
// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { useRefFn } from "./../../components/hooks/useRefFn";
import { AnalyticsContext } from "./analyticsContext";

export type MatomoManager = {
  mtm: Array<any>;
  paq: Array<any>;
};

export const AnalyticsProvider = ({ children }: { children: ReactNode }) => {
  const [initialized, setInitialized] = useState(false);

  const matomoManager = useRefFn<MatomoManager>(() => {
    // Will happen because client components are not fully rendered on the server
    // So at first this will be undefined
    if (typeof window === "undefined") {
      return { mtm: [], paq: [] };
    }

    (window as any).matomoTagManagerAsyncInit = function () {
      setInitialized(true);
    };

    const _mtm = ((window as any)._mtm = (window as any)._mtm ?? []);
    _mtm.push({ "mtm.startTime": new Date().getTime(), event: "mtm.Start" });
    (function () {
      var d = document,
        g = d.createElement("script"),
        s = d.getElementsByTagName("script")[0];
      g.async = true;
      g.src =
        "https://matomo.muehlena.de/js/container_n1K9mFKR_dev_775b85419e263613d10b4f33.js";
      s.parentNode?.insertBefore(g, s);
    })();

    return { mtm: _mtm, paq: (window as any)._paq ?? [] };
  });

  const contextValue = useMemo<AnalyticsContext>(
    () => ({
      initialized,
      mtm: matomoManager.current.mtm,
      paq: matomoManager.current.paq,
    }),
    [initialized, matomoManager]
  );

  return (
    <AnalyticsContext.Provider value={contextValue}>
      {children}
    </AnalyticsContext.Provider>
  );
};
