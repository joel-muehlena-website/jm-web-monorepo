'use client'
import {useContext} from "react"
import {AnalyticsContext} from "./analyticsContext"

export const useAnalytics = () => {
    const context = useContext(AnalyticsContext)

    if (context === undefined) {
        throw new Error("useAnalytics must be used within a AnalyticsProvider")
    }

    return context;
}