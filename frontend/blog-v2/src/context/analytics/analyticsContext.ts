'use client'

import {createContext} from "react";
import {MatomoManager} from "./provider";

export type AnalyticsContext = {
    initialized: boolean
} & MatomoManager

export const DefaultAnalyticsContextValue: AnalyticsContext = {
    initialized: false,
    mtm: [],
    paq: []
}

export const AnalyticsContext = createContext<AnalyticsContext>(DefaultAnalyticsContextValue);