'use client'
import {useContext} from "react"
import {ErrorManagerContext} from "./errorContext"

export const useErrorManager = () => {
    const context = useContext(ErrorManagerContext)

    if (context === undefined || context === null) {
        throw new Error("errorManager must be used within a ErrorManagerContextProvider")
    }

    return context;
}