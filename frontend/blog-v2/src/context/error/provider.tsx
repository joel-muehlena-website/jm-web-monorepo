'use client'
import { ReactNode, useEffect, useMemo } from "react"
 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import { useRefFn } from "./../../components/hooks/useRefFn";
import { ErrorManagerContext } from "./errorContext";
import { ErrorService } from "../../service/error.service";

export const ErrorManagerProvider = ({ children }: { children: ReactNode }) => {
  const errorService = useRefFn(() => new ErrorService())

  useEffect(() => {
    const svc = errorService.current
    svc.start()

    return () => svc.stop()
  }, [errorService])

  const contextValue = useMemo<ErrorManagerContext>(() => ({
    errorManager: errorService.current
  }), [errorService])

  return (
    <ErrorManagerContext.Provider value={contextValue}>
      {children}
    </ErrorManagerContext.Provider>
  )
};