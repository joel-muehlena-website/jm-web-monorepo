'use client'

import { createContext } from "react";
import { ErrorService } from "../../service/error.service";

export type ErrorManagerContext = {
  errorManager: ErrorService;
}

export const DefaultErrorManagerContextValue: ErrorManagerContext = {
  errorManager: new ErrorService()
}

export const ErrorManagerContext = createContext<ErrorManagerContext>(DefaultErrorManagerContextValue);