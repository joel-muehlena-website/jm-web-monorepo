'use client'

import {createContext} from "react";
import {HTTPInterceptorManager} from "./../../http";

export type HttpInterceptorManagerContextValue = {
    manager: HTTPInterceptorManager;
}

export const DefaultHttpInterceptorContextValue: HttpInterceptorManagerContextValue = {
    manager: new HTTPInterceptorManager()
}

export const HttpInterceptorManagerContext = createContext<HttpInterceptorManagerContextValue>(DefaultHttpInterceptorContextValue);