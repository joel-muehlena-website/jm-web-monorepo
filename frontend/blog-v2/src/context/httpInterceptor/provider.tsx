'use client'
import {ReactNode, useMemo} from "react"
 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {HTTPInterceptorManager} from "./../../http";
import {useRefFn} from "./../../components/hooks/useRefFn";
import {HttpInterceptorManagerContext, HttpInterceptorManagerContextValue} from "./httpInterceptorContext";

export const HttpInterceptorContextProvider = ({children}: { children: ReactNode }) => {
    const httpInterceptor = useRefFn<HTTPInterceptorManager>(() => new HTTPInterceptorManager())

    const contextValue = useMemo<HttpInterceptorManagerContextValue>(() => ({
        manager: httpInterceptor.current
    }), [httpInterceptor])

    return (
        <HttpInterceptorManagerContext.Provider value={contextValue}>
            {children}
        </HttpInterceptorManagerContext.Provider>
    )
};