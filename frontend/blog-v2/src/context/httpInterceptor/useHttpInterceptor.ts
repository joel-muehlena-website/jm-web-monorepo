'use client'
import {useContext} from "react"
import {HttpInterceptorManagerContext} from "./httpInterceptorContext"

export const useHttpInterceptorManager = () => {
    const context = useContext(HttpInterceptorManagerContext)

    if (context === undefined || context === null) {
        throw new Error("useHttpInterceptor must be used within a HttpInterceptorContextProvider")
    }

    return context;
}