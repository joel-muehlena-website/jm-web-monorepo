"use client"

// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React, { ReactNode } from "react"
import { AuthContextProvider } from "./auth"
import { HttpInterceptorContextProvider } from "./httpInterceptor"
import { ErrorManagerProvider } from "./error"
import { DebugViewProvider } from "../components/DebugView/context"
import { AnalyticsProvider } from "./analytics"

export type Props = {
  children: ReactNode
  authURLBase: string
}

export const Providers = ({ children, authURLBase }: Props) => {
  return (
    <DebugViewProvider>
      <AnalyticsProvider>
        <ErrorManagerProvider>
          <HttpInterceptorContextProvider>
            <AuthContextProvider authURLBase={authURLBase}>
              {children}
            </AuthContextProvider>
          </HttpInterceptorContextProvider>
        </ErrorManagerProvider>
      </AnalyticsProvider>
    </DebugViewProvider>
  )
}