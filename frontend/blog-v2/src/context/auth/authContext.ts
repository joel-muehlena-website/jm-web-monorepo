'use client'

import {User} from "../../service/auth.service";
import {createContext} from "react";

export type AuthContextValue<T = User> = {
    isAuthenticated: boolean;
    accessToken: string | null;
    user: T | null;
}

export const DefaultAuthContextValue: AuthContextValue = {
    isAuthenticated: false,
    accessToken: null,
    user: null
}

export const AuthContext = createContext<AuthContextValue>(DefaultAuthContextValue);