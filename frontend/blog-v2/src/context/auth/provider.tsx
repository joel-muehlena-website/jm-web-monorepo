'use client'
import {ReactNode, useCallback, useEffect, useMemo, useRef, useState} from "react"
 // @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error
import React from "react";
import {AuthContext, AuthContextValue} from "./authContext";
import {AuthService, User} from "./../../service/auth.service";
import {LOGGER} from "./../../logger/logger";
import {useRefFn} from "./../../components/hooks/useRefFn";
import {AuthInterceptor} from "./../../interceptors/auth.interceptor";
import {RefreshAuthInterceptor} from "./../../interceptors/refreshAuth.interceptor";
import {setNextAccessToken} from "./../../queries/client";
import {from, Observable, of, switchMap, zip} from "rxjs";
import {useHttpInterceptorManager} from "../httpInterceptor";

export const JM_AUTH_INTERCEPTOR_KEY = "jm::api::auth";
export const JM_AUTH_REFRESH_INTERCEPTOR_KEY = "jm::api::refresh-auth";

export const AuthContextProvider = ({children, authURLBase}: { children: ReactNode, authURLBase: string }) => {
    const authServiceRef = useRefFn<AuthService>(() => new AuthService("client", authURLBase));
    const loggerRef = useRef(LOGGER.getSubLogger({name: "AuthContextProvider"}));
    const [isInitialized, setIsInitialized] = useState(false);
    const {manager} = useHttpInterceptorManager();

    const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
    const [accessToken, setAccessToken] = useState<string | null>(null);
    const [user, setUser] = useState<User | null>(null)

    const reset = useCallback(() => {
        setIsAuthenticated(false);
        setAccessToken(null);
        setUser(null);
    }, []);

    // Init auth service
    useEffect(() => {
        const logger = loggerRef.current;

        if (isInitialized) {
            return
        }

        const authService = authServiceRef.current;

        logger.debug("Initializing auth system for context aware components");

        authService.enablePeriodicRefresh(5 * 60);

        const sub = authService.refresh().subscribe({
            error(err) {
                logger.error("Error while initial refreshing auth", err);
                setIsAuthenticated(false);
            },
            next(value) {
                setIsAuthenticated(value);
            },
            complete() {
                logger.debug("Auth system initialized");
                setIsInitialized(true);
            },
        });

        return () => sub.unsubscribe()

    }, [isInitialized, authURLBase, authServiceRef, loggerRef])

    useEffect(() => {
        const authService = authServiceRef.current;
        manager.register(JM_AUTH_INTERCEPTOR_KEY, new AuthInterceptor(authService))
        manager.register(JM_AUTH_REFRESH_INTERCEPTOR_KEY, new RefreshAuthInterceptor(authService))
    }, [manager, authServiceRef])

    // Access token & user handler
    useEffect(() => {
        const authService = authServiceRef.current;
        if (authService === undefined) {
            loggerRef.current.error("Auth service not initialized");
            return;
        }

        const logger = loggerRef.current;

        const sub = zip(authService.accessToken, authService.user)
            .pipe(
                switchMap<[string, User | null], Observable<[string, User | null, [boolean, string]]>>(
                    ([accessToken, user]) => {
                        setAccessToken(accessToken);
                        setUser(user);

                        if (accessToken == "") return of<[string, User | null, [boolean, string]]>([accessToken, user, [false, "Access token is empty"]]);

                        return from(setNextAccessToken(accessToken, user?.expiresAtUTC ?? new Date(), false))
                            .pipe(
                                switchMap((val) => {
                                    return of<[string, User | null, [boolean, string]]>([accessToken, user, val])
                                })
                            )
                    }
                )
            )
            .subscribe({
                error(err) {
                    logger.error("Error fetching access token or user", err);
                    reset();
                },
                next([_, __, [setNextAccessTokenResult, msg]]) {
                    if (!setNextAccessTokenResult && msg !== "Access token is empty") {
                        logger.error("Failed to set next access token:", msg);
                    } else {
                        logger.trace("Set next access token:", msg);
                    }
                }
            });

        return () => sub.unsubscribe();
    }, [reset, loggerRef, authServiceRef]);

    useEffect(() => {
        loggerRef.current.debug("Auth system changed. Is currently authenticated:", true, "User:", user);
        if (accessToken !== undefined) loggerRef.current.trace("Access token:", accessToken);
    }, [user, accessToken])

    const contextValue = useMemo<AuthContextValue>(() => ({
        isAuthenticated,
        user,
        accessToken
    }), [isAuthenticated, user, accessToken]);

    return (
        <AuthContext.Provider value={contextValue}>
            {children}
        </AuthContext.Provider>
    )
};