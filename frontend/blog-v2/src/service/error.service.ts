import { addSeconds } from "date-fns";
import { interval, Subject, Subscription } from "rxjs";

export type Error = {}

export type RichError = {
  error: Error;
  addedAt: Date;
  toShowSeconds: number;
  dueDate: Date;
}

export class ErrorService {
  public static readonly DEFAULT_ERROR_SHOW_TIME = 5;
  private _sub: Subscription | null = null;

  private _errors: Array<RichError> = [];

  get errors(): Array<Error> {
    return this._errors.map(e => e.error);
  }

  private _errorWatcher: Subject<Array<RichError>> = new Subject<Array<RichError>>();

  get errorWatcher(): Subject<Array<RichError>> {
    return this._errorWatcher;
  }

  start(checkIntervalSeconds = 1) {
    if (this._sub) return

    const source = interval(1000 * checkIntervalSeconds)

    this._sub = source.subscribe(() => {
      const now = new Date();
      this._errors = this._errors.filter(e => e.dueDate > now);
      this.updateErrors();
    });
  }

  stop() {
    if (!this._sub) return

    this._sub.unsubscribe();
    this._sub = null;
  }

  addError(error: Error) {
    const now = new Date();

    this._errors.push({
      error,
      addedAt: now,
      toShowSeconds: ErrorService.DEFAULT_ERROR_SHOW_TIME,
      dueDate: addSeconds(now, ErrorService.DEFAULT_ERROR_SHOW_TIME)
    });
    this.updateErrors();
  }

  removeError(error: Error) {
    this._errors = this._errors.filter(e => e.error !== error);
    this.updateErrors();
  }

  removeErrorIndex(i: number) {
    this._errors.splice(i, 1);
    this.updateErrors();
  }

  private updateErrors() {
    this._errorWatcher.next(this._errors);
  }
}