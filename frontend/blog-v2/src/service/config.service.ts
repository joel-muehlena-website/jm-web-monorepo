import { ApplicationConfig, applicationConfig } from "../ApplicationConfig";
import { LOGGER } from "./../logger/logger";
import { Logger } from "tslog";

type Dict = {
  [key: string]: any
}

interface Config extends Dict {
  appLoginUrl: string
  authUrlBase: string
  apiUrlBase: string
  rootUrl: string
}

export class ConfigService {
  private static readonly appConfig = applicationConfig;
  private readonly m_logger: Logger<unknown> = LOGGER.getSubLogger({ name: "config-service" });

  constructor() {
    this.m_logger.info("Loaded config service");
  }

  public get appLoginUrl() {
    return ConfigService.appConfig.auth.loginUrl;
  }

  public static config(): ApplicationConfig {
    return ConfigService.appConfig;
  }
}