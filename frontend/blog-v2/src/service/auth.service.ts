import { BehaviorSubject, firstValueFrom, from, Observable, Subject, Subscriber } from "rxjs";
import { addSeconds, format } from 'date-fns';
import { utcToZonedTime } from 'date-fns-tz';
import { Logger } from "tslog";
import { LOGGER } from "./../logger/logger";
import { jwtDecode } from "jwt-decode";
import { URLSearchParams } from "url";

export type User = {
  email: string;
  firstName: string;
  lastName: string;
  avatar: string;
  username: string;
  locale: string;
  id: string;
  issuedAtUTC: Date;
  issuedAt: Date;
  expiresAtUTC: Date;
  expiresAt: Date;
};

export class AuthService {
  protected readonly m_refreshTokenName = "refresh_token";
  protected readonly m_logger: Logger<unknown> = LOGGER.getSubLogger({ name: "auth-service-" + this.loggerSuffix });
  private m_isAuth = new BehaviorSubject<boolean>(false);
  private m_accessToken = new BehaviorSubject<string>("");
  private m_user = new BehaviorSubject<User | null>(null);
  private intervalID: undefined | NodeJS.Timeout = undefined;

  constructor(private readonly loggerSuffix: string, private readonly authURLBase: string) {
    this.m_logger.info("Loaded auth service with auth url base: ", this.authURLBase);
  }

  public get accessToken(): BehaviorSubject<string> {
    return this.m_accessToken;
  }

  public get user(): BehaviorSubject<User | null> {
    return this.m_user;
  }

  public get isAuth(): BehaviorSubject<boolean> {
    return this.m_isAuth;
  }

  public static decodeUser(token: string): User | null {
    if (token === "") {
      return null;
    }

    const decoded: any = jwtDecode(token);

    const iat = new Date(decoded.iat * 1000);
    const exp = new Date(decoded.exp * 1000);

    // TODO generate from user locale
    const timeZone = 'Europe/Berlin'

    // TODO validate object keys
    const user: User = {
      email: decoded.email,
      firstName: decoded.given_name,
      lastName: decoded.family_name,
      avatar: decoded.picture,
      username: decoded.preferred_username,
      locale: decoded.locale,
      id: decoded.sub,
      issuedAtUTC: iat,
      issuedAt: utcToZonedTime(iat, timeZone), // Somehow not working when printing to stdout, but if e.g. getHours() is called the time-zoned value is returned
      expiresAtUTC: exp,
      expiresAt: utcToZonedTime(exp, timeZone), // Somehow not working when printing to stdout, but if e.g. getHours() is called the time-zoned value is returned
    }

    //this.m_logger.trace("Decoded user:", user);

    return user;
  }

  public static isAuthorized(baseURL: string, accessToken: string, permission: string): Observable<boolean> {
    LOGGER.debug("Checking if authorized for permission: ", permission);

    return new Observable((observer) => {
      AuthService.checkAuthorized(baseURL, accessToken, permission, observer)
    });
  }

  private static async checkAuthorized(baseURL: string, accessToken: string, permission: string, observer?: Subscriber<boolean>): Promise<boolean> {
    const logger = LOGGER.getSubLogger({ name: "auth-service" })
    const params = new URLSearchParams({ permission: permission });

    const headers = new Headers();
    headers.append("Authorization", `Bearer ${accessToken}`);

    try {
      const data = await fetch(
        baseURL + `/auth/authorized?${params.toString()}`,
        { credentials: "include", headers, next: { revalidate: 0 }, cache: "no-store" }
      )

      let msg = {}
      try {
        msg = await data.json();
      } catch (error) {
        logger.error("Error while parsing response json", error);
      }
      logger.debug(`Fetched authorization status for [${permission}]:`, data.status, data.statusText, msg);


      let isAuthorized = false;

      if (data.status === 200) {
        isAuthorized = true;
      } else if (data.status === 401 || data.status === 403) {
        isAuthorized = false;
      } else {
        if (observer !== undefined) {
          observer.error(new Error(`Failed to check authorization: ${data.statusText} (${data.status})`));
          observer.next(false);
          observer.complete();
        }
        return false;
      }

      if (observer !== undefined) {
        observer.next(isAuthorized);
      }

      observer?.complete();
      return isAuthorized;
    } catch (error) {
      if (observer !== undefined) {
        observer.error(error);
      }
    }

    observer?.next(false);
    observer?.complete();
    return false;
  }

  public enablePeriodicRefresh(seconds: number): void {
    if (this.intervalID !== undefined) {
      this.disablePeriodicRefresh();
    }

    const time = format(addSeconds(new Date(0), seconds), 'mm:ss');

    this.m_logger.info("Enabling periodic refresh with interval of", time, "minutes");
    this.intervalID = setInterval(async () => {
      this.m_logger.trace("Refreshing auth via periodic refresh");
      let val = await firstValueFrom(this.refresh());
      if (!val) {
        this.m_logger.error("Error while refreshing auth periodically");
      }
    }, seconds * 1000);
  }

  public disablePeriodicRefresh(): void {
    if (this.intervalID !== undefined) {
      this.m_logger.info("Disabling periodic refresh");

      clearInterval(this.intervalID);
      this.intervalID = undefined;
    }
  }

  public refresh(refresh_token?: string): Observable<boolean> {
    this.m_logger.info("Refreshing Auth: Trying to fetch auth status with refresh token from auth server");

    const headers = new Headers();
    if (refresh_token !== undefined) {
      headers.append("Cookie", `${this.m_refreshTokenName}=${refresh_token}`);
      this.m_logger.trace("Refreshing Auth: Using refresh token", refresh_token);
    }

    const refreshCall = this.refreshCaller(headers, this.m_accessToken, this.m_isAuth, this.m_user);

    return from(refreshCall);
  }

  protected async refreshCaller(headers: Headers, accessTokenObserver: Subject<string>, isAuthObserver: Subject<boolean>, userObserver: Subject<User | null>): Promise<boolean> {
    let res;
    try {
      res = await fetch(`${this.authURLBase}/auth/refresh`, { credentials: "include", cache: "no-store", headers });

      this.m_logger.trace("Refreshing Auth: Fetched auth status", res);
      if (res.status === 200) {
        this.m_logger.debug("Auth status:", res.status, "Auth initialized");
        const data = await res.json() as { code: number; access_token: string; };
        accessTokenObserver.next(data.access_token);
        isAuthObserver.next(true);
        userObserver.next(AuthService.decodeUser(data.access_token));
        return true;
      }

      this.m_logger.debug("Auth status:", res.status, await res.text());

      this.resetAuth(accessTokenObserver, isAuthObserver, userObserver);
      return false;
    } catch (error) {
      this.m_logger.error("Error while initializing auth", error);
      if (res !== undefined) console.error(res);
    }
    this.resetAuth(accessTokenObserver, isAuthObserver, userObserver);
    return false;
  }

  private resetAuth(accessTokenObserver: Subject<string> = this.m_accessToken, isAuthObserver: Subject<boolean> = this.m_isAuth, userObserver: Subject<User | null> = this.m_user) {
    accessTokenObserver.next("");
    isAuthObserver.next(false);
    userObserver.next(null);
  }
}