import { cookies } from 'next/headers'
import { AuthService, User } from "./auth.service";
import { catchError, firstValueFrom, of, Subject, zip } from 'rxjs';
import { ConfigService } from './config.service';
import { isAfter } from 'date-fns';
import { applicationConfig } from '../ApplicationConfig';

export type ServerSession = {
  isAuthenticated: boolean;
  accessToken: string;
  user: User | null;
}

export class ServerAuthService extends AuthService {
  private static readonly accessTokenCookieName: string = applicationConfig.auth.cookie.accessTokenName;
  private readonly refreshTokenCookieName: string = "refresh_token";

  constructor() {
    super("server", ConfigService.config().auth.urlBase);
  }

  async getServerSession(): Promise<ServerSession> {
    const accessToken = cookies().get(ServerAuthService.accessTokenCookieName);
    const user = AuthService.decodeUser(accessToken?.value ?? "");

    if (accessToken === undefined || user === null || isAfter(new Date(), new Date(user.expiresAtUTC))) {
      this.m_logger.trace("Get Server Session: No access token found, trying to refresh auth");
      const refreshCookie = cookies().get(this.refreshTokenCookieName);

      // Not even logged in
      if (refreshCookie === undefined) {
        this.m_logger.trace("Get Server Session: No refresh token found, user is not authenticated");
        return {
          isAuthenticated: false,
          accessToken: "",
          user: null
        }
      }

      const headers = new Headers();
      headers.append("Cookie", `${this.m_refreshTokenName}=${refreshCookie.value}`);
      headers.append("X-Req-Instance", "server")
      this.m_logger.trace("Refreshing Auth: Using refresh token", refreshCookie);

      const accessTokenSubject = new Subject<string>();
      const isAuthSubject = new Subject<boolean>();
      const userSubject = new Subject<User | null>();

      const joined = zip([accessTokenSubject, isAuthSubject, userSubject]).pipe(catchError((e => {
        this.m_logger.error("Get Server Session: Error refreshing auth", e);
        return of<[string, boolean, User | null]>(["", false, null])
      })));
      const waitJoined = firstValueFrom(joined)

      const refreshSuccess = await this.refreshCaller(headers, accessTokenSubject, isAuthSubject, userSubject);
      const [accessToken, isAuthenticated, user] = await waitJoined;

      if (!refreshSuccess || !isAuthenticated) {
        this.m_logger.trace("Get Server Session: Refreshed auth, but user is not authenticated");
        return {
          isAuthenticated: false,
          accessToken: "",
          user: null
        }
      }

      if (user === null) {
        this.m_logger.trace("Get Server Session: Refreshed auth, but user is null, user is not authenticated");
        return {
          isAuthenticated: false,
          accessToken: "",
          user: null
        }
      }

      this.m_logger.debug("Get Server Session: Refreshed auth, returning session");
      this.m_logger.trace("Refreshed session", { accessToken, isAuthenticated, user });

      return {
        isAuthenticated,
        accessToken,
        user
      }
    }

    this.m_logger.debug("Get Server Session: Access token found, using it to decode user");

    // use access token
    return {
      isAuthenticated: true,
      accessToken: accessToken.value,
      user
    }

  }

}