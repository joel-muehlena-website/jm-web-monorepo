import {Logger} from "tslog";
import {LOGGER} from "./../logger/logger";
import {ConfigService} from "./../service/config.service";
import {ServerAuthService} from "./server-auth.service";

export type Services = {
    authService: ServerAuthService
    configService: ConfigService
}

export class ServiceProvider {
    private static m_instance: ServiceProvider | null = null;
    private m_services: Services;
    private readonly m_logger: Logger<unknown> = LOGGER.getSubLogger({name: "service-provider"});

    private constructor() {
        this.m_services = this.initServices();
    }

    public get services(): Services {
        return this.m_services;
    }

    public static getInstance(): ServiceProvider {
        if (ServiceProvider.m_instance === null) {
            ServiceProvider.m_instance = new ServiceProvider();
        }

        return ServiceProvider.m_instance;
    }


    private initServices(): Services {
        this.m_logger.debug("Initializing services");

        const configService = new ConfigService()

        return {
            authService: new ServerAuthService(configService),
            configService,
        };
    }
}