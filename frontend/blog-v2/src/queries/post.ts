import { applicationConfig } from "../ApplicationConfig";
import { OptionalFilterOptions, Post } from "../types/post";
import { FetchError } from "./fetchError";

const {
  gateway: { url: API_GW_URL },
  requests: { defaultRevalidateTime: REVALIDATE_DEFAULT_TIME }
} = applicationConfig.api;

const buildPostQuery = (filterOptions: OptionalFilterOptions): string => {
  const query = new URLSearchParams();
  if (filterOptions.page !== undefined) {
    query.append("page", filterOptions.page.toString());
  }
  if (filterOptions.limit !== undefined) {
    query.append("limit", filterOptions.limit.toString());
  }
  if (filterOptions.authorId !== undefined) {
    query.append("authorId", filterOptions.authorId);
  }
  if (filterOptions.tags !== undefined) {
    filterOptions.tags.forEach((tag) => query.append("tags", tag));
  }
  if (filterOptions.category !== undefined) {
    query.append("category", filterOptions.category);
  }

  return query.toString();
}

export const getPosts = async (
  filterOptions: OptionalFilterOptions
): Promise<Array<Post>> => {
  const query = buildPostQuery(filterOptions)

  try {
    const res = await fetch(`${API_GW_URL}/blog/v1/post?${query}`, {
      cache: "no-cache",
    });

    console.log(`${API_GW_URL}/blog/v1/post?${query}`)


    if (!res.ok) {
      throw new FetchError("Failed to get posts", res.status, await res.json());
    }

    const obj: {
      code: number;
      data: { posts: Array<Post>; post_cnt: number };
    } = await res.json();

    return new Promise((res) => res(obj.data.posts));
  } catch (error: unknown) {
    if (error instanceof FetchError) throw error.toNextServerSerializable();
    throw new Error(`[Catch] Failed to get posts: ${error instanceof Error ? error.message : error}`);
  }
};

export const getPost = async (id: string): Promise<Post> => {
  try {
    const res = await fetch(`${API_GW_URL}/blog/v1/post/${id}`, {
      next: { revalidate: REVALIDATE_DEFAULT_TIME },
    });

    if (!res.ok) {
      throw new FetchError("Failed to get post by id", res.status, await res.json());
    }

    const obj: {
      code: number;
      data: { post: Post };
    } = await res.json();

    return new Promise((res) => res(obj.data.post));
  } catch (error: unknown) {
    if (error instanceof FetchError) throw error.toNextServerSerializable();
    throw new Error(`[Catch] Failed to get post by id: ${error instanceof Error ? error.message : error}`);
  }
};