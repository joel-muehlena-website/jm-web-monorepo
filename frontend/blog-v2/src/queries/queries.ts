import { GenericAPIResponse } from "./types";
import { applicationConfig } from "../ApplicationConfig";
import { FetchError } from "./fetchError";

const {
  gateway: { url: API_GW_URL },
  requests: { defaultRevalidateTime: REVALIDATE_DEFAULT_TIME }
} = applicationConfig.api;

export const getCategories = async (): Promise<Array<string>> => {
  try {
    const res = await fetch(`${API_GW_URL}/blog/v1/category`, {
      next: { revalidate: 60 },
    });

    if (!res.ok) {
      throw new FetchError("Failed to get categories", res.status, await res.json()).toNextServerSerializable();

    }

    const obj: GenericAPIResponse<{ categories: Array<string> }> = await res.json();

    return obj.data.categories
  } catch (error: unknown) {
    if (error instanceof FetchError) throw error.toNextServerSerializable();
    throw new Error(`[Catch] Failed to get categories: ${error instanceof Error ? error.message : error}`);
  }
}
