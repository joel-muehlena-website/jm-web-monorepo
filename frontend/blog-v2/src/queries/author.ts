import { Author } from "../types/post";
import { FetchError } from "./fetchError";
import { applicationConfig } from "../ApplicationConfig";

const {
  gateway: { url: API_GW_URL },
  requests: { defaultRevalidateTime: REVALIDATE_DEFAULT_TIME }
} = applicationConfig.api;

export const getAuthorIdByUserId = async (userId: string): Promise<string> => {
  try {
    const res = await fetch(`${API_GW_URL}/blog/v1/author/${userId}?type=authorIdByUserId`, {
      next: { revalidate: 3600 },
    });

    if (!res.ok) {
      throw new FetchError("Failed to get author by user id", res.status, await res.json());
    }

    const obj: {
      code: number;
      data: { author: Author };
    } = await res.json();

    return obj.data.author.id;
  } catch (error: unknown) {
    if (error instanceof FetchError) throw error.toNextServerSerializable();
    throw new Error(`[Catch] Failed to get author by user id: ${error instanceof Error ? error.message : error}`);
  }
}

export const getAuthor = async (id: string): Promise<Author> => {
  try {
    const res = await fetch(`${API_GW_URL}/blog/v1/author/${id}`, {
      next: { revalidate: REVALIDATE_DEFAULT_TIME },
    });

    if (!res.ok) {
      throw new FetchError("Failed to get author by id", res.status, await res.json());
    }

    const obj: {
      code: number;
      data: { author: Author };
    } = await res.json();

    return new Promise((res) => res(obj.data.author));
  } catch (error: unknown) {
    if (error instanceof FetchError) throw error.toNextServerSerializable();
    throw new Error(`[Catch] Failed to get author by id: ${error instanceof Error ? error.message : error}`);
  }
};