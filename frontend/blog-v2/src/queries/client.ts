import { firstValueFrom } from "rxjs";
import { ServerAccessTokenSetter } from "../app/api/auth/accessToken/route";
import { applicationConfig } from "../ApplicationConfig";
import { HTTPClient, HTTPInterceptorManager, HTTPResponse } from "../http";
import { CreatePost, EditPost } from "../types/post";

const { gateway: { url: API_GW_URL } } = applicationConfig.api;

export const deletePost = async (interceptors: HTTPInterceptorManager, id: string): Promise<HTTPResponse> => {
  const client = new HTTPClient(interceptors)

  try {
    return await firstValueFrom(client.DELETE(`${API_GW_URL}/blog/v1/post/${id}`));
  } catch (error: unknown) {
    throw new Error(`Failed to delete post: ${error instanceof Error ? error.message : error}`);
  }
}

export const editPost = async (interceptors: HTTPInterceptorManager, post: EditPost): Promise<HTTPResponse> => {
  const client = new HTTPClient(interceptors)

  try {
    const headers = new Headers({
      "Content-Type": "application/json",
    })

    return await firstValueFrom(client.PATCH(`${API_GW_URL}/blog/v1/post/${post.id}`, {
      body: JSON.stringify({ post }),
      headers,
    }));
  } catch (error: unknown) {
    throw new Error(`Failed to edit post: ${Object.keys(error as any).includes("message") ? (error as Error).message : error}`);
  }
}

export const createPost = async (interceptors: HTTPInterceptorManager, post: CreatePost): Promise<HTTPResponse<{
  data: { id: string }
}>> => {
  const client = new HTTPClient(interceptors)

  try {
    const headers = new Headers({
      "Content-Type": "application/json",
    })

    return await firstValueFrom(client.POST(`${API_GW_URL}/blog/v1/post`, {
      method: "POST",
      body: JSON.stringify(post),
      headers,
    }));
  } catch (error: unknown) {
    throw new Error(`Failed to get posts: ${error instanceof Error ? error.message : error}`);
  }
}

/**
 *  Should be called from the client side.
 *  Can be used to set the access token as a cookie.
 *
 *  Saves the backend some API calls to the auth server.
 */
export const setNextAccessToken = async (accessToken: string, expires: Date, secure: boolean): Promise<[boolean, string]> => {
  if (accessToken === "") return [false, "Access token is empty"];

  try {
    const res = await fetch("/api/auth/accessToken", {
      method: "POST",
      credentials: "same-origin",
      mode: "same-origin",
      cache: "no-store",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        accessToken, cookieOpts: {
          expires: expires,
          secure,
        }
      } as ServerAccessTokenSetter)
    })

    if (!res.ok || res.status !== 200) {
      return [false, `Error setting access token: ${res.status}"`];
    } else {
      const resj = await res.json()
      return [true, resj];
    }
  } catch (error: unknown) {
    return [false, `Error setting access token: ${error instanceof Error ? error.message : error}`];
  }
}