export type GenericAPIResponse<T> = {
    code: number;
    data: T;
};

export type GenericAPIErrorResponse = {
    code: number;
    message: string;
    errors: Array<string>;
}

export type DefaultNextJSBoundaryError = {
    message: string;
    status: number;
    response: GenericAPIErrorResponse;
}

export interface NextJSBoundaryError {
    toNextServerSerializable(): Error
}