import {GenericAPIErrorResponse, NextJSBoundaryError} from "./types";

export class FetchError extends Error implements NextJSBoundaryError {
    public static FetchErrorName = "FetchError";

    constructor(message: string, private readonly _status: number, private _errorResponse: GenericAPIErrorResponse | null = null) {
        super(message);
        this.name = FetchError.FetchErrorName;
    }

    get status() {
        return this._status;
    }

    get response(): GenericAPIErrorResponse | null {
        return this._errorResponse;
    }

    set response(response: GenericAPIErrorResponse | null) {
        this._errorResponse = response;
    }

    get message() {
        return JSON.stringify(this);
    }

    toNextServerSerializable(): Error {
        return new Error(JSON.stringify({message: this.message, status: this.status, response: this.response}));
    }
}