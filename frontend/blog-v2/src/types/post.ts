export interface Post {
    id: string;
    title: string;
    authorId: string;
    authorFirstName: string;
    authorLastName: string;
    authorAvatar: string;
    description: string;
    content: string;
    category: string;
    createdAt: string;
    updatedAt: string;
    tags: Array<string>;
}

export type CreatePost = Omit<Post, "id" | "authorFirstName" | "authorLastName" | "authorAvatar" | "createdAt" | "updatedAt">;
export type EditPost = CreatePost & { id: Post["id"] };

export interface Author {
    id: string;
    userId: string;
    firstName: string;
    lastName: string;
    avatar?: string;
    alias?: string;
    biography?: string;
    website?: string;
    createdAt: string;
    updatedAt: string;
    socials: Array<{ name: string; value: string }>;
}

export type FilterOptions = {
    // Should start from 0
    page: number,
    limit: number,
    authorId: string,
    tags: Array<string>,
    category: string
}

export type OptionalFilterOptions = Partial<FilterOptions>;