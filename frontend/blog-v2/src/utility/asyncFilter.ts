import {ServerSession} from "./../service/server-auth.service";

export const asyncFilter = async <T>(arr: Array<T>, session: ServerSession, predicate: (item: T, session: ServerSession) => Promise<boolean>): Promise<Array<T>> => {
    const results = await Promise.all(arr.map(r => predicate(r, session)));

    return arr.filter((_v, index) => results[index]);
};