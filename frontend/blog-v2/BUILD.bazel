load("@aspect_rules_js//js:defs.bzl", "js_library")
load("@aspect_rules_ts//ts:defs.bzl", "ts_config")
load("@npm//:defs.bzl", "npm_link_all_packages")
load("@rules_oci//oci:defs.bzl", "oci_image", "oci_image_index", "oci_push")
load("@rules_pkg//:pkg.bzl", "pkg_tar")
load("//api:archs.bzl", "ARCHS")
load("//bzl:next.bzl", "next")
load("//tools/lint:linters.bzl", "eslint_test")

npm_link_all_packages(name = "node_modules")

# gazelle:js_tsconfig enabled

APP_PAGES_DEPS = [
    "//frontend/blog-v2/src/app",
    "//frontend/blog-v2/src/app/403",
    "//frontend/blog-v2/src/app/api/auth/accessToken",
    "//frontend/blog-v2/src/app/author/[id]",
    "//frontend/blog-v2/src/app/post/create",
    "//frontend/blog-v2/src/app/post/edit/[id]",
    "//frontend/blog-v2/src/app/post/my",
    "//frontend/blog-v2/src/app/posts/(all)",
    "//frontend/blog-v2/src/app/posts/(specific)/[id]",
]

#################################
#        JS config files        #
#################################

filegroup(
    name = "swcrc",
    srcs = [".swcrc"],
    visibility = [":__subpackages__"],
)

js_library(
    name = "tsconfig_files",
    srcs = [
        "tsconfig.json",
    ],
    visibility = ["//visibility:public"],
)

ts_config(
    name = "tsconfig",
    src = "tsconfig.json",
    visibility = [":__subpackages__"],
)

js_library(
    name = "package_json",
    srcs = ["package.json"],
    visibility = ["//visibility:public"],
)

#################################
#            Utility            #
#################################

eslint_test(
    name = "lint",
    srcs = [
    ] + APP_PAGES_DEPS,
)

#################################
#       Next Build & Run        #
#################################

next(
    name = "next",
    srcs = [
        "//frontend/blog-v2/public",
    ] + APP_PAGES_DEPS,
    data = [
        "next.config.mjs",
        "package.json",
        "postcss.config.js",
        "tailwind.config.js",
        "tsconfig.json",
        "//:node_modules/next",
        "//:node_modules/typescript",
        "//frontend/blog-v2:node_modules/@next/mdx",
        "//frontend/blog-v2:node_modules/@types/node",
        "//frontend/blog-v2:node_modules/@types/react",
        "//frontend/blog-v2:node_modules/autoprefixer",
        "//frontend/blog-v2:node_modules/postcss",
        "//frontend/blog-v2:node_modules/react",
        "//frontend/blog-v2:node_modules/react-dom",
        "//frontend/blog-v2:node_modules/rehype-autolink-headings",
        "//frontend/blog-v2:node_modules/rehype-katex",
        "//frontend/blog-v2:node_modules/rehype-slug",
        "//frontend/blog-v2:node_modules/remark-gemoji",
        "//frontend/blog-v2:node_modules/remark-gfm",
        "//frontend/blog-v2:node_modules/remark-math",
        "//frontend/blog-v2:node_modules/tailwindcss",
    ],
    next_bin = "./node_modules/next/dist/bin/next",
    next_js_binary = "//:next_js_binary",
)

#################################
#          Containers           #
#################################

pkg_tar(
    name = "dist_tar",
    srcs = [
        ":next",
    ],
    remap_paths = {
        "/dist": "/usr/share/nginx/html",
    },
    visibility = ["//visibility:private"],
)

[oci_image(
    name = "blog-frontend_image_base_linux_{}".format(arch),
    base = "@cgr_node_linux_{}".format(arch),
    tars = [
        ":dist_tar",
    ],
    visibility = ["//visibility:private"],
) for arch in ARCHS]

oci_image_index(
    name = "blog-frontend_image_multi",
    images = [
        ":blog-frontend_image_base_linux_amd64",
        ":blog-frontend_image_base_linux_arm64",
    ],
    visibility = ["//visibility:private"],
)

oci_push(
    name = "push_gitlab",
    image = ":blog-frontend_image_multi",
    remote_tags = ["latest"],
    repository = "registry.gitlab.com/joel-muehlena-website/jm-web-monorepo/frontend/blog",
)
