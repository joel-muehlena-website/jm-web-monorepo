/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        "jm-roboto": "var(--font-roboto)",
        "jm-montserrat": "var(--font-montserrat)",
      },
      margin: {
        "center": "0 auto",
      },
      boxShadow: {
        "3xl": "0 35px 60px -15px rgba(0, 0, 0, 0.3)",
      },
      backgroundImage: {},
      gridTemplateColumns: {
        'post': 'minmax(0, 0.4fr) minmax(0,3fr) minmax(0,1fr)',
      },
      height: {
        "footer": "4rem",
        "page": "calc(100vh - 4rem)",
      },
      minHeight: {
        "page": "calc(100vh - 4rem)",
      },
    },
  },
  plugins: [],
};
