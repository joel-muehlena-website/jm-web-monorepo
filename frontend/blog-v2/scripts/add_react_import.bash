#!/bin/bash

set -e -o pipefail

baseDir="$(
    cd -- "$(dirname "$0")" >/dev/null 2>&1
    pwd -P
)"

DIRECTORY="$baseDir/../src"

IMPORT_STATEMENT='// @ts-expect-error - Otherwise, react will not be bundled resulting in "React is not defined" error\nimport React from "react";'

find "$DIRECTORY" -type f -name "*.tsx" | while read -r file; do
    if ! grep -qE "^import .*\b(React)\b.* from ['\"]react['\"];?$" "$file"; then
        first_line=$(head -n 1 "$file")
        if [[ "$first_line" =~ "use client" ]] || [[ "$first_line" =~ "use server" ]]; then
            sed -i "2a\ $IMPORT_STATEMENT" "$file"
        else
            sed -i "1s|^|$IMPORT_STATEMENT\n|" "$file"
        fi
    fi
done
