#!/bin/bash

# set -e

baseDir="$(
    cd -- "$(dirname "$0")" >/dev/null 2>&1
    pwd -P
)"

buildozer \
  'set allow_js True' \
  'set declaration True' \
  'set declaration_map True' \
  'set incremental True' \
  'set preserve_jsx True' \
  'set resolve_json_module True' \
  'set source_map True' \
  //frontend/blog-v2/src/...:%ts_project

buildozer \
  'set visibility //frontend/blog-v2:__subpackages__' \
  //frontend/blog-v2/src/...:%ts_project

buildozer \
  'new_load @bazel_skylib//lib:partial.bzl partial' \
  'new_load @aspect_rules_swc//swc:defs.bzl swc' \
  //frontend/blog-v2/src/...:__pkg__

buildozer 'set transpiler partial.make(swc,source_maps="true",swcrc="//frontend/blog-v2:swcrc")' //frontend/blog-v2/src/...:%ts_project

find $baseDir/../src -name BUILD.bazel -exec buildifier -mode=fix {} \;