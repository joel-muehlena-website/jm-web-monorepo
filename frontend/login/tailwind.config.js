/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#ff8709',
        'secondary': '#ffd603',
        'dark': '#2c3e50',
        'submit': '#27ae60'
      },
    },
  },
  plugins: [],
}

