import express from "express";
import cors from "cors";

const PORT = 8080;

const app = express();

app.use(cors())

app.post("/auth/authorize", (request, response) => {
  response.statusCode = 401;
  response.json({});
});

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});