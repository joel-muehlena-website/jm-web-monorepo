// @ts-expect-error - Otherwise, vite will not bundle react for this file resulting in "React is not defined" error
import React from "react"
import { LoginForm } from "./LoginForm"


export const App = () => {
  return (
    <div className="bg-gradient-to-br from-secondary to-primary h-[100dvh] w-[100dvw] grid place-items-center">
      <LoginForm />
    </div>
  )
}
