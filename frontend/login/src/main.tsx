import React from 'react'
import ReactDOM from 'react-dom/client'
import { App } from './App'
import './index.sass'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'

if (import.meta.env.VITE_AUTH_SERVER_URL === undefined) {
  throw new Error('VITE_AUTH_SERVER_URL is not defined')
} else {
  if (import.meta.env.MODE !== "production") {
    console.debug('VITE_AUTH_SERVER_URL:', import.meta.env.VITE_AUTH_SERVER_URL)
  }
}

const queryClient = new QueryClient()

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <App />
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  </React.StrictMode>,
)