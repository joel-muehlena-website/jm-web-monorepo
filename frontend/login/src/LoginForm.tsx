import { useState } from "react";
import { useMutation } from "@tanstack/react-query";
import { useForm } from "react-hook-form";

type Inputs = {
  email: string;
  password: string;
};

const authServerUrl = import.meta.env.VITE_AUTH_SERVER_URL;
const authorizationEndpoint = "auth/authorize?provider=email&with_refresh=true";

// eslint-disable-next-line no-control-regex
const mailRegex =
  /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/gm;

export const LoginForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();

  const [stateError, setStateError] = useState<string>("");

  const params = new URLSearchParams(window.location.search);

  const mutation = useMutation({
    mutationFn: (login: Inputs) => {
      return fetch(`${authServerUrl}/${authorizationEndpoint}`, {
        method: "POST",
        body: JSON.stringify(login),
        headers: { "Content-Type": "application/json" },
      });
    },
    onMutate: () => {
      setStateError("");
    },
    onSuccess: async (response) => {
      let res: {message: string, refresh_token?: string} = { message: "" };

      try {
        res = await response.json();
      } catch (e) {
        console.error("Failed to parse response", e);
      }

      switch (response.status) {
        case 200:
          if (params.has("setback_url")) {
            try {
              const setbackResult = await fetch(params.get("setback_url")!, {
                method: "POST",
                body: JSON.stringify({
                  refreshToken: res.refresh_token
                }),
                headers: {
                  "Content-Type": "application/json"
                }
              });

              console.log("SB RES", setbackResult);
            }catch(err) {
              console.error("Failed to call setback url", err);
            }
          }

          if (params.has("redirect_url")) {
            window.location.href = params.get("redirect_url")!;
          } else {
            setStateError("No redirect url specified in redirect_url");
          }

          break;
        case 401 || 403:
          setStateError("Invalid email or password");
          break;
        default:
          setStateError(res.message);
          break;
      }
    },
  });

  // TODO: Add error if response code is != 200 like 401

  return (
    <div className="bg-white shadow-2xl w-[25rem] rounded-lg py-14 px-14">
      <div className="text-red-700 text-center w-full">
        {mutation.isError ? `An error occurred: ${mutation.error.message}` : ""}
        {stateError !== "" ? `An error occurred: ${stateError}` : ""}
        &nbsp;
      </div>

      <div className="grid place-content-center">
        <h1 className="text-center text-5xl font-bold inline-block relative z-0 after:content-[''] after:absolute after:bottom-0 after:rounded-sm after:z-10 after:block after:w-full after:h-[0.2rem] after:bg-gradient-to-tl after:from-secondary after:to-primary">
          Login
        </h1>
      </div>
      <form
        onSubmit={handleSubmit((data: Inputs) => mutation.mutate(data))}
        className="flex flex-col gap-6 mt-4"
      >
        <div className="flex flex-col gap-2">
          <label htmlFor="email" className="text-2xl">
            Email
          </label>
          <input
            type="text"
            {...register("email", {
              required: "Email is required",
              pattern: {
                value: mailRegex,
                message: "Entered value does not match email format",
              },
            })}
            className="bg-dark rounded-sm text-lg text-white px-2 py-2"
          />
          {errors.email && (
            <small className="text-red-700 text-sm">
              {errors.email.message}
            </small>
          )}
        </div>
        <div className="flex flex-col gap-2">
          <label htmlFor="password" className="text-2xl">
            Password
          </label>
          <input
            type="password"
            {...register("password", { required: "Password is required" })}
            className="bg-dark rounded-sm text-lg text-white px-2 py-2"
          />
          {errors.password && (
            <small className="text-red-700 text-sm">
              {errors.password.message}
            </small>
          )}
        </div>

        <div>
          <button
            className={`text-xl text-white bg-submit px-4 py-2 rounded-sm ${
              mutation.isPending ? "opacity-75" : ""
            }`}
            disabled={mutation.isPending}
            type="submit"
          >
            {mutation.isPending ? <>Loading</> : "Login"}
          </button>
        </div>
      </form>
    </div>
  );
};
