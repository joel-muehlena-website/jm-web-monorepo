export const SITE = {
  title: "JM-Web Documentation",
  description:
    "Documentation and stuff for the jm-web monorepo and therefore joel.muehlena.de website",
  defaultLanguage: "en-us",
} as const;

export const OPEN_GRAPH = {
  image: {
    src: "",
    alt: "",
  },
  twitter: "joel_muehlena",
};

export const KNOWN_LANGUAGES = {
  English: "en",
} as const;
export const KNOWN_LANGUAGE_CODES = Object.values(KNOWN_LANGUAGES);

export const GITHUB_EDIT_URL = `https://gitlab.com/joel-muehlena-website/jm-web-monorepo/-/tree/main/docs`;

// See "Algolia" section of the README for more information.
export const ALGOLIA = {
  indexName: "XXXXXXXXXX",
  appId: "XXXXXXXXXX",
  apiKey: "XXXXXXXXXX",
};

export type Sidebar = Record<
  (typeof KNOWN_LANGUAGE_CODES)[number],
  Record<string, { text: string; link: string }[]>
>;
export const SIDEBAR: Sidebar = {
  en: {
    "Getting started": [
      { text: "Introduction", link: "en/getting-started/introduction" },
      { text: "Readme", link: "en/getting-started/readme" },
    ],
    "API": [
      {text: "Introduction", link: "en/api/introduction"},
      {text: "Swagger",link: "en/api/swagger"}
    ]
  },
};
