package apigateway

import (
	"net/http"
	"sync"
)

type handlerSwapper struct {
	lock    sync.Mutex
	handler http.Handler
}

func (swapper *handlerSwapper) setHandler(h http.Handler) {
	swapper.lock.Lock()
	swapper.handler = h
	swapper.lock.Unlock()
}

func (swapper *handlerSwapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	swapper.lock.Lock()
	h := swapper.handler
	swapper.lock.Unlock()
	h.ServeHTTP(w, r)
}
