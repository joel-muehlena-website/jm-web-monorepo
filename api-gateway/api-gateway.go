package apigateway

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gopkg.in/yaml.v3"

	"github.com/riandyrn/otelchi"

	"github.com/go-chi/cors"
	"go.uber.org/zap"

	netpprof "net/http/pprof"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/log"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/metrics"
	metrics_middleware "gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/metrics/middleware"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/probes"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/telemetry"

	_ "go.opentelemetry.io/contrib"
)

type ApiGateway struct {
	endpoints     map[string]EndpointsConfig
	endpointsLock sync.RWMutex
	router        *chi.Mux
	swapper       *handlerSwapper
	logger        *zap.Logger
	middleware    *MiddlewareRegistry

	port     uint
	bindAddr string

	metrics          *metrics.Metrics
	probes           *probes.Probes
	monitoringServer *http.Server

	telemetry *telemetry.Telemetry

	chiPrometheusMiddleware        func(next http.Handler) http.Handler
	chiPrometheusMiddlewarePattern func(next http.Handler) http.Handler

	enableInternalApiLock sync.RWMutex
	enableInternalApi     bool

	corsLock sync.RWMutex
	cors     CorsConfig
}

type RouteHandler interface {
	http.Handler
	Handle(w http.ResponseWriter, r *http.Request) error
}

type GenericRouteHandler struct {
	logger  *zap.Logger
	version *VersionConfig
	path    string
	prefix  string
}

func New(config *Config, configServerUrl string) *ApiGateway {
	// TODO: Zap logger otel integration (ctx & trace id)

	logger, err := log.New("api-gateway", config.Log, func(placeholder string) string { return "" }, nil)
	if err != nil {
		panic(err)
	}

	return NewWithLogger(logger, config, configServerUrl)
}

func NewWithLogger(logger *zap.Logger, config *Config, configServerUrl string) *ApiGateway {
	if logger == nil {
		panic("logger must not be nil")
	}

	apiGateway := new(ApiGateway)
	apiGateway.router = nil
	apiGateway.swapper = &handlerSwapper{}

	apiGateway.logger = logger

	apiGateway.logger.Debug("Config", zap.Any("config", config))

	apiGateway.bindAddr = config.Server.BindAddr
	apiGateway.port = config.Server.Port

	apiGateway.telemetry = telemetry.New(config.Monitoring.Telemetry, logger)
	err := apiGateway.telemetry.Init()
	if err != nil {
		logger.Fatal("Failed to initialize telemetry", zap.Error(err))
	}

	monitoringLogger := apiGateway.logger.Named("monitoring")

	apiGateway.setupMonitoring(monitoringLogger, config.Monitoring)

	err = apiGateway.probes.Register("endpoints_loaded", true, true, false, false, nil, nil)
	if err != nil {
		apiGateway.logger.Fatal("Failed to register probe", zap.Error(err))
	}

	apiGateway.chiPrometheusMiddlewarePattern = metrics_middleware.NewPatternMiddleware("api-gateway")
	apiGateway.chiPrometheusMiddleware = metrics_middleware.NewMiddleware("api-gateway")

	apiGateway.logger.Info("Starting API-Gateway")
	apiGateway.logger.Info("Copyright Joel Mühlena 2023")
	apiGateway.middleware = NewMiddlewareRegistry(apiGateway.logger)
	apiGateway.enableInternalApi = true

	apiGateway.cors = config.Cors

	if apiGateway.enableInternalApi {
		apiGateway.router = chi.NewRouter()
		apiGateway.router.Use(middleware.StripSlashes)
		apiGateway.setupInternalApi()
	}

	apiGateway.endpoints = make(map[string]EndpointsConfig, 0)

	endpoints := make([]EndpointsConfig, 0)
	for _, endpoint := range config.Endpoints {
		if endpoint.EndpointConfigLocationConfigServer.FilePath != "" {
			if endpoint.EndpointConfigLocationConfigServer.ConfigServerUrl == "" {
				endpoint.EndpointConfigLocationConfigServer.ConfigServerUrl = configServerUrl
			}

			endpoint, err := LocateEndpointFromConfigServer(endpoint.EndpointConfigLocationConfigServer)
			if err != nil {
				apiGateway.logger.Error("Failed to load endpoint config", zap.Error(err))
				panic(err)
			}

			endpoints = append(endpoints, endpoint)
		} else if endpoint.EndpointConfigLocationLocalFile.FilePath != "" {
			fileContent, err := os.ReadFile(endpoint.EndpointConfigLocationLocalFile.FilePath)
			if err != nil {
				apiGateway.logger.Error("Failed to read endpoint config file", zap.Error(err))
				panic(err)
			}

			endpoint := EndpointsConfig{}
			err = yaml.Unmarshal(fileContent, &endpoint)
			if err != nil {
				apiGateway.logger.Error("Failed to unmarshal endpoint config file", zap.Error(err))
				panic(err)
			}

			endpoints = append(endpoints, endpoint)
		}
	}

	err = apiGateway.AppendEndpoints(endpoints...)
	if err != nil {
		apiGateway.logger.Error("Failed to append endpoint configs", zap.Error(err))
		panic(err)
	}

	apiGateway.probes.SetStartup(true)

	return apiGateway
}

func (gw *ApiGateway) setupMonitoring(logger *zap.Logger, config MonitoringConfig) {
	if config.Metrics.Endpoint == "" {
		config.Metrics.Endpoint = "/metrics"
	}

	if config.Probes.EndpointPrefix == "" {
		config.Probes.EndpointPrefix = "/"
	}

	if config.Metrics.Endpoint == config.Probes.EndpointPrefix {
		logger.Warn("Detected overlapping prefixes for probes & metrics. Prefixing probes with /probes", zap.String("prefix", config.Probes.EndpointPrefix))
		config.Probes.EndpointPrefix = "/probes" + config.Probes.EndpointPrefix
	}

	gw.metrics = metrics.New(logger, config.Metrics)
	gw.probes = probes.New(logger, config.Probes)
	gw.probes.SetStartup(false)

	mux := http.NewServeMux()

	mux.Handle(config.Probes.EndpointPrefix, gw.probes.Handler())
	mux.Handle(config.Metrics.Endpoint, gw.metrics.Handler())

	if config.EnableProfiling {
		mux.HandleFunc("/debug/pprof/", netpprof.Index)
		mux.HandleFunc("/debug/pprof/cmdline", netpprof.Cmdline)
		mux.HandleFunc("/debug/pprof/profile", netpprof.Profile)
		mux.HandleFunc("/debug/pprof/symbol", netpprof.Symbol)
		mux.HandleFunc("/debug/pprof/trace", netpprof.Trace)
	}

	gw.monitoringServer = &http.Server{
		Addr:    fmt.Sprintf("%s:%d", config.Metrics.BindAddr, config.Metrics.Port),
		Handler: mux,
	}

	go func() {
		logger.Info("Starting server",
			zap.Uint16("port", config.Metrics.Port),
			zap.String("bindAddr", config.Metrics.BindAddr),
			zap.String("metrics_prefix", config.Metrics.Endpoint),
			zap.String("probes_prefix", config.Probes.EndpointPrefix))

		err := gw.monitoringServer.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.Fatal("Server listen failed", zap.Error(err))
		} else if err != nil && errors.Is(err, http.ErrServerClosed) {
			logger.Info("Http server closed gracefully")
		}
	}()
}

func (gw *ApiGateway) AppendEndpoints(endpoints ...EndpointsConfig) error {
	errors := make(map[string]error, 0)

	gw.endpointsLock.Lock()
	for _, endpoint := range endpoints {
		if err := endpoint.Validate(); err != nil {
			gw.logger.Error("invalid endpoint appended", zap.Error(err), zap.Any("endpoint", endpoint))
			errors[endpoint.Name] = err
		}

		if _, ok := gw.endpoints[endpoint.Name]; ok {
			gw.logger.Error("endpoint config with this name already appended", zap.Any("endpoint", endpoint))
			errors[endpoint.Name] = fmt.Errorf("endpoint config with this name already appended")
		}

		gw.endpoints[endpoint.Name] = endpoint
	}
	gw.endpointsLock.Unlock()

	gw.reloadEndpoints()

	return nil
}

func (gw *ApiGateway) setupInternalApi() {
	gw.router.Route("/internal/api/v1", func(r chi.Router) {
		r.Post("/config", func(w http.ResponseWriter, r *http.Request) {
			// TODO post a new config
			// TODO use raft to distribute to all other gateways?
			// TODO use gossip (hc/serf) for member control
			// TODO configure via gateway api in k8s?
		})
	})
}

func (gw *ApiGateway) Probes() *probes.Probes {
	return gw.probes
}

func transformCorsConfig(cfg CorsConfig) cors.Options {
	return cors.Options{
		AllowedOrigins:   cfg.AllowedOrigins,
		AllowedMethods:   cfg.AllowedMethods,
		AllowedHeaders:   cfg.AllowedHeaders,
		ExposedHeaders:   cfg.ExposedHeaders,
		AllowCredentials: cfg.AllowCredentials,
		MaxAge:           cfg.MaxAge,
	}
}

func (gw *ApiGateway) reloadEndpoints() {
	gw.probes.SetLiveAndReady("endpoints_loaded", false)
	gw.logger.Info("Reloading endpoints")
	gw.endpointsLock.RLock()
	defer gw.endpointsLock.RUnlock()
	gw.router = chi.NewRouter()
	gw.router.Use(middleware.StripSlashes)
	gw.router.Use(log.ChiZapLogger(gw.logger.Named("chi")))

	gw.router.Use(gw.chiPrometheusMiddlewarePattern)
	gw.router.Use(otelchi.Middleware("api-gateway", otelchi.WithChiRoutes(gw.router), otelchi.WithFilter(func(r *http.Request) bool {
		// FIXME: Remove after making provider configurable and dynamic
		return !strings.HasPrefix(r.URL.Path, "/auth/meta")
	})))

	gw.corsLock.RLock()
	if gw.cors.Enabled {
		gw.logger.Info("Enabling CORS", zap.Strings("origins", gw.cors.AllowedOrigins), zap.Strings("methods", gw.cors.AllowedMethods), zap.Strings("headers", gw.cors.AllowedHeaders))
		gw.router.Use(cors.Handler(transformCorsConfig(gw.cors)))
	}
	gw.corsLock.RUnlock()

	gw.enableInternalApiLock.RLock()
	isInternalApiEnabled := gw.enableInternalApi
	gw.enableInternalApiLock.RUnlock()

	if isInternalApiEnabled {
		gw.setupInternalApi()
	}

	prefixes := make(map[string]map[string]chi.Router, 1)
	prefixes[""] = make(map[string]chi.Router, 1)
	prefixes[""]["v1"] = gw.router.Route("/v1", func(r chi.Router) {})

	for _, endpointCfg := range gw.endpoints {

		var prefix map[string]chi.Router
		var router chi.Router
		var ok bool

		// If prefix is not yet registered, register
		if prefix, ok = prefixes[endpointCfg.Prefix]; !ok {
			prefixes[endpointCfg.Prefix] = make(map[string]chi.Router, 0)
			prefix = prefixes[endpointCfg.Prefix]
		}

		for _, endpoint := range endpointCfg.Endpoints {
			for _, h := range endpoint.HandlerConfig {

				// Set default version if nothing is set
				if h.Versions == nil || len(h.Versions) == 0 {
					h.Versions = []VersionConfig{{Path: "v1", PreHandlers: []string{}, PostHandlers: []string{}}}
				}

				for _, version := range h.Versions {
					// If version is not yet registered inside prefix, register
					if router, ok = prefix[version.Path]; !ok {
						prefix[version.Path] = gw.router.Route(fmt.Sprintf("/%s/%s", endpointCfg.Prefix, version.Path), func(r chi.Router) {})
						router = prefix[version.Path]
					}

					// Set default service
					if version.Upstream.Service == "" {
						version.Upstream = endpoint.Upstream
					}

					// Set default path behavior
					if version.PathBehavior == nil {
						version.PathBehavior = new(PathBehavior)
						*version.PathBehavior = endpoint.PathBehavior
					}

					// Create real handlers for different types here
					if strings.ToUpper(endpoint.Type) == EndpointTypeWS {
						gw.logger.Debug("Registering websocket endpoint", zap.String("path", endpoint.Path))
						router.Handle(endpoint.Path, NewWSHandler(gw.logger, &version))
					} else {
						gw.logger.Debug("Registering http endpoint", zap.String("path", endpoint.Path))
						router.Method(strings.ToUpper(h.Method), endpoint.Path, NewHTTPHandler(gw.logger, &version, endpointCfg.Prefix, true, gw.middleware))
					}
				}
			}
		}

		router.NotFound(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(404)
			w.Write([]byte("route does not exist"))
		})

		router.MethodNotAllowed(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(405)
			w.Write([]byte("method is not valid"))
		})
	}

	// Print routes if debug logging is enabled
	if gw.logger.Level() == zap.DebugLevel {
		loggerSuggared := gw.logger.Sugar()
		chi.Walk(gw.router, func(method string, route string, handler http.Handler, middlewares ...func(http.Handler) http.Handler) error {
			loggerSuggared.Debugf("[%s]: '%s' has %d middlewares", method, route, len(middlewares))
			return nil
		})
	}

	gw.middleware.Reload()

	gw.swapper.setHandler(gw.router)

	gw.probes.SetLiveAndReady("endpoints_loaded", true)
}

func (gw *ApiGateway) MiddlewareRegistry() *MiddlewareRegistry {
	return gw.middleware
}

func (gw *ApiGateway) EnableInternalApi() {
	gw.enableInternalApiLock.Lock()
	gw.enableInternalApi = true
	gw.enableInternalApiLock.Unlock()
	gw.reloadEndpoints()
}

func (gw *ApiGateway) DisableInternalApi() {
	gw.enableInternalApiLock.Lock()
	gw.enableInternalApi = false
	gw.enableInternalApiLock.Unlock()
	gw.reloadEndpoints()
}

func (gw *ApiGateway) Cors(cfg CorsConfig) {
	gw.corsLock.Lock()
	gw.cors = cfg
	gw.corsLock.Unlock()
	gw.reloadEndpoints()
}

func (gw *ApiGateway) ListenAndServe() {
	go func() {
		gw.logger.Info("Starting server", zap.Uint("port", gw.port), zap.String("bindAddr", gw.bindAddr))
		if err := http.ListenAndServe(fmt.Sprintf("%s:%d", gw.bindAddr, gw.port), gw.swapper); err != nil {
			gw.logger.Fatal("Server listen failed", zap.Error(err))
		}
	}()

	defer gw.logger.Sync()

	// Wait for CTRL+C, Kill or TERM (Docker) to quit
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGKILL, syscall.SIGTERM)
	sig := <-c

	gw.logger.Info("Shutting down api-gateway", zap.String("signal", sig.String()))

	if gw.telemetry != nil {
		gw.telemetry.Shutdown(context.Background())
	}
}
