package apigateway

import (
	"fmt"
	"io"
	"net/http"
	"net/url"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/propagation"
	"go.uber.org/zap"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/log"
)

type HTTPHandler struct {
	GenericRouteHandler
	followRedirects    bool
	middlewareRegistry *MiddlewareRegistry

	linkLogger *log.ZapTraceLinkLogger
}

const (
	HTTPContentType string = "Content-Type"
)

func NewHTTPHandler(logger *zap.Logger, version *VersionConfig, prefix string, followRedirects bool, middleware *MiddlewareRegistry) *HTTPHandler {
	handler := new(HTTPHandler)
	handler.logger = logger
	handler.version = version
	handler.prefix = prefix
	handler.followRedirects = followRedirects
	handler.middlewareRegistry = middleware

	handler.linkLogger = log.NewZapTraceLinkLogger(handler.logger)

	return handler
}

func (h *HTTPHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := h.Handle(w, r)
	if err != nil {
		h.linkLogger.Ctx(r.Context()).Error("HTTP proxying failed", zap.Error(err))
	}
}

func (h *HTTPHandler) Handle(w http.ResponseWriter, r *http.Request) error {
	t := otel.Tracer("api-gateway")
	ctx, span := t.Start(r.Context(), "http-handler")
	defer span.End()

	logger := h.linkLogger.Ctx(ctx)
	logger.Debug("Http handler called")

	w, r, mwErr := h.runMiddleware(logger, w, r, h.version.PreHandlers)
	if mwErr != nil {
		w.WriteHeader(int(mwErr.HTTPErrorCode))
		w.Write([]byte(mwErr.Error()))
		return fmt.Errorf("aborted due to middleware error: %w", mwErr)
	}

	path := h.version.PathBehavior.cleanPath(r.URL.Path, h.version.Path, h.prefix)

	u, err := url.JoinPath(h.version.Upstream.Service, path)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("failed to join url path: %v", err)))
		return fmt.Errorf("failed to join url path: %w", err)
	}

	req, err := http.NewRequestWithContext(ctx, r.Method, u, r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("failed to create http request: %v", err)))
		return fmt.Errorf("failed to create http request: %w", err)
	}

	// TODO client opts
	client := http.Client{
		Transport: otelhttp.NewTransport(http.DefaultTransport),
	}

	if !h.followRedirects {
		client.CheckRedirect = func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}
	}

	req.URL.RawQuery = r.URL.RawQuery

	// TODO define rules for headers
	for k, v := range r.Header {
		req.Header[k] = v
	}

	logger.Debug("Calling upstream service", zap.String("url", req.URL.String()))
	res, err := client.Do(req)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf("failed to execute http request: %v", err)))
		return fmt.Errorf("failed to execute http request: %w", err)
	}

	logger.Debug("Upstream service responded without error", zap.Int("code", res.StatusCode))

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("failed to read http body: %w", err)
	}

	// TODO copy response headers
	w.Header().Add(HTTPContentType, res.Header.Get(HTTPContentType))

	// Add trace header to response headers, so that the full trace can be followed
	otel.GetTextMapPropagator().Inject(ctx, propagation.HeaderCarrier(w.Header()))

	w.WriteHeader(res.StatusCode)

	_, err = w.Write(body)
	if err != nil {
		return fmt.Errorf("failed to write received http body to initial request: %w", err)
	}

	h.runMiddleware(logger, w, r, h.version.PostHandlers)

	return nil
}

func (h *HTTPHandler) runMiddleware(logger *zap.Logger, w http.ResponseWriter, r *http.Request, middlewareNames []string) (http.ResponseWriter, *http.Request, *MiddlewareError) {
	var err *MiddlewareError
	for _, middlewareName := range middlewareNames {
		middleware := h.middlewareRegistry.Get(MiddlewareName(middlewareName))

		if middleware == nil {
			logger.Error("Middleware not found", zap.String("middlewareName", middlewareName))
			continue
		}

		data, ok := h.version.Data[middlewareName]

		if !ok {
			data = nil
		}

		logger.Debug("Running middleware", zap.String("middlewareName", middlewareName))
		w, r, err = middleware.Handle(w, r, data)
		if err != nil {
			return w, r, err
		}
	}

	return w, r, nil
}
