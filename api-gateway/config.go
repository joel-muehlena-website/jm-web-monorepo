package apigateway

import (
	"net/http"
	"strings"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/log"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/metrics"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/probes"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/telemetry"
)

type CorsConfig struct {
	// Copied options from https://github.com/go-chi/cors/blob/9aca6170f98f10a194574513b925dfa26664d520/cors.go#L30C6-L30C14
	// because this middleware will be used in the api-gateway to handle cors

	Enabled bool `yaml:"enabled" toml:"enabled" json:"enabled"`
	// AllowedOrigins is a list of origins a cross-domain request can be executed from.
	// If the special "*" value is present in the list, all origins will be allowed.
	// An origin may contain a wildcard (*) to replace 0 or more characters
	// (i.e.: http://*.domain.com). Usage of wildcards implies a small performance penalty.
	// Only one wildcard can be used per origin.
	// Default value is ["*"]
	AllowedOrigins []string `yaml:"allowedOrigins" toml:"allowedOrigins" json:"allowedOrigins"`

	// AllowOriginFunc is a custom function to validate the origin. It takes the origin
	// as argument and returns true if allowed or false otherwise. If this option is
	// set, the content of AllowedOrigins is ignored.
	AllowOriginFunc func(r *http.Request, origin string) bool `yaml:"-" toml:"-" json:"-"`

	// AllowedMethods is a list of methods the client is allowed to use with
	// cross-domain requests. Default value is simple methods (HEAD, GET and POST).
	AllowedMethods []string `yaml:"allowedMethods" toml:"allowedMethods" json:"allowedMethods"`

	// AllowedHeaders is list of non simple headers the client is allowed to use with
	// cross-domain requests.
	// If the special "*" value is present in the list, all headers will be allowed.
	// Default value is [] but "Origin" is always appended to the list.
	AllowedHeaders []string `yaml:"allowedHeaders" toml:"allowedHeaders" json:"allowedHeaders"`

	// ExposedHeaders indicates which headers are safe to expose to the API of a CORS
	// API specification
	ExposedHeaders []string `yaml:"exposedHeaders" toml:"exposedHeaders" json:"exposedHeaders"`

	// AllowCredentials indicates whether the request can include user credentials like
	// cookies, HTTP authentication or client side SSL certificates.
	AllowCredentials bool `yaml:"allowCredentials" toml:"allowCredentials" json:"allowCredentials"`

	// MaxAge indicates how long (in seconds) the results of a preflight request
	// can be cached
	MaxAge int `yaml:"maxAge" toml:"maxAge" json:"maxAge"`

	// OptionsPassthrough instructs preflight to let other potential next handlers to
	// process the OPTIONS method. Turn this on if your application handles OPTIONS.
	OptionsPassthrough bool `yaml:"optionsPassthrough" toml:"optionsPassthrough" json:"optionsPassthrough"`
}

type Config struct {
	Server     ServerConfig             `yaml:"server" toml:"server" json:"server"`
	Api        ApiConfig                `yaml:"api" toml:"api" json:"api"`
	Cors       CorsConfig               `yaml:"cors" toml:"cors" json:"cors"`
	Monitoring MonitoringConfig         `yaml:"monitoring" toml:"monitoring" json:"monitoring"`
	Log        log.Config               `yaml:"log" toml:"log" json:"log"`
	Endpoints  []EndpointConfigLocation `yaml:"endpoints" toml:"endpoints" json:"endpoints"`
}

type ServerConfig struct {
	BindAddr string `yaml:"bindAddr" toml:"bindAddr" json:"bindAddr"`
	Port     uint   `yaml:"port" toml:"port" json:"port"`
}

type MonitoringConfig struct {
	EnableProfiling bool             `yaml:"enableProfiling" toml:"enableProfiling" json:"enableProfiling"`
	Metrics         metrics.Config   `yaml:"metrics" toml:"metrics" json:"metrics"`
	Probes          probes.Config    `yaml:"probes" toml:"probes" json:"probes"`
	Telemetry       telemetry.Config `yaml:"telemetry" toml:"telemetry" json:"telemetry"`
}

type ApiConfig struct {
	Enabled bool   `yaml:"enabled" toml:"enabled" json:"enabled"`
	Prefix  string `yaml:"prefix" toml:"prefix" json:"prefix"`
}

type EndpointConfigLocation struct {
	EndpointConfigLocationConfigServer EndpointConfigLocationConfigServer `yaml:"configServer" toml:"configServer" json:"configServer"`
	EndpointConfigLocationLocalFile    EndpointConfigLocationLocalFile    `yaml:"localFile" toml:"localFile" json:"localFile"`
}

type EndpointConfigLocationConfigServer struct {
	FilePath        string `yaml:"filePath" toml:"filePath" json:"filePath"`
	ConfigServerUrl string `yaml:"configServerUrl,omitempty" toml:"configServerUrl,omitempty" json:"configServerUrl,omitempty"`
}

type EndpointConfigLocationLocalFile struct {
	FilePath string `yaml:"filePath" toml:"filePath" json:"filePath"`
}

const (
	EndpointTypeWS   = "WS"
	EndpointTypeHTTP = "HTTP"
)

type EndpointsConfig struct {
	validated bool `yaml:"-" toml:"-" json:"-"`

	Name   string `yaml:"name" toml:"name" json:"name"`
	Prefix string `yaml:"prefix" toml:"prefix" json:"prefix"`
	// Endpoints which this gateway should handle
	Endpoints []EndpointConfig `yaml:"endpoints" toml:"endpoints" json:"endpoints"`
}

func (cfg *EndpointsConfig) Validate() error {
	cfg.validated = true

	return nil
}

func (cfg *EndpointsConfig) Validated() bool {
	return cfg.validated
}

type EndpointConfig struct {
	Path    string `yaml:"path" toml:"path" json:"path"`
	Enabled *bool  `yaml:"enabled" toml:"enabled" json:"enabled"`

	// This is ommited if type is ws
	HandlerConfig []HandlerConfig `yaml:"handlers" toml:"handlers" json:"handlers"`
	Upstream      UpstreamConfig  `yaml:"upstream" toml:"upstream" json:"upstream"`
	PathBehavior  PathBehavior    `yaml:"pathBehavior" toml:"pathBehavior" json:"pathBehavior"`
	Limit         LimitConfig     `yaml:"limit" toml:"limit" json:"limit"`

	// Supported types are
	// - http
	// - ws
	//
	// http:
	//
	// ws:
	// Does not support methods. Traffic will be forwarded to the given service and path.
	Type string `yaml:"type" toml:"type" json:"type"`
}

type HandlerConfig struct {
	Method   string          `yaml:"method" toml:"method" json:"method"`
	Versions []VersionConfig `yaml:"versions" toml:"versions" json:"versions"`
}

type VersionConfig struct {
	Path         string                    `yaml:"path" toml:"path" json:"path"`
	Upstream     UpstreamConfig            `yaml:"upstream" toml:"upstream" json:"upstream"`
	PathBehavior *PathBehavior             `yaml:"pathBehavior,omitempty" toml:"pathBehavior,omitempty" json:"pathBehavior,omitempty"`
	Limit        *LimitConfig              `yaml:"limit,omitempty" toml:"limit,omitempty" json:"limit,omitempty"`
	PreHandlers  []string                  `yaml:"preHandlers" toml:"preHandlers" json:"preHandlers"`
	PostHandlers []string                  `yaml:"postHandlers" toml:"postHandlers" json:"postHandlers"`
	Data         map[string]MiddlewareData `yaml:"data" toml:"data" json:"data"`
}

type UpstreamConfig struct {
	Service string `yaml:"service" toml:"service" json:"service"`
}

type PathBehavior struct {
	StripPrefix      bool `yaml:"stripPrefix" toml:"stripPrefix" json:"stripPrefix"`
	StripVersion     bool `yaml:"stripVersion" toml:"stripVersion" json:"stripVersion"`
	AddTrailingSlash bool `yaml:"addTrailingSlash" toml:"addTrailingSlash" json:"addTrailingSlash"`
}

func (ph *PathBehavior) cleanPath(path string, version string, prefix string) string {
	if ph.StripPrefix && prefix != "" {
		path = strings.Replace(path, "/"+prefix, "", 1)
	}

	if ph.StripVersion && version != "" {
		path = strings.Replace(path, "/"+version, "", 1)
	}

	if ph.AddTrailingSlash && !strings.HasSuffix(path, "/") {
		path += "/"
	}

	return path
}

type LimitConfig struct{}
