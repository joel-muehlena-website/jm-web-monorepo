package apigateway

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"go.uber.org/zap"
	"nhooyr.io/websocket"
)

type WSHandler struct {
	GenericRouteHandler
}

func NewWSHandler(logger *zap.Logger, version *VersionConfig) *WSHandler {
	handler := new(WSHandler)
	handler.logger = logger
	handler.version = version

	return handler
}

func (h *WSHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := h.Handle(w, r)
	if err != nil {
		h.logger.Error("WS proxying failed", zap.Error(err))
	}
}

func (h *WSHandler) Handle(w http.ResponseWriter, r *http.Request) error {
	h.logger.Debug("Websocket handler called")
	requestingClient, err := websocket.Accept(w, r, &websocket.AcceptOptions{})
	if err != nil {
		return fmt.Errorf("failed to accept WS connection: %w", err)
	}

	path := h.version.PathBehavior.cleanPath(r.URL.Path, h.version.Path, h.prefix)
	ctx, cancel := context.WithTimeout(r.Context(), 10*time.Second)
	defer cancel()
	remoteClient, _, err := websocket.Dial(ctx, fmt.Sprintf("%s%s", h.version.Upstream.Service, path), nil)
	if err != nil {
		return fmt.Errorf("failed to create WS connection to remote service: %w", err)
	}

	ctx, cancelCause := context.WithCancelCause(context.Background())
	go wsReaderRoutine(ctx, cancelCause, h.logger, requestingClient, remoteClient)
	go wsWriterRoutine(ctx, cancelCause, h.logger, requestingClient, remoteClient)

	return nil
}

func wsReaderRoutine(ctx context.Context, cancelCause context.CancelCauseFunc, logger *zap.Logger, requestingClient *websocket.Conn, remoteClient *websocket.Conn) {
	logger = logger.Named("WS-Reader")
	logger.Debug("Starting routine")
	defer wsCleanup(logger, requestingClient, remoteClient, cancelCause)

	for {
		select {
		case <-ctx.Done():
			logger.Debug("Reader context ended", zap.NamedError("cause", context.Cause(ctx)))
			return
		default:
			msgType, data, err := requestingClient.Read(ctx)
			if err != nil && !errors.Is(err, context.Canceled) {
				wsHandlerError(logger.Named("read"), cancelCause, err, requestingClient, remoteClient)
				break
			}

			err = remoteClient.Write(ctx, msgType, data)
			if err != nil && !errors.Is(err, context.Canceled) {
				wsHandlerError(logger.Named("write"), cancelCause, err, requestingClient, remoteClient)
			}
		}
	}
}

func wsWriterRoutine(ctx context.Context, cancelCause context.CancelCauseFunc, logger *zap.Logger, requestingClient *websocket.Conn, remoteClient *websocket.Conn) {
	logger = logger.Named("WS-Writer")
	logger.Debug("Starting routine")
	defer wsCleanup(logger, requestingClient, remoteClient, cancelCause)

	for {
		select {
		case <-ctx.Done():
			logger.Debug("Writer context ended", zap.NamedError("cause", context.Cause(ctx)))
			return
		default:
			msgType, data, err := remoteClient.Read(ctx)
			if err != nil && !errors.Is(err, context.Canceled) {
				wsHandlerError(logger.Named("read"), cancelCause, err, remoteClient, requestingClient)
				break
			}

			err = requestingClient.Write(ctx, msgType, data)
			if err != nil && !errors.Is(err, context.Canceled) {
				wsHandlerError(logger.Named("write"), cancelCause, err, remoteClient, requestingClient)
			}
		}
	}
}

func wsCleanup(logger *zap.Logger, a *websocket.Conn, b *websocket.Conn, cancelCause context.CancelCauseFunc) {
	logger.Debug("Stopping routine")

	// To be safe close both connections and context
	// If already closed which is most likely the case this does nothing
	a.Close(websocket.StatusAbnormalClosure, "Handle routine ended")
	b.Close(websocket.StatusAbnormalClosure, "Handle routine ended")
	cancelCause(fmt.Errorf("WS handle routine ended"))
}

func wsHandlerError(logger *zap.Logger, cancelCause context.CancelCauseFunc, err error, a *websocket.Conn, b *websocket.Conn) {
	closeStatus := websocket.CloseStatus(err)

	if closeStatus == -1 {
		logger.Debug("No close error, not closing WS")
		logger.Error("Error handling WS connection. Continue with next iteration", zap.Error(err))
		return
	}

	// Close handlers
	cancelCause(err)

	if closeStatus == websocket.StatusNormalClosure {
		b.Close(websocket.StatusNormalClosure, "connection closed")
		return
	}

	logger.Debug("Closing websocket abnormally", zap.Error(err), zap.Int("close status", int(closeStatus)))
	b.Close(closeStatus, err.Error())
}
