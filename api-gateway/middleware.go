package apigateway

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"sync/atomic"

	"github.com/mitchellh/mapstructure"

	"go.uber.org/zap"
)

type Middleware interface {
	Init(logger *zap.Logger)
	Handle(w http.ResponseWriter, r *http.Request, data MiddlewareData) (http.ResponseWriter, *http.Request, *MiddlewareError)
	Reload(ctx context.Context)
	Shutdown(ctx context.Context)
}

var ErrNotAuthenticated = MiddlewareError{
	HTTPErrorCode: uint16(http.StatusForbidden),
	Err:           fmt.Errorf("user or entity is not authenticated"),
}

var ErrNotAuthorized = MiddlewareError{
	HTTPErrorCode: uint16(http.StatusUnauthorized),
	Err:           fmt.Errorf("user or entity is not authorized to perform this action"),
}

type MiddlewareError struct {
	HTTPErrorCode uint16 `json:"code"`
	Err           error  `json:"error"`
}

func (mwe *MiddlewareError) Error() string {
	return fmt.Sprintf("failed to handle middleware with code %d and err: %s", mwe.HTTPErrorCode, mwe.Err.Error())
}

type MiddlewareName string

type MiddlewareData map[string]interface{}

var ErrMiddlewareExistsNoOverride = fmt.Errorf("failed to add middleware, because this key already exists and overriding is not enabled")

type MiddlewareRegistry struct {
	lock       sync.RWMutex
	isOverride atomic.Bool
	logger     *zap.Logger
	baseLogger *zap.Logger

	middleware map[MiddlewareName]Middleware
}

func NewMiddlewareRegistry(logger *zap.Logger) *MiddlewareRegistry {
	reg := new(MiddlewareRegistry)
	reg.baseLogger = logger.Named("middleware")
	reg.logger = reg.baseLogger.Named("registry")
	reg.middleware = make(map[MiddlewareName]Middleware, 0)

	return reg
}

func (reg *MiddlewareRegistry) Use(name MiddlewareName, mw Middleware) error {
	reg.lock.Lock()

	_, ok := reg.middleware[name]

	if ok && !reg.isOverride.Load() {
		return ErrMiddlewareExistsNoOverride
	}

	reg.middleware[name] = mw

	reg.lock.Unlock()

	mw.Init(reg.baseLogger)

	return nil
}

func (reg *MiddlewareRegistry) Get(name MiddlewareName) Middleware {
	reg.lock.RLock()
	mw, ok := reg.middleware[name]

	if !ok {
		return nil
	}

	reg.lock.RUnlock()

	return mw
}

func (reg *MiddlewareRegistry) Remove(name MiddlewareName) {
	reg.lock.Lock()
	delete(reg.middleware, name)
	reg.lock.Unlock()
}

func (reg *MiddlewareRegistry) Reload() {
	reg.lock.Lock()
	defer reg.lock.Unlock()

	for _, mw := range reg.middleware {
		mw.Reload(context.TODO())
	}
}

func (reg *MiddlewareRegistry) Override(enable bool) {
	reg.isOverride.Store(true)
}

func ParseMiddlewareData[T any](middlewareData MiddlewareData) (parsed T, err error) {
	if middlewareData == nil || len(middlewareData) == 0 {
		err = fmt.Errorf("passed data is nil or has no items")
		return
	}

	err = mapstructure.Decode(middlewareData, parsed)
	return
}
