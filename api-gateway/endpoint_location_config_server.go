package apigateway

import (
	gomstoolkit_util "gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/util"
	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/utility"
)

func LocateEndpointFromConfigServer(config EndpointConfigLocationConfigServer) (EndpointsConfig, error) {
	result := gomstoolkit_util.FetchConfigServerConfig(config.ConfigServerUrl, config.FilePath)
	parsed := utility.ParseConfig[EndpointsConfig](result)
	return *parsed, nil
}
