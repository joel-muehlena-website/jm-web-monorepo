#!/bin/bash

set -e -o pipefail

baseDir="$(
    cd -- "$(dirname "$0")" >/dev/null 2>&1
    pwd -P
)"

image=$1
registry="${2:-registry.gitlab.com/joel-muehlena-website/jm-web-monorepo/base}"
path="${3:-$image:latest}"

echo "Building image: ${image} as ${registry}/${path}"
echo "Using folder ${baseDir}/${image}"

docker run --rm --workdir /work -v ${baseDir}/${image}:/work \
  cgr.dev/chainguard/apko \
  build apko.yaml "${registry}/${path}" image.tar \
  --arch host

docker load -i ${baseDir}/${image}/image.tar
docker tag ${registry}/${path}-amd64 ${registry}/${path}
docker image rm ${registry}/${path}-amd64

# rm -f ${baseDir}/${image}/image.tar