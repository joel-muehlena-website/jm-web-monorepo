# mTLS certs and keys for dev purposes

This files can be used to run dev services with TLS over http and http/2 servers. They should not be used in production and are considered unsafe. The client must authenticate the server and vice versa.