package log

import (
	"context"

	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type ZapTraceLinkLogger struct {
	*zap.Logger
}

func NewZapTraceLinkLogger(logger *zap.Logger) *ZapTraceLinkLogger {
	return &ZapTraceLinkLogger{
		logger,
	}
}

func (l *ZapTraceLinkLogger) Ctx(ctx context.Context) *zap.Logger {
	spanCtx := trace.SpanContextFromContext(ctx)
	var traceID string
	if spanCtx.HasTraceID() {
		traceID = spanCtx.TraceID().String()
	} else {
		traceID = "no-trace-id"
	}

	ctxLogger := l.Logger.With(zap.String("traceID", traceID))

	return ctxLogger
}
