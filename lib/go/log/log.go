package log

import (
	"fmt"
	"log"
	"log/slog"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"dario.cat/mergo"
	"github.com/gin-gonic/gin"
	"github.com/go-chi/httplog/v2"
	"github.com/samber/lo"
	"go.uber.org/zap"
	"go.uber.org/zap/exp/zapslog"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc/metadata"
)

func New(serviceName string, userConfig Config, pathMapper func(string) string, additionalFields map[string]any) (*zap.Logger, error) {
	config := DefaultLoggerConfig

	if err := mergo.Merge(&config, userConfig, mergo.WithOverride); err != nil {
		return nil, fmt.Errorf("failed to merge user config with default config: %w", err)
	}

	if !config.Enabled {
		return zap.NewNop(), nil
	}

	config.LogErrorPaths = lo.Map(config.LogErrorPaths, func(path string, _ int) string {
		expanded := os.Expand(path, pathMapper)
		_, err := CreateLogDir(path)
		if err != nil {
			log.Fatal("Failed to create log dir: " + err.Error())
		}
		return expanded
	})

	config.LogOutputPaths = lo.Map(config.LogOutputPaths, func(path string, _ int) string {
		expanded := os.Expand(path, pathMapper)
		_, err := CreateLogDir(path)
		if err != nil {
			log.Fatal("Failed to create log dir: " + err.Error())
		}
		return expanded
	})

	if config.UseLevelColors {
		config.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	}

	zapCfg := zap.Config{
		OutputPaths:      config.LogOutputPaths,
		ErrorOutputPaths: config.LogErrorPaths,
		EncoderConfig:    config.EncoderConfig,
		Encoding:         config.Encoder,
		Level:            zap.NewAtomicLevelAt(GetLevelFromString(config.LogLevel)),
	}

	if additionalFields == nil {
		additionalFields = make(map[string]any)
	}

	additionalFields["service_name"] = serviceName
	zapCfg.InitialFields = additionalFields

	logger, err := zapCfg.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to build logger from config: %w", err)
	}

	if config.UseServiceNameAsBaseName {
		logger = logger.Named(serviceName)
	} else if config.LoggerBaseName != "" {
		logger = logger.Named(config.LoggerBaseName)
	}

	logger.Sync()

	// Replace global logger
	// Ignoring zap recommendations here on purpose because it is easier than passing the logger everywhere
	zap.ReplaceGlobals(logger)

	return logger, nil
}

// Removed `-0700` of spec because using UTC always has +0000
const RFC3339_TIME_FORMAT string = "2006-01-02T15:04:05"

func CustomTimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.UTC().Format(RFC3339_TIME_FORMAT))
}

func GetLevelFromString(level string) zapcore.Level {
	var lvl zapcore.Level

	switch level {
	case "debug":
		lvl = zapcore.DebugLevel
	case "info":
		lvl = zapcore.InfoLevel
	case "warn":
		lvl = zapcore.WarnLevel
	case "error":
		lvl = zapcore.ErrorLevel
	case "panic":
		lvl = zapcore.PanicLevel
	default:
		lvl = zapcore.InfoLevel
	}

	return lvl
}

func GinZapLogger(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		// some evil middlewares modify this values
		path := c.Request.URL.Path
		query := c.Request.URL.RawQuery
		c.Next()

		end := time.Now()
		latency := end.Sub(start)

		if len(c.Errors) > 0 {
			// Append error field if this is an erroneous request.
			for _, e := range c.Errors.Errors() {
				logger.Error(e)
			}
		} else {
			fields := []zapcore.Field{
				zap.Int("status", c.Writer.Status()),
				zap.String("method", c.Request.Method),
				zap.String("path", path),
				zap.String("query", query),
				zap.String("ip", c.ClientIP()),
				zap.String("user-agent", c.Request.UserAgent()),
				zap.Duration("latency", latency),
				zap.String("time", end.Format(RFC3339_TIME_FORMAT)),
			}
			logger.Info(path, fields...)
		}
	}
}

func ChiZapLogger(logger *zap.Logger) func(next http.Handler) http.Handler {
	// Maybe add plain zap functionality here
	// Currently the experimental slog interface of zap is used to rely on the httplog package
	sl := slog.New(zapslog.NewHandler(logger.Core(), &zapslog.HandlerOptions{LoggerName: logger.Name()}))

	httplogLogger := &httplog.Logger{
		Logger: sl,
		Options: httplog.Options{
			Concise:         false,
			RequestHeaders:  false,
			ResponseHeaders: false,
		},
	}

	return httplog.RequestLogger(httplogLogger)
}

func ZapMap2FieldsString(m map[string]string) []zap.Field {
	fields := make([]zap.Field, 0, len(m))
	for k, v := range m {
		fields = append(fields, zap.String(k, v))
	}
	return fields
}

type ZapGrpcMetadata struct {
	m metadata.MD
}

func ZapMap2FieldsGrpcMetadata(m metadata.MD) *ZapGrpcMetadata {
	md := new(ZapGrpcMetadata)
	md.m = m
	return md
}

func (md *ZapGrpcMetadata) MarshalLogObject(enc zapcore.ObjectEncoder) error {
	for k, v := range md.m {
		enc.AddString(k, strings.Join(v, ","))
	}

	return nil
}

func CreateLogDir(path string) (bool, error) {
	// TODO: Add suport for custom mapping function
	expandedPath := os.Expand(path, func(placeholderName string) string {
		switch placeholderName {
		default:
			return ""
		}
	})

	_, err := os.Stat(expandedPath)
	if err == nil {
		return false, nil
	}

	if os.IsNotExist(err) {
		dir := filepath.Dir(expandedPath)
		if err = os.MkdirAll(dir, os.ModePerm); err != nil {
			return false, err
		}

		return true, nil
	}

	return false, err
}
