package log

import (
	"go.uber.org/zap/zapcore"
)

type Config struct {
	LoggerBaseName           string                `yaml:"baseName" toml:"baseName"`
	UseServiceNameAsBaseName bool                  `yaml:"useServiceNameAsBaseName" toml:"useServiceNameAsBaseName"`
	LogLevel                 string                `yaml:"logLevel" toml:"logLevel"`
	LogOutputPaths           []string              `yaml:"stdoutPaths" toml:"stdoutPaths"`
	LogErrorPaths            []string              `yaml:"stderrPaths" toml:"stderrPaths"`
	Encoder                  string                `yaml:"encoder" toml:"encoder"`
	EncoderConfig            zapcore.EncoderConfig `yaml:"encoderCfg" toml:"encoderCfg"`
	Enabled                  bool                  `yaml:"enabled" toml:"enabled"`
	UseLevelColors           bool                  `yaml:"useLevelColors" toml:"useLevelColors"`
}

var DefaultLoggerConfig Config = Config{
	LoggerBaseName:           "",
	UseServiceNameAsBaseName: false,
	Enabled:                  true,
	LogLevel:                 "debug",
	LogOutputPaths:           []string{"stdout"},
	LogErrorPaths:            []string{"stderr"},
	Encoder:                  "json",
	EncoderConfig: zapcore.EncoderConfig{
		MessageKey:    "message",
		LevelKey:      "level",
		EncodeLevel:   zapcore.LowercaseLevelEncoder,
		TimeKey:       "time",
		EncodeTime:    CustomTimeEncoder,
		CallerKey:     "caller",
		EncodeCaller:  zapcore.FullCallerEncoder,
		NameKey:       "name",
		EncodeName:    zapcore.FullNameEncoder,
		StacktraceKey: "stacktrace",
		FunctionKey:   "function",
	},
}
