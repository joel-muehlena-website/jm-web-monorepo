package probes

import "github.com/prometheus/client_golang/prometheus"

var (
	metricIsStartup = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "service_probes_startup",
		Help: "Service startup probe",
	})

	metricIsReady = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "service_probes_ready",
		Help: "Service readiness probe",
	})

	metricIsLive = prometheus.NewGauge(prometheus.GaugeOpts{
		Name: "service_probes_live",
		Help: "Service liveness probe",
	})
)
