package probes

import (
	"encoding/json"
	"net/http"

	"go.uber.org/zap"
)

type ProbesState interface {
	Startup() bool
	Live() bool
	Ready() bool
}

type probeHandler struct {
	logger *zap.Logger
	state  ProbesState
}

type probeResponse struct {
	Code      uint16 `json:"code"`
	Msg       string `json:"msg"`
	ProbeType string `json:"probeType"`
}

func (p *probeHandler) startupHandler(res http.ResponseWriter, req *http.Request) {
	probeRes := probeResponse{ProbeType: "startup", Code: 200, Msg: "Service startup probe successful"}

	if !p.state.Startup() {
		probeRes.Code = 400
		probeRes.Msg = "Service startup probe failed"
	}

	sendProbeResponse(p.logger, res, probeRes)
}

func (p *probeHandler) liveHandler(res http.ResponseWriter, req *http.Request) {
	probeRes := probeResponse{ProbeType: "live", Code: 200, Msg: "Ready or liveness probe successful"}

	if !p.state.Live() {
		probeRes.Code = 400
		probeRes.Msg = "Service startup probe failed"
	}

	sendProbeResponse(p.logger, res, probeRes)
}

func (p *probeHandler) readyHandler(res http.ResponseWriter, req *http.Request) {
	probeRes := probeResponse{ProbeType: "ready", Code: 200, Msg: "Ready or liveness probe successful"}

	if !p.state.Ready() {
		probeRes.Code = 400
		probeRes.Msg = "Service startup probe failed"
	}

	sendProbeResponse(p.logger, res, probeRes)
}

func sendProbeResponse(logger *zap.Logger, res http.ResponseWriter, probeRes probeResponse) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(int(probeRes.Code))

	bytes, err := json.Marshal(probeRes)
	if err != nil {
		logger.Error("Failed to encode probe json response", zap.Error(err))
	}

	res.Write(bytes)
}
