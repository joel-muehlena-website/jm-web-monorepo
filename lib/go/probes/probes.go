package probes

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"path"
	"sync/atomic"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/puzpuzpuz/xsync/v3"
	"go.uber.org/multierr"
	"go.uber.org/zap"
)

type ProbeState struct {
	readyAware bool
	liveAware  bool

	Ready bool `json:"ready"`
	Live  bool `json:"live"`

	LiveHandler  func() bool
	ReadyHandler func() bool
}

type Probes struct {
	startup             atomic.Bool
	probeStates         *xsync.MapOf[string, ProbeState]
	logger              *zap.Logger
	server              *http.Server
	combineLiveAndReady bool

	metricsRegistry *prometheus.Registry
}

func New(logger *zap.Logger, config Config) *Probes {
	probes := &Probes{}

	if config.Port == 0 {
		logger.Warn("The provided port is not possible or was not set, falling back to default port 9089")
		config.Port = 9089
	}

	if config.EndpointPrefix == "" {
		config.EndpointPrefix = "/"
	}

	if config.LiveEndpoint == "" {
		config.LiveEndpoint = path.Join(config.EndpointPrefix, "live")
	} else {
		config.LiveEndpoint = path.Join(config.EndpointPrefix, config.LiveEndpoint)
	}

	if config.ReadyEndpoint == "" {
		config.ReadyEndpoint = path.Join(config.EndpointPrefix, "ready")
	} else {
		config.ReadyEndpoint = path.Join(config.EndpointPrefix, config.ReadyEndpoint)
	}

	if config.StartupEndpoint == "" {
		config.StartupEndpoint = path.Join(config.EndpointPrefix, "startup")
	} else {
		config.StartupEndpoint = path.Join(config.EndpointPrefix, config.StartupEndpoint)
	}

	probes.combineLiveAndReady = config.CombineLiveAndReady
	if probes.combineLiveAndReady {
		logger.Info("Live and ready probes are combined. Live probes will return the value of ready probes.")
	}

	probes.probeStates = xsync.NewMapOf[string, ProbeState]()

	probes.logger = logger.Named("probes")
	probes.server = probes.initServer(config.ReadyEndpoint, config.LiveEndpoint, config.StartupEndpoint, config.BindAddr, config.Port)

	probes.metricsRegistry = prometheus.NewRegistry()
	prometheus.MustRegister(probes.metricsRegistry)

	probes.metricsRegistry.MustRegister(metricIsLive)
	probes.metricsRegistry.MustRegister(metricIsReady)
	probes.metricsRegistry.MustRegister(metricIsStartup)

	return probes
}

func (p *Probes) Handler() http.Handler {
	return p.server.Handler
}

func (p *Probes) initServer(readyEndpoint, liveEndpoint, startupEndpoint, bindAddr string, port uint16) *http.Server {
	handler := probeHandler{
		logger: p.logger.Named("http"),
		state:  p,
	}

	mux := http.NewServeMux()
	mux.HandleFunc(readyEndpoint, handler.readyHandler)
	mux.HandleFunc(liveEndpoint, handler.liveHandler)
	mux.HandleFunc(startupEndpoint, handler.startupHandler)

	server := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", bindAddr, port),
		Handler: mux,
	}

	return server
}

func (p *Probes) Shutdown(ctx context.Context) {
	p.server.Shutdown(ctx)
}

func (p *Probes) Startup() bool {
	return p.startup.Load()
}

func (p *Probes) Register(key string, liveAware, readyAware, live, ready bool, liveHandler, readyHandler func() bool) error {
	_, loaded := p.probeStates.LoadOrStore(key, ProbeState{
		liveAware:    liveAware,
		readyAware:   readyAware,
		Ready:        ready,
		ReadyHandler: readyHandler,
		Live:         live,
		LiveHandler:  liveHandler,
	})

	if loaded {
		return fmt.Errorf("probe with key %s already exists", key)
	}

	return nil
}

func (p *Probes) SetLive(key string, live bool) error {
	var err error = nil
	newValue, ok := p.probeStates.Compute(key, func(oldValue ProbeState, loaded bool) (newValue ProbeState, delete bool) {
		if !loaded {
			err = fmt.Errorf("probe state with key %s does not exist", key)
			return oldValue, false
		}

		if !oldValue.liveAware {
			err = fmt.Errorf("probe with key %s is not live aware (check register)", key)
			return oldValue, false
		}

		oldValue.Live = live

		return oldValue, false
	})

	p.logger.Debug("Altered live state for: `"+key+"`", zap.String("key", key), zap.Bool("live", live), zap.Bool("ok", ok), zap.Error(err))

	if ok {
		if newValue.Live {
			metricIsLive.Set(1)
		} else {
			metricIsLive.Set(0)
		}
	}

	return err
}

func (p *Probes) SetReady(key string, ready bool) error {
	var err error = nil
	newValue, ok := p.probeStates.Compute(key, func(oldValue ProbeState, loaded bool) (newValue ProbeState, delete bool) {
		if !loaded {
			err = fmt.Errorf("probe state with key %s does not exist", key)
			return oldValue, false
		}

		if !oldValue.readyAware {
			err = fmt.Errorf("probe with key %s is not ready aware (check register).", key)
			return oldValue, false
		}

		oldValue.Ready = ready

		return oldValue, false
	})

	p.logger.Debug("Altered ready state for: `"+key+"`", zap.Bool("ready", ready), zap.Bool("ok", ok), zap.Error(err))

	if ok {
		if newValue.Ready {
			metricIsReady.Set(1)
		} else {
			metricIsReady.Set(0)
		}
	}

	return err
}

func (p *Probes) SetLiveAndReady(key string, liveAndReady bool) error {
	err := multierr.Append(p.SetLive(key, liveAndReady), p.SetReady(key, liveAndReady))

	return err
}

func (p *Probes) Live() bool {
	if p.combineLiveAndReady {
		return p.Ready()
	}

	isLive := true

	p.probeStates.Range(func(key string, value ProbeState) bool {
		if value.liveAware {
			if value.LiveHandler != nil {
				isLive = isLive && value.LiveHandler()
			} else {
				isLive = isLive && value.Live
			}
		}

		return isLive
	})

	return isLive
}

func (p *Probes) Ready() bool {
	isReady := true

	p.probeStates.Range(func(key string, value ProbeState) bool {
		if value.readyAware {
			p.logger.Debug("Checking ready state for: `"+key+"`", zap.String("key", key), zap.Bool("ready", value.Ready))
			if value.ReadyHandler != nil {
				isReady = isReady && value.ReadyHandler()
			} else {
				isReady = isReady && value.Ready
			}
		}

		return isReady
	})

	return isReady
}

func (p *Probes) SetStartup(val bool) {
	p.logger.Debug("Startup was set", zap.Bool("value", val))
	p.startup.Store(val)

	if val {
		metricIsStartup.Set(1)
	} else {
		metricIsStartup.Set(0)
	}
}

func (p *Probes) ListenAndServe() error {
	host, port, err := net.SplitHostPort(p.server.Addr)
	if err != nil {
		return fmt.Errorf("failed to split host and port: %w", err)
	}

	go func() {
		p.logger.Info("Starting probe server", zap.String("port", port), zap.String("host", host))
		err := p.server.ListenAndServe()
		if err != nil {
			p.logger.Fatal("Failed to start server", zap.Error(err))
		}
	}()

	return nil
}
