package probes

type Config struct {
	Port                uint16 `yaml:"port" json:"port" toml:"port"`
	BindAddr            string `yaml:"bindAddr" json:"bindAddr" toml:"bindAddr"`
	ReadyEndpoint       string `yaml:"readyEndpoint" json:"readyEndpoint" toml:"readyEndpoint"`
	LiveEndpoint        string `yaml:"liveEndpoint" json:"liveEndpoint" toml:"liveEndpoint"`
	StartupEndpoint     string `yaml:"startupEndpoint" json:"startupEndpoint" toml:"startupEndpoint"`
	CombineLiveAndReady bool   `yaml:"combineLiveAndReady" json:"combineLiveAndReady" toml:"combineLiveAndReady"`
	EndpointPrefix      string `yaml:"endpointPrefix" json:"endpointPrefix" toml:"endpointPrefix"`
}
