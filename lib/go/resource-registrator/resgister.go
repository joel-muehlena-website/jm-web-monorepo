package resource_registrator

import (
	"context"

	"go.uber.org/zap"

	"google.golang.org/grpc"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/rust/ds-auth/proto/resource"
)

func Register(zap *zap.Logger, ctx context.Context, registerRequest *resource.RegisterRequest, conn grpc.ClientConnInterface) (*resource.RegisterResponse, error) {
	client := resource.NewResourceManagerClient(conn)
	return client.Register(ctx, registerRequest)
}
