package resource_registrator

import (
	"context"

	"go.uber.org/zap"

	"google.golang.org/grpc"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/rust/ds-auth/proto/resource"
)

func Unregister(zap *zap.Logger, ctx context.Context, registerRequest *resource.UnregisterRequest, conn grpc.ClientConnInterface) (*resource.UnregisterResponse, error) {
	client := resource.NewResourceManagerClient(conn)
	return client.Unregister(ctx, registerRequest)
}
