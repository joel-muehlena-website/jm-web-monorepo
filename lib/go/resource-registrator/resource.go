package resource_registrator

type ApplicationResources struct {
	ResourceServerAddress string
	HealthCheckUrl        string
	Resources             Resources
	Claims                map[string]string
}

type ResourceConfig struct {
	ResourceServerAddress string    `yaml:"resourceServerAddress" toml:"resourceServerAddress"`
	Resources             Resources `yaml:"resources" toml:"resources"`
}

type Resources []Resource

type Resource struct {
	Name       string             `yaml:"name" toml:"name"`
	Domain     string             `yaml:"domain" toml:"domain"`
	Actions    []string           `yaml:"actions" toml:"actions"`
	Attributes ResourceAttributes `yaml:"attributes" toml:"attributes"`
}
type ResourceAttributes struct {
	Constraints map[string]ResourceConstraint `yaml:"constraints" toml:"constraints"`
}

type ResourceConstraint struct{}
