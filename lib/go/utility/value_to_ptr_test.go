package utility

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewPtrVal(t *testing.T) {
	valInt := NewPtrVal(5)
	assert.Equal(t, 5, *valInt)

	valString := NewPtrVal("testVal")
	assert.Equal(t, "testVal", *valString)

	valBool := NewPtrVal(true)
	assert.Equal(t, true, *valBool)

	valUint := NewPtrVal(uint(3))
	assert.Equal(t, uint(3), *valUint)
}
