package utility

func NewPtrVal[T any](val T) *T {
	v := new(T)
	*v = val
	return v
}
