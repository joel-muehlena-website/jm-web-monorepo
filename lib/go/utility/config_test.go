package utility

import (
	"testing"

	"github.com/go-test/deep"
	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v3"

	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db"
	"gitlab.com/joelMuehlena/jm-web-monorepo/go-ms-toolkit/db/pg"
)

func TestParseConfig(t *testing.T) {
	assert := assert.New(t)

	type testCfg struct {
		Port     uint16                  `yaml:"port"`
		DBConfig pg.PostgreSQLPoolConfig `yaml:"dbConfig"`
	}

	t.Run("Should parse config without errors", func(t *testing.T) {
		cfg := testCfg{
			Port: 3300,
			DBConfig: pg.PostgreSQLPoolConfig{
				DBConfig: db.DBConfig{},
			},
		}

		cfgBytes, err := yaml.Marshal(&cfg)

		assert.Nil(err)

		parsedCfg := ParseConfig[testCfg](cfgBytes)

		assert.Nil(deep.Equal(*parsedCfg, cfg))
	})

	t.Run("Should panic on error", func(t *testing.T) {
		assert.Panics(func() {
			falseValue := `port: "3000"
dbConfig:
  general:
    username: 23`

			ParseConfig[testCfg]([]byte(falseValue))
		})
	})
}
