package utility

import (
	"os"
	"strconv"
)

const DEFAULT_PORT uint16 = 3000

func ConfigurePort(initPort uint16) uint16 {
	var port uint16 = initPort

	if port == 0 {
		port = DEFAULT_PORT
	}

	if os.Getenv("PORT") != "" {
		val, err := strconv.ParseUint(os.Getenv("PORT"), 10, 32)
		if err == nil {
			port = uint16(val)
		}
	}

	return port
}
