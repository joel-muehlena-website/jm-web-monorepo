package utility

import (
	"os"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConfigurePort(t *testing.T) {
	t.Parallel()

	assert := assert.New(t)

	t.Run("Should set port to default if start is 0", func(t *testing.T) {
		port := ConfigurePort(0)

		assert.Equal(DEFAULT_PORT, port)
	})

	t.Run("Should not alter port if is configured and no env is set", func(t *testing.T) {
		var setPort uint16 = 4506
		port := ConfigurePort(setPort)

		assert.Equal(setPort, port)
	})

	t.Run("Should set port from env if PORT is set", func(t *testing.T) {
		var setPort uint16 = 4506
		err := os.Setenv("PORT", strconv.FormatUint(uint64(setPort), 10))

		assert.Nil(err)

		port := ConfigurePort(0)

		assert.Equal(setPort, port)
	})

	t.Run("Should override start port if env PORT is set", func(t *testing.T) {
		var setPort uint16 = 4506
		err := os.Setenv("PORT", strconv.FormatUint(uint64(setPort), 10))

		assert.Nil(err)

		port := ConfigurePort(3459)

		assert.Equal(setPort, port)
	})
}
