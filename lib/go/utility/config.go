package utility

import "gopkg.in/yaml.v3"

// TODO return error
func ParseConfig[T any](data []byte) *T {
	var cfg T
	err := yaml.Unmarshal(data, &cfg)
	if err != nil {
		panic("Failed to parse config: " + err.Error())
	}

	return &cfg
}
