package utility_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/utility"
)

type testDataS[T comparable] struct {
	value  T
	data   []T
	result bool
}

func checker[T comparable](testSet []testDataS[T], t *testing.T) {
	for _, td := range testSet {
		assert.Equal(t, td.result, utility.Contains(td.data, td.value))
	}
}

func TestContains(t *testing.T) {
	t.Run("Work with strings", func(t *testing.T) {
		testSet := []testDataS[string]{
			{
				value:  "test",
				result: true,
				data:   []string{"abc", "test"},
			},
			{
				value:  "test",
				result: false,
				data:   []string{"abc", "nice"},
			},
		}

		checker(testSet, t)
	})

	t.Run("Work with int", func(t *testing.T) {
		testSet := []testDataS[int]{
			{
				value:  2,
				result: true,
				data:   []int{3, 5, 6, 2},
			},
			{
				value:  1,
				result: false,
				data:   []int{2, 4, 6, 8},
			},
		}

		checker(testSet, t)
	})
}
