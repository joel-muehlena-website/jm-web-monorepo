package telemetry

import (
	"context"
	"fmt"
	"strings"
	"time"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.24.0"
	"go.opentelemetry.io/otel/trace"
)

// TODO try re-init if fails and enabled via go routine

type Telemetry struct {
	logger   *zap.Logger
	config   Config
	Provider *sdktrace.TracerProvider
}

func New(cfg Config, logger *zap.Logger) *Telemetry {
	logger = logger.Named("telemetry")
	tel := new(Telemetry)

	tel.logger = logger
	tel.config = cfg

	return tel
}

func (tel *Telemetry) Shutdown(ctx context.Context) {
	if tel.Provider != nil {
		tel.Provider.Shutdown(ctx)
	}
}

func (tel *Telemetry) Init() error {
	if !tel.config.Enabled {
		tel.logger.Info("Telemetry is not enabled, skipping initialization")
		return nil
	}

	tel.logger.Info("Initializing telemetry provider")

	err := validateConfig(tel.config)
	if err != nil {
		tel.logger.Error("Failed to initialize telemetry. Failed to validate config.", zap.Error(err), zap.Any("config", tel.config))
		return err
	}

	err = tel.initProvider()
	if err != nil {
		tel.logger.Error("Failed to initialize telemetry. Provider initialization failed.", zap.Error(err))
	} else {
		tel.logger.Info("Telemetry initialized successfully")
	}

	return err
}

func (tel *Telemetry) initProvider() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second) // TODO
	defer cancel()

	serviceConfig, err := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceName(tel.config.Name),
			semconv.ServiceInstanceID(tel.config.ID),
			semconv.ServiceNamespace(tel.config.Namespace),
			semconv.ServiceVersion(tel.config.Version),
		),
	)
	if err != nil {
		return fmt.Errorf("failed to create service resource: %w", err)
	}

	res, err := resource.New(ctx,
		resource.WithAttributes(serviceConfig.Attributes()...),
		resource.WithContainer(),
		resource.WithFromEnv(),
		resource.WithHost(),
		resource.WithOS(),
		resource.WithProcess(),
		resource.WithTelemetrySDK(),
	)
	if err != nil {
		return fmt.Errorf("failed to create resource: %w", err)
	}

	otel.SetTextMapPropagator(newPropagator())

	tracerProvider, err := tel.newTraceProvider(ctx, res)
	if err != nil {
		return fmt.Errorf("failed to create trace provider: %w", err)
	}

	otel.SetTracerProvider(tracerProvider)

	tel.Provider = tracerProvider

	return nil
}

func newPropagator() propagation.TextMapPropagator {
	return propagation.NewCompositeTextMapPropagator(
		propagation.TraceContext{},
		propagation.Baggage{},
	)
}

func (tel *Telemetry) newTraceProvider(ctx context.Context, res *resource.Resource) (*sdktrace.TracerProvider, error) {
	traceExporter, err := tel.setupOTELCollectorGRPC(ctx)
	if err != nil {
		if tel.config.BackupCollectorConfig.Enabled {
			zap.L().Error("Failed to initialize collector connection", zap.Error(err))

			traceExporter, err = tel.setupBackupCollector(ctx)

			if err != nil {
				return nil, fmt.Errorf("failed to setup collector & backup collector connection: %w", err)
			}
		} else {
			return nil, fmt.Errorf("failed to setup collector connection: %w", err)
		}
	}

	// Register the trace exporter with a TracerProvider, using a batch
	// span processor to aggregate spans before export.
	batchSpanProcessor := sdktrace.NewBatchSpanProcessor(traceExporter)
	tracerProvider := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithResource(res),
		sdktrace.WithSpanProcessor(batchSpanProcessor),
	)

	return tracerProvider, nil
}

func (tel *Telemetry) setupOTELCollectorGRPC(ctx context.Context) (*otlptrace.Exporter, error) {
	tel.logger.Debug("OTEL Collector gRPC connection", zap.String("url", fmt.Sprintf("%s:%d", tel.config.CollectorConfig.URL, tel.config.CollectorConfig.Port)))

	conn, err := grpc.DialContext(ctx,
		fmt.Sprintf("%s:%d", tel.config.CollectorConfig.URL, tel.config.CollectorConfig.Port),
		grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock(),
	)
	if err != nil && strings.Contains(err.Error(), "failed to create gRPC connection to collector: context deadline exceeded") {
		tel.logger.Info("Failed to create gRPC connection to collector. Retrying in background...")
	} else if err != nil {
		return nil, fmt.Errorf("failed to create gRPC connection to collector: %w", err)
	}

	// Set up a trace exporter
	traceExporter, err := otlptracegrpc.New(ctx, otlptracegrpc.WithGRPCConn(conn), otlptracegrpc.WithCompressor("gzip"))
	if err != nil {
		return nil, fmt.Errorf("failed to create trace exporter: %w", err)
	}

	return traceExporter, nil
}

func (tel *Telemetry) setupBackupCollector(ctx context.Context) (*otlptrace.Exporter, error) {
	switch tel.config.BackupCollectorConfig.Type {
	case "file":
		tel.setupFileCollector(ctx)
	case "console":
		return tel.setupConsoleCollector(ctx)
	case "empty":
		return tel.setupEmptyCollector(ctx)
	}

	return nil, fmt.Errorf("unsupported type: %s", tel.config.BackupCollectorConfig.Type)
}

func (tel *Telemetry) setupFileCollector(ctx context.Context) (*otlptrace.Exporter, error) {
	col := FileExporter{
		logger: tel.logger,
	}
	col.SetFile(tel.config.BackupCollectorConfig.FilePath, true)

	return otlptrace.New(ctx, &col)
}

func (tel *Telemetry) setupConsoleCollector(ctx context.Context) (*otlptrace.Exporter, error) {
	col := ConsoleExporter{
		logger: tel.logger,
	}

	return otlptrace.New(ctx, &col)
}

func (tel *Telemetry) setupEmptyCollector(ctx context.Context) (*otlptrace.Exporter, error) {
	col := EmptyExporter{
		logger: tel.logger,
	}

	return otlptrace.New(ctx, &col)
}

func (tel *Telemetry) Tracer() trace.Tracer {
	return otel.Tracer(tel.config.Name)
}

func (tel *Telemetry) Enabled() bool {
	return tel.config.Enabled
}
