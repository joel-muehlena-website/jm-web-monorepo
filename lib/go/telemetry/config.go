package telemetry

import (
	"fmt"

	"gitlab.com/joelMuehlena/jm-web-monorepo/lib/go/utility"
)

type Config struct {
	Enabled               bool                  `yaml:"enabled" toml:"enabled" json:"enabled"`
	Name                  string                `yaml:"name" toml:"name" json:"name"`
	ID                    string                `yaml:"id" toml:"id" json:"id"`
	Namespace             string                `yaml:"namespace" toml:"namespace" json:"namespace"`
	Version               string                `yaml:"version" toml:"version" json:"version"`
	UseCollector          bool                  `yaml:"useCollector" toml:"useCollector" json:"useCollector"`
	CollectorConfig       CollectorConfig       `yaml:"collector" toml:"collector" json:"collector"`
	BackupCollectorConfig BackupCollectorConfig `yaml:"backupCollector" toml:"backupCollector" json:"backupCollector"`
}

type CollectorConfig struct {
	URL  string `yaml:"url" toml:"url" json:"url"`
	Port uint16 `yaml:"port" toml:"port" json:"port"`
}

type BackupCollectorConfig struct {
	Enabled  bool   `yaml:"enabled" toml:"enabled" json:"enabled"`
	Type     string `yaml:"type" toml:"type" json:"type"`
	FilePath string `yaml:"filePath" toml:"filePath" json:"filePath"`
}

func validateConfig(cfg Config) error {
	if cfg.Name == "" {
		return fmt.Errorf("invalid name set: a name is required")
	}

	if cfg.Name == "" {
		return fmt.Errorf("invalid ID set: an id is required")
	}

	if cfg.BackupCollectorConfig.Enabled {
		if !utility.Contains(GetAllowedBackupTypes(), cfg.BackupCollectorConfig.Type) {
			return fmt.Errorf("this backup collector type is not allowed: %s", cfg.BackupCollectorConfig.Type)
		}
	}

	return nil
}

func GetAllowedBackupTypes() []string {
	return []string{
		"console",
		"file",
		"empty",
	}
}
