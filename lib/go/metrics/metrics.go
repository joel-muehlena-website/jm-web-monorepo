package metrics

import (
	"context"
	"fmt"
	"net"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"go.uber.org/zap"
)

type Metrics struct {
	logger *zap.Logger
	server *http.Server
}

func New(logger *zap.Logger, config Config) *Metrics {
	if config.Endpoint == "" {
		config.Endpoint = "/metrics"
	}
	metrics := &Metrics{
		logger: logger.Named("metrics"),
		server: initServer(config.Endpoint, config.BindAddr, config.Port),
	}

	return metrics
}

func (m *Metrics) Handler() http.Handler {
	return m.server.Handler
}

func initServer(metricsEndpoint, bindAddr string, port uint16) *http.Server {
	mux := http.NewServeMux()
	mux.Handle(metricsEndpoint, promhttp.Handler())

	server := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", bindAddr, port),
		Handler: mux,
	}

	return server
}

func (m *Metrics) Shutdown(ctx context.Context) {
	m.server.Shutdown(ctx)
}

func (m *Metrics) ListenAndServe() error {
	host, port, err := net.SplitHostPort(m.server.Addr)
	if err != nil {
		return fmt.Errorf("failed to split host and port: %w", err)
	}

	go func() {
		m.logger.Info("Starting probe server", zap.String("port", port), zap.String("host", host))
		err := m.server.ListenAndServe()
		if err != nil {
			m.logger.Fatal("Failed to start server", zap.Error(err))
		}
	}()

	return nil
}
