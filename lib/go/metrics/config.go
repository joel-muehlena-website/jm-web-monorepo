package metrics

type Config struct {
	Port     uint16 `yaml:"port" json:"port" toml:"port"`
	BindAddr string `yaml:"bindAddr" json:"bindAddr" toml:"bind_addr"`
	Endpoint string `yaml:"endpoint" json:"endpoint" toml:"endpoint"`
}
