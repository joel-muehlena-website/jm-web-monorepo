CREATE OR REPLACE FUNCTION preventUpdateCreatedAt() RETURNS trigger AS $PREVENTUPDATECREATEDATFORCATEGORY$
BEGIN
  if(NEW.createdat != OLD.createdat)
  then
      NEW.createdat = OLD.createdat;
  end if;

  return NEW;
END;
$PREVENTUPDATECREATEDATFORCATEGORY$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION setUpdatedAtOnUpdate() RETURNS trigger AS $SETUPDATEDATONUPDATE$
BEGIN
  NEW.updatedat = now();

  return NEW;
END;
$SETUPDATEDATONUPDATE$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION setCreatedAtAndUpdatedAt() RETURNS trigger AS $SETCREATEDATANDUPDATEDAT$
DECLARE
  time timestamp := now();
BEGIN
  NEW.updatedat = time;
  NEW.createdat = time;

  return NEW;
END;
$SETCREATEDATANDUPDATEDAT$ LANGUAGE plpgsql;